# KOSOS
A unix like cooperative multithreading OS for [KOS](https://ksp-kos.github.io/KOS/). 

## Usage
Check out this repository to a folder called `kosos` in your `Ships/Scripts` folder.

Add a script called `kosos.ks` to your `boot` folder with the following content:

```
runpath("0:/kosos/install").
```

Set one of your cores bootfile to `kosos.ks`. If your core has enough space it should automatically boot into KOSOS once on the landing pad.

## Useful commands

By default KOSOS will boot into a [kash](/doc/kash.md) shell. Here are some useful commands:

 - *To show a list of available commands*: `ls /usr/bin`
 - *Add a button to the screen to execute a node*: `button "Exec Node" exec &`