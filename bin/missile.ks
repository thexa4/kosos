local  Dock to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    PROGRAM_INIT.

    if not hastarget {
      self:print(
      self,"No target selected").
      self:exit(
      self,1).
      return self.
    } set 
    
    self:target to  target. set 
    self:heading to  ship:facing:vector. set 
    self:eta to  10. set 
    self:throttle to  1.

    lock steering to self:heading.
    lock throttle to self:throttle.

    self:start(

    self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["aim"] to { parameter self. local delta to  self:target:position - ship:position. local speed to  100.
    if delta:mag > 50000 { set 
      speed to  speed + 400 * min(1, (delta:mag - 50000) / 100000).
    } set 
    speed to  speed + max(0, (1000 - delta:mag) / 10) + max(0, 70000 - ship:altitude) / 20. local velDiff to  self:target:velocity:orbit - ship:velocity:orbit. set 
    self:heading to  velDiff + delta:normalized * speed.
    
    print "speed: " + speed at (0, 0).
    print "delta: " + self:heading:mag at (0, 1).}.
set  prototype["run"] to { parameter self. local deltapos to  self:target:position - ship:position. local vel to  ship:velocity:orbit.

    if deltapos:mag < 200 {
      stage.
    }

    if vel:mag > 300 and ship:altitude > 15000 {

      self:aim(

      self). local alignment to  self:heading:normalized * ship:facing:vector - 0.7.
      if self:heading:mag < 0.2 or alignment < 0 { set 
        self:throttle to  0.
        return.
      } set 

      self:throttle to  self:heading:mag * alignment / 10.
    }}.
set  prototype["deconstruct"] to { parameter self,_.
    unlock throttle.
    unlock steering. set 
    ship:control:neutralize to  true.}.
return  result.}):call(). set 
export to  Dock.