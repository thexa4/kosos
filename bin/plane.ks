local  Plane to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","Plane").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:pid to  id.
    set  self:status to  0.
    set  self:input to  input.
    set  self:output to  output.
    set  self:env to  env.
    set  self:fs to  fs.
    set  self:resultCallback to  resultCallback.
    set  self:running to  false.
    if args:length = 1 or args[1] = "help" {
      self:help(self).
      return self.
    } local cmds to  lex("takeoff", "takeoff", "heading", "heading", "hold", "hold", "ditch", "ditch", "land", "land", "set", false).
    if not cmds:haskey(args[1]) {
      self:help(self).
      return self.
    }
    set  self:chosenCommand to  args[1].
    set  self:desiredState to  cmds[args[1]].
    set  self:desiredAlt to  false.
    set  self:desiredHeading to  false.
    set  self:desiredSpeed to  false.
    set  self:desiredGroundRef to  false.
    set  self:waitUntilDone to  false. local toParse to  args:sublist(2, args:length - 2).
    until toParse:length = 0 {
      if toParse[0] = "wait" {
        set  self:waitUntilDone to  true.
        set  toParse to  toParse:sublist(1, toParse:length).
      } else {
        if toParse:length < 2 {
          self:help(self).
          return self.
        } local param to  toParse[0].
        if param = "alt" {
          set  self:desiredAlt to  toParse[1]:tonumber(-1000).
          if self:desiredAlt = -1000 {
            self:help(self).
            return self.
          }
        } else if param = "heading" {
          set  self:desiredHeading to  toParse[1]:tonumber(-1000).
          if self:desiredHeading = -1000 {
            self:help(self).
            return self.
          }
        } else if param = "speed" {
          set  self:desiredSpeed to  toParse[1]:tonumber(-1000).
          if self:desiredSpeed = -1000 {
            self:help(self).
            return self.
          }
        } else if param = "groundref" {
          set  self:desiredGroundRef to  toParse[1]:tonumber(-1000).
          if self:desiredGroundRef = -1000 {
            self:help(self).
            return self.
          }
        } else {
          output:append(output,"Unable to set property " + param, Callback:null).
          self:help(self).
          return self.
        }

        set  toParse to  toParse:sublist(2, toParse:length).
      }
    }

    set  self:stateFile to  false.
    set  self:altFile to  false.
    set  self:headingFile to  false.
    set  self:speedFile to  false.
    set  self:groundRefFile to  false.
    set  self:toRead to  0.

    set  self:loading to  true.

    if not self:desiredState:istype("Boolean") {
      set  self:toRead to  self:toRead + 1.
    }
    if not self:desiredAlt:istype("Boolean") {
      set  self:toRead to  self:toRead + 1.
    }
    if not self:desiredHeading:istype("Boolean") {
      set  self:toRead to  self:toRead + 1.
    }
    if not self:desiredSpeed:istype("Boolean") {
      set  self:toRead to  self:toRead + 1.
    }
    if not self:desiredGroundRef:istype("Boolean") {
      set  self:toRead to  self:toRead + 1.
    }
    if not self:desiredState:istype("Boolean") {
      fs:modify(fs,"/sys/class/plane/state", Callback: new(Callback,self, "onOpenState")).
    }
    if not self:desiredAlt:istype("Boolean") {
      fs:modify(fs,"/sys/class/plane/targetAlt", Callback: new(Callback,self, "onOpenAlt")).
    }
    if not self:desiredHeading:istype("Boolean") {
      fs:modify(fs,"/sys/class/plane/targetHeading", Callback: new(Callback,self, "onOpenHeading")).
    }
    if not self:desiredSpeed:istype("Boolean") {
      fs:modify(fs,"/sys/class/plane/targetSpeed", Callback: new(Callback,self, "onOpenSpeed")).
    }
    if not self:desiredGroundRef:istype("Boolean") {
      fs:modify(fs,"/sys/class/plane/groundAltReference", Callback: new(Callback,self, "onOpenGroundRef")).
    }
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onOpenState"] to { parameter self,file.
    if not self:loading
      return.
    
    if file:istype("Boolean") {
      return self:helpNotRunning(self).
    }

    set  self:stateFile to  file.
    set  self:toRead to  self:toRead - 1.
    if self:toRead = 0 {
      self:onAllLoaded(self).
    }}.
set  prototype["onOpenAlt"] to { parameter self,file.
    if not self:loading
      return.
    
    if file:istype("Boolean") {
      return self:helpNotRunning(self).
    }

    set  self:altFile to  file.
    set  self:toRead to  self:toRead - 1.
    if self:toRead = 0 {
      self:onAllLoaded(self).
    }}.
set  prototype["onOpenHeading"] to { parameter self,file.
    if not self:loading
      return.
    
    if file:istype("Boolean") {
      return self:helpNotRunning(self).
    }

    set  self:headingFile to  file.
    set  self:toRead to  self:toRead - 1.
    if self:toRead = 0 {
      self:onAllLoaded(self).
    }}.
set  prototype["onOpenSpeed"] to { parameter self,file.
    if not self:loading
      return.
    
    if file:istype("Boolean") {
      return self:helpNotRunning(self).
    }

    set  self:speedFile to  file.
    set  self:toRead to  self:toRead - 1.
    if self:toRead = 0 {
      self:onAllLoaded(self).
    }}.
set  prototype["onOpenGroundRef"] to { parameter self,file.
    if not self:loading
      return.
    
    if file:istype("Boolean") {
      return self:helpNotRunning(self).
    }

    set  self:groundRefFile to  file.
    set  self:toRead to  self:toRead - 1.
    if self:toRead = 0 {
      self:onAllLoaded(self).
    }}.
set  prototype["onAllLoaded"] to { parameter self.
    set  self:loading to  false.

    if not self:desiredState:istype("Boolean") {
local  __var33 to 
      self:stateFile. __var33:write(__var33,"" + self:desiredState, Callback:null).
    }
    if not self:desiredAlt:istype("Boolean") {
local  __var34 to 
      self:altFile. __var34:write(__var34,"" + self:desiredAlt, Callback:null).
    }
    if not self:desiredHeading:istype("Boolean") {
local  __var35 to 
      self:headingFile. __var35:write(__var35,"" + self:desiredHeading, Callback:null).
    }
    if not self:desiredSpeed:istype("Boolean") {
local  __var36 to 
      self:speedFile. __var36:write(__var36,"" + self:desiredSpeed, Callback:null).
    }
    if not self:desiredGroundRef:istype("Boolean") {
local  __var37 to 
      self:groundRefFile. __var37:write(__var37,"" + self:desiredGroundRef, Callback:null).
    }
    if self:waitUntilDone {
      
      if self:chosenCommand = "takeoff" or self:chosenCommand = "land" or self:chosenCommand = "ditch" {
        if self:chosenCommand <> "takeoff" {
          print("todo: implement waiting on " + self:chosenCommand).
          
          set  self:status to  1.
local  __var38 to 
          self:fs:loop. __var38:defer(__var38,self:resultCallback, self:pid).
          return.
        }
local  __var39 to 
        self:fs:loop. __var39:register(__var39,self).
        set  self:running to  true.
        return.
      }
      set  self:status to  0.
local  __var40 to 
      self:fs:loop. __var40:defer(__var40,self:resultCallback, self:pid).
      return.
      
    } else {
      set  self:status to  0.
local  __var41 to 
      self:fs:loop. __var41:defer(__var41,self:resultCallback, self:pid).
    }}.
set  prototype["helpNotRunning"] to { parameter self.
local  __var42 to 
    self:output. __var42:callback(__var42,"Unable to open planed config files.", Callback:null).
local  __var43 to 
    self:output. __var43:callback(__var43,"Is planed running?", Callback:null).
local  __var44 to 
    self:output. __var44:callback(__var44,"If not, try starting with 'planed detach'", Callback:null).
    
    set  self:status to  1.
    set  self:loading to  false.
local  __var45 to 
    self:fs:loop. __var45:defer(__var45,self:resultCallback, self:pid).}.
set  prototype["help"] to { parameter self.
local  __var46 to 
    self:output. __var46:append(__var46,"Usage: plane <subcommand> [wait] [alt <altitude>] [heading <heading>] [speed <speed>] [groundref <0|1>]", Callback:null).
local  __var47 to 
    self:output. __var47:append(__var47,"", Callback:null).
local  __var48 to 
    self:output. __var48:append(__var48,"Subcommands available:", Callback:null).
local  __var49 to 
    self:output. __var49:append(__var49," - help      Shows this help", Callback:null).
local  __var50 to 
    self:output. __var50:append(__var50," - set       Set flight parameters", Callback:null).
local  __var51 to 
    self:output. __var51:append(__var51," - takeoff   Launch plane", Callback:null).
local  __var52 to 
    self:output. __var52:append(__var52," - heading   Keep constant heading", Callback:null).
local  __var53 to 
    self:output. __var53:append(__var53," - hold      Keep plane level, free roll", Callback:null).
local  __var54 to 
    self:output. __var54:append(__var54," - ditch     Try landing immediately", Callback:null).
local  __var55 to 
    self:output. __var55:append(__var55," - land      Land at KSC runway", Callback:null).
    set  self:status to  1.
    set  self:running to  false.
local  __var56 to 
    self:fs:loop. __var56:defer(__var56,self:resultCallback, self:pid).}.
set  prototype["onSignal"] to { parameter self,signal.
    if not self:running {
      return false.
    }
    if signal = "End" {
      self:stopDaemon(self).
      return true.
    }}.
set  prototype["run"] to { parameter self.
    if self:chosenCommand = "takeoff" and ship:status = "LANDED" {
      set  self:status to  0.
      set  self:exited to  true.
      set  self:running to  false.
local  __var57 to 
      self:fs:loop. __var57:deregister(__var57,self).
local  __var58 to 
      self:fs:loop. __var58:defer(__var58,self:resultCallback, self:pid).
    }}.
return  result.}):call().
set  export to  Plane.