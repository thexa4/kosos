local  UnattendedPlaneContractd to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    self:progSetup(
    self,id, input, output, args, env, fs, resultCallback).

    self:start(

    self). set 

    self:state to  "pre-launch".
    if ship:status <> "LANDED" and ship:status <> "PRELAUNCH" set 
      self:state to  "choosing".
    print(ship:status). set 
    
    self:exec_waiting to  false. set 

    self:onExitCallback to  Callback: new( Callback,self, "onExecExit"). set 
    self:onStartCallback to  Callback: new( Callback,self, "onExecStart"). set 
    self:lastTick to  0. set 
    self:destination to  false. set 

    self:currentCommand to  false. set 

    self:lastHeading to  -1. set 
    self:speed to  220.
    if env:haskey("PLANE_CRUISE_SPEED") { set 
      self:speed to  self:env:PLANE_CRUISE_SPEED:toscalar(self:speed).
    } set 

    self:cruiseAlt to  5000.
    if env:haskey("PLANE_CRUISE_ALT") { set 
      self:cruiseAlt to  self:env:PLANE_CRUISE_ALT:toscalar(self:cruiseAlt).
    }
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["run"] to { parameter self.
    if self:exec_waiting
      return.

    if time:seconds - self:lastTick < 5 {
      return.
    } set 
    self:lastTick to  time:seconds.

    if self:state = "pre-launch" {
      self:runprog(
      self,"plane takeoff wait"). set 
      self:state to  "takeoff".
      return.
    }
    if self:state = "takeoff" {
      if (alt:radar < 200)
        return.
      self:runprog(
      self,"plane set alt " + self:cruiseAlt + " speed " + self:speed). set 
      self:state to  "choosing". set 
      self:lastHeading to  -1.
      return.
    }

    if self:state = "choosing" { local waypoints to  ALLWAYPOINTS(). local destination to  false. local closest to  1000000000.

      for waypoint in waypoints {
        if waypoint:body = ship:body and not waypoint:grounded and waypoint:name <> ship:name { local dist to  (waypoint:position - ship:position):mag. local waypointDir to  (waypoint:geoposition:altitudeposition(0) - ship:geoposition:altitudeposition(0)):normalized.

          if waypoint:ALTITUDE > 20000 and alt:radar < 17000 { set 
            dist to  dist + 20000.
          } local alignment to  waypointDir * facing:vector.
          if alignment > 0.7 { set 
            dist to  dist + 20000.
          }

          if dist < closest { set 
            destination to  waypoint. set 
            closest to  dist.
          }
        }
      }

      if destination:istype("boolean")
      {
        self:runprog(
        self,"plane land wait"). set 
        self:state to  "landing".
        return.
      } else { set 
        self:destination to  destination.
        self:print(
        self,"Heading to: " + destination:name). local newHeading to  round(destination:geoposition:heading). set 
        self:lastHeading to  newHeading. local destAlt to  self:cruiseAlt.
        if destination:ALTITUDE > 20000 { set 
          destAlt to  20000.
        }
        self:runprog(
        self,"plane heading alt " + destAlt + " heading " + newHeading + " speed " + self:speed). set 
        self:state to  "flying".
      }
      return.
    }

    if self:state = "flying" { local found to  false.
      for waypoint in allwaypoints() {
        if waypoint:name = self:destination:name { set 
          found to  true.
        }
      }
      if not found { set 
        self:state to  "choosing".
        return.
      } local newHeading to  round(self:destination:geoposition:heading).
      if newHeading <> self:lastHeading { set 
        self:lastHeading to  newHeading.
        self:runprog(
        self,"plane set heading " + newHeading).
      }
      return.
    }

    self:exit(

    self,1).}.
set  prototype["runprog"] to { parameter self,cmd.
    self:print(
    self,cmd).
local  __var62 to 
    self:fs. __var62:exec( __var62,cmd, self:env, self:input, self:output, self:onExitCallback, self:onStartCallback). set 
    self:exec_waiting to  true.}.
set  prototype["onExecStart"] to { parameter self,result.
    if result:istype("Boolean") {
      self:print(
      self,"Exec failed: program not found, bailing").
      self:exit(
      self,2). set 
      self:exec_waiting to  false.
      return.
    } set 
    self:currentCommand to  result.}.
set  prototype["onExecExit"] to { parameter self,pid.
    if not self:exec_waiting
      return. set 
    
    result to  self:currentCommand:status. set 
    self:currentCommand to  false.

    if result <> 0 {
      self:print(
      self,"Exec failed: error code " + result + ", bailing").
      self:exit(
      self,2).
    } set 
    self:exec_waiting to  false.}.
return  result.}):call(). set 
export to  UnattendedPlaneContractd.