local Land to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Land").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).

    if ship:body:atm:exists {
      set self:stage to  "aerobrakeSetup".
    } else {
      set self:stage to  "slamSetup".
    }
    self:print(self,"Landing anywhere").
    set self:lowestPart to  2. //TODO: measure lowest point of rocket during setup
    if env:haskey("ROCKET_HEIGHT")
      set self:lowestPart to  -self:env:ROCKET_HEIGHT:toscalar(self:lowestPart).
    
    set self:heatResistance to  5.
    if env:haskey("ROCKET_HEAT_RESISTANCE")
      set self:heatResistance to  self:env:ROCKET_HEAT_RESISTANCE:toscalar(self:heatResistance).

    if self:lowestPart >= 0 {
      self:print(self,"Please set correct height using ROCKET_HEIGHT env variable").
      self:exit(self,1).
      return self.
    }
    set self:safetyMargin to  1.3.
    set self:maxThrottle to  1.
    set self:aeroVars to  lex().

    set self:laser to  false.
    for p in ship:partsTagged("landing") {
      local mods to  p:modulesnamed("LaserDistModule").
      if mods:length > 0 {
        set self:laser to  mods[0].
        self:print(self,"Laser detected").
      }
    }

    self:start(self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["step"] to { parameter self,input.}.
set  prototype["run"] to { parameter self.
    self[self:stage](self).}.
set  prototype["aerobrakeSetup"] to { parameter self.
    self:print(self,"Aerobraking").
    lock steering to retrograde.
    set self:aeroVars:qsum to  0.
    set self:aeroVars:qt to  time:seconds.
    set self:aeroVars:qpid to  pidloop(-1, 0, -0.1, 0, 1).

    set self:desiredThrottle to  0.
    lock throttle to self:desiredThrottle.

    set self:stage to  "aerobrake".}.
set  prototype["aerobrake"] to { parameter self.
    local tDiff to  time:seconds - self:aeroVars:qt + 0.00001.
    if tDiff > 0.5 {
      local prevSum to  self:aeroVars:qsum.
      set self:aeroVars:qsum to  max(0, self:aeroVars:qsum + ship:q - 0.01).
      set self:aeroVars:qsum to  self:aeroVars:qsum * max(0, 1 - ship:body:atm:altitudepressure(ship:altitude)).
      print "qsum: " + self:aeroVars:qsum + "     " at (0,0).

      local qDiff to  self:aeroVars:qsum - prevSum.
      set self:aeroVars:qt to  time:seconds.

      local qRate to  qDiff / tDiff.

      print "qrate: " + qRate + "     " at (0,1).
      if self:aeroVars:qsum < 2 {
        set self:desiredThrottle to  0.
      } else {
        set self:aeroVars:qpid:setpoint to  max(0, ((self:heatResistance * 2) - self:aeroVars:qsum) / self:heatResistance).
        local t to  self:aeroVars:qpid:update(time:seconds, qRate).
        set self:desiredThrottle to  t.
        print "throttle: " + t + "     " at (0,2).
      }
    } else {
      if self:aeroVars:qsum < 2 {
        set self:desiredThrottle to  0.
      }
    }

    if alt:radar < 70000 and alt:radar > 55000 {
      brakes on.
    } else {
      brakes off.
    }

    if alt:radar < 20000 {
      brakes on.
      set self:stage to  "slamSetup".
    }}.
set  prototype["slamSetup"] to { parameter self.
    self:print(self,"Hoverslam").
    lock steering to (srfretrograde:vector * up:vector) * up:vector + vxcl(up:vector, srfretrograde:vector) * 1.1.
    set self:g to  body:mu / body:radius^2.
    set self:maxAcc to  ship:availablethrust / ship:mass.
    if self:maxAcc / self:g > 5 {
      set self:maxThrottle to  (self:g * 5) / self:maxAcc.
      set self:maxAcc to  self:g * 5.
    }
    set self:gRatio to  self:g / (self:maxAcc + 0.0001).
    set self:ratioLimit to  self:gRatio / (1 - self:gRatio). // Infinite sum
    self:print(self,"gRatio: " + self:gRatio).
    self:print(self,"ratioLimit: " + self:ratioLimit).
    
    set self:desiredThrottle to  0.
    lock throttle to self:desiredThrottle.

    set self:stage to  "slam".

    if not self:laser:istype("Boolean") {
      self:laser:setfield("Enabled", true).
    }}.
set  prototype["slam"] to { parameter self.
    local vel to  ship:velocity:surface.
    if vel:mag < 0.1 or alt:radar - self:lowestPart < 0.01 {
      set self:stage to  "drop".
      self:print(self,"drop").
      return.
    }

    local velZeroTime to  vel:mag / (self:maxAcc + 0.00001).
    local gravZeroTime to  velZeroTime * self:ratioLimit.
    local burnMinEta to  velZeroTime + gravZeroTime.
    local burnMaxEta to  burnMinEta * self:safetyMargin.

    local impactEta to  (alt:radar - self:lowestPart) / -ship:verticalspeed.
    if not self:laser:istype("Boolean") {
      local newDist to  self:laser:getfield("Distance").
      if newDist > 0 {
        set impactEta to  min(impactEta, newDist / vel:mag).
      }
    }

    if impactEta < 0
      set impactEta to  999999.
    set self:desiredThrottle to  self:maxThrottle * ((burnMaxEta - impactEta) / (burnMaxEta - burnMinEta)).
    print "impact: " + impactEta at (0, 3).

    if impactEta < 10 and not gear {
      gear on.
    }}.
set  prototype["drop"] to { parameter self.
    if not self:laser:istype("Boolean") {
      self:laser:setfield("Enabled", false).
    }
    unlock throttle.
    rcs on.
    lock steering to "kill".
    set self:stage to  "wait".
    self:print(self,"wait").
    set self:dropTime to  time:seconds.}.
set  prototype["wait"] to { parameter self.
    set ship:control:pilotmainthrottle to  0.
    if self:dropTime + 15 > time:seconds
      return.
    
    self:print(self,"landed").
    set self:stage to  "none".

    unlock steering.
    self:exit(self,0).}.
set  prototype["none"] to { parameter self.
    self:print(self,"deregister failed").}.
return  result.}):call().
set export to  Land.