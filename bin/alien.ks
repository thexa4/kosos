local  Alien to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    self:progSetup(
    self,id, input, output, args, env, fs, resultCallback).
    if args:length <> 2 {
      self:print(
      self,"Need target to run.").
      self:print(
      self,"Usage:").
      self:print(
      self,"alien <prog>").
      self:exit(
      self,1).
      return self.
    }

    if not fs:loop:framework:istype("Boolean") {
      self:print(
      self,"Error: another framework is already running, please wait for it to exit.").
      self:exit(
      self,1).
      return self.
    } set 
    fs:loop:framework to  {
      declare parameter callback.

      runpath(args[1], callback).
      self:exit(
      self,0).
    }.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
export to  Alien.