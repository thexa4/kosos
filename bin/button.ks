@lazyglobal off.
"kpp 1.1"."https://gitlab.com/thexa4/kos-kpp".


local DelegateFile to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,fs, readDelegate, writeDelegate.local self to __cls:prototype:copy. set self:type to __cls.
    set self:fs to fs.
    set self:readDelegate to readDelegate.
    set self:writeDelegate to writeDelegate.
    set self:stream to false. return self.}.

  set r:prototype:getType to {parameter self. return "file". }.
  set r:prototype:read to {parameter self,callback. ({local __lhs to self:fs:loop. return __lhs:defer(__lhs,callback, self:readDelegate()).}):call(). }.
  set r:prototype:write to {parameter self,data, callback.
    if not data:istype("Boolean")
      self:writeDelegate(data).
    ({local __lhs to self:fs:loop. return __lhs:defer(__lhs,callback, true).}):call().
  }.
  set r:prototype:append to {parameter self,data, callback.
    ({local __lhs to self. return __lhs:write(__lhs,data, callback).}):call().
  }.
}).

local Button to Class:__new(Class, {parameter r.
  r:mix(r:prototype,Program:prototype).

  set r:__new to{parameter __cls,id, input, output, args, env, fs, resultCallback.local self to __cls:prototype:copy. set self:type to __cls. ({local __lhs to self. return __lhs:progSetup(__lhs,id, input, output, args, env, fs, resultCallback).}):call().
    set self:subprogram to false.
    set self:starting to false.
    set self:pipe to false.

    if args:length <> 3 {
      self:print(self,"Usage: button <label> <script>").
      self:exit(self,1).
      return self.
    }

    set self:label to args[1].
    set self:command to args[2].

    set self:clicksToIgnore to 0.

    set self:button to self:type:addButton(self:type,self).
    set self:button:text to self:label.
    set self:button:toggle to true.
    set self:button:ontoggle to {
      parameter _.
      if self:clicksToIgnore = 0 {
        fs:loop:defer(fs:loop, Callback:__new(Callback,self, "onClick"), 0).
      } else {
        set self:clicksToIgnore to self:clicksToIgnore - 1.
      }
    }. return self.}.

  set r:addButton to {parameter self,program.
    if not self:haskey("gui") {
      set self:buttonProgs to lex().
      set self:buttons to list().
      set self:buttonCounter to 0.
      set self:gui to GUI(0, 64).
      self:gui:show().

      set self:folder to MemoryFilesystemDir:__new( MemoryFilesystemDir).
      self:folder:registerFile(self:folder,"hideAll", DelegateFile:__new(DelegateFile,program:fs, { return "". }, {
        parameter data.
        clearguis().
      })).
      program:fs:mount(program:fs,self:folder, "/sys/class/buttons").
    }
    local button to self:gui:addButton().
    self:buttonProgs:add(program, button).
    self:buttons:add(button).

    local subfolder to MemoryFilesystemDir:__new( MemoryFilesystemDir).
    subfolder:registerFile(subfolder,"remove", DelegateFile:__new(DelegateFile,program:fs, { return "0". }, {
      parameter data.
      program:onSignal(program,"End").
    })).
    subfolder:registerFile(subfolder,"state", DelegateFile:__new(DelegateFile,program:fs, {
      if button:pressed {
        return "1".
      } else {
        return "0".
      }
    }, {
      parameter data.
      set button:pressed to (data <> "0").
    })).
    subfolder:registerFile(subfolder,"label", DelegateFile:__new(DelegateFile,program:fs, {
      return button:text.
    }, {
      parameter data.
      set button:text to data.
    })).
    subfolder:registerFile(subfolder,"command", DelegateFile:__new(DelegateFile,program:fs, {
      return program:command.
    }, {
      parameter data.
      set program:command to data.
    })).
    set self:buttonCounter to self:buttonCounter + 1.
    self:folder:registerFile(self:folder,"" + self:buttonCounter, subfolder).
    
    return button.
  }.

  set r:removeButton to {parameter self,program.
    if not self:haskey("gui") {
      return.
    }
    if not self:buttonProgs:haskey(program) {
      return.
    }
    local button to self:buttonProgs[program].
    set self:buttons[self:buttons:indexof(button)] to false.
    button:dispose().

    if self:buttons:length = 0 {
      program:fs:unmount(program:fs,"/sys/class/buttons").
      
      self:gui:dispose().
      self:remove("gui").
      self:remove("buttons").
      self:remove("buttonProgs").
      self:remove("buttonCounter").
      self:remove("folder").
    }
  }.

  set r:prototype:onClick to {parameter self,_.
    if self:starting 
      return.
    
    if self:subprogram:istype("Boolean") {
      set self:starting to true.
      set self:button:enabled to false.
      set self:pipe to Pipe:__new( Pipe).
      self:fs:exec(self:fs,self:command, self:env, self:pipe, self:output, Callback:__new(Callback,self, "onDone"), Callback:__new(Callback,self, "onStart")).
    } else {
      if not self:button:pressed {
        set self:clicksToIgnore to self:clicksToIgnore + 1.
        set self:button:pressed to true.
      }
      if self:pipe:istype("Boolean") {
        return.
      }
      local handled to self:pipe:signal(self:pipe,"End").
      if not handled {
        self:print(self,"Unhandled signal").
      }
    }
  }.

  set r:prototype:onStart to {parameter self,result.
    if result:istype("Boolean") {
      self:print(self,"Button program not found: " + self:command).
      set self:button:enabled to true.
      if self:button:pressed {
        set self:clicksToIgnore to self:clicksToIgnore + 1.
        set self:button:pressed to false.
      }
      return.
    }
    set self:starting to false.
    set self:button:enabled to true.
    set self:button:pressed to true.
    set self:subprogram to result.
  }.

  set r:prototype:onDone to {parameter self,result.
    set self:subprogram to false.
    if self:button:pressed {
      set self:clicksToIgnore to self:clicksToIgnore + 1.
      set self:button:pressed to false.
    }
    set self:pipe to false.
  }.

  set r:prototype:deconstruct to {parameter self,_.
    if not self:pipe:istype("Boolean") {
      self:pipe:signal(self:pipe,"End").
    }
    set self:subprogram to false.
    set self:pipe to false.
    self:type:removeButton(self:type,self).
  }.
}).
set export to Button.