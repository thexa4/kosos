local  Kill to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id. set 
    self:status to  0.

    if args:length < 2 or args:length > 3 or (args:length = 3 and args[1] <> "-9") {
      output:append(
      output,"Usage: kill [-9] <id>", Callback:null). set 
      self:status to  1.
local  __var0 to 
      fs:loop. __var0:defer( __var0,resultCallback, self:pid).
      return self.
    } local kid to  args[args:length - 1]:tonumber(-1).
    if kid = -1 or not fs:progs:haskey(kid) {
      output:append(
      output,"Bad id: " + args[args:length - 1], Callback:null). set 
      self:status to  1.
local  __var1 to 
      fs:loop. __var1:defer( __var1,resultCallback, self:pid).
      return self.
    } local shouldKill to  false.
    if args[1] = "-9" { set 
      shouldKill to  true.
    } local prog to  fs:progs[kid].
    
    if not shouldKill and not prog:haskey("onSignal") {
      output:append(
      output,"Unable to quit " + fs:cmds[kid]:join(" "), Callback:null). set 
      self:status to  2.
local  __var2 to 
      fs:loop. __var2:defer( __var2,resultCallback, self:pid).
      return self.
    }

    if prog:haskey("onSignal") { local handled to  prog:onSignal( prog,"End").
      if not shouldKill and not handled {
        output:append(
        output,"Quit ignored by " + fs:cmds[kid]:join(" "), Callback:null). set 
        self:status to  2.
local  __var3 to 
        fs:loop. __var3:defer( __var3,resultCallback, self:pid).
        return self.
      }
    }

    if shouldKill {
local  __var4 to 
      fs:loop. __var4:deregister( __var4,prog).
    }
local  __var5 to 

    fs:loop. __var5:defer( __var5,resultCallback, self:pid).
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
export to  Kill.