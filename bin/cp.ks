local _Callback to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_Callback").
set  result["new"] to { parameter type,target, method.
local self to prototype:copy.
set  self["type"] to  result.
    set self:call to  { parameter self, args.
      return target[method]:call(target, args).
    }.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call().
set _Callback:null to  lex("call", { parameter s,_. }).
global Callback to _Callback.
local TodoCopyOneAsync to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","TodoCopyOneAsync").
set  result["new"] to { parameter type,sourcePath, destPath, doneCallback, errorCallback, env, fs.
local self to prototype:copy.
set  self["type"] to  result.
    set self:sourcePath to  sourcePath.
    set self:destPath to  destPath.
    set self:env to  env.
    set self:fs to  fs.
    set self:doneCallback to  doneCallback.
    set self:errorCallback to  errorCallback.
    
    set self:file to  false.
    set self:errored to  false.
    set self:data to  false.
    print("cp " + sourcePath + " to " + destPath + " @one").
local __var183 to self:fs. __var183:open(__var183,sourcePath, Callback:new(Callback,self, "onOpenSource")).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onOpenSource"] to { parameter self,file.
    if file:istype("Boolean") {
      if not self:errored {
        set self:errored to  true.
local __var184 to self:errorCallback. __var184:call(__var184,"Source file not found: " + self:sourcePath).
      }
      return.
    }

    if file:getType(file) = "file" {
      set self:file to  file.
      file:read(file,Callback:new(Callback,self, "onRead")).
      return.
    }

    if not self:errored {
      set self:errored to  true.
local __var185 to self:errorCallback. __var185:call(__var185,"Source file has unknown type " + file:getType(file) + ": " + self:sourcePath).
    }}.
set  prototype["onRead"] to { parameter self,data.
    if data:istype("Boolean") {
      if self:data = false {
        if not self:errored {
          set self:errored to  true.
local __var186 to self:errorCallback. __var186:call(__var186,"Source file has unknown type " + file:getType(file) + ": " + self:sourcePath).
        }
        return.
      }
local __var187 to self:fs. __var187:create(__var187,self:destPath, Callback:new(Callback,self, "onOpenDest")).
      return.
    }

    if self:data:istype("Boolean") {
      set self:data to  data.
    } else {
      set self:data to  self:data + char(10) + data.
    }
local __var188 to self:file. __var188:read(__var188,Callback:new(Callback,self, "onRead")).}.
set  prototype["onOpenDest"] to { parameter self,file.
    if file:istype("Boolean") {
      set self:errored to  true.
local __var189 to self:errorCallback. __var189:call(__var189,"Unable to create destination file " + self:destPath).
      return.
    }

    file:write(file,self:data, Callback:new(Callback,self, "onWriteDone")).}.
set  prototype["onWriteDone"] to { parameter self,writeStatus.
    if writeStatus {
local __var190 to self:doneCallback. __var190:call(__var190,self).
    } else {
      set self:errored to  true.
local __var191 to self:errorCallback. __var191:call(__var191,"Unable to write to destination file " + self:destPath).
    }}.
return  result.}):call().
local TodoProcessRecursiveArg to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","TodoProcessRecursiveArg").
set  result["new"] to { parameter type,sourcePath, destPath, doneCallback, errorCallback, env, fs.
local self to prototype:copy.
set  self["type"] to  result.
    set self:sourcePath to  sourcePath.
    set self:destPath to  destPath.
    set self:env to  env.
    set self:fs to  fs.
    set self:doneCallback to  doneCallback.
    set self:errorCallback to  errorCallback.
    
    set self:file to  false.
    set self:errored to  false.
    set self:data to  false.
    print("cp " + sourcePath + " to " + destPath + " @preprocess").
local __var192 to self:fs. __var192:open(__var192,sourcePath, Callback:new(Callback,self, "onOpenSource")).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onOpenSource"] to { parameter self,item.
    if item:istype("Boolean") {
      set self:errored to  true.
local __var193 to self:errorCallback. __var193:call(__var193,"Source item not found: " + self:sourcePath).
      return.
    }

    local itemType to  item:getType(item).
    if itemType = "dir" {
      if self:sourcePath:endswith("/") {
        TodoCopyRecursiveAsync:new(TodoCopyRecursiveAsync,self:sourcePath, self:destPath, self:doneCallback, self:errorCallback, self:env, self:fs).
      } else {
        local sourcePathParts to  self:sourcePath:split("/").
        local newPath to  self:destPath + "/" + sourcePathParts[sourcePathParts:length - 1].
local __var194 to self:fs. __var194:mkdir(__var194,newPath, Callback:new(Callback,self, "onMkdir")).
      }
    } else {
      local sourcePathParts to  self:sourcePath:split("/").
      set self:destPath to  self:destPath + "/" + sourcePathParts[sourcePathParts:length - 1].
      TodoCopyOneAsync:new(TodoCopyOneAsync,self:sourcePath, newpath, self:doneCallback, self:errorCallback, self:env, self:fs).
    }}.
set  prototype["onMkdir"] to { parameter self,_.
    TodoCopyRecursiveAsync:new(TodoCopyRecursiveAsync,self:sourcePath, self:destPath, self:doneCallback, self:errorCallback, self:env, self:fs).}.
return  result.}):call().
local TodoCopyRecursiveAsync to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","TodoCopyRecursiveAsync").
set  result["new"] to { parameter type,sourcePath, destPath, doneCallback, errorCallback, env, fs.
local self to prototype:copy.
set  self["type"] to  result.
    set self:sourcePath to  sourcePath.
    set self:destPath to  destPath.
    set self:env to  env.
    set self:fs to  fs.
    set self:doneCallback to  doneCallback.
    set self:errorCallback to  errorCallback.
    
    set self:file to  false.
    set self:errored to  false.
    set self:data to  false.

    set self:itemsInFlight to  0.
    set self:processQueue to  queue().
    print("cp " + sourcePath + " to " + destPath + " @recursive").
local __var195 to self:fs. __var195:open(__var195,sourcePath, Callback:new(Callback,self, "onOpenSource")).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onOpenSource"] to { parameter self,dir.
    if dir:istype("Boolean") {
      set self:errored to  true.
local __var196 to self:errorCallback. __var196:call(__var196,"Source item not found: " + self:sourcePath).
      return.
    }

    if dir:getType(dir) <> "dir" {
      set self:errored to  true.
local __var197 to self:errorCallback. __var197:call(__var197,"Source item is not a directory: " + self:sourcePath).
      return.
    }

    dir:read(dir,Callback:new(Callback,self, "onReadDir")).}.
set  prototype["onReadDir"] to { parameter self,items.
    if items:istype("Boolean") {
      set self:errored to  true.
local __var198 to self:errorCallback. __var198:call(__var198,"Unable to read folder contents: " + self:sourcePath).
      return.
    }

    set self:itemsInFlight to  items:length.
    for item in items {
      self:processQueue:push(item).
    }

    if self:itemsInFlight = 0 {
local __var199 to self:doneCallback. __var199:call(__var199,self).
      return.
    }

    local firstPath to  self:sourcePath + "/" + self:processQueue:peek().
local __var200 to self:fs. __var200:open(__var200,firstPath, Callback:new(Callback,self, "onOpenItem")).}.
set  prototype["onOpenItem"] to { parameter self,file.
    if self:errored {
      return.
    }

    local curSourcePath to  self:sourcePath + "/" + self:processQueue:peek().
    if file:istype("Boolean") {
      set self:errored to  true.
local __var201 to self:errorCallback. __var201:call(__var201,"Source item is not a directory: " + self:curSourcePath).
      return.
    }

    if file:getType(file) = "dir" {
      local curDestPath to  self:sourcePath + "/" + self:processQueue:peek().
      //new TodoCopyRecursiveAsync(self->)
      crash().
    } else {
      crash().
    }}.
set  prototype["onChildErrored"] to { parameter self,msg.
    if self:errored {
      return.
    }

    set self:inFlightCount to  self:inFlightCount - 1.
    set self:errored to  true.
local __var202 to self:errorCallback. __var202:call(__var202,msg).}.
set  prototype["onChildDone"] to { parameter self,_.
    if self:errored {
      return.
    }

    set self:inFlightCount to  self:inFlightCount - 1.
    if self:inFlightCount = 0 {
local __var203 to self:doneCallback. __var203:call(__var203,self).
    }}.
return  result.}):call().
local Cp to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Cp").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).

    if args:length < 3 {
      self:help(self).
      return self.
    }
    set self:dest to  Util:absPath(Util,args[args:length - 1], env).
    set self:todo to  Queue().
    set self:recursive to  false.
    local sources to  args:sublist(1, args:length - 2).
    for s in sources {
      if s = "-r" {
        set self:recursive to  true.
      } else {
        self:todo:push(s).
      }
    }

    if self:todo:length = 0 {
      self:print(self,"Please specify files to copy").
      self:exit(self,1).
    }

    set self:inFlightCount to  0.
    set self:errored to  false.

    set self:destType to  "none".
    fs:modify(fs,self:dest, Callback:new(Callback,self, "onOpenDest")).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["help"] to { parameter self.
local __var204 to self:output. __var204:append(__var204,"Usage: cp [-r] <source> <destFile>", Callback:null).
local __var205 to self:output. __var205:append(__var205,"       cp [-r] <source> [<source> ...] <destFolder>", Callback:null).
      self:exit(self,1).}.
set  prototype["copyOne"] to { parameter self,file, newpath.
    set self:inFlightCount to  self:inFlightCount + 1.
    return TodoCopyOneAsync:new(TodoCopyOneAsync,file, newpath, Callback:new(Callback,self, "onTodoDone"), Callback:new(Callback,self, "onTodoFailed"), self:env, self:fs).}.
set  prototype["onOpenDest"] to { parameter self,file.
    if not file:istype("Boolean") {
      if file:getType(file) = "dir" {
        set self:destType to  "dir".
      }
      if file:getType(file) = "file" {
        set self:destType to  "file".
      }
    }

    if self:recursive {
      if self:destType = "file" or self:destType = "none" {
        self:print(self,"Unable to copy recursively to non-directory").
        self:exit(self,1).
        return.
      }

      for pathname in self:todo {
        set self:inFlightCount to  self:inFlightCount + 1.
        TodoProcessRecursiveArg:new(TodoProcessRecursiveArg,pathname, self:dest, Callback:new(Callback,self, "onTodoDone"), Callback:new(Callback,self, "onTodoFailed"), self:env, self:fs).
      }
    } else {
      if self:destType = "file" or self:destType = "none" {
        if self:todo:length > 1 {
          self:print(self,"Unable to copy multiple files to one file").
          self:exit(self,1).
          return.
        }

        self:copyOne(self,self:todo:pop(), self:dest).
      } else {
        for pathname in self:todo {
          local pathParts to  pathname:split("/").
          local newPath to  self:dest + "/" + pathParts[pathParts:length - 1].
          self:copyOne(self,pathname, newPath).
        }
      }
    }}.
set  prototype["onTodoDone"] to { parameter self,todo.
    set self:inFlightCount to  self:inFlightCount - 1.
    if self:inFlightCount = 0 {
      if self:errored {
        self:exit(self,1).
      } else {
        self:exit(self,0).
      }
    }}.
set  prototype["onTodoFailed"] to { parameter self,msg.
    self:print(self,msg).
    set self:errored to  true.
    self:onTodoDone(self,false).}.
return  result.}):call().

set export to  Cp.
