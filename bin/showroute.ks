local _Callback to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_Callback").
set  result["new"] to { parameter type,target, method.
local self to prototype:copy.
set  self["type"] to  result.
    set self:call to  { parameter self, args.
      return target[method]:call(target, args).
    }.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call().
set _Callback:null to  lex("call", { parameter s,_. }).
global Callback to _Callback.
local ShowRoute to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","ShowRoute").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:pid to  id.
    if args:length <> 1 {
      output:write(output,"Usage: showroute").
      set self:status to  1.
local __var54 to fs:loop. __var54:defer(__var54,resultCallback, id).
      return self.
    }
    set self:output to  output.
    set self:status to  -1.
    set self:streaming to  true.
    set self:interrupted to  false.
    set self:resultCallback to  resultCallback.
    set self:fs to  fs.
    set self:waypoints to  list().
    clearvecdraws().
    set input:signalHandler to  Callback:new(Callback,self, "onSignal").
    input:read(input,Callback:new(Callback,self, "onRead")).
    set self:file to  input.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["run"] to { parameter self.
    local prev to  ship:position.
    local first to  true.
    for _waypoint in self:waypoints {
      local dir to  _waypoint[2] - prev.
      set _waypoint[3]:start to  prev.
      set _waypoint[3]:vec to  dir.
      set _waypoint[3]:color:g to  1 - _waypoint[3]:color:g.
      if first
        set _waypoint[3]:show to  false.
      set first to  false.
      set prev to  _waypoint[2].
    }
    self:onSignal(self,"End").}.
set  prototype["onSignal"] to { parameter self,signal.
    if self:interrupted
      return false.
    if signal <> "End"
      return false.
    set self:interrupted to  true.
    set self:status to  0.
local __var55 to self:fs:loop. __var55:defer(__var55,self:resultCallback, self:pid).
    return true.}.
set  prototype["onRead"] to { parameter self,data.
    if self:interrupted
      return.
    if data:istype("Boolean") {
      if not self:streaming {
local __var56 to self:output. __var56:append(__var56,"showroute: read failed", Callback:null).
        set self:status to  2.
        clearvecdraws().
local __var57 to self:fs:loop. __var57:defer(__var57,self:resultCallback, self:pid).
      } else {
local __var58 to self:fs:loop. __var58:register(__var58,self).
      }
      return.
    }
    local lines to  data:split(char(10)).
    for line in lines {
      self:handleLine(self,line).
    }
    if self:streaming {
local __var59 to self:file. __var59:read(__var59,Callback:new(Callback,self, "onRead")).
    }}.
set  prototype["handleLine"] to { parameter self,data.
    if data:trim:startsWith("#")
      return.
    if data:trim:startsWith("G;") {
      local parts to  data:trim:split(";").
      if parts:length <> 3 {
        output:append(output,"Bad waypoint: " + data, Callback:null).
      } else {
        local lat to  parts[1]:tonumber(0).
        local lng to  parts[2]:tonumber(0).
        local geo to  latlng(lat, lng).
        local pos to  geo:altitudeposition(geo:terrainheight + 10).
        self:waypoints:add(list(
          data,
          geo,
          pos,
          vecdraw(ship:position, v(0,0,0), rgb(1, 1, 1), "", 1.0, true)
        )).
      }
    } else {
local __var60 to self:output. __var60:append(__var60,"Unknown waypoint: " + data, Callback:null).
    }}.
set  prototype["onClose"] to { parameter self,_.
    if self:interrupted or not self:streaming
      return false.
    set self:streaming to  false.
    return true.}.
return  result.}):call().
set export to  ShowRoute.
