local KashAutocompleter to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","KashAutocompleter").
set  result["new"] to { parameter type,fs, env, output.
local self to prototype:copy.
set  self["type"] to  result.
    set self:fs to  fs.
    set self:env to  env.
    set self:output to  output.
    set self:term to  false.
    set self:running to  false.
    set self:prevLine to  false.
    set self:tocomplete to  0.
    set self:suggestions to  list().
    set self:redo to  false.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["autocomplete"] to { parameter self,term.
    if self:running {
      return.
    }
    if term:line = self:prevLine {
      set self:redo to  true.
    } else {
      set self:redo to  false.
    }
    set self:tocomplete to  0.
    set self:term to  term.
    set self:prevLine to  term:line.
    set self:running to  true.
    set self:suggestions to  list().

    if term:line:contains(" ") {
local __var58 to self:fs. __var58:open(__var58,self:env["CWD"], Callback:new(Callback,self, "onOpenCwd")).
    } else {
      local parts to  self:env:PATH:split(":").
      for pathname in parts {
        set self:tocomplete to  self:tocomplete + 1.
local __var59 to self:fs. __var59:open(__var59,Util:absPath(Util,pathname, self:env), Callback:new(Callback,self, "onOpenPathDir")).
      }
    }}.
set  prototype["onOpenPathDir"] to { parameter self,dir.
    if dir:istype("Boolean") or dir:getType(dir) <> "dir" {
      set self:tocomplete to  self:tocomplete - 1.
      if self:tocomplete = 0 {
        self:onAllFinished(self).
      }
      return.
    }

    dir:read(dir,Callback:new(Callback,self, "onReadPathDir")).}.
set  prototype["onOpenCwd"] to { parameter self,dir.
    if dir:istype("Boolean") or dir:getType(dir) <> "dir" {
      self:onAllFinished(self).
      return.
    }
    
    dir:read(dir,Callback:new(Callback,self, "onReadCwdDir")).}.
set  prototype["onReadCwdDir"] to { parameter self,files.
    if files:istype("Boolean") {
      self:onAllFinished(self).
      return.
    }

    local lineParts to  self:prevLine:split(" ").
    local completion to  lineParts[lineParts:length - 1].
    local prefix to  lineParts:sublist(0, lineParts:length - 1):join(" ").

    for l in files {
      if l:startsWith(completion) {
        local newLine to  prefix + " " + l.
        print("line: " + l).
        print("combined: " + newLine).
        if not self:suggestions:contains(newLine) {
          self:suggestions:add(newLine).
        }
      }
    }

    self:onAllFinished(self).}.
set  prototype["onReadPathDir"] to { parameter self,files.
    set self:tocomplete to  self:tocomplete - 1.
    if not files:istype("Boolean") {
      for l in files {
        if l:startsWith(self:prevLine) {
          local length to  0.
          if l:endsWith(".ks") {
            set length to  3.
          }
          if l:endsWith(".ksm") {
            set length to  4.
          }
          set l to  l:substring(0, l:length - length).
          if not self:suggestions:contains(l) {
            self:suggestions:add(l).
          }
        }
      }
    }
    if self:tocomplete = 0 {
      self:onAllFinished(self).
    }}.
set  prototype["longestPrefix"] to { parameter self.
    local maxPrefix to  0.
    until false {
      for l in self:suggestions {
        if l:length <= maxPrefix
          return l.
      }
      local reference to  self:suggestions[0][maxPrefix].
      for l in self:suggestions {
        if l[maxPrefix] <> reference {
          return l:substring(0, maxPrefix).
        }
      }
      set maxPrefix to  maxPrefix + 1.
    }}.
set  prototype["onAllFinished"] to { parameter self.
    set self:running to  false.
    if self:term:line <> self:prevLine {
      return.
    }
    if self:redo {
      for l in self:suggestions {
local __var60 to self:output. __var60:append(__var60,l, Callback:null).
      }
    } else {
      if self:suggestions:length = 0 {
        return.
      }
      local maxPrefix to  self:longestPrefix(self).
      
      if self:prevLine <> maxPrefix {
        set self:term:line to  maxPrefix.
        set self:term:cursorPos to  maxPrefix:length.
      }
    }}.
return  result.}):call().
local KashJobGroup to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","KashJobGroup").
set  result["new"] to { parameter type,id, cmd, fs, env, output, setupCallback, needInputCallback, exitCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:id to  id.
    set self:cmd to  cmd.
    set self:fs to  fs.
    set self:env to  env.
    set self:input to  false.
    set self:output to  output.
    set self:setupCallback to  setupCallback.
    set self:needInputCallback to  needInputCallback.
    set self:exitCallback to  exitCallback.
    set self:exitStatus to  -1.
    set self:lastJobId to  -1.
    set self:status to  "setup".
    set self:jobs to  list().
    set self:pids to  lex().
    set self:pidCommands to  lex().
    set self:pidOutputs to  lex().
    set self:running to  lex().
    set self:pipeStack to  Stack().
    set self:backgrounded to  false.
    set self:booted to  false.

    local pipeParts to  cmd:split(" | ").
    for part in pipeParts {
      self:pipeStack:push(part).
    }

    local jobPipe to  Pipe:new(Pipe).
    set self:newInput to  jobPipe.
    set self:halfConnectedOutput to  false.
    set self:currentPart to  self:pipeStack:pop().
local __var61 to self:fs. __var61:exec(__var61,
      self:currentPart,
      self:env,
      jobPipe,
      self:output,
      Callback:new(Callback,self, "onJobExit"),
      Callback:new(Callback,self, "onExec")
    ).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onExec"] to { parameter self,prog.
    if prog:istype("Boolean") {
      set self:status to  self:currentPart + ": command not found".
local __var62 to self:fs:loop. __var62:defer(__var62,self:setupCallback, self).
      if not self:halfConnectedOutput:istype("Boolean") {
local __var63 to self:halfConnectedOutput. __var63:close(__var63,0).
      }
      self:onSignal(self,"End").
      return.
    }

    if self:lastJobId = -1 {
      set self:lastJobId to  prog:pid.
    }
    self:pidCommands:add(prog:pid, self:currentPart).
    self:pidOutputs:add(prog:pid, self:halfConnectedOutput).
    self:jobs:add(prog).
    self:running:add(prog:pid, true).
    self:pids:add(prog:pid, prog).

    if self:pipeStack:length = 0 {
      set self:input to  self:newInput.
      set self:newInput to  false.
      set self:halfConnectedOutput to  false.
      set self:status to  "Running".
      set self:booted to  true.
local __var64 to self:fs:loop. __var64:defer(__var64,self:setupCallback, self).
      set self:input:sendCallback to  Callback:new(Callback,self, "onNeedInput").
      if self:input:waiting {
        self:onNeedInput(self,self:input:identifier).
      }
      return.
    }

    local jobPipe to  Pipe:new(Pipe).
    local lastPipe to  self:newInput.
    set self:newInput to  jobPipe.
    set self:halfConnectedOutput to  lastPipe.
    set self:currentPart to  self:pipeStack:pop().
local __var65 to self:fs. __var65:exec(__var65,
      self:currentPart,
      self:env,
      jobPipe,
      lastPipe,
      Callback:new(Callback,self, "onJobExit"),
      Callback:new(Callback,self, "onExec")
    ).}.
set  prototype["onNeedInput"] to { parameter self,_.
local __var66 to self:needInputCallback. __var66:call(__var66,self).}.
set  prototype["onJobExit"] to { parameter self,pid.
    if not self:running:haskey(pid) {
      if self:pidCommands:haskey(pid) {
        print("kash: Repeated job exit in [" + self:pidCommands[pid] + "]").
      } else {
        print("Invalid pid supplied to resultCallback: " + pid).
      }
      return.
    }
    self:running:remove(pid).
    self:running:add(pid, false).

    if pid = self:lastJobId {
      set self:exitStatus to  self:pids[pid]:status.
local __var67 to self:fs:loop. __var67:defer(__var67,Callback:new(Callback,self, "onSignal"), "End").
    } else {
local __var68 to self:pidOutputs[pid]. __var68:close(__var68,0).
    }

    local anyRunning to  false.
    for pid in self:running:keys {
      if self:running[pid] {
        set anyRunning to  true.
      }
    }
    if not anyRunning {
      set self:status to  "Exit " + self:exitCallback.
local __var69 to self:fs:loop. __var69:defer(__var69,self:exitCallback, self).
    }}.
set  prototype["onSignal"] to { parameter self,signal.
    if signal = "End" {
      local handled to  false.
      for pid in self:running:keys {
        if self:running[pid] and self:pids[pid]:haskey("onSignal") {
local __var70 to self:pids[pid].
          if __var70:onSignal(__var70,signal) {
            set handled to  true.
          }
        }
      }
      return handled.
    }
local __var71 to self:input.
return  __var71:signal(__var71,signal).}.
return  result.}):call().
local Kash to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Kash").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).

    set self:focus to  false.
    set self:jobs to  lex().
    set self:history to  list().
    set self:autocomplete to  KashAutocompleter:new(KashAutocompleter,fs, env, output).

    self:readPromptLine(self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getPrompt"] to { parameter self.
    local hostname to  core:tag.
    if hostname:length = 0 {
      set hostname to  ship:name.
    }
    return hostname + ":" + self:env:CWD + " # ".}.
set  prototype["readPromptLine"] to { parameter self.
    set self:input:prompt to  self:getPrompt(self).
    if self:input:haskey("setHistory") {
local __var72 to self:input. __var72:setHistory(__var72,self:history).
      set self:input:completionHandler to  Callback:new(Callback,self:autocomplete, "autocomplete").
    }
    self:readLine(self).}.
set  prototype["readJobLine"] to { parameter self.
    set self:input:prompt to  "".
    self:readLine(self).}.
set  prototype["nextJobId"] to { parameter self.
    local id to  0.
    for job in self:jobs:keys {
      if job > id
        set id to  job.
    }
    return id + 1.}.
set  prototype["onRead"] to { parameter self,line.
    if self:focus:istype("Boolean") {
      self:onPromptInput(self,line).
    } else {
local __var73 to self:focus:input. __var73:append(__var73,line, Callback:null).
    }}.
set  prototype["onPromptInput"] to { parameter self,data.
    if data:istype("Boolean") {
      self:exit(self,0).
      return.
    }
    self:print(self,self:getPrompt(self) + data).
    self:history:add(data).
    set data to  data:trim.

    if data = "exit" {
      self:exit(self,0).
      return.
    }

    if data = "jobs" {
      for id in self:jobs:keys {
        self:print(self,"[" + id + "]+  " + self:jobs[id]:status + "    " + self:jobs[id]:cmd).
      }
      self:readPromptLine(self).
      return.
    }

    if data:startswith("fg") {
      local parts to  data:split(" ").
      local jobStr to  "current".
      local jobId to  -1.
      if parts:length = 1 {
        for id in self:jobs:keys {
          if id > jobId {
            set jobId to  id.
          }
        }
      } else {
        set jobStr to  parts:sublist(1, parts:length - 1):join(" ").
        set jobId to  jobStr:tonumber(-1).
      }
      if not self:jobs:haskey(jobId) {
        self:print(self,"-kash: fg: " + jobStr + ": no such job").
        self:readPromptLine(self).
        return.
      }
      set self:focus to  self:jobs[jobId].
      self:print(self,self:focus:cmd).
      if self:focus:input:waiting {
        self:readPromptLine(self).
      }
      return.
    }
    
    if data:startswith("export ") {
      local parts to  Util:splitArgs(Util,data).
      for part in parts:sublist(1, parts:length - 1) {
        local split to  part:split("=").
        if split:length = 1 {
          self:print(self,"-kash: export: invalid assignment: " + split[0]).
          return.
        }
        if self:env:haskey(split[0]) {
          self:env:remove(split[0]).
        }
        self:env:add(split[0], split:sublist(1, split:length - 1):join("=")).
      }
      self:readPromptLine(self).
      return.
    }
    
    if data:startswith("cd ") {
      local parts to  Util:splitArgs(Util,data).
      local pathname to  Util:absPath(Util,parts[1], self:env).
      self:cwd(self,pathname).
      self:readPromptLine(self).
      return.
    }

    local launchToBackground to  false.
    if data:endswith("&") {
      set launchToBackground to  true.
      set data to  data:substring(0, data:length - 1).
    }

    local job to  KashJobGroup:new(KashJobGroup,
      self:nextJobId(self),
      data,
      self:fs,
      self:env,
      self:output,
      Callback:new(Callback,self, "onJobSetup"),
      Callback:new(Callback,self, "onJobNeedInput"),
      Callback:new(Callback,self, "onJobExit")
    ).
    if launchToBackground {
      set job:backgrounded to  true.
    }}.
set  prototype["onJobSetup"] to { parameter self,job.
    if not job:booted {
      self:print(self,"-kash: " + job:status).
      self:readPromptLine(self).
      return.
    }
    if self:jobs:haskey(job:id) {
      print("kash: Duplicate job setup error").
      return.
    }
    self:jobs:add(job:id, job).
    if job:backgrounded {
      self:print(self,"[" + job:id + "] " + job:lastJobid).
      self:readPromptLine(self).
    } else {
      set self:focus to  job.
      if self:focus:input:waiting {
        self:readJobLine(self).
      }
    }}.
set  prototype["onJobNeedInput"] to { parameter self,job.
    if self:focus:istype("Boolean")
      return.
    if job <> self:focus
      return.
    self:readJobLine(self).}.
set  prototype["onJobExit"] to { parameter self,job.
    if not self:jobs:haskey(job:id) {
      print("Job " + job:id + " exited but is not registered. Could be duplicate callback?").
      return.
    }
    self:print(self,"[" + job:id + "]+  Exit " + job:exitStatus).
    self:jobs:remove(job:id).
    if not self:focus:istype("Boolean") and self:focus = job {
      set self:focus to  false.
      self:readPromptLine(self).
    }}.
set  prototype["onSignal"] to { parameter self,type.
    if self:focus:istype("Boolean")
      return false.
local __var74 to self:focus.
    
    local childResult to  __var74:onSignal(__var74,type).
    if childResult
      return true.
    
    if type = "Home" {
      self:print(self,"[" + self:focus:id + "]  Backgrounded    " + self:focus:cmd).
      set self:focus to  false.
      self:readPromptLine(self).
      return true.
    }

    return false.}.
return  result.}):call().
set export to  Kash.