local ManeuverNode to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","ManeuverNode").
set  result["new"] to { parameter type,timeS, vel.
local self to prototype:copy.
set  self["type"] to  result.
    set self:timeS to  timeS.
local __var287 to self:type.
    set self:nodeBasis to  __var287:velocitytoNodeBasis(__var287,vel, timeS).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["dV"] to { parameter self.
local __var288 to self:type.
return  __var288:nodeBasisToVelocity(__var288,self:nodeBasis, self:timeS) - velocityat(ship, self:timeS):orbit.}.
set  prototype["toNode"] to { parameter self.
local __var289 to self:type.
    local basis to  __var289:maneuverNodeBasis(__var289,self:timeS).
    local vec to  self:dV(self).
    return Node(self:timeS, vec * basis:radial, vec * basis:normal, vec * basis:prograde).}.
set  result["maneuverNodeBasis"] to { parameter self,timeS.
    local progradeVec to  velocityat(ship, timeS):orbit:normalized.
    local upVec to  (positionat(ship, timeS) - ship:body:position):normalized.
    local normalVec to  vcrs(progradeVec, upVec).
    local radialVec to  vcrs(normalVec, progradeVec).
    return lex(
      "radial", radialVec,
      "normal", normalVec,
      "prograde", progradeVec
    ).}.
set  result["velocityToNodeBasis"] to { parameter self,vec, timeS.
    local basis to  self:maneuverNodeBasis(self,timeS).
    return v(basis:radial * vec, basis:normal * vec, basis:prograde * vec).}.
set  result["nodeBasisToVelocity"] to { parameter self,vec, timeS.
    local basis to  self:maneuverNodeBasis(self,timeS).
    return vec:x * basis:radial + vec:y * basis:normal + vec:z * basis:prograde.}.
set  result["timeForAltitude"] to { parameter self,_alt.
    parameter sbody to ship:body.
    parameter _range to 1.
    if sbody:altitudeOf(positionat(ship, time)) > _alt
      return false.
    if sbody:altitudeOf(positionat(ship, time:seconds + eta:apoapsis)) < _alt
      return false.
    local lower to  time:seconds.
    local upper to  time:seconds + eta:apoapsis.
    local mid to  (lower + upper) / 2.
    local midAlt to  sbody:altitudeOf(positionat(ship, time(mid))).
    until (abs(_alt - midAlt) < _range) {
      if (midAlt < _alt) {
        set lower to  mid.
      } else {
        set upper to  mid.
      }
      set mid to  (lower + upper) / 2.
      set midAlt to  sbody:altitudeOf(positionat(ship, time(mid))).
    }
    return mid.}.
set  result["circularization"] to { parameter self,_alt.
    local intersectionTime to  self:timeForAltitude(self,_alt).
    if intersectionTime:istype("Boolean")
      return false.
     
    local startVel to  velocityat(ship, intersectionTime):orbit.
    local progradeVec to  startVel:normalized.
    local upVec to  (positionat(ship, intersectionTime) - ship:body:position):normalized.
    local horizonVec to  vxcl(upVec, progradeVec):normalized.

    local desiredSpeed to  sqrt(ship:body:MU / (ship:body:radius + _alt)).
    local desiredVelocity to  horizonVec * desiredSpeed.

    return ManeuverNode:new(ManeuverNode,intersectionTime, desiredVelocity).}.
return  result.}):call().
local Launch to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Launch").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).

    if args:length < 2 or args:length > 3   {
      self:print(self,"Usage: launch <height> [<inclination>]").
      self:exit(self,1).
      return self.
    }

    set self:targetHeight to  args[1]:tonumber(-1).
    if self:targetHeight = -1 {
      self:print(self,"Bad targetheight " + args[1]).
      self:exit(self,1).
      return self.
    }
    
    set self:heading to  0.
    if args:length = 3 {
      set self:heading to  args[2]:tonumber(self:heading).
    }

    set self:rotationTime to  20.
    set self:flatAltitude to  30000 + (self:targetHeight - 70000) / 3.
    local targetTWR to  1.5.
    if self:flatAltitude > 70000 {
      set self:flatAltitude to  70000.
    }
    if not body:atm:exists {
      set self:flatAltitude to  10000.
      set targetTWR to  5.
    }

    set self:pitch_pid to  pidloop(0.02, 0, 0.2).

    set self:stage to  "apoapsis".
    set self:launchTWR to  1.5.
    if self:env:haskey("LAUNCH_TWR") {
      set self:launchTWR to  self:env:LAUNCH_TWR:toscalar(self:launchTWR).
    }
    set self:maxAtmoTWR to  3.
    if self:env:haskey("MAX_ATMO_TWR") {
      set self:maxAtmoTWR to  self:env:MAX_ATMO_TWR:toscalar(self:maxAtmoTWR).
    }
    set self:maxAoA to  20.
    if self:env:haskey("LAUNCH_AOA") {
      set self:maxAoA to  self:env:LAUNCH_AOA:toscalar(self:maxAoA).
    }

    self:setFlightDirection(self).
    lock steering to self:desiredFlightDirection.
    set self:desiredThrottle to  0.
    lock throttle to self:desiredThrottle.

    self:print(self,"Launching").
    self:start(self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["maxThrottle"] to { parameter self.
    local pressure to  body:atm:altitudepressure(ship:altitude).
    if pressure > 0.00001 {
      local maxAcc to  ship:availablethrust / ship:mass.
      local kerbinGravity to  10.
      local tmax to  (kerbinGravity * (pressure * self:launchTWR + (1 - pressure) * self:maxAtmoTWR)) / (maxAcc + 0.001).
      return tmax.
    }
    return 1.}.
set  prototype["availableAcceleration"] to { parameter self.
    local maxAcc to  ship:availablethrust / ship:mass.
    return maxAcc * self:maxThrottle(self).}.
set  prototype["localGravity"] to { parameter self.
    local localGravity to  body:mu / ((body:radius + ship:altitude)^2).

    local _angularVelocity to  vxcl(up:vector, ship:velocity:orbit):mag / (body:radius + ship:altitude).
    local centrifugalAccel to  _angularVelocity^2 * (body:radius + ship:altitude).
    return localGravity - centrifugalAccel.}.
set  prototype["desiredUpwardSpeed"] to { parameter self.
    if body:atm:exists {
      local minAcc to  120.
      set minAcc to  min(400, minAcc + ship:altitude / 50).
      return min(minAcc, sqrt(body:atm:altitudepressure(ship:altitude)) * 10000).
    }

    if alt:radar < 500 {
      return max(0, 100 - alt:radar / 5).
    }
    return 0.}.
set  prototype["desiredPitch"] to { parameter self.
    local desiredUp to  self:desiredUpwardSpeed(self).
    local currentUp to  ship:verticalspeed.

    local gravity to  self:localGravity(self).
    local availableAcc to  self:availableAcceleration(self) + 0.001.

    local currentAngle to  90 - vang(up:vector, ship:facing:vector).

    local stableAngle to  90 - vang(up:vector, ship:velocity:surface).
    if ship:velocity:surface:mag < 50 and ship:altitude < 1000 {
      set stableAngle to  90.
    }

    local maxAngle to  min(90, stableAngle + self:maxAoA).
    local minAngle to  max(0, stableAngle - self:maxAoA).
    
    local pressure to  body:atm:altitudepressure(ship:altitude).
    if pressure <= 0.00001 {
      set maxAngle to  90.
      set minAngle to  0.
    }

    local maxAcc to  sin(maxAngle) * availableAcc - gravity.
    local minAcc to  sin(minAngle) * availableAcc - gravity.
    local currentAcc to  sin(currentAngle) * availableAcc - gravity.

    set self:pitch_pid:maxoutput to  maxAcc - currentAcc.
    set self:pitch_pid:minoutput to  minAcc - currentAcc.
    set self:pitch_pid:setpoint to  desiredUp.

    local pidAcc to  self:pitch_pid:update(time:seconds, currentUp) + currentAcc.
    local pidAngle to  arcsin((pidAcc + gravity) / (availableAcc + 0.0001)).

    return pidAngle.}.
set  prototype["run"] to { parameter self.
    self[self:stage](self).}.
set  prototype["apoapsis"] to { parameter self.    
    self:setFlightDirection(self).
    if alt:radar > 200 and gear {
      gear off.
    }
    if ship:apoapsis >= self:targetHeight + 200 {
      set self:toVacuumLoop to  PIDLOOP(1/200, 0, 0, 0, 1).
      set self:toVacuumLoop:setpoint to  self:targetHeight + 200.
      set self:throttleTo to  self:toVacuumLoop:update(time:seconds, ship:apoapsis).
      lock throttle to self:throttleTo.
      set self:stage to  "powerToVacuum".
      self:print(self,"powerToVacuum").
    }}.
set  prototype["setFlightDirection"] to { parameter self.
    set self:desiredThrottle to  self:maxThrottle(self).
    local _orbitNess to  max(0, min(1, (ship:altitude - 500) / 6000)).
    local _vel to  ship:velocity:surface * (1 - _orbitNess) + ship:velocity:orbit * _orbitNess.
    local fwdGeo to  ship:body:geopositionof(_vel + ship:position).
    local flatNess to  min(1, max(0, (ship:velocity:surface:normalized * up:vector - 0.5) / 0.2)).
    local curHeading to  self:heading * flatNess + (fwdGeo:heading - 90) * (1 - flatNess).

    local desiredFlightVector to  (up + R(0, -min(90, 90 - self:desiredPitch(self)), 180)):vector + angleaxis(curHeading, up:vector).
    if ship:availablethrust < 0.01 {
      set self:desiredFlightDirection to  lookdirup(prograde:vector, ship:facing:upvector).
    } else {
      set self:desiredFlightDirection to  lookdirup(desiredFlightVector, ship:facing:upvector).
    }}.
set  prototype["powerToVacuum"] to { parameter self.
    if ship:q <= 0.00001 {
      lock throttle to 0.
      set self:setupStart to  time:seconds.
      lock steering to prograde.
      set self:stage to  "setupCoast".
      self:print(self,"setupCoast").
    } else {
      self:setFlightDirection(self).
      set self:throttleTo to  self:toVacuumLoop:update(time:seconds, ship:apoapsis).
    }}.
set  prototype["setupCoast"] to { parameter self.    if Career():CANMAKENODES {
      if self:setupStart + 5 < time:seconds {   
        set self:node to  Node(ManeuverNode:timeForAltitude(ManeuverNode,self:targetHeight), 0, 0, 0).
        add self:node.
        set self:subProc to  ShellExec:new(ShellExec,
          "circularize",
          self:fs,
          self:env,
          self:input,
          Callback:new(Callback,self, "onNodeCircularized")
        ).
        set self:stage to  "wait".
        set ship:control:pilotmainthrottle to  0.
        unlock throttle.
        unlock steering.
        self:print(self,"circularize node").
      }
    } else {
      set self:stage to  "primitive_circularize_init".
    }}.
set  prototype["onNodeCircularized"] to { parameter self,exec.
    if exec:status <> 0 {
      self:print(self,"Failed to circularize node!").
      exit(1).
      return.
    }
    set self:subProc to  ShellExec:new(ShellExec,
      "exec force",
      self:fs,
      self:env,
      self:input,
      Callback:new(Callback,self, "onNodeExec")
    ).
    set self:stage to  "wait".
    self:print(self,"running exec").}.
set  prototype["onNodeExec"] to { parameter self,exec.
    if exec:status <> 0 {
      self:print(self,"Failed to exec node!").
      self:exit(self,1).
      return.
    }
    set self:stage to  "wait".
    self:print(self,"Launch done").
    self:exit(self,0).}.
set  prototype["primitive_circularize_init"] to { parameter self.
     lock steering to prograde.
     lock throttle to 0.
     set self:stage to  "primitive_circularize_wait".}.
set  prototype["primitive_circularize_init"] to { parameter self.
    if eta:periapsis < eta:apoapsis {
      lock throttle to 1.
      set self:stage to  "primitive_circularize".
    }}.
set  prototype["primitive_circularize"] to { parameter self.
    if alt:apoapsis - alt:periapsis < 1000 {
      self:exit(self,0).
      set self:stage to  "none".
    }}.
set  prototype["wait"] to { parameter self.}.
set  prototype["none"] to { parameter self.
    print("fail to quit launch").}.
set  prototype["deconstruct"] to { parameter self,_.
    if self:stage <> "wait" {
      unlock throttle.
      unlock steering.
    }}.
return  result.}):call().
set export to  Launch.