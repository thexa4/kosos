local  Echo to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:pid to  id.
    output:append(
    output,args:sublist(1, args:length - 1):join(" "), Callback:null).
    set  self:status to  0.
local  __var0 to 
    fs:loop. __var0:defer( __var0,resultCallback, id).
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call().
set  export to  Echo.