local  LexerToken to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","LexerToken").
set  result["new"] to { parameter type,definition, text, filename, lineNumber, charOffset, isWhitespace.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:definition to  definition.
    set  self:text to  text.
    set  self:filename to  filename.
    set  self:lineNumber to  lineNumber.
    set  self:charOffset to  charOffset.
    set  self:isWhitespace to  isWhitespace.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call().
local  LexerTokenDefSimple to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","LexerTokenDefSimple").
set  result["new"] to { parameter type,domain, id, roughRegex, length.
local  self to  prototype:copy.
set  self["type"] to  result.
    parameter whitespace to false.
    set  self:domain to  domain.
    set  self:id to  id.
    set  self:roughRegex to  "^" + roughRegex.
    set  self:l to  length.
    set  self:whitespace to  whitespace.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["match"] to { parameter self,text.
    if text:matchesPattern(self:roughRegex) {
      return text:substring(0, self:l).
    }
    return false.}.
return  result.}):call().
local  LexerTokenDefDynamic to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","LexerTokenDefDynamic").
set  result["new"] to { parameter type,domain, id, roughRegex, refiner.
local  self to  prototype:copy.
set  self["type"] to  result.
    parameter whitespace to false.
    set  self:domain to  domain.
    set  self:id to  id.
    set  self:roughRegex to  "^" + roughRegex.
    set  self:refiner to  refiner.
    set  self:whitespace to  whitespace.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["match"] to { parameter self,text.
    if text:matchesPattern(self:roughRegex) { local length to  self:refiner(text).
      return text:substring(0, length).
    }
    return false.}.
return  result.}):call(). local LexerDefs to  list(
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "NEWLINE", "\n", 1, true),
	LexerTokenDefDynamic:new(LexerTokenDefDynamic,list("kos", "kpp"), "WHITESPACE", "(\s|\p{C})+", {
		// Try matching bigger identifiers until it no longer works
		declare parameter input.
		local length to 0.
		local matches to true.
		until matches = false {
			set length to length + 1.
			local regex to "^(\s|\p{C}){" + (length + 1) + "}".
			set matches to input:matchespattern(regex).
		}
		return length.
	}, true),
  LexerTokenDefDynamic:new(LexerTokenDefDynamic,list("kos", "kpp"), "COMMENTLINE", "//[^\n]*", {
			// Try matching bigger identifiers until it no longer works
			declare parameter input.
			local length to -1.
			local matches to true.
			until matches = false {
				set length to length + 1.
				local regex to "^//[^\n]{" + (length + 1) + "}".
				set matches to input:matchespattern(regex).
			}
			return length + 2.
		}, true),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "LAZYGLOBAL", "lazyglobal\b", 10),
	
		//Math
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "ACCESSOR", "->", 2),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "PLUSMINUS", "(\+|-)", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "MULT", "\*", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "DIV", "/", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "POWER", "\^", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "E", "e((?=\d)|\b)", 1),

		//Logic
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "NOT", "not\b", 3),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "AND", "and\b", 3),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "OR", "or\b", 2),
	LexerTokenDefDynamic:new(LexerTokenDefDynamic,list("kos", "kpp"), "TRUEFALSE", "(true\b|\bfalse\b)", {
		declare parameter input.
		return choose 4 if input:startswith("t") else 5.
	}),
	LexerTokenDefDynamic:new(LexerTokenDefDynamic,list("kos", "kpp"), "COMPARATOR", "(<>|>=|<=|==|>|<)", {
		declare parameter input.
		if input:startswith("<>") { return 2. }
		if input:startswith(">=") { return 2. }
		if input:startswith("<=") { return 2. }
		return 1.
	}),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "ASSIGNMENT", "=", 1),

		//Instructions tokens
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "SET", "set\b", 3),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "TO", "to\b", 2),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "IS", "is\b", 2),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "IF", "if\b", 2),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "ELSE", "else\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "UNTIL", "until\b", 5),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "STEP", "step\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "DO", "do\b", 2),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "LOCK", "lock\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "UNLOCK", "unlock\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "PRINT", "print\b", 5),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "AT", "at\b", 2),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "ON", "on\b", 2),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "TOGGLE", "toggle\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "WAIT", "wait\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "WHEN", "when\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "THEN", "then\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "OFF", "off\b", 3),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "STAGE", "stage\b", 5),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "CLEARSCREEN", "clearscreen\b", 11),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "ADD", "add\b", 3),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "REMOVE", "remove\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "LOG", "log\b", 3),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "BREAK", "break\b", 5),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "PRESERVE", "preserve\b", 8),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "DECLARE", "declare\b", 7),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "DEFINED", "defined\b", 7),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "LOCAL", "local\b", 5),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "GLOBAL", "global\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "PARAMETER", "parameter\b", 9),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "FUNCTION", "function\b", 8),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "RETURN", "return\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "SWITCH", "switch\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "COPY", "copy\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "FROM", "from\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "RENAME", "rename\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "VOLUME", "volume\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "FILE", "file\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "DELETE", "delete\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "EDIT", "edit\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "RUN", "run\b", 3),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "RUNPATH", "runpath\b", 7),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "RUNONCEPATH", "runoncepath\b", 11),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "ONCE", "once\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "COMPILE", "compile\b", 7),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "LIST", "list\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "REBOOT", "reboot\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "SHUTDOWN", "shutdown\b", 8),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "FOR", "for\b", 3),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "UNSET", "unset\b", 5),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "CHOOSE", "choose\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "MIX", "mix\b", 3),

		//Generic
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "INCLUDE", "#include\b", 8),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "MIFNDEF", "#ifndef\b", 7),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "MIFDEF", "#ifdef\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "MELSE", "#else\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "MENDIF", "#endif\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "MDEFINE", "#define\b", 7),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "MUNDEF", "#undef\b", 6),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "MPRAGMAONCE", "#pragma once\b", 12),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "BRACKETOPEN", "\(", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "BRACKETCLOSE", "\)", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "CURLYOPEN", "\{", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "CURLYCLOSE", "\}", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "SQUAREOPEN", "\[", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "SQUARECLOSE", "\]", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "COMMA", ",", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "COLON", ":", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "IN", "in\b", 2),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "ARRAYINDEX", "#", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "ALL", "all\b", 3),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "CLASS", "class\b", 5),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "FUNC", "func\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "INIT", "init\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "SELF", "self\b", 4),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "VAR", "var\b", 3),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "AWAIT", "await\b", 5),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "ASYNC", "async\b", 5),

	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "NEW", "new\b", 3),
	LexerTokenDefDynamic:new(LexerTokenDefDynamic,list("kos", "kpp"), "DOUBLE", "\d[_\d]*\.\d[_\d]*", {
		declare parameter input.
		local prefixlength to -1.
		local matches to true.

		until matches = false {
			set prefixlength to prefixlength + 1.
			local regex to "^[_\d]{" + (prefixlength) + "}".
			set matches to input:matchespattern(regex).
		}
		local rest to input:substring(prefixlength, input:length - prefixlength).

		local postfixlength to -1.
		set matches to true.
		until matches = false {
			set postfixlength to postfixlength + 1.
			local regex to "^[_\d]{" + (postfixlength) + "}".
			set matches to rest:matchespattern(regex).
		}

		return prefixlength + postfixlength - 1.
	}),
	LexerTokenDefDynamic:new(LexerTokenDefDynamic,list("kos", "kpp"), "INTEGER", "\d[_\d]*", {
		// Try matching bigger identifiers until it no longer works
		declare parameter input.
		local length to -1.
		local matches to true.
		until matches = false {
			set length to length + 1.
			local regex to "^\d[_\d]{" + (length + 1) + "}".
			set matches to input:matchespattern(regex).
		}
		return length + 1.
	}),
	LexerTokenDefDynamic:new(LexerTokenDefDynamic,list("kos", "kpp"), "STRING", "@?\" + char(34) + "(\" + char(34) + "\" + char(34) + "|[^\" + char(34) + "])*\" + char(34), {
		declare parameter input.
		local iterator to input:iterator.
		iterator:next.

		local foundquote to false.
		local length to 0.
		until(false) {
			set length to length + 1.
			iterator:next.
			
			if (iterator:atend and not foundquote) {
				print "Assertion failure, unterminated string found.".
				return 99999.
			}
			if (foundquote) {
				if (iterator:atend or iterator:value <> char(34)) {
					return length.
				} else {
					set foundquote to false.
				}
			} else {
				if (iterator:value = char(34)) {
					set foundquote to true.
				}
			}
		}
	}),
	LexerTokenDefDynamic:new(LexerTokenDefDynamic,list("kos", "kpp"), "IDENTIFIER", "[_\p{L}]\w*", {
		// Try matching bigger identifiers until it no longer works
		declare parameter input.
		local length to -1.
		local matches to true.
		until matches = false {
			set length to length + 1.
			local regex to "^[_\p{L}]\w{" + (length + 1) + "}".
			set matches to input:matchespattern(regex).
		}
		return length + 1.
	}),
	LexerTokenDefDynamic:new(LexerTokenDefDynamic,list("kos", "kpp"), "FILEIDENT", "[_\p{L}]\w*(\.[_\p{L}]\w*)*", {
		// Try matching bigger identifiers until it no longer works
		declare parameter input.
		local length to -1.
		local matches to true.
		until matches = false {
			set length to length + 1.
			local regex to "^[_\p{L}][\w\.\p{L}]{" + (length + 1) + "}".
			set matches to input:matchespattern(regex).
		}
		return length + 1.
	}),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos"), "EOI", "\.", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kpp"), "EOI", "(\.|;)", 1),
	LexerTokenDefSimple:new(LexerTokenDefSimple,list("kos", "kpp"), "ATSIGN", "@", 1)
). local VarIdentifiers to  list(
		"IDENTIFIER", "NOT", "AND", "OR", "TRUEFALSE", "SET", "TO",
		"IS", "IF", "ELSE", "UNTIL", "STEP", "DO", "LOCK", "UNLOCK",
		"PRINT", "AT", "ON", "TOGGLE", "WAIT", "WHEN", "THEN", "OFF",
		"STAGE", "CLEARSCREEN", "ADD", "REMOVE", "LOG", "BREAK",
		"PRESERVE", "DECLARE", "DEFINED", "LOCAL", "GLOBAL", "PARAMETER",
		"FUNCTION", "RETURN", "SWITCH", "COPY", "FROM", "RENAME",
		"VOLUME", "FILE", "DELETE", "EDIT", "RUN", "RUNPATH",
		"RUNONCEPATH", "ONCE", "COMPILE", "LIST", "REBOOT", "SHUTDOWN",
		"FOR", "UNSET", "CHOOSE", "LAZYGLOBAL", "IN", "ALL", "CLASS",
		"FUNC", "INIT", "SELF", "VAR", "NEW", "MIX", "AWAIT", "ASYNC"
	).
local  ParserNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ParserNode").
set  result["new"] to { parameter type.
local  self to  prototype:copy.
set  self["type"] to  result.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    // Must be returned in the order they are read
    print "Unimplemented children in node!".
    return list().}.
set  prototype["toSExpr"] to { parameter self.
local  __var0 to  self:type. local result to  "(" + __var0:name(__var0).
    for child in self:children(self) {
      set  result to  result + ", " + child:toSExpr(child).
    }
    set  result to  result + ")".
    return result.}.
set  prototype["string"] to { parameter self. local result to  "".
    for c in self:children(self) {
      set  result to  result + c:string(c).
    }
    return result.}.
set  prototype["update_state"] to { parameter self,parser.}.
set  prototype["simplify"] to { parameter self,context.
    return self.}.
set  prototype["desugar"] to { parameter self,context.
    return self.}.
return  result.}):call().
local  NoneNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","NoneNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["new"] to { parameter type.
local  self to  prototype:copy.
set  self["type"] to  result.
return  self.}.
set  result["__new"] to  result["new"].
set  result["name"] to { parameter self.
    return "None".}.
set  prototype["children"] to { parameter self.
    return list().}.
set  prototype["toString"] to { parameter self.
    return "".}.
return  result.}):call().
local  LiteralNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","LiteralNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["create"] to { parameter self,identifier, whitespace, text. local textToken to  LexerToken:new(LexerToken,identifier, text, "<builtin>", -1, -1, false). local whitespacelist to  list().
    if whitespace:istype("list") {
      set  whitespacelist to  whitespace:copy.
    } else {
      if not whitespace:istype("boolean") {
        whitespacelist:add(LexerToken:new(LexerToken,"WHITESPACE", whitespace, "<builtin>", -1, -1, true)).
      }
    }

    return LiteralNode:new(LiteralNode,textToken, whitespacelist).}.
set  result["name"] to { parameter self.
    return "LiteralNode".}.
set  result["new"] to { parameter type,token, whitespace.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:whitespace to  whitespace.
    set  self:token to  token.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list().}.
set  prototype["toSExpr"] to { parameter self.
    return """" + self:token:text + """".}.
set  prototype["string"] to { parameter self. local result to  "".
    for w in self:whitespace {
      set  result to  result + w:text.
    }
    return result + self:token:text.}.
set  prototype["debug"] to { parameter self.
    return self:token:text + " on " + self:token:filename + ":" + self:token:lineNumber + ":" + self:token:charOffset + " [" + self:token:definition + "].".}.
set  prototype["desugar"] to { parameter self,context.
    if self:token:definition = "EOI" { local newToken to  self:token:copy. local result to  self:copy.
      set  newToken:text to  ".".
      set  result:token to  newToken.
      return result.
    }
    return self.}.
set  result["replaceWhitespace"] to { parameter self,target, replacement. local targetChildren to  target:children(target).
    if targetChildren:length = 0 { local whitespacelist to  list().
      if replacement:istype("list") {
        set  whitespacelist to  replacement:copy.
      } else {
        if not replacement:istype("boolean") {
          whitespacelist:add(LexerToken:new(LexerToken,"WHITESPACE", replacement, "<builtin>", -1, -1, true)).
        }
      } local result to  LiteralNode:new(LiteralNode,target:token, whitespacelist).
      return list(result, target:whitespace).
    } local elem to  targetChildren[0]. local results to  self:replaceWhitespace(self,elem, replacement). local replacedChild to  results[0]. local origWhitespace to  results[1]. local newChildren to  targetChildren:sublist(1, targetChildren:length - 1).
    newChildren:insert(0, replacedChild).
local  __var1 to  target:type. local newElem to  __var1:__new(__var1,newChildren).
    return list(newElem, origWhitespace).}.
return  result.}):call().
local  ParserState to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ParserState").
set  result["new"] to { parameter type,pWindow, definitions, nodePrototype.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:window to  pWindow.
    set  self:definitions to  definitions.
    set  self:parts to  list().
    set  self:errors to  list().
    set  self:warnings to  list().
    set  self:matched to  false.
    set  self:fatal to  false.
    set  self:nodePrototype to  nodePrototype.
    set  self:todo to  nodePrototype:format(nodePrototype):copy.
    set  self:alternatives to  queue().
    set  self:value to  false.
    set  self:blocked to  false.
    set  self:metaStack to  stack().

    for t in self:todo[0] {
      self:alternatives:push(t).
    }

    set  self:nextToken to  false.
    self:eatToken(self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["eatToken"] to { parameter self.
    set  self:prev_window to  self:window. local whitespace to  list().
    set  self:nextToken to  false.

    until(self:window:value:istype("Boolean")) { local current to  self:window:value.
      if current:istype("Boolean") {
        set  self:atend to  true.
        set  self:fatal to  true.
        print("lexer error").
        self:errors:add(self:window:error).
        return.
      }
local  __var2 to  self:window.
      set  self:window to  __var2:next(__var2).
      if current:isWhitespace {
        whitespace:add(current).
      } else {
        set  self:nextToken to  LiteralNode:new(LiteralNode,current, whitespace).
        return.
      }
    }}.
set  prototype["recordFail"] to { parameter self. local result to  self:copy.
    set  result:alternatives to  result:alternatives:copy. local lastAlt to  result:alternatives:pop().
    if not lastAlt:istype("string") {
      set  lastAlt to  lastAlt:name(lastAlt).
    }

    if result:alternatives:length = 0 {
      set  result:atend to  true.
      if self:parts:length > 0 { local todo to  self:todo[self:parts:length].
local  __var3 to  self:nodePrototype. local error to  "Error while trying to parse " + __var3:name(__var3) + ", expected " + lastAlt + " but reached the end of the file.".
        if not self:nexttoken:istype("Boolean")
{
local  __var4 to  self:nodePrototype.
local  __var5 to  self:nextToken.
          set  error to  "Error while trying to parse " + __var4:name(__var4) + ", expected " + lastAlt + " but found: " + __var5:debug(__var5).
}
        set  result to  result:with_fatal(result,error).
      }
    }
    return result.}.
set  prototype["recordMatch"] to { parameter self,nextState. local result to  nextState:copy.
    set  result:parts to  self:parts:copy.
    result:parts:add(result:value).
    set  result:value to  false.
    set  result:todo to  self:todo.
    set  result:nodePrototype to  self:nodePrototype.
    set  result:matched to  false.

    set  result:alternatives to  queue().
    if result:parts:length < result:todo:length {
      for t in result:todo[result:parts:length] {
        result:alternatives:push(t).
      }
    } else {
      result:completeMatch(result).
    }
    return result.}.
set  prototype["matchToken"] to { parameter self,id.
    if self:nextToken:istype("Boolean") {
      return self:recordFail(self).
    }
    if self:nextToken:token:definition = id { local result to  self:copy.
      set  result:parts to  self:parts:copy.
      result:parts:add(self:nextToken).
      set  result:alternatives to  queue().
      if result:parts:length < result:todo:length {
        for t in result:todo[result:parts:length] {
          result:alternatives:push(t).
        }
      } else {
        result:completeMatch(result).
      }
      result:eatToken(result).
      return result.
    } else {
      return self:recordFail(self).
    }}.
set  prototype["completeMatch"] to { parameter self.
    set  self:matched to  true. local prototype to  self:nodePrototype.
    //print("matched: " + prototype->name());
    set  self:value to  prototype:new(prototype,self:parts).}.
set  prototype["branchTodo"] to { parameter self. local result to  self:copy.
    
    set  result:nodePrototype to  self:nextTodo(self).
local  __var6 to  result:nodePrototype.
    set  result:todo to  __var6:format(__var6):copy.
    set  result:alternatives to  queue().
    set  result:value to  false.
    set  result:parts to  list().
    //print("try: " + result->nodePrototype->name());
    //print(result->nextToken);

    if result:todo:length > 0 {
      for t in result:todo[0] {
        result:alternatives:push(t).
      }
    } else {
      result:completeMatch(result).
    }
    return result.}.
set  prototype["nextTodo"] to { parameter self.
    if self:alternatives:length = 0 {
      return false.
    }
    return self:alternatives:peek().}.
set  prototype["currentLexerToken"] to { parameter self.
    return self:nextToken.}.
set  prototype["include"] to { parameter self,lWindow. local result to  self:copy.
    set  result:window to  ParserWindow:new(ParserWindow,lWindow, self:prev_window).
    set  result:nextToken to  false.
    result:eatToken(result).
    return result.}.
set  prototype["handle_meta"] to { parameter self. local definition to  self:nextToken:token:definition.

    if self:metaStack:length > 0 and definition <> "EOF" { local result to  self:copy.

      if definition = "MELSE" {
        set  result:metaStack to  self:metaStack:copy. local current to  result:metaStack:pop().
        result:metaStack:push(not current).
        result:eatToken(result).
        return result.
      }

      if definition = "MENDIF" {
        set  result:metaStack to  self:metaStack:copy.
        result:metaStack:pop().
        result:eatToken(result).
        return result.
      } local shouldSkip to  false.
      for s in self:metaStack {
        set  shouldSkip to  shouldSkip or s.
      }
      
      if shouldSkip {

        if definition = "MIFDEF" or definition = "MIFNDEF" {
          set  result:metaStack to  result:metaStack:copy.
          result:metaStack:push(false).
        }

        result:eatToken(result).

        return result.
      }
    }

    if definition = "MDEFINE" { local result to  self:copy.

      result:eatToken(result). local identifier to  result:nextToken:token.

      result:eatToken(result). local value to  result:nextToken:token.
      if result:definitions:haskey(identifier:text)
        result:definitions:remove(identifier:text).
      result:definitions:add(identifier:text, value).

      result:eatToken(result).
      return result.
    }
    if definition = "MUNDEF" { local result to  self:copy.

      result:eatToken(result). local identifier to  result:nextToken:token.

      result:definitions:remove(identifier:text).

      result:eatToken(result).
      return result.
    }
    if definition = "MIFDEF" or definition = "MIFNDEF" { local result to  self:copy.
      
      result:eatToken(result). local identifier to  result:nextToken:token.

      set  result:metaStack to  result:metaStack:copy. local shouldSkip to  result:definitions:haskey(identifier:text).
      if definition = "MINFDEF"
        set  shouldSkip to  not shouldSkip.
      result:metaStack:push(shouldSkip).

      result:eatToken(result).
      return result.
    }

    if self:definitions:haskey(self:nextToken:token:text) { local result to  self:copy.
      set  result:nextToken to  self:nextToken:copy.
      set  result:nextToken:token to  self:definitions[self:nextToken:token:text].
      return result.
    }

    return self.}.
set  prototype["with_fatal"] to { parameter self,msg. local result to  self:copy.
    set  result:errors to  self:errors:copy.
    result:errors:add(msg).
    set  result:fatal to  true.
    return result.}.
set  prototype["skip"] to { parameter self,n.}.
set  prototype["define"] to { parameter self,id, value.}.
set  prototype["undefine"] to { parameter self,id.}.
set  prototype["addWarning"] to { parameter self,str.}.
return  result.}):call().
local  LexerWindow to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","LexerWindow").
set  result["new"] to { parameter type,l, offset.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:lexer to  l.
    set  self:offset to  offset.
    set  self:cache to  list().
    set  self:atend to  l:atend.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["get"] to { parameter self,n. local i to  self:offset + n.
    until self:lexer:atend or self:cache:length > i {
      self:cache:add(self:lexer:value).
local  __var7 to 
      self:lexer. __var7:next(__var7).
    }

    if self:cache:length > i {
      return self:cache[i].
    }
    return false.}.
set  prototype["next"] to { parameter self.
    if self:atend {
      return false.
    } local hasNext to  self:get(self,2). local result to  self:copy.
    set  result:atend to  hasNext:istype("Boolean").
    set  result:offset to  self:offset + 1.
    return result.}.
return  result.}):call().
local  Lexer to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","Lexer").
set  result["new"] to { parameter type,defs, domain, text, filename, noeof.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:defs to  list().
    for d in defs {
      if d:domain:find(domain) > -1 {
        self:defs:add(d).
      }
    }
    set  self:remainingText to  text.
    set  self:filename to  filename.
    set  self:lineNumber to  1.
    set  self:charOffset to  1.
    set  self:noeof to  noeof.
    set  self:atend to  false.
    set  self:error to  false.
    set  self:window to  LexerWindow:new(LexerWindow,self, 0).
    self:next(self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["next"] to { parameter self. local t to  self:remainingText.
    if t:length = 0 {
      if not self:noeof {
        set  self:value to  LexerToken:new(LexerToken,"EOF", "", self:filename, self:lineNumber, self:charOffset, false).
        set  self:noeof to  true.
        return.
      }
      set  self:value to  false.
      set  self:error to  self:filename + ": end of file".
      set  self:atend to  true.
      return.
    }

    for d in self:defs { local m to  d:match(d,t).
      if m:istype("string") {
        set  self:remainingText to  self:remainingText:remove(0, m:length).
        set  self:value to  LexerToken:new(LexerToken,d:id, m, self:filename, self:lineNumber, self:charOffset, d:whitespace).

        set  self:charOffset to  self:charOffset + m:length.
        if (d:id = "NEWLINE") {
          set  self:lineNumber to  self:lineNumber + 1.
          set  self:charOffset to  1.
        }
        return.
      }
    }
    
    set  self:error to  "Unexpected character found in file " + self:filename + ":" + self:lineNumber + ", char " + self:charOffset + ": '" + self:remainingText:substring(0, min(10, self:remainingText:length)) + "'...".
    set  self:atend to  true.
    set  self:value to  false.}.
return  result.}):call().
local  ParserWindow to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ParserWindow").
set  result["new"] to { parameter type,lWindow, continueWith.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:window to  lWindow.
    set  self:continueWith to  continueWith.
    set  self:atend to  false.
    if continueWith:istype("Boolean") {
      set  self:atend to  lWindow:atend.
    }
    set  self:value to  lWindow:get(lWindow,0).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["next"] to { parameter self.
    if self:window:atend {
      if not self:continueWith:istype("Boolean") {
        return self:continueWith.
      }
      if self:value:istype("Boolean") {
        return self.
      } local result to  self:copy.
      set  result:error to  "Premature end of file".
      set  result:value to  false.
      return result.
    } local result to  self:copy.
local  __var8 to  self:window.
    set  result:window to  __var8:next(__var8).
local  __var9 to  result:window.
    set  result:value to  __var9:get(__var9,0).
    if result:window:atend and self:continueWith:istype("Boolean") {
      set  result:atend to  true.
    }
    return result.}.
return  result.}):call().
local  BottomUpStrategyTodo to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","BottomUpStrategyTodo").
set  result["new"] to { parameter type,node.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:constructor to  node:type.
    set  self:sourceNodes to  node:children(node).
    set  self:attributes to  lex().
    if node:haskey("attributes")
      set  self:attributes to  node:attributes.
    set  self:replacementNodes to  list().
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["isDone"] to { parameter self.
    return self:replacementNodes:length = self:sourceNodes:length.}.
set  prototype["current"] to { parameter self.
    return self:sourceNodes[self:replacementNodes:length].}.
set  prototype["addReplaced"] to { parameter self,replacement.
    self:replacementNodes:add(replacement).}.
set  prototype["compute"] to { parameter self,methodname, context. local type to  self:constructor. local temp to  type:new(type,self:replacementNodes).
    set  temp:attributes to  self:attributes.
    return temp[methodname]:call(temp, context).}.
return  result.}):call().
local  BottomUpStrategy to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","BottomUpStrategy").
set  result["new"] to { parameter type,node, methodname, context.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:stack to  stack().
    set  self:todo to  BottomUpStrategyTodo:new(BottomUpStrategyTodo,node).
    set  self:methodname to  methodname.

    set  self:atend to  false.
    set  self:value to  false.
    set  self:context to  context.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["next"] to { parameter self.
local  __var10 to  self:todo.
    if __var10:isDone(__var10) {
local  __var11 to  self:todo. local transformed to  __var11:compute(__var11,self:methodname, self:context).
      if self:stack:length = 0 {
        set  self:value to  transformed.
        set  self:atend to  true.
      } else {
        set  self:todo to  self:stack:pop().
local  __var12 to 
        self:todo. __var12:addReplaced(__var12,transformed).
      }
      return.
    }
local  __var13 to  self:todo. local current to  __var13:current(__var13).
local  __var14 to  current:type.
    if __var14:name(__var14) = "LiteralNode" {
      if current:haskey(self:methodname)
        set  current to  current[self:methodname]:call(current, self:context).
local  __var15 to 
      self:todo. __var15:addReplaced(__var15,current).
    } else { local newTodo to  BottomUpStrategyTodo:new(BottomUpStrategyTodo,current).
      self:stack:push(self:todo).
      set  self:todo to  newTodo.
    }}.
return  result.}):call().
local  Parser to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","Parser").
set  result["new"] to { parameter type,lexWindow, lexerDefs, domain, readFileFn, startSymbol, definitions.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:stack to  stack().
    set  self:current to  ParserState:new(ParserState,
      ParserWindow:new(ParserWindow,lexWindow, false),
      definitions,
      startSymbol
     ).
    
    set  self:lexerDefs to  lexerDefs.
    set  self:domain to  domain.
    set  self:readFileFn to  readFileFn.

    set  self:value to  false.
    set  self:atend to  false.
    set  self:errors to  list().
    set  self:warnings to  list().
    set  self:file_cache to  lex().
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["next"] to { parameter self.
    if self:atend or self:current:blocked
      return.
local  __var16 to  self:current. local new_state to  __var16:handle_meta(__var16).
    if new_state <> self:current {
      set  self:current to  new_state.
      return.
    }
local  __var17 to  self:current. local next_todo to  __var17:nextTodo(__var17).
    if next_todo:istype("Boolean") {
      // None matched.
      if self:stack:length = 0 {
        set  self:atend to  true.
        return.
      }

      set  self:current to  self:stack:pop().
local  __var18 to  self:current.
      set  self:current to  __var18:recordFail(__var18).

      if self:current:fatal {
        set  self:errors to  self:current:errors.
        set  self:warnings to  self:current:warnings.
        set  self:atend to  true.
      }
      return.
    }

    if next_todo:istype("String") {
local  __var19 to  self:current.
      set  self:current to  __var19:matchToken(__var19,next_todo).
    } else {
      self:stack:push(self:current).
local  __var20 to  self:current.
      set  self:current to  __var20:branchTodo(__var20).
    }

    self:_unwind_matches(self).}.
set  prototype["_unwind_matches"] to { parameter self.
    until (not self:current:matched) or self:current:blocked
    {
      if self:current:fatal {
        set  self:errors to  self:current:errors.
        set  self:warnings to  self:current:warnings.
        set  self:atend to  true.
        return.
      }
local  __var21 to 
    
      self:current:value. __var21:update_state(__var21,self).
      if self:current:blocked {
        return.
      }

      if self:stack:length = 0 {
        set  self:value to  self:current:value.
        set  self:atend to  true.
        return.
      } else { local prev to  self:current.
        set  self:current to  self:stack:pop().
local  __var22 to  self:current.
        set  self:current to  __var22:recordMatch(__var22,prev).
      }
    }
    if self:current:fatal {
      set  self:errors to  self:current:errors.
      set  self:warnings to  self:current:warnings.
      set  self:atend to  true.
      return.
    }}.
set  prototype["onFileRead"] to { parameter self,filename, content.
    if content:istype("Boolean") {
      self:file_cache:add(filename, false).
    } else { local l to  Lexer:new(Lexer,self:lexerDefs, self:domain, content, filename, true). local lwindow to  LexerWindow:new(LexerWindow,l, 0).
      self:file_cache:add(filename, lwindow).
    }

    if self:current:blocked {
local  __var23 to 
      self:current:value. __var23:update_state(__var23,self).
    }
    self:_unwind_matches(self).}.
return  result.}):call().
local  EmptyStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","EmptyStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "EmptyStmt".}.
set  result["format"] to { parameter self.
    return list(list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:eoi to  parts[0].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:eoi).}.
return  result.}):call().
local  NoneNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","NoneNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "None".}.
set  result["format"] to { parameter self.
    return list().}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list().}.
return  result.}):call().
local  NumberNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","NumberNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "NumberNode".}.
set  result["format"] to { parameter self.
    return list(list("INTEGER", "DOUBLE")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:literal to  parts[0].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:literal).}.
return  result.}):call().
local  SciNumberTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","SciNumberTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "SciNumberNodeTrailer".}.
set  result["format"] to { parameter self.
    return list(list("E"), list("PLUSMINUS", NoneNode), list("INTEGER")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:eliteral to  parts[0].
    set  self:plusminus to  parts[1].
    set  self:exponent to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:eliteral, self:plusminus, self:exponent).}.
return  result.}):call().
local  SciNumberNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","SciNumberNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "SciNumber".}.
set  result["format"] to { parameter self.
    return list(list(NumberNode), list(SciNumberTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:number to  parts[0].
    set  self:trailer to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["simplify"] to { parameter self,context. local tailType to  self:trailer:type.
    if tailType:name(tailType) = "None" {
      return self:number.
    }
    return self.}.
set  prototype["children"] to { parameter self.
    return list(self:number, self:trailer).}.
return  result.}):call().
local  TernaryExprNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","TernaryExprNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "TernaryExpr".}.
set  result["format"] to { parameter self.
    return list(list("CHOOSE"), list(ExprNode), list("IF"), list(ExprNode), list("ELSE"), list(ExprNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:chooseLiteral to  parts[0].
    set  self:trueExpr to  parts[1].
    set  self:ifLiteral to  parts[2].
    set  self:testExpr to  parts[3].
    set  self:elseLiteral to  parts[4].
    set  self:elseExpr to  parts[5].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:chooseLiteral, self:trueExpr, self:ifLiteral, self:testExpr, self:elseLiteral, self:elseExpr).}.
return  result.}):call().
local  FactorTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","FactorTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "FactorTrailer".}.
set  result["format"] to { parameter self.
    return list(list("POWER"), list(SuffixNode), list(FactorTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:power to  parts[0].
    set  self:suffix to  parts[1].
    set  self:tail to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:power, self:suffix, self:tail).}.
set  prototype["desugar"] to { parameter self,context. local suffix to  self:suffix. local tail to  self:tail. local instructions to  list().
local  __var24 to  suffix:type.
    if __var24:name(__var24) = "ExprBreakupPropagation" {
      set  instructions to  suffix:statements:copy.
      set  suffix to  suffix:inner.
    }
local  __var25 to  tail:type.
    if __var25:name(__var25) = "ExprBreakupPropagation" {
      for i in tail:statements {
        instructions:add(i).
      }
      set  tail to  tail:inner.
    }

    if instructions:length = 0
      return self. local inner to  FactorTrailerNode:new(FactorTrailerNode,list(self:power, suffix, tail)).
    instructions:insert(0, inner).
    return ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,instructions).}.
return  result.}):call().
local  FactorNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","FactorNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "Factor".}.
set  result["format"] to { parameter self.
    return list(list(SuffixNode), list(FactorTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:suffix to  parts[0].
    set  self:tail to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:suffix, self:tail).}.
set  prototype["simplify"] to { parameter self,context. local tailType to  self:tail:type.
    if tailType:name(tailType) = "None" {
      return self:suffix.
    }
    return self.}.
set  prototype["desugar"] to { parameter self,context. local suffix to  self:suffix. local tail to  self:tail. local instructions to  list().
local  __var26 to  suffix:type.
    if __var26:name(__var26) = "ExprBreakupPropagation" {
      set  instructions to  suffix:statements:copy.
      set  suffix to  suffix:inner.
    }
local  __var27 to  tail:type.
    if __var27:name(__var27) = "ExprBreakupPropagation" {
      for i in tail:statements {
        instructions:add(i).
      }
      set  tail to  tail:inner.
    }

    if instructions:length = 0
      return self. local inner to  FactorNode:new(FactorNode,list(suffix, tail)).
    instructions:insert(0, inner).
    return ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,instructions).}.
return  result.}):call().
local  UnaryExprInnerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","UnaryExprInnerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "UnaryExprInner".}.
set  result["format"] to { parameter self.
    return list(list("PLUSMINUS", "NOT", "DEFINED"), list(FactorNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:operator to  parts[0].
    set  self:operand to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:operator, self:operand).}.
return  result.}):call().
local  UnaryExprNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","UnaryExprNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "UnaryExpr".}.
set  result["format"] to { parameter self.
    return list(list(UnaryExprInnerNode, FactorNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:inner to  parts[0].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:inner).}.
set  prototype["simplify"] to { parameter self,context.
    return self:inner.}.
return  result.}):call().
local  MultDivExprTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","MultDivExprTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "MultDivExprTrailer".}.
set  result["format"] to { parameter self.
    return list(list("MULT", "DIV"), list(UnaryExprNode), list(MultDivExprTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:operator to  parts[0].
    set  self:operand to  parts[1].
    set  self:tail to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:operator, self:operand, self:tail).}.
return  result.}):call().
local  MultDivExprNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","MultDivExprNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "MultDivExpr".}.
set  result["format"] to { parameter self.
    return list(list(UnaryExprNode), list(MultDivExprTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:expr to  parts[0].
    set  self:tail to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:expr, self:tail).}.
set  prototype["simplify"] to { parameter self,context. local tailType to  self:tail:type.
    if tailType:name(tailType) = "None" {
      return self:expr.
    }
    return self.}.
return  result.}):call().
local  PlusMinusExprTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","PlusMinusExprTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "PlusMinusExprTrailer".}.
set  result["format"] to { parameter self.
    return list(list("PLUSMINUS"), list(MultDivExprNode), list(PlusMinusExprTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:operator to  parts[0].
    set  self:operand to  parts[1].
    set  self:tail to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:operator, self:operand, self:tail).}.
set  prototype["desugar"] to { parameter self,context. local operand to  self:operand. local tail to  self:tail. local instructions to  list().
local  __var28 to  operand:type.
    if __var28:name(__var28) = "ExprBreakupPropagation" {
      set  instructions to  operand:statements:copy.
      set  operand to  operand:inner.
    }
local  __var29 to  tail:type.
    if __var29:name(__var29) = "ExprBreakupPropagation" {
      for i in tail:statements {
        instructions:add(i).
      }
      set  tail to  tail:inner.
    }

    if instructions:length = 0
      return self. local inner to  PlusMinusExprTrailerNode:new(PlusMinusExprTrailerNode,list(self:operator, operand, tail)).
    instructions:insert(0, inner).
    return ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,instructions).}.
return  result.}):call().
local  PlusMinusExprNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","PlusMinusExprNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "PlusMinusExpr".}.
set  result["format"] to { parameter self.
    return list(list(MultDivExprNode), list(PlusMinusExprTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:expr to  parts[0].
    set  self:tail to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:expr, self:tail).}.
set  prototype["simplify"] to { parameter self,context. local tailType to  self:tail:type.
    if tailType:name(tailType) = "None" {
      return self:expr.
    }
    return self.}.
set  prototype["desugar"] to { parameter self,context. local expr to  self:expr. local tail to  self:tail. local instructions to  list().
local  __var30 to  expr:type.
    if __var30:name(__var30) = "ExprBreakupPropagation" {
      set  instructions to  expr:statements:copy.
      set  expr to  expr:inner.
    }
local  __var31 to  tail:type.
    if __var31:name(__var31) = "ExprBreakupPropagation" {
      for i in tail:statements {
        instructions:add(i).
      }
      set  tail to  tail:inner.
    }

    if instructions:length = 0
      return self. local inner to  PlusMinusExprNode:new(PlusMinusExprNode,list(expr, tail)).
    instructions:insert(0, inner).
    return ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,instructions).}.
return  result.}):call().
local  CompareExprTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","CompareExprTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "CompareExprTrailer".}.
set  result["format"] to { parameter self.
    return list(list("COMPARATOR", "ASSIGNMENT"), list(PlusMinusExprNode), list(compareExprTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:operator to  parts[0].
    set  self:operand to  parts[1].
    set  self:tail to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:operator, self:operand, self:tail).}.
set  prototype["desugar"] to { parameter self,context. local operand to  self:operand. local tail to  self:tail. local instructions to  list().
local  __var32 to  operand:type.
    if __var32:name(__var32) = "ExprBreakupPropagation" {
      set  instructions to  operand:statements:copy.
      set  operand to  operand:inner.
    }
local  __var33 to  tail:type.
    if __var33:name(__var33) = "ExprBreakupPropagation" {
      for i in tail:statements {
        instructions:add(i).
      }
      set  tail to  tail:inner.
    }

    if instructions:length = 0
      return self. local inner to  CompareExprTrailerNode:new(CompareExprTrailerNode,list(self:operator, operand, tail)).
    instructions:insert(0, inner).
    return ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,instructions).}.
return  result.}):call().
local  CompareExprNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","CompareExprNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "CompareExpr".}.
set  result["format"] to { parameter self.
    return list(list(PlusMinusExprNode), list(CompareExprTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:expr to  parts[0].
    set  self:tail to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:expr, self:tail).}.
set  prototype["simplify"] to { parameter self,context. local tailType to  self:tail:type.
    if tailType:name(tailType) = "None" {
      return self:expr.
    }
    return self.}.
set  prototype["desugar"] to { parameter self,context. local expr to  self:expr. local tail to  self:tail. local instructions to  list().
local  __var34 to  expr:type.
    if __var34:name(__var34) = "ExprBreakupPropagation" {
      set  instructions to  expr:statements:copy.
      set  expr to  expr:inner.
    }
local  __var35 to  tail:type.
    if __var35:name(__var35) = "ExprBreakupPropagation" {
      for i in tail:statements {
        instructions:add(i).
      }
      set  tail to  tail:inner.
    }

    if instructions:length = 0
      return self. local inner to  CompareExprNode:new(CompareExprNode,list(expr, tail)).
    instructions:insert(0, inner).
    return ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,instructions).}.
return  result.}):call().
local  AndExprTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","AndExprTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "AndExprTrailer".}.
set  result["format"] to { parameter self.
    return list(list("AND"), list(CompareExprNode), list(AndExprTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:operator to  parts[0].
    set  self:operand to  parts[1].
    set  self:tail to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:operator, self:operand, self:tail).}.
set  prototype["desugar"] to { parameter self,context. local operand to  self:operand. local tail to  self:tail. local instructions to  list().
local  __var36 to  operand:type.
    if __var36:name(__var36) = "ExprBreakupPropagation" {
      set  instructions to  operand:statements:copy.
      set  operand to  operand:inner.
    }
local  __var37 to  tail:type.
    if __var37:name(__var37) = "ExprBreakupPropagation" {
      for i in tail:statements {
        instructions:add(i).
      }
      set  tail to  tail:inner.
    }

    if instructions:length = 0
      return self. local inner to  AndExprTrailerNode:new(AndExprTrailerNode,list(self:operator, operand, tail)).
    instructions:insert(0, inner).
    return ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,instructions).}.
return  result.}):call().
local  AndExprNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","AndExprNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "AndExpr".}.
set  result["format"] to { parameter self.
    return list(list(CompareExprNode), list(AndExprTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:expr to  parts[0].
    set  self:tail to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:expr, self:tail).}.
set  prototype["simplify"] to { parameter self,context. local tailType to  self:tail:type.
    if tailType:name(tailType) = "None" {
      return self:expr.
    }
    return self.}.
set  prototype["desugar"] to { parameter self,context. local expr to  self:expr. local tail to  self:tail. local instructions to  list().
local  __var38 to  expr:type.
    if __var38:name(__var38) = "ExprBreakupPropagation" {
      set  instructions to  expr:statements:copy.
      set  expr to  expr:inner.
    }
local  __var39 to  tail:type.
    if __var39:name(__var39) = "ExprBreakupPropagation" {
      for i in tail:statements {
        instructions:add(i).
      }
      set  tail to  tail:inner.
    }

    if instructions:length = 0
      return self. local inner to  AndExprNode:new(AndExprNode,list(expr, tail)).
    instructions:insert(0, inner).
    return ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,instructions).}.
return  result.}):call().
local  OrExprTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","OrExprTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "OrExprTrailer".}.
set  result["format"] to { parameter self.
    return list(list("OR"), list(AndExprNode), list(OrExprTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:operator to  parts[0].
    set  self:operand to  parts[1].
    set  self:tail to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:operator, self:operand, self:tail).}.
return  result.}):call().
local  OrExprNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","OrExprNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "OrExpr".}.
set  result["format"] to { parameter self.
    return list(list(AndExprNode), list(OrExprTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:expr to  parts[0].
    set  self:tail to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:expr, self:tail).}.
set  prototype["simplify"] to { parameter self,context. local tailType to  self:tail:type.
    if tailType:name(tailType) = "None" {
      return self:expr.
    }
    return self.}.
return  result.}):call().
local  InstructionBlockStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","InstructionBlockStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "InstructionBlockStmt".}.
set  result["format"] to { parameter self.
    return list(list("CURLYOPEN"), list(InstructionListNode, NoneNode), list("CURLYCLOSE")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:open to  parts[0].
    set  self:instructions to  parts[1].
    set  self:close to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:open, self:instructions, self:close).}.
return  result.}):call().
local  ExprNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ExprNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "Expr".}.
set  result["format"] to { parameter self.
    return list(list(TernaryExprNode, OrExprNode, InstructionBlockStmtNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:inner to  parts[0].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:inner).}.
set  prototype["simplify"] to { parameter self,context.
    return self:inner.}.
return  result.}):call().
local  BracketExprAtom to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","BracketExprAtom").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "BracketExprAtom".}.
set  result["format"] to { parameter self.
    return list(list("BRACKETOPEN"), list(ExprNode), list("BRACKETCLOSE")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:open to  parts[0].
    set  self:expr to  parts[1].
    set  self:close to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:open, self:expr, self:close).}.
set  prototype["desugar"] to { parameter self,context.
local  __var40 to  self:expr:type.
    if __var40:name(__var40) = "ExprBreakupPropagation" { local destructured to  BracketExprAtom:new(BracketExprAtom,self:open, self:expr:inner, self:close). local parts to  self:expr:statements:copy.
      parts:insert(0, destructured).
      return ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,parts).
    }
    
    return self.}.
set  result["create"] to { parameter self,expr.
    return BracketExprAtom:new(BracketExprAtom,list(
      LiteralNode:create(LiteralNode,"BRACKETOPEN", "", "("),
      expr,
      LiteralNode:create(LiteralNode,"BRACKETCLOSE", "", ")")
    )).}.
return  result.}):call().
local  AtomNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","AtomNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "Atom".}.
set  result["format"] to { parameter self. local allowed to  VarIdentifiers:copy.
    allowed:insert(0, "TRUEFALSE").
    allowed:insert(0, SciNumberNode).
    allowed:add("FILEIDENT").
    allowed:add(BracketExprAtom).
    allowed:add("STRING").
    return list(allowed).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:literal to  parts[0].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:literal).}.
set  prototype["simplify"] to { parameter self,context. local type to  self:literal:type.
    if type:name(type) = "SciNumber" or type:name(type) = "BracketExprAtom" {
      return self:literal.
    }
    return self.}.
return  result.}):call().
local  ArrayAccessNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ArrayAccessNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ArrayAccess".}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:subject to  parts[0].
    set  self:bopen to  parts[1].
    set  self:index to  parts[2].
    set  self:bclose to  parts[3].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:subject, self:bopen, self:index, self:bclose).}.
set  result["create"] to { parameter self,subject, key.
    return ArrayAccessNode:new(ArrayAccessNode,list(
      subject,
      LiteralNode:create(LiteralNode,"SQUAREOPEN", "", "["),
      key,
      LiteralNode:create(LiteralNode,"SQUARECLOSE", "", "]")
    )).}.
return  result.}):call().
local  ArrayBracketTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ArrayBracketTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ArrayBracketTrailer".}.
set  result["format"] to { parameter self.
    return list(list("SQUAREOPEN"), list(ExprNode), list("SQUARECLOSE")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:open to  parts[0].
    set  self:expr to  parts[1].
    set  self:close to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:open, self:expr, self:close).}.
set  prototype["suffix_simplify"] to { parameter self,subject.
    return ArrayAccessNode:new(ArrayAccessNode,list(subject, self:open, self:expr, self:close)).}.
return  result.}):call().
local  ArrayHashTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ArrayHashTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ArrayHashTrailer".}.
set  result["format"] to { parameter self.
    return list(list("ARRAYINDEX"), list("IDENTIFIER", "INTEGER")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:hash to  parts[0].
    set  self:identifier to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:hash, self:identifier).}.
set  prototype["suffix_simplify"] to { parameter self,subject.
    return ArrayAccessNode:new(ArrayAccessNode,list(subject, LiteralNode:create(LiteralNode,"SQUAREOPEN", "", "["), self:identifier, LiteralNode:create(LiteralNode,"SQUARECLOSE", "", "]"))).}.
return  result.}):call().
local  ArrayTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ArrayTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ArrayTrailer".}.
set  result["format"] to { parameter self.
    return list(list(ArrayBracketTrailerNode, ArrayHashTrailerNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:trailer to  parts[0].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:trailer).}.
set  prototype["suffix_simplify"] to { parameter self,subject.
local  __var41 to  self:trailer.
return  __var41:suffix_simplify(__var41,subject).}.
return  result.}):call().
local  ArgListTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ArgListTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ArgListTrailer".}.
set  result["format"] to { parameter self.
    return list(list("COMMA"), list(ExprNode), list(ArgListTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:comma to  parts[0].
    set  self:expr to  parts[1].
    set  self:trailer to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:comma, self:expr, self:trailer).}.
set  prototype["to_list"] to { parameter self,current. local result to  current:copy.
    result:add(self:comma).
    result:add(self:expr).
local  __var42 to  self:trailer:type.

    if __var42:name(__var42) = "None"
      return result.
local  __var43 to  self:trailer.
return  __var43:to_list(__var43,result).}.
set  prototype["to_parameter_list"] to { parameter self. local rest to  NoneNode:new(NoneNode,list()).
local  __var44 to  self:trailer:type.
    if __var44:name(__var44) <> "None" {
local  __var45 to  self:trailer.
      set  rest to  __var45:to_parameter_list(__var45).
    }
    return DeclareParameterClauseTrailer:new(DeclareParameterClauseTrailer,list(
      LiteralNode:create(LiteralNode,"COMMA", "", ","),
      self:expr,
      NoneNode:new(NoneNode,list()),
      rest
    )).}.
return  result.}):call().
local  ArgListNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ArgListNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ArgList".}.
set  result["format"] to { parameter self.
    return list(list(ExprNode), list(ArgListTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:expr to  parts[0].
    set  self:trailer to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:expr, self:trailer).}.
set  prototype["to_list"] to { parameter self. local result to  list(self:expr).
local  __var46 to  self:trailer:type.
    if __var46:name(__var46) = "None"
      return result.
local  __var47 to  self:trailer.
return  __var47:to_list(__var47,result).}.
set  prototype["to_parameter_list"] to { parameter self. local rest to  NoneNode:new(NoneNode,list()).
local  __var48 to  self:trailer:type.
    if __var48:name(__var48) <> "None" {
local  __var49 to  self:trailer.
      set  rest to  __var49:to_parameter_list(__var49).
    }
    return DeclareParameterClauseTrailer:new(DeclareParameterClauseTrailer,list(
      LiteralNode:create(LiteralNode,"COMMA", "", ","),
      self:expr,
      NoneNode:new(NoneNode,list()),
      rest
    )).}.
return  result.}):call().
local  NonSingleStatementGuardNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","NonSingleStatementGuardNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "NonSingleStatementGuard".}.
set  result["format"] to { parameter self.
    return list(list(InstructionListNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:instructions to  parts[0].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:instructions).}.
return  result.}):call().
local  ExprBreakupPropagationNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ExprBreakupPropagationNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ExprBreakupPropagation".}.
set  result["gen_variable"] to { parameter self.
    if not self:prototype:haskey("counter")
      set  self:prototype:counter to  0. local token to  LexerToken:new(LexerToken,"VARIDENTIFIER", "__var" + self:prototype:counter, "<builtin>", -1, -1, false). local varname to  LiteralNode:new(LiteralNode,token, list(LexerToken:new(LexerToken,"SPACE", " ", "<builtin>", -1, -1, true))).
    set  self:prototype:counter to  self:prototype:counter + 1.
    return varname.}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:inner to  parts[0].
    set  self:statements to  parts:sublist(1, parts:length - 1).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self. local result to  self:statements:copy.
    result:insert(0, self:inner).
    return result.}.
set  prototype["to_block"] to { parameter self,new_inner.
    function recursion {
      parameter lst, inner.
      if lst:length = 0
        return inner. local current to  lst[0]. local rest to  lst:sublist(1, lst:length - 1).
      return InstructionListNode:new(InstructionListNode,list(current, recursion(rest, inner))).
    } local instructionList to  recursion(self:statements, new_inner).
    return NonSingleStatementGuardNode:new(NonSingleStatementGuardNode,list(instructionList)).}.
return  result.}):call().
local  DeclareFunctionClause to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","DeclareFunctionClause").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "DeclareFunctionClause".}.
set  result["format"] to { parameter self.
    return list(
      list("FUNCTION"),
      list("IDENTIFIER"),
      list(InstructionBlockStmtNode),
      list("EOI", NoneNode)
    ).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:function to  parts[0].
    set  self:identifier to  parts[1].
    set  self:instruction_list to  parts[2].
    set  self:eoi to  parts[3].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:function, self:identifier, self:instruction_list, self:eoi).}.
return  result.}):call().
local  DeclareIdentifierClause to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","DeclareIdentifierClause").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "DeclareIdentifierClause".}.
set  result["format"] to { parameter self.
    return list(VarIdentifiers, list("TO", "IS"), list(ExprNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:identifier to  parts[0].
    set  self:to to  parts[1].
    set  self:expr to  parts[2].
    set  self:eoi to  parts[3].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:identifier, self:to, self:expr, self:eoi).}.
return  result.}):call().
local  DeclareLockClause to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","DeclareLockClause").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "DeclareLockClause".}.
set  result["format"] to { parameter self.
    return list(
      list("LOCK"),
      VarIdentifiers,
      list("TO"),
      list(exprNode),
      list("EOI")
    ).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:lock to  parts[0].
    set  self:identifier to  parts[1].
    set  self:to to  parts[2].
    set  self:expr to  parts[3].
    set  self:eoi to  parts[4].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(
      self:lock,
      self:identifier,
      self:to,
      self:expr,
      self:eoi
    ).}.
return  result.}):call().
local  DeclareParameterClauseDefaultPart to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","DeclareParameterClauseDefaultPart").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "DeclareParameterClauseDefaultPart".}.
set  result["format"] to { parameter self.
    return list(list("TO", "IS"), list(ExprNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:to to  parts[0].
    set  self:expr to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:to, self:expr).}.
return  result.}):call().
local  DeclareParameterClauseTrailer to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","DeclareParameterClauseTrailer").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "DeclareParameterClauseTrailer".}.
set  result["format"] to { parameter self.
    return list(list("COMMA"), VarIdentifiers, list(DeclareParameterClauseDefaultPart, NoneNode), list(DeclareParameterClauseTrailer, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:comma to  parts[0].
    set  self:identifier to  parts[1].
    set  self:default to  parts[2].
    set  self:tail to  parts[3].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:comma, self:identifier, self:default, self:tail).}.
return  result.}):call().
local  DeclareParameterClause to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","DeclareParameterClause").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "DeclareParameterClause".}.
set  result["format"] to { parameter self.
    return list(
      list("PARAMETER"),
      VarIdentifiers,
      list(DeclareParameterClauseDefaultPart, NoneNode),
      list(DeclareParameterClauseTrailer, NoneNode),
      list("EOI")
    ).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:parameter to  parts[0].
    set  self:identifier to  parts[1].
    set  self:defaults to  parts[2].
    set  self:trailer to  parts[3].
    set  self:eoi to  parts[4].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:parameter, self:identifier, self:defaults, self:trailer, self:eoi).}.
return  result.}):call().
local  DeclareStmtScopeNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","DeclareStmtScopeNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "DeclareScope".}.
set  result["format"] to { parameter self.
    return list(
      list("LOCAL", "GLOBAL")
    ).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:scope to  parts[0].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(
      self:scope
    ).}.
return  result.}):call().
local  DeclareStmtDeclarePrefixNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","DeclareStmtDeclarePrefixNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "DeclareDeclarePrefix".}.
set  result["format"] to { parameter self.
    return list(
      list("DECLARE"),
      list(DeclareStmtScopeNode, NoneNode)
    ).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:declare to  parts[0].
    set  self:scope to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(
      self:declare,
      self:scope
    ).}.
return  result.}):call().
local  DeclareStmtScopedDeclarationNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","DeclareStmtScopedDeclarationNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "DeclareScopedDeclaration".}.
set  result["format"] to { parameter self.
    return list(
      list(DeclareStmtDeclarePrefixNode, DeclareStmtScopeNode),
      list(DeclareParameterClause, DeclareFunctionClause, DeclareIdentifierclause, DeclareLockClause)
    ).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:prefix to  parts[0].
    set  self:definition to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(
      self:prefix,
      self:definition
    ).}.
set  result["create_local"] to { parameter self,identifier, value.
    parameter whitespace to char(10). local prefix to  DeclareStmtScopeNode:new(DeclareStmtScopeNode,list(LiteralNode:create(LiteralNode,"LOCAL", whitespace, "local"))). local strippedIdentifier to  LiteralNode:replaceWhitespace(LiteralNode,identifier, " "). local strippedValue to  LiteralNode:replaceWhitespace(LiteralNode,value, " "). local definition to  DeclareIdentifierClause:new(DeclareIdentifierClause,list(strippedIdentifier[0], LiteralNode:create(LiteralNode,"TO", " ", "to"), strippedValue[0], LiteralNode:create(LiteralNode,"EOI", "", "."))).
    return DeclareStmtScopedDeclarationNode:new(DeclareStmtScopedDeclarationNode,list(prefix, definition)).}.
set  result["create_global"] to { parameter self,identifier, value.
    parameter whitespace to char(10). local prefix to  DeclareStmtScopeNode:new(DeclareStmtScopeNode,list(LiteralNode:create(LiteralNode,"GLOBAL", char(10), "global"))). local strippedIdentifier to  LiteralNode:replaceWhitespace(LiteralNode,identifier, " "). local strippedValue to  LiteralNode:replaceWhitespace(LiteralNode,value, " "). local definition to  DeclareIdentifierClause:new(DeclareIdentifierClause,list(strippedIdentifier[0], LiteralNode:create(LiteralNode,"TO", " ", "to"), strippedValue[0], LiteralNode:create(LiteralNode,"EOI", "", "."))).
    return DeclareStmtScopedDeclarationNode:new(DeclareStmtScopedDeclarationNode,list(prefix, definition)).}.
return  result.}):call().
local  DeclareStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","DeclareStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "Declare".}.
set  result["format"] to { parameter self.
    return list(
      list(
        DeclareParameterClause,
        DeclareFunctionClause,
        DeclareLockClause,
        DeclareStmtScopedDeclarationNode
      )
    ).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:definition to  parts[0].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(
      self:definition
    ).}.
return  result.}):call().
local  FunctionCallNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","FunctionCallNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "FunctionCall".}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:subject to  parts[0].
    set  self:bopen to  parts[1].
    set  self:args to  parts:copy.
    self:args:remove(0).
    self:args:remove(0).
    self:args:remove(self:args:length - 1).
    set  self:bclose to  parts[parts:length - 1].

    set  self:attributes to  lex().
    set  self:attributes:is_object_call to  false.
local  __var50 to  self:subject:type.
    if __var50:name(__var50) = "StructureAccess" {
local  __var51 to  self:subject.
      if __var51:is_object_accessor(__var51)
        set  self:attributes:is_object_call to  true.
    }
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self. local result to  self:args:copy.
    result:insert(0, self:subject).
    result:insert(1, self:bopen).
    result:add(self:bclose).
    return result.}.
set  prototype["desugar"] to { parameter self,context. local call to  self. local precursors to  list().
local  __var52 to  call:subject:type.

    if __var52:name(__var52) = "ExprBreakupPropagation" {
      for stmt in call:subject:statements {
        precursors:add(stmt).
      } local parts to  list(call:subject:inner, call:bopen).
      for a in call:args {
        parts:add(a).
      }
      parts:add(call:bclose).
      set  call to  FunctionCallNode:new(FunctionCallNode,parts).
      set  call:attributes to  self:attributes.
    } local newArgs to  list(). local isModified to  false.
    for arg in call:args {
local  __var53 to  arg:type.
      if __var53:name(__var53) = "ExprBreakupPropagation" {
        set  isModified to  true.

        for stmt in arg:statements {
          precursors:add(stmt).
        }
        newArgs:add(arg:inner).
      } else {
        newArgs:add(arg).
      }
    }
    if isModified { local parts to  list(call:subject, call:bopen).
      for a in newArgs {
        parts:add(a).
      }
      parts:add(call:bclose).
      set  call to  FunctionCallNode:new(FunctionCallNode,parts).
      set  call:attributes to  self:attributes.
    }

    if call:attributes:is_object_call {
local  __var54 to  call:subject:type.
      if __var54:name(__var54) = "StructureAccess" { local shouldCapture to  true. local variable to  call:subject:subject.
local  __var55 to  call:subject:subject:type.

        if __var55:name(__var55) = "Atom" {
          set  shouldCapture to  false.
        }

        if shouldCapture {
          set  variable to  ExprBreakupPropagationNode:gen_variable(ExprBreakupPropagationNode). local capturedDef to  DeclareStmtScopedDeclarationNode:create_local(DeclareStmtScopedDeclarationNode,variable, call:subject:subject).
          precursors:add(capturedDef).
        } local functionSubject to  StructureAccessNode:create(StructureAccessNode,variable, call:subject:suffix). local comma to  LiteralNode:create(LiteralNode,"COMMA", "", ","). local variableStripped to  LiteralNode:replaceWhitespace(LiteralNode,variable, ""). local parts to  list(functionSubject, call:bopen, variableStripped[0]).
        if call:args:length > 0
          parts:add(comma).
        
        for a in call:args {
          parts:add(a).
        }
        parts:add(call:bclose).
        set  call to  FunctionCallNode:new(FunctionCallNode,parts).
        set  call:attributes to  self:attributes.
      }
    }
    if precursors:length = 0 {
      return call.
    }

    precursors:insert(0, call).
    return ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,precursors).}.
set  result["create"] to { parameter self,subject, args. local parts to  list(
      subject,
      LiteralNode:create(LiteralNode,"BRACKETOPEN", "", "(")
    ). local first to  true.
    for a in args {
      if first {
        set  first to  false.
      } else {
        parts:add(LiteralNode:create(LiteralNode,"COMMA", "", ",")).
      }
      parts:add(a).
    }
    parts:add(LiteralNode:create(LiteralNode,"BRACKETCLOSE", "", ")")).
    return FunctionCallNode:new(FunctionCallNode,parts).}.
return  result.}):call().
local  FunctionCallTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","FunctionCallTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "FunctionCallTrailer".}.
set  result["format"] to { parameter self.
    return list(list("BRACKETOPEN"), list(ArgListNode, NoneNode), list("BRACKETCLOSE")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:open to  parts[0].
    set  self:args to  parts[1].
    set  self:close to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:open, self:args, self:close).}.
set  prototype["suffix_simplify"] to { parameter self,subject. local arglist to  list().
local  __var56 to  self:args:type.
    if __var56:name(__var56) <> "None"
{
local  __var57 to  self:args.
      set  arglist to  __var57:to_list(__var57).
}
    
    arglist:insert(0, subject).
    arglist:insert(1, self:open).
    arglist:add(self:close).
    return FunctionCallNode:new(FunctionCallNode,arglist).}.
set  prototype["to_parameters"] to { parameter self.
    parameter selfName to "self". local params to  NoneNode:new(NoneNode,list()).
local  __var58 to  self:args:type.
    if __var58:name(__var58) <> "None" {
local  __var59 to  self:args.
      set  params to  __var59:to_parameter_list(__var59).
    }
    return DeclareParameterClause:new(DeclareParameterClause,list(
      LiteralNode:create(LiteralNode,"PARAMETER", " ", "parameter"),
      LiteralNode:create(LiteralNode,"IDENTIFIER", " ", selfName),
      NoneNode:new(NoneNode,list()),
      params,
      LiteralNode:create(LiteralNode,"EOI", "", ".")
    )).}.
return  result.}):call().
local  FunctionCaptureTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","FunctionCaptureTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "FunctionCaptureTrailer".}.
set  result["format"] to { parameter self.
    return list(list("ATSIGN")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:atsign to  parts[0].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:atsign).}.
return  result.}):call().
local  SuffixtermTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","SuffixtermTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "SuffixtermTrailer".}.
set  result["format"] to { parameter self.
    return list(list(FunctionCallTrailerNode, ArrayTrailerNode, FunctionCaptureTrailerNode), list(SuffixtermTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:action to  parts[0].
    set  self:tail to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:action, self:tail).}.
set  prototype["suffix_simplify"] to { parameter self,subject. local new_subject to  subject.
local  __var60 to  self:action.
    set  new_subject to  __var60:suffix_simplify(__var60,new_subject).
local  __var61 to  self:tail:type.
    if __var61:name(__var61) = "SuffixtermTrailer" {
local  __var62 to  self:tail.
      set  new_subject to  __var62:suffix_simplify(__var62,new_subject).
    }
    return new_subject.}.
return  result.}):call().
local  ObjectConstructionNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ObjectConstructionNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ObjectConstruction".}.
set  result["format"] to { parameter self.
    return list(list("NEW"), list(AtomNode), list("BRACKETOPEN"), list(ArgListNode, NoneNode), list("BRACKETCLOSE")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:newLiteral to  parts[0].
    set  self:typeIdentifier to  parts[1].
    set  self:open to  parts[2].
    set  self:args to  parts[3].
    set  self:close to  parts[4].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["simplify"] to { parameter self,context. local strippedNew to  LiteralNode:replaceWhitespace(LiteralNode,self:newLiteral, ""). local replacedType to  LiteralNode:replaceWhitespace(LiteralNode,self:typeIdentifier, strippedNew[1]). local access to  StructureAccessNode:new(StructureAccessNode,list(replacedType[0], LiteralNode:create(LiteralNode,"ACCESSOR", "", "->"), strippedNew[0])). local parts to  list(access, self:open).
local  __var63 to  self:args:type.
    if __var63:name(__var63) <> "None" {
local  __var64 to  self:args.
      for arg in __var64:to_list(__var64) {
        parts:add(arg).
      }
    }
    parts:add(self:close).
    return FunctionCallNode:new(FunctionCallNode,parts).}.
set  prototype["children"] to { parameter self.
    return list(self:newLiteral, self:typeIdentifier, self:open, self:args, self:close).}.
return  result.}):call().
local  SuffixtermNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","SuffixtermNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "Suffixterm".}.
set  result["format"] to { parameter self.
    return list(list(ObjectConstructionNode, AtomNode), list(SuffixtermTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:subject to  parts[0].
    set  self:tail to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:subject, self:tail).}.
set  prototype["is_function_call"] to { parameter self.
local  __var65 to  self:tail:action:type.
    if __var65:name(__var65) = "FunctionCallTrailerNode"
      return true.
    return false.}.
set  prototype["suffix_simplify"] to { parameter self,subject.
local  __var66 to  self:tail:type.
    if __var66:name(__var66) = "None"
      return subject.
local  __var67 to  self:tail.
return  __var67:suffix_simplify(__var67,subject).}.
return  result.}):call().
local  StructureAccessNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","StructureAccessNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "StructureAccess".}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:subject to  parts[0].
    set  self:colon to  parts[1].
    set  self:suffix to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["is_object_accessor"] to { parameter self.
    return self:colon:token:definition = "ACCESSOR".}.
set  prototype["children"] to { parameter self.
    return list(self:subject, self:colon, self:suffix).}.
set  prototype["desugar"] to { parameter self,context. local result to  self. local propagation to  list().
local  __var68 to  self:subject:type.

    if __var68:name(__var68) = "ExprBreakupPropagation" {
      set  propagation to  self:subject:statements:copy.
      set  result to  StructureAccessNode:new(StructureAccessNode,list(self:subject:inner, self:colon, self:suffix)).
    }

    if self:is_object_accessor(self) { local colon to  LiteralNode:create(LiteralNode,"COLON", "", ":").
      set  result to  StructureAccessNode:new(StructureAccessNode,list(result:subject, colon, result:suffix)).
    }

    if propagation:length = 0
      return result.

    propagation:insert(0, result).
    return ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,propagation).}.
set  result["create"] to { parameter self,subject, suffix.
    return StructureAccessNode:new(StructureAccessNode,list(
      subject,
      LiteralNode:create(LiteralNode,"COLON", "", ":"),
      suffix
    )).}.
return  result.}):call().
local  SuffixTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","SuffixTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "SuffixTrailer".}.
set  result["format"] to { parameter self.
    return list(list("COLON", "ACCESSOR"), list(SuffixtermNode), list(SuffixTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:colon to  parts[0].
    set  self:suffix to  parts[1].
    set  self:tail to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:colon, self:suffix, self:tail).}.
set  prototype["suffix_simplify"] to { parameter self,subject. local new_subject to  StructureAccessNode:new(StructureAccessNode,list(subject, self:colon, self:suffix:subject)).
local  __var69 to  self:suffix.
    set  new_subject to  __var69:suffix_simplify(__var69,new_subject).
local  __var70 to  self:tail:type.
    if __var70:name(__var70) <> "None" {
local  __var71 to  self:tail.
      set  new_subject to  __var71:suffix_simplify(__var71,new_subject).
    }
    return new_subject.}.
return  result.}):call().
local  SuffixNormalNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","SuffixNormalNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "SuffixNormal".}.
set  result["format"] to { parameter self.
    return list(list(SuffixtermNode), list(SuffixTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:term to  parts[0].
    set  self:trailer to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["simplify"] to { parameter self,context.
local  __var72 to  self:term. local subject to  __var72:suffix_simplify(__var72,self:term:subject).
local  __var73 to  self:trailer:type.
    if __var73:name(__var73) <> "None" {
local  __var74 to  self:trailer.
      set  subject to  __var74:suffix_simplify(__var74,subject).
    }
    return subject.}.
set  prototype["children"] to { parameter self.
    return list(self:term, self:trailer).}.
return  result.}):call().
local  AsyncResolveStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","AsyncResolveStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "AsyncResolveStmt".}.
set  result["format"] to { parameter self.
    return list(list("VAR"), VarIdentifiers, list("ASSIGNMENT"), list("AWAIT"), VarIdentifiers, list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:varLiteral to  parts[0].
    set  self:identifier to  parts[1].
    set  self:assignment to  parts[2].
    set  self:awaitStmt to  parts[3].
    set  self:promiseVar to  parts[4].
    set  self:eoi to  parts[5].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:varLiteral, self:identifier, self:assignment, self:awaitStmt, self:promiseVar, self:eoi).}.
return  result.}):call().
local  VarDeclarationStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","VarDeclarationStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "VarDeclarationStmt".}.
set  result["format"] to { parameter self.
    return list(list("VAR"), VarIdentifiers, list("ASSIGNMENT"), list(ExprNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:varLiteral to  parts[0].
    set  self:identifier to  parts[1].
    set  self:assignment to  parts[2].
    set  self:value to  parts[3].
    set  self:eoi to  parts[4].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:varLiteral, self:identifier, self:assignment, self:value, self:eoi).}.
set  prototype["as_kos"] to { parameter self,value. local varWhitespace to  LiteralNode:replaceWhitespace(LiteralNode,self:varLiteral, ""). local localToken to  LiteralNode:create(LiteralNode,"LOCAL", varWhitespace[1], "local"). local toToken to  LiteralNode:create(LiteralNode,"TO", " ", "to "). local scopeNode to  DeclareStmtScopeNode:new(DeclareStmtScopeNode,list(localToken)). local strippedIdentifier to  LiteralNode:replaceWhitespace(LiteralNode,self:identifier, " "). local strippedValue to  LiteralNode:replaceWhitespace(LiteralNode,value, " "). local identifierDeclarationNode to  DeclareIdentifierClause:new(DeclareIdentifierClause,list(strippedIdentifier[0], toToken, strippedValue[0], self:eoi)).
    return DeclareStmtScopedDeclarationNode:new(DeclareStmtScopedDeclarationNode,list(scopeNode, identifierDeclarationNode)).}.
set  prototype["desugar"] to { parameter self,context.
local  __var75 to  self:value:type.
    if __var75:name(__var75) = "ExprBreakupPropagation" { local inner to  false.
local  __var76 to  self:value:inner:type.
      if __var76:name(__var76) = "SuffixAwait" {
        set  inner to  AsyncResolveStmtNode:new(AsyncResolveStmtNode,list(self:varLiteral, self:identifier, self:assignment, self:value:inner:asyncStmt, self:value:inner:suffix, self:eoi)).
      } else {
        set  inner to  self:as_kos(self,self:value:inner).
      }
local  __var77 to  self:value.
return  __var77:to_block(__var77,inner).
    } else {
      return self:as_kos(self,self:value).
    }}.
return  result.}):call().
local  SuffixAwaitNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","SuffixAwaitNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "SuffixAwait".}.
set  result["format"] to { parameter self.
    return list(list("AWAIT"), list(SuffixNormalNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:asyncStmt to  parts[0].
    set  self:suffix to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:asyncStmt, self:suffix).}.
set  prototype["desugar"] to { parameter self,context.
local  __var78 to  self:suffix:type.
    if __var78:name(__var78) = "LiteralNode"
      return self. local spaceToken to  LexerToken:new(LexerToken,"SPACE", " ", "<builtin>", -1, -1, true). local newLineToken to  LexerToken:new(LexerToken,"NEWLINE", char(10), "<builtin>", -1, -1, true). local varToken to  LexerToken:new(LexerToken,"VAR", "var", "<builtin>", -1, -1, false). local equalsToken to  LexerToken:new(LexerToken,"EQUALS", "=", "<builtin>", -1, -1, false). local eoiToken to  LexerToken:new(LexerToken,"EOI", ";", "<builtin>", -1, -1, false). local varname to  ExprBreakupPropagationNode:gen_variable(ExprBreakupPropagationNode). local setStmt to  VarDeclarationStmtNode:new(VarDeclarationStmtNode,list(
      LiteralNode:new(LiteralNode,varToken, list(newLineToken)),
      varname,
      LiteralNode:new(LiteralNode,equalsToken, list(spaceToken)),
      self:suffix,
      LiteralNode:new(LiteralNode,eoiToken, list())
    )). local inner to  SuffixAwaitNode:new(SuffixAwaitNode,list(self:asyncStmt, varname)).
    return ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,list(inner, setStmt)).}.
return  result.}):call().
local  SuffixNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","SuffixNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "Suffix".}.
set  result["format"] to { parameter self.
    return list(list(SuffixAwaitNode, SuffixNormalNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:inner to  parts[0].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["simplify"] to { parameter self,context.
    return self:inner.}.
set  prototype["children"] to { parameter self.
    return list(self:inner).}.
return  result.}):call().
local  SetStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","SetStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "SetStmt".}.
set  result["format"] to { parameter self.
    return list(list("SET"), list(SuffixNode), list("TO"), list(ExprNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:set to  parts[0].
    set  self:lvalue to  parts[1].
    set  self:to to  parts[2].
    set  self:expr to  parts[3].
    set  self:eoi to  parts[4].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:set, self:lvalue, self:to, self:expr, self:eoi).}.
set  result["create"] to { parameter self,subject, value.
    return SetStmtNode:new(SetStmtNode,list(
      LiteralNode:create(LiteralNode,"SET", char(10), "set "),
      subject,
      LiteralNode:create(LiteralNode,"TO", " ", "to "),
      value,
      LiteralNode:create(LiteralNode,"EOI", "", ".")
    )).}.
return  result.}):call().
local  IfElseTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","IfElseTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "IfElseTrailerStmt".}.
set  result["format"] to { parameter self.
    return list(list("ELSE"), list(InstructionNode), list("EOI", NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:else to  parts[0].
    set  self:instruction to  parts[1].
    set  self:eoi to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:else, self:instruction, self:eoi).}.
return  result.}):call().
local  IfStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","IfStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "IfStmt".}.
set  result["format"] to { parameter self.
    return list(list("IF"), list(ExprNode), list(InstructionNode), list("EOI", NoneNode), list(IfElseTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:if to  parts[0].
    set  self:expr to  parts[1].
    set  self:instruction to  parts[2].
    set  self:eoi to  parts[3].
    set  self:else to  parts[4].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:if, self:expr, self:instruction, self:eoi, self:else).}.
set  prototype["desugar"] to { parameter self,context. local arg to  self:expr. local result to  self. local propagation to  list().
local  __var79 to  arg:type.

    if __var79:name(__var79) = "ExprBreakupPropagation" {
      set  propagation to  arg:statements:copy.
      set  arg to  arg:inner.
      set  result to  IfStmtNode:new(IfStmtNode,list(self:if, arg, self:instruction, self:eoi, self:else)).
    }
local  __var80 to  self:instruction:type.

    if __var80:name(__var80) = "NonSingleStatementGuard" { local c_open to  LiteralNode:create(LiteralNode,"CURLYOPEN", char(10), "{"). local c_close to  LiteralNode:create(LiteralNode,"CURLYCLOSE", char(10), "}"). local block to  InstructionBlockStmtNode:new(InstructionBlockStmtNode,list(c_open, self:instruction:instructions, c_close)).
      set  result to  IfStmtNode:new(IfStmtNode,list(self:if, arg, block, self:eoi, self:else)).
    }

    if propagation:length = 0
      return result.
    
    propagation:insert(0, result). local breakup to  ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,propagation).

    return breakup:to_block(breakup,result).}.
set  result["create"] to { parameter self,expr, instruction.
    return IfStmtNode:new(IfStmtNode,list(
      LiteralNode:create(LiteralNode,"IF", char(10), "if"),
      expr,
      instruction,
      NoneNode:new(NoneNode,list()),
      NoneNode:new(NoneNode,list())
    )).}.
return  result.}):call().
local  UntilStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","UntilStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "UntilStmt".}.
set  result["format"] to { parameter self.
    return list(list("UNTIL"), list(ExprNode), list(InstructionNode), list("EOI", NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:until to  parts[0].
    set  self:expr to  parts[1].
    set  self:instruction to  parts[2].
    set  self:eoi to  parts[3].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:until, self:expr, self:instruction, self:eoi).}.
return  result.}):call().
local  FromLoopStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","FromLoopStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "FromLoopStmt".}.
set  result["format"] to { parameter self.
    return list(
      list("FROM"),
      list(InstructionBlockStmtNode),
      list("UNTIL"),
      list(ExprNode),
      list("STEP"),
      list(InstructionBlockStmtNode),
      list("DO"),
      list(InstructionNode),
      list("EOI", NoneNode)
    ).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:from to  parts[0].
    set  self:setup to  parts[1].
    set  self:untilLiteral to  parts[2].
    set  self:until to  parts[3].
    set  self:stepLiteral to  parts[4].
    set  self:step to  parts[5].
    set  self:doLiteral to  parts[6].
    set  self:do to  parts[7].
    set  self:eoi to  parts[8].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(
      self:from,
      self:setup,
      self:untilLiteral,
      self:until,
      self:stepLiteral,
      self:step,
      self:doLiteral,
      self:do,
      self:eoi
    ).}.
return  result.}):call().
local  UnlockStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","UnlockStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "UnlockStmt".}.
set  result["format"] to { parameter self.
    return list(list("UNLOCK"), VarIdentifiers, list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:unlock to  parts[0].
    set  self:identifier to  parts[1].
    set  self:eoi to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:unlock, self:identifier, self:eoi).}.
return  result.}):call().
local  PrintPositionNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","PrintPositionNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "PrintPosition".}.
set  result["format"] to { parameter self.
    return list(
      list("AT"),
      list("BRACKETOPEN"),
      list(ExprNode),
      list("COMMA"),
      list(exprNode),
      list("BRACKETCLOSE")
    ).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:at to  parts[0].
    set  self:bopen to  parts[1].
    set  self:column to  parts[2].
    set  self:comma to  parts[3].
    set  self:line to  parts[4].
    set  self:bclose to  parts[5].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:at, self:bopen, self:column, self:comma, self:line, self:bclose).}.
return  result.}):call().
local  PrintStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","PrintStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "PrintStmt".}.
set  result["format"] to { parameter self.
    return list(list("PRINT"), list(ExprNode), list(PrintPositionNode, NoneNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:print to  parts[0].
    set  self:expr to  parts[1].
    set  self:position to  parts[2].
    set  self:eoi to  parts[3].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:print, self:expr, self:position, self:eoi).}.
return  result.}):call().
local  OnStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","OnStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "OnStmt".}.
set  result["format"] to { parameter self.
    return list(list("ON"), list("VARIDENTIFIER"), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:on to  parts[0].
    set  self:identifier to  parts[1].
    set  self:eoi to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:on, self:identifier, self:eoi).}.
return  result.}):call().
local  ToggleStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ToggleStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ToggleStmt".}.
set  result["format"] to { parameter self.
    return list(list("TOGGLE"), list("VARIDENTIFIER"), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:toggle to  parts[0].
    set  self:identifier to  parts[1].
    set  self:eoi to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:toggle, self:identifier, self:eoi).}.
return  result.}):call().
local  WaitStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","WaitStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "WaitStmt".}.
set  result["format"] to { parameter self.
    return list(list("WAIT"), list("UNTIL", noneNode), list(exprNode), list("EOI", noneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:wait to  parts[0].
    set  self:until to  parts[1].
    set  self:expr to  parts[2].
    set  self:eoi to  parts[3].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(
      self:wait,
      self:until,
      self:expr,
      self:eoi
    ).}.
return  result.}):call().
local  WhenStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","WhenStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "WhenStmt".}.
set  result["format"] to { parameter self.
    return list(list("WHEN"), list(exprNode), list("THEN"), list(instructionNode), list("EOI", noneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:when to  parts[0].
    set  self:expr to  parts[1].
    set  self:then to  parts[2].
    set  self:instruction to  parts[3].
    set  self:eoi to  parts[4].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(
      self:when,
      self:expr,
      self:then,
      self:instruction,
      self:eoi
    ).}.
return  result.}):call().
local  StageStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","StageStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "StageStmt".}.
set  result["format"] to { parameter self.
    return list(list("STAGE"), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:stage to  parts[0].
    set  self:eoi to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:stage, self:eoi).}.
return  result.}):call().
local  ClearscreenStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ClearscreenStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ClearscreenStmt".}.
set  result["format"] to { parameter self.
    return list(list("CLEARSCREEN"), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:clearscreen to  parts[0].
    set  self:eoi to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:clearscreen, self:eoi).}.
return  result.}):call().
local  AddStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","AddStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "AddStmt".}.
set  result["format"] to { parameter self.
    return list(list("ADD"), list(ExprNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:addStmt to  parts[0].
    set  self:expr to  parts[1].
    set  self:eoi to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:addStmt, self:expr, self:eoi).}.
return  result.}):call().
local  RemoveStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","RemoveStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "RemoveStmt".}.
set  result["format"] to { parameter self.
    return list(list("REMOVE"), list(ExprNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:removeStmt to  parts[0].
    set  self:expr to  parts[1].
    set  self:eoi to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:removeStmt, self:expr, self:eoi).}.
return  result.}):call().
local  LogStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","LogStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "LogStmt".}.
set  result["format"] to { parameter self.
    return list(list("LOG"), list(ExprNode), list("TO"), list(ExprNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:logToken to  parts[0].
    set  self:value to  parts[1].
    set  self:toToken to  parts[2].
    set  self:filename to  parts[3].
    set  self:eoi to  parts[4].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:logToken, self:value, self:toToken, self:filename, self:eoi).}.
return  result.}):call().
local  BreakStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","BreakStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "BreakStmt".}.
set  result["format"] to { parameter self.
    return list(list("BREAK"), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:breakToken to  parts[0].
    set  self:eoi to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:breakToken, self:eoi).}.
return  result.}):call().
local  PreserveStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","PreserveStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "PreserveStmt".}.
set  result["format"] to { parameter self.
    return list(list("PRESERVE"), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:preserveToken to  parts[0].
    set  self:eoi to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:preserveToken, self:eoi).}.
return  result.}):call().
local  ReturnStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ReturnStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ReturnStmt".}.
set  result["format"] to { parameter self.
    return list(list("RETURN"), list(ExprNode, NoneNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:returnToken to  parts[0].
    set  self:result to  parts[1].
    set  self:eoi to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:returnToken, self:result, self:eoi).}.
set  prototype["desugar"] to { parameter self,context.
local  __var81 to  self:result:type.
    if __var81:name(__var81) <> "ExprBreakupPropagation" {
      return self.
    }
local  __var82 to  self:result.
return  __var82:to_block(__var82,ReturnStmtNode:create(ReturnStmtNode,self:result:inner)).}.
set  result["create"] to { parameter self,subject. local returnToken to  LiteralNode:create(LiteralNode,"RETURN", char(10), "return "). local eoiToken to  LiteralNode:create(LiteralNode,"EOI", "", ".").
    return ReturnStmtNode:new(ReturnStmtNode,list(returnToken, subject, eoiToken)).}.
return  result.}):call().
local  SwitchStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","SwitchStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "SwitchStmt".}.
set  result["format"] to { parameter self.
    return list(list("SWITCH"), list("TO"), list(ExprNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:switchToken to  parts[0].
    set  self:toToken to  parts[1].
    set  self:identifier to  parts[2].
    set  self:eoi to  parts[3].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:switchToken, self:toToken, self:identifier, self:eoi).}.
return  result.}):call().
local  CopyStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","CopyStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "CopyStmt".}.
set  result["format"] to { parameter self.
    return list(list("COPY"), list(ExprNode), list("FROM", "TO"), list(ExprNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:copyToken to  parts[0].
    set  self:sourceIdentifier to  parts[1].
    set  self:fromToIdentifier to  parts[2].
    set  self:destIdentifier to  parts[4].
    set  self:eoi to  parts[5].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:copyToken, self:sourceIdentifier, self:fromToIdentifier, self:destIdentifier, self:eoi).}.
return  result.}):call().
local  RenameStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","RenameStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "RenameStmt".}.
set  result["format"] to { parameter self.
    return list(list("RENAME"), list("VOLUME", "FILE", NoneNode), list(ExprNode), list("TO"), list(ExprNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:renameToken to  parts[0].
    set  self:filter to  parts[1].
    set  self:fromIdentifier to  parts[2].
    set  self:toToken to  parts[3].
    set  self:destIdentifier to  parts[4].
    set  self:eoi to  parts[5].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:renameToken, self:filter, self:fromIdentifier, self:toToken, self:destIdentifier, self:eoi).}.
return  result.}):call().
local  DeleteTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","DeleteTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "DeleteTrailer".}.
set  result["format"] to { parameter self.
    return list(list("FROM"), list(ExprNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:fromToken to  parts[0].
    set  self:expr to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:fromToken, self:expr).}.
return  result.}):call().
local  DeleteStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","DeleteStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "DeleteStmt".}.
set  result["format"] to { parameter self.
    return list(list("DELETE"), list(ExprNode), list(DeleteTrailerNode, NoneNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:deleteToken to  parts[0].
    set  self:expr to  parts[1].
    set  self:from to  parts[2].
    set  self:eoi to  parts[3].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:compileToken, self:expr, self:from, self:eoi).}.
return  result.}):call().
local  EditStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","EditStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "EditStmt".}.
set  result["format"] to { parameter self.
    return list(list("EDIT"), list(ExprNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:editToken to  parts[0].
    set  self:expr to  parts[1].
    set  self:eoi to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:editToken, self:expr, self:eoi).}.
return  result.}):call().
local  RunArgListNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","RunArgListNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "RunArgList".}.
set  result["format"] to { parameter self.
    return list(list("BRACKETOPEN"), list(ArgListNode), list("BRACKETCLOSE")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:bopen to  parts[0].
    set  self:arglist to  parts[1].
    set  self:bclose to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:bopen, self:arglist, self:bclose).}.
return  result.}):call().
local  RunStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","RunStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "RunPathStmt".}.
set  result["format"] to { parameter self.
    return list(list("RUN"), list("ONCE", NoneNode), list("FILEIDENT", "STRING"), list(RunArgListNode, NoneNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:runToken to  parts[0].
    set  self:onceToken to  parts[1].
    set  self:identifier to  parts[2].
    set  self:arglist to  parts[3].
    set  self:eoi to  parts[4].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:runToken, self:onceToken, self:identifier, self:arglist, self:eoi).}.
return  result.}):call().
local  RunPathTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","RunPathTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "RunPathTrailer".}.
set  result["format"] to { parameter self.
    return list(list("COMMA"), list(ArgListNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:comma to  parts[0].
    set  self:arglist to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:comma, self:arglist).}.
return  result.}):call().
local  RunPathStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","RunPathStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "RunPathStmt".}.
set  result["format"] to { parameter self.
    return list(list("RUNPATH", "RUNONCEPATH"), list("BRACKETOPEN"), list(ExprNode), list(RunPathTrailerNode, NoneNode), list("BRACKETCLOSE"), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:runPathToken to  parts[0].
    set  self:bopen to  parts[1].
    set  self:expr to  parts[2].
    set  self:args to  parts[3].
    set  self:bclose to  parts[4].
    set  self:eoi to  parts[5].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:runPathToken, self:bopen, self:expr, self:args, self:bclose, self:eoi).}.
return  result.}):call().
local  CompileTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","CompileTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "CompileTrailer".}.
set  result["format"] to { parameter self.
    return list(list("TO"), list(ExprNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:toToken to  parts[0].
    set  self:expr to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:toToken, self:expr).}.
return  result.}):call().
local  CompileStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","CompileStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "CompileStmt".}.
set  result["format"] to { parameter self.
    return list(list("COMPILE"), list(ExprNode), list(CompileTrailerNode, NoneNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:compileToken to  parts[0].
    set  self:expr to  parts[1].
    set  self:to to  parts[2].
    set  self:eoi to  parts[3].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:compileToken, self:expr, self:to, self:eoi).}.
return  result.}):call().
local  ListDestinationTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ListDestinationTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ListDestinationTrailer".}.
set  result["format"] to { parameter self.
    return list(list("IN"), list("IDENTIFIER")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:inToken to  parts[0].
    set  self:identifier to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:inToken, self:identifier).}.
return  result.}):call().
local  ListIdentifierTrailerNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ListIdentifierTrailerNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ListIdentifierTrailer".}.
set  result["format"] to { parameter self.
    return list(list("IDENTIFIER"), list(ListDestinationTrailerNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:identifier to  parts[0].
    set  self:trailer to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:identifier, self:trailer).}.
return  result.}):call().
local  ListStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ListStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ListStmt".}.
set  result["format"] to { parameter self.
    return list(list("LIST"), list(ListIdentifierTrailerNode, NoneNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:listToken to  parts[0].
    set  self:trailer to  parts[1].
    set  self:eof to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:listToken, self:trailer, self:eof).}.
return  result.}):call().
local  RebootStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","RebootStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "RebootStmt".}.
set  result["format"] to { parameter self.
    return list(list("REBOOT"), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:rebootToken to  parts[0].
    set  self:eoi to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:rebootToken, self:eoi).}.
return  result.}):call().
local  ShutdownStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ShutdownStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ShutdownStmt".}.
set  result["format"] to { parameter self.
    return list(list("SHUTDOWN"), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:shutdownToken to  parts[0].
    set  self:eoi to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:shutdownToken, self:eoi).}.
return  result.}):call().
local  ForStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ForStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ForStmt".}.
set  result["format"] to { parameter self.
    return list(list("FOR"), VarIdentifiers, list("IN"), list(SuffixNode), list(InstructionNode), list("EOI", NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:forToken to  parts[0].
    set  self:identifier to  parts[1].
    set  self:inToken to  parts[2].
    set  self:collection to  parts[3].
    set  self:instruction to  parts[4].
    set  self:eoi to  parts[5].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:forToken, self:identifier, self:inToken, self:collection, self:instruction, self:eoi).}.
set  result["create"] to { parameter self,identifier, collection, instruction.
    return ForStmtNode:new(ForStmtNode,list(
      LiteralNode:create(LiteralNode,"FOR", char(10), "for "),
      identifier,
      LiteralNode:create(LiteralNode,"IN", " ", "in "),
      collection,
      instruction,
      NoneNode:new(NoneNode,list())
    )).}.
set  prototype["desugar"] to { parameter self,context. local arg to  self:collection. local result to  self. local propagation to  list().
local  __var83 to  arg:type.

    if __var83:name(__var83) = "ExprBreakupPropagation" {
      set  propagation to  arg:statements:copy.
      set  arg to  arg:inner.
      set  result to  ForStmtNode:new(ForStmtNode,list(self:forToken, self:identifier, self:inToken, arg, self:instruction, self:eoi)).
    }
local  __var84 to  self:instruction:type.

    if __var84:name(__var84) = "NonSingleStatementGuard" { local c_open to  LiteralNode:create(LiteralNode,"CURLYOPEN", char(10), "{"). local c_close to  LiteralNode:create(LiteralNode,"CURLYCLOSE", char(10), "}"). local block to  InstructionBlockStmtNode:new(InstructionBlockStmtNode,list(c_open, self:instruction:instructions, c_close)).
      set  result to  ForStmtNode:new(ForStmtNode,list(self:forToken, self:identifier, self:inToken, arg, block, self:eoi)).
    }

    if propagation:length = 0
      return result.
    
    propagation:insert(0, result). local breakup to  ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,propagation).

    return breakup:to_block(breakup,result).}.
return  result.}):call().
local  UnsetStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","UnsetStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "UnsetStmt".}.
set  result["format"] to { parameter self.
    return list(list("UNSET"), list("IDENTIFIER", "ALL"), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:unsetToken to  parts[0].
    set  self:identifier to  parts[1].
    set  self:eoi to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:unsetToken, self:identifier, self:eoi).}.
return  result.}):call().
local  ClassInitMemberNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ClassInitMemberNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ClassInitMember".}.
set  result["format"] to { parameter self.
    return list(list("INIT"), list(FunctionCallTrailerNode), list("ASYNC", NoneNode), list("CURLYOPEN"), list(InstructionListNode, NoneNode), list("CURLYCLOSE")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:init to  parts[0].
    set  self:args to  parts[1].
    set  self:asyncStmt to  parts[2].
    set  self:open to  parts[3].
    set  self:instructions to  parts[4].
    set  self:close to  parts[5].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:init, self:args, self:asyncStmt, self:open, self:instructions, self:close).}.
set  prototype["to_instructions"] to { parameter self,instructions, resultIdentifier, prototypeIdentifier. local legacyIdentifier to  char(34) + "__new" + char(34). local stringIdentifier to  char(34) + "new" + char(34). local typeIdentifier to  char(34) + "type" + char(34). local resultEntry to  ArrayAccessNode:new(ArrayAccessNode,list(
      resultIdentifier,
      LiteralNode:create(LiteralNode,"SQUAREOPEN", "", "["),
      LiteralNode:create(LiteralNode,"STRING", "", stringIdentifier),
      LiteralNode:create(LiteralNode,"SQUARECLOSE", "", "]")
    )). local legacyEntry to  ArrayAccessNode:new(ArrayAccessNode,list(
      resultIdentifier,
      LiteralNode:create(LiteralNode,"SQUAREOPEN", "", "["),
      LiteralNode:create(LiteralNode,"STRING", "", legacyIdentifier),
      LiteralNode:create(LiteralNode,"SQUARECLOSE", "", "]")
    )). local legacyStmt to  SetStmtNode:create(SetStmtNode,legacyEntry, resultEntry).
    set  instructions to  InstructionListNode:new(InstructionListNode,list(legacyStmt, instructions)).
local  __var85 to  self:args. local parameters to  __var85:to_parameters(__var85,"type"). local selfIdentifier to  LiteralNode:create(LiteralNode,"IDENTIFIER", " ", "self"). local returnStmt to  ReturnStmtNode:create(ReturnStmtNode,selfIdentifier). local innerInstructions to  false.
local  __var86 to  self:instructions:type.
    if __var86:name(__var86) = "InstructionList" {
local  __var87 to  self:instructions.
      set  innerInstructions to  __var87:append(__var87,list(returnStmt)).
    } else {
      set  innerInstructions to  InstructionListNode:new(InstructionListNode,list(self:instructions, returnStmt)).
    } local selfcopyStmt to  DeclareStmtScopedDeclarationNode:create_local(DeclareStmtScopedDeclarationNode,selfIdentifier, StructureAccessNode:new(StructureAccessNode,list(
      prototypeIdentifier,
      LiteralNode:create(LiteralNode,"COLON", "", ":"),
      LiteralNode:create(LiteralNode,"IDENTIFIER", "", "copy")
    ))). local typeEntry to  ArrayAccessNode:create(ArrayAccessNode,selfIdentifier, LiteralNode:create(LiteralNode,"STRING", "", typeIdentifier)). local typeStmt to  SetStmtNode:create(SetStmtNode,typeEntry, resultIdentifier).
    set  innerInstructions to  InstructionListNode:new(InstructionListNode,list(typeStmt, innerInstructions)).
    set  innerInstructions to  InstructionListNode:new(InstructionListNode,list(selfcopyStmt, innerInstructions)).
    set  innerInstructions to  InstructionListNode:new(InstructionListNode,list(parameters, innerInstructions)). local functionDef to  InstructionBlockStmtNode:new(InstructionBlockStmtNode,list(
      LiteralNode:create(LiteralNode,"BRACKETOPEN", "", "{"),
      innerInstructions,
      LiteralNode:create(LiteralNode,"BRACKETCLOSE", "", "}")
    )). local setStmt to  SetStmtNode:create(SetStmtNode,resultEntry, functionDef).

    set  instructions to  InstructionListNode:new(InstructionListNode,list(setStmt, instructions)).
    return instructions.}.
return  result.}):call().
local  ClassFunctionStaticMemberIdentifierNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ClassFunctionStaticMemberIdentifierNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ClassFunctionStaticMemberIdentifier".}.
set  result["format"] to { parameter self.
    return list(list("SELF"), list("COLON"), VarIdentifiers).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:selfToken to  parts[0].
    set  self:colonToken to  parts[1].
    set  self:identifier to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:selfToken, self:colonToken, self:identifier).}.
return  result.}):call().
local  ClassFunctionMemberNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ClassFunctionMemberNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ClassFunctionMember".}.
set  result["format"] to { parameter self. local allowedIdentifiers to  VarIdentifiers:copy.
    allowedIdentifiers:insert(0, ClassFunctionStaticMemberIdentifierNode).
    return list(list("FUNC"), allowedIdentifiers, list(FunctionCallTrailerNode), list("CURLYOPEN"), list(InstructionListNode, NoneNode), list("CURLYCLOSE")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:func to  parts[0].
    set  self:identifier to  parts[1].
    set  self:args to  parts[2].
    set  self:open to  parts[3].
    set  self:instructions to  parts[4].
    set  self:close to  parts[5].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:func, self:identifier, self:args, self:open, self:instructions, self:close).}.
set  prototype["to_instructions"] to { parameter self,instructions, resultIdentifier, prototypeIdentifier. local destination to  prototypeIdentifier. local identifier to  self:identifier.
local  __var88 to  self:identifier:type.
    if __var88:name(__var88) = "ClassFunctionStaticMemberIdentifier" {
      set  destination to  resultIdentifier.
      set  identifier to  self:identifier:identifier.
    } local stringIdentifier to  char(34) + identifier:token:text + char(34). local prototypeEntry to  ArrayAccessNode:new(ArrayAccessNode,list(
      destination,
      LiteralNode:create(LiteralNode,"SQUAREOPEN", "", "["),
      LiteralNode:create(LiteralNode,"STRING", "", stringIdentifier),
      LiteralNode:create(LiteralNode,"SQUARECLOSE", "", "]")
    )).
local  __var89 to  self:args. local parameters to  __var89:to_parameters(__var89). local functionDef to  InstructionBlockStmtNode:new(InstructionBlockStmtNode,list(
      LiteralNode:create(LiteralNode,"BRACKETOPEN", "", "{"),
      InstructionListNode:new(InstructionListNode,list(parameters, self:instructions)),
      LiteralNode:create(LiteralNode,"BRACKETCLOSE", "", "}")
    )). local setStmt to  SetStmtNode:create(SetStmtNode,prototypeEntry, functionDef).

    set  instructions to  InstructionListNode:new(InstructionListNode,list(setStmt, instructions)).
    return instructions.}.
return  result.}):call().
local  ClassMixNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ClassMixNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ClassMix".}.
set  result["format"] to { parameter self.
    return list(list("MIX"), VarIdentifiers).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:mixToken to  parts[0].
    set  self:identifier to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:mixToken, self:identifier).}.
set  prototype["to_instructions"] to { parameter self,instructions, resultIdentifier, prototypeIdentifier. local sourceIdentifier to  StructureAccessNode:create(StructureAccessNode,self:identifier, LiteralNode:create(LiteralNode,"IDENTIFIER", "", "prototype")). local destIdentifier to  prototypeIdentifier. local keyIdentifier to  LiteralNode:create(LiteralNode,"IDENTIFIER", "", "k"). local innerCopy to  SetStmtNode:create(SetStmtNode,ArrayAccessNode:create(ArrayAccessNode,destIdentifier, keyIdentifier), ArrayAccessNode:create(ArrayAccessNode,sourceIdentifier, keyIdentifier)). local underscoreString to  LiteralNode:create(LiteralNode,"STRING", "", char(34)+"__"+char(34)). local startsWithCall to  FunctionCallNode:create(FunctionCallNode,StructureAccessNode:create(StructureAccessNode,keyIdentifier, LiteralNode:create(LiteralNode,"IDENTIFIER", "", "startsWith")), list(underscoreString)). local negatedStartsWith to  UnaryExprInnerNode:new(UnaryExprInnerNode,list(LiteralNode:create(LiteralNode,"NOT", " ", "not "), startsWithCall)). local ifStmt to  IfStmtNode:create(IfStmtNode,negatedStartsWith, innerCopy). local keysLookup to  StructureAccessNode:create(StructureAccessNode,
      sourceIdentifier,
      LiteralNode:create(LiteralNode,"IDENTIFIER", "", "keys")
    ). local forLoop to  ForStmtNode:create(ForStmtNode,keyIdentifier, keysLookup, ifStmt).

    return InstructionListNode:new(InstructionListNode,list(forLoop, instructions)).}.
return  result.}):call().
local  ClassMemberListNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ClassMemberListNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ClassMemberList".}.
set  result["format"] to { parameter self.
    return list(list(ClassInitMemberNode, ClassFunctionMemberNode, ClassMixNode), list(ClassMemberListNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:member to  parts[0].
    set  self:tail to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:member, self:tail).}.
set  prototype["to_list"] to { parameter self,out.
    out:add(self:member).
local  __var90 to  self:tail:type.
    if __var90:name(__var90) <> "None" {
local  __var91 to 
      self:tail. __var91:to_list(__var91,out).
    }}.
return  result.}):call().
local  ClassDefinitionStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ClassDefinitionStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "ClassDefinitionStmt".}.
set  result["format"] to { parameter self. local allowedIdentifiers to  VarIdentifiers:copy.
    return list(list("CLASS"), allowedIdentifiers, list("CURLYOPEN"), list(ClassMemberListNode, NoneNode), list("CURLYCLOSE")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:class to  parts[0].
    set  self:identifier to  parts[1].
    set  self:open to  parts[2].
    set  self:defs to  parts[3].
    set  self:close to  parts[4].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:class, self:identifier, self:open, self:defs, self:close).}.
set  prototype["members"] to { parameter self.
local  __var92 to  self:defs:type.
    if __var92:name(__var92) = "None" {
      return list().
    } local result to  list().
local  __var93 to 
    self:defs. __var93:to_list(__var93,result).
    return result.}.
set  prototype["desugar"] to { parameter self,context. local resultIdentifier to  LiteralNode:create(LiteralNode,"IDENTIFIER", " ", "result"). local prototypeIdentifier to  LiteralNode:create(LiteralNode,"IDENTIFIER", " ", "prototype"). local instructions to  ReturnStmtNode:create(ReturnStmtNode,resultIdentifier). local hasConstructor to  false. local todo to  self:members(self).

    until todo:length = 0 { local member to  todo[todo:length - 1].
      set  todo to  todo:sublist(0, todo:length - 1).
local  __var94 to  member:type.

      if __var94:name(__var94) = "ClassInitMember"
        set  hasConstructor to  true.
      
      set  instructions to  member:to_instructions(member,instructions, resultIdentifier, prototypeIdentifier).
    }
    if not hasConstructor {
      print(self:identifier).
      crash("TODO: implement support for default initializers").
    } local lexToken to  LiteralNode:create(LiteralNode,"IDENTIFIER", " ", "lex"). local identifierAsString to  LiteralNode:create(LiteralNode,"STRING", "", char(34) + self:identifier:token:text + char(34)). local prototypeVal to  FunctionCallNode:create(FunctionCallNode,lexToken, list(
    )). local resultVal to  FunctionCallNode:create(FunctionCallNode,lexToken, list(
      LiteralNode:create(LiteralNode,"STRING", "", char(34) + "prototype" + char(34)),
      prototypeIdentifier,
      LiteralNode:create(LiteralNode,"STRING", "", char(34) + "classname" + char(34)),
      identifierAsString
    )). local prototypeDef to  DeclareStmtScopedDeclarationNode:create_local(DeclareStmtScopedDeclarationNode,prototypeIdentifier, prototypeVal). local resultDef to  DeclareStmtScopedDeclarationNode:create_local(DeclareStmtScopedDeclarationNode,resultIdentifier, resultVal).
    set  instructions to  InstructionListNode:new(InstructionListNode,list(prototypeDef, InstructionListNode:new(InstructionListNode,list(resultDef, instructions)))). local block to  BracketExprAtom:create(BracketExprAtom,InstructionBlockStmtNode:new(InstructionBlockStmtNode,list(
      LiteralNode:create(LiteralNode,"BRACKETOPEN", "", "{"),
      instructions,
      LiteralNode:create(LiteralNode,"BRACKETCLOSE", "", "}")
    ))). local evalStmt to  FunctionCallNode:create(FunctionCallNode,
      StructureAccessNode:new(StructureAccessNode,list(
        block,
        LiteralNode:create(LiteralNode,"COLON", "", ":"),
        LiteralNode:create(LiteralNode,"IDENTIFIER", "", "call")
      )),
      list()
    ).
    return DeclareStmtScopedDeclarationNode:create_local(DeclareStmtScopedDeclarationNode,self:identifier, evalStmt).}.
return  result.}):call().
local  IdentifierLedExprNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","IdentifierLedExprNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "IdentifierLedExpr".}.
set  result["format"] to { parameter self.
    return list(list("ON", "OFF", NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:tail to  parts[0].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:tail).}.
set  prototype["simplify"] to { parameter self,context. local tailType to  self:tail:type.
    if tailType:name(tailType) = "None" {
      return self:tail.
    }
    return self.}.
return  result.}):call().
local  IdentifierLedAssignmentNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","IdentifierLedAssignmentNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "IdentifierLedAssignment".}.
set  result["format"] to { parameter self.
    return list(list("ASSIGNMENT"), list(ExprNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:assignment to  parts[0].
    set  self:value to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:assignment, self:value).}.
set  prototype["simplify"] to { parameter self,context.
    return self.}.
return  result.}):call().
local  IdentifierLedStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","IdentifierLedStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "IdentifierLedStmt".}.
set  result["format"] to { parameter self.
    return list(list(SuffixNode), list(IdentifierLedAssignmentNode, IdentifierLedExprNode), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:suffix to  parts[0].
    set  self:expr to  parts[1].
    set  self:eoi to  parts[2].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:suffix, self:expr, self:eoi).}.
set  prototype["to_kos"] to { parameter self,subject, value.
local  __var95 to  self:expr:type.
    if __var95:name(__var95) = "IdentifierLedAssignment" {
      return SetStmtNode:new(SetStmtNode,list(
        LiteralNode:create(LiteralNode,"SET", " ", "set "),
        subject,
        LiteralNode:create(LiteralNode,"TO", " ", "to "),
        value
      )).
    }

    return self.}.
set  prototype["desugar"] to { parameter self,context. local subject to  self:suffix. local propagation to  list().
local  __var96 to  subject:type.

    if __var96:name(__var96) = "ExprBreakupPropagation" {
      for stmt in subject:statements {
        propagation:add(stmt).
      }
      set  subject to  subject:inner.
    }
local  __var97 to  self:expr:type.

    if __var97:name(__var97) = "IdentifierLedAssignment" { local value to  self:expr:value.
local  __var98 to  value:type.
      if __var98:name(__var98) = "ExprBreakupPropagation" {
        for stmt in value:statements {
          propagation:add(stmt).
        }
        set  value to  value:inner.
      }
local  __var99 to  value:type.

      if __var99:name(__var99) = "SuffixAwait" { local varname to  ExprBreakupPropagationNode:gen_variable(ExprBreakupPropagationNode). local var_literal to  LiteralNode:create(LiteralNode,"VAR", char(10), "var"). local assignment_literal to  LiteralNode:create(LiteralNode,"ASSIGNMENT", " ", "="). local inner to  AsyncResolveStmtNode:new(AsyncResolveStmtNode,list(var_literal, varname, assignment_literal, value:asyncStmt, value:suffix, self:eoi)).
        if propagation:length = 0
          return inner. local breakup to  ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,list(inner, propagation)).
        return breakup:to_block(breakup,inner).
      } local replacedSubjectParts to  LiteralNode:replaceWhitespace(LiteralNode,subject, " "). local set_literal to  LiteralNode:create(LiteralNode,"SET", replacedSubjectParts[1], "set "). local to_literal to  LiteralNode:create(LiteralNode,"TO", " ", "to "). local stmt to  SetStmtNode:new(SetStmtNode,list(set_literal, replacedSubjectParts[0], to_literal, value, self:eoi)).

      if propagation:length = 0
        return stmt.

      propagation:insert(0, stmt). local breakup to  ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,propagation).
      return breakup:to_block(breakup,stmt).
    } local expr to  self:expr.
local  __var100 to  expr:type.
    if __var100:name(__var100) = "ExprBreakupPropagation" {
      for stmt in expr:statements {
        propagation:add(stmt).
      }
      set  expr to  expr:inner.
    } local stmt to  IdentifierLedStmtNode:new(IdentifierLedStmtNode,list(subject, expr, self:eoi)).

    if propagation:length = 0
      return stmt.

    propagation:insert(0, stmt). local breakup to  ExprBreakupPropagationNode:new(ExprBreakupPropagationNode,propagation).
    return breakup:to_block(breakup,stmt).}.
return  result.}):call().
local  DirectiveNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","DirectiveNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "Directive".}.
set  result["format"] to { parameter self.
    return list(list("ATSIGN"), list("LAZYGLOBAL"), list("ON", "OFF"), list("EOI")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:atsign to  parts[0].
    set  self:lazyglobal to  parts[1].
    set  self:status to  parts[2].
    set  self:eoi to  parts[3].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:atsign, self:lazyglobal, self:status, self:eoi).}.
return  result.}):call().
local  IncludeStmtNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","IncludeStmtNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "IncludeStmt".}.
set  result["format"] to { parameter self.
    return list(list("INCLUDE"), list("STRING")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:include to  parts[0].
    set  self:target to  parts[1].

    set  self:unexecuted to  true.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:include, self:target).}.
set  prototype["filename"] to { parameter self. local full to  self:target:token:text. local stripped to  full:substring(1, full:length - 2).
    if stripped:startswith("0:") {
      return "/archive/" + stripped:substring(3, stripped:length - 3).
    }
    return stripped.}.
set  prototype["update_state"] to { parameter self,parser.
    set  parser:current:blocked to  true. local filename to  self:filename(self).
    if parser:file_cache:haskey(filename) { local lwindow to  parser:file_cache[filename].
      set  parser:current:blocked to  false.
      if lwindow:istype("Boolean") {
local  __var101 to  parser:current.
        set  parser:current to  __var101:with_fatal(__var101,"Unable to open for inclusion: " + filename).
      } else {
        // check if we already ran inclusion
        if self:unexecuted {
          set  self:unexecuted to  false.
local  __var102 to  parser:current.
          set  parser:current to  __var102:include(__var102,lwindow).
        }
      }
      return.
    }

    parser:readFileFn(filename).}.
set  prototype["simplify"] to { parameter self,context.
    return NoneNode:new(NoneNode,list()).}.
return  result.}):call().
local  InstructionNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","InstructionNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "Instruction".}.
set  result["format"] to { parameter self.
    return list(list(
      EmptyStmtNode,
      SetStmtNode,
      IfStmtNode,
      UntilStmtNode,
      FromLoopStmtNode,
      UnlockStmtNode,
      PrintStmtNode,
      OnStmtNode,
      ToggleStmtNode,
      WaitStmtNode,
      WhenStmtNode,
      StageStmtNode,
      ClearscreenStmtNode,
      AddStmtNode,
      RemoveStmtNode,
      LogStmtNode,
      BreakStmtNode,
      PreserveStmtNode,
      DeclareStmtNode,
      ReturnStmtNode,
      SwitchStmtNode,
      CopyStmtNode,
      RenameStmtNode,
      DeleteStmtNode,
      EditStmtNode,
      RunStmtNode,
      RunPathStmtNode,
      CompileStmtNode,
      ListStmtNode,
      RebootStmtNode,
      ShutdownStmtNode,
      ForStmtNode,
      UnsetStmtNode,
      InstructionBlockStmtNode,
      VarDeclarationStmtNode,
      ClassDefinitionStmtNode,
      IdentifierLedStmtNode,
      DirectiveNode,
      IncludeStmtNode
    )).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:instruction to  parts[0].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["simplify"] to { parameter self,context.
    return self:instruction.}.
set  prototype["children"] to { parameter self.
    return list(self:instruction).}.
return  result.}):call().
local  InstructionListNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","InstructionListNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "InstructionList".}.
set  result["format"] to { parameter self.
    return list(list(InstructionNode), list(InstructionListNode, NoneNode)).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:instruction to  parts[0].
    set  self:tail to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["simplify"] to { parameter self,context. local tailType to  self:tail:type.
    if tailType:name(tailType) = "None" {
      return self:instruction.
    }
    return self.}.
set  prototype["children"] to { parameter self.
    return list(self:instruction, self:tail).}.
set  prototype["append"] to { parameter self,instruction_list.
local  __var103 to  self:tail:type. local tailType to  __var103:name(__var103).
    if tailType <> "InstructionList" {
      if instruction_list:length = 0
        return self:copy. local last to  instruction_list[instruction_list:length - 1]. local rest to  instruction_list:sublist(0, instruction_list:length - 1).
      
      until rest:length = 0 { local current to  rest[rest:length - 1].
        set  rest to  rest:sublist(0, rest:length - 1).
        set  last to  InstructionListNode:new(InstructionListNode,list(current, last)).
      }

      if tailType <> "None" {
        set  last to  InstructionListNode:new(InstructionListNode,list(self:tail, last)).
      }

      return InstructionListNode:new(InstructionListNode,list(self:instruction, last)).
    } else {
local  __var104 to  self:tail. local tail to  __var104:append(__var104,instruction_list).
      return InstructionListNode:new(InstructionListNode,list(self:instruction, tail)).
    }}.
return  result.}):call().
local  ProgramNode to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","ProgramNode").
for k in  ParserNode:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  ParserNode:prototype[k].
set  result["name"] to { parameter self.
    return "Program".}.
set  result["format"] to { parameter self.
    return list(list(InstructionListNode, NoneNode), list("EOF")).}.
set  result["new"] to { parameter type,parts.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:instructionList to  parts[0].
    set  self:eof to  parts[1].
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["children"] to { parameter self.
    return list(self:instructionList, self:eof).}.
set  prototype["desugar"] to { parameter self,context.
local  __var105 to  self:instructionList:type.
    if __var105:name(__var105) = "NonSingleStatementGuard" {
      return ProgramNode:new(ProgramNode,list(self:instructionList:instructions, self:eof)).
    }

    return self.}.
set  prototype["analyze"] to { parameter self,context.
    return self.}.
set  prototype["minimize"] to { parameter self,context.
    return self.}.
return  result.}):call().
local  NodePrinterFrame to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","NodePrinterFrame").
set  result["new"] to { parameter type,node.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:items_left to  node:children(node).
    set  self:done to  false.
    if self:items_left:length = 0
      set  self:done to  true.
    
    set  self:strings to  list().
local  __var106 to  node:type.
    if __var106:name(__var106) = "LiteralNode" {
      for n in node:whitespace {
        self:strings:add(n:text).
      }
      self:strings:add(node:token:text).
    }
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["next"] to { parameter self.
    if self:done
      return false. local result to  self:items_left[0].
    set  self:items_left to  self:items_left:sublist(1, self:items_left:length - 1).
    if self:items_left:length = 0
      set  self:done to  true.
    
    return NodePrinterFrame:new(NodePrinterFrame,result).}.
return  result.}):call().
local  NodePrinter to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","NodePrinter").
set  result["new"] to { parameter type,node, print_func.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:stack to  stack().
    set  self:current to  NodePrinterFrame:new(NodePrinterFrame,node).

    set  self:done to  false.

    set  self:linebuffer to  "".
    set  self:print_func to  print_func.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["print"] to { parameter self,strings.
    for s in strings {
      if s:contains(char(10)) { local parts to  s:split(char(10)). local last to  parts[parts:length - 1].
        for p in parts:sublist(0, parts:length - 1) {
          self:print_func(self:linebuffer + p).
          set  self:linebuffer to  "".
        }
        set  self:linebuffer to  last.
      } else {
        set  self:linebuffer to  self:linebuffer + s.
      }
    }}.
set  prototype["next"] to { parameter self.
    if self:current:done {
      self:print(self,self:current:strings).
      if self:stack:empty {
        set  self:current to  false.
        self:print_func(self:linebuffer).
        set  self:done to  true.
        return.
      }

      set  self:current to  self:stack:pop().
      return.
    }

    self:stack:push(self:current).
local  __var107 to  self:current.
    set  self:current to  __var107:next(__var107).}.
return  result.}):call().
local  KernelBuild to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype,"classname","KernelBuild").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).

    if args:length <> 1 {
      print("Usage:").
      print("kernel-build").
      self:exit(self,1).
      return self.
    }

    set  self:priority to  50.
    if env:haskey("PRIORITY") {
      set  self:priority to  env:PRIORITY:toscalar(50).
    }
    
    set  self:filename to  "kosos".
    set  self:file to  "/kosos/kosos.kpp".
    set  self:file_handle to  false.
    set  self:file_lines to  false.
    fs:open(fs,self:file, Callback:new(Callback,self, "onOpenMain")).

    set  self:include_filename to  false.
    set  self:include_file to  false.
    set  self:include_lines to  false.
    set  self:stage to  "parse".
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onOpenMain"] to { parameter self,file.
    if file:istype("Boolean") {
      print("Unable to open " + self:file + ", file not found.").
      self:exit(self,2).
      return.
    }

    set  self:file_handle to  file.
    set  self:file_lines to  list().
    file:read(file,Callback:new(Callback,self, "onReadMain")).}.
set  prototype["onReadMain"] to { parameter self,data.
    if data:istype("Boolean") {
      if self:file_lines:length = 0 {
        print("Unable to open " + self:file + ", read error.").
        self:exit(self,3).
      } else { local lexer to  Lexer:new(Lexer,LexerDefs, "kpp", self:file_lines:join(char(10)), self:file, false).
        set  self:parser to  Parser:new(Parser,lexer:window, LexerDefs, "kpp", {
          declare parameter filename.
          self:print(self,"Compiling: " + filename).
          
          set  self:include_filename to  filename.
          set  self:include_file to  false.
          set  self:include_lines to  list().

          if not filename:startswith("/") {
            set  filename to  self:env:CWD + filename.
          }
local  __var108 to 

          self:fs. __var108:open(__var108,filename, Callback:new(Callback,self, "onOpenInclude")).
          self:stop(self).
        }, ProgramNode, lex(
          "KPP", LexerToken:new(LexerToken,"INTEGER", "1", "make2", -1, -1, false)
        )).

        set  self:file_lines to  list().
        set  self:file_handle to  false.
        self:start(self).
      }
      return.
    }

    self:file_lines:add(data).
local  __var109 to 
    self:file_handle. __var109:read(__var109,Callback:new(Callback,self, "onReadMain")).}.
set  prototype["onOpenInclude"] to { parameter self,file.
    if file:istype("Boolean") {
local  __var110 to 
      self:parser. __var110:onFileRead(__var110,self:include_filename, false).
      self:start(self).
      return.
    }
    set  self:include_file to  file.
    file:read(file,Callback:new(Callback,self, "onReadInclude")).}.
set  prototype["onReadInclude"] to { parameter self,data.
    if data:istype("Boolean") {
      if self:include_lines:length = 0 {
local  __var111 to 
        self:parser. __var111:onFileRead(__var111,self:include_filename, false).
      } else {
local  __var112 to 
        self:parser. __var112:onFileRead(__var112,self:include_filename, self:include_lines:join(char(10))).
      }
      set  self:include_lines to  false.
      set  self:include_file to  false.
      self:start(self).
      return.
    }
    self:include_lines:add(data).
local  __var113 to 
    self:include_file. __var113:read(__var113,Callback:new(Callback,self, "onReadInclude")).}.
set  prototype["run"] to { parameter self.
    if self:stage = "parse"
      self:parse(self).
    if self:stage = "simplify"
      self:simplify(self).
    if self:stage = "desugar"
      self:desugar(self).
    if self:stage = "print_stage"
      self:print_stage(self).}.
set  prototype["parse"] to { parameter self.
    for i in range(self:priority) {
      if not self:parser:atend {
local  __var114 to 
        self:parser. __var114:next(__var114).
      }
    }
    if not self:parser:atend {
      return.
    }
    
    if self:parser:value:istype("Boolean") {
      print("Parse failed:").
      print(self:parser:errors).
      self:exit(self,4).
      return.
    }

    set  self:stage to  "simplify".
    set  self:simplifier to  BottomUpStrategy:new(BottomUpStrategy,self:parser:value, "simplify", false).
    set  self:parser to  false.}.
set  prototype["simplify"] to { parameter self.
    for i in range(self:priority) {
      if not self:simplifier:atend {
local  __var115 to 
        self:simplifier. __var115:next(__var115).
      }
    }
    if not self:simplifier:atend {
      return.
    }

    set  self:stage to  "desugar".
    set  self:async_result to  lex("errors", list()).
    set  self:desugarer to  BottomUpStrategy:new(BottomUpStrategy,self:simplifier:value, "desugar", self:async_result).
    set  self:simplifier to  false.}.
set  prototype["desugar"] to { parameter self.
    for i in range(self:priority) {
      if not self:desugarer:atend and self:async_result:errors:length = 0 {
local  __var116 to 
        self:desugarer. __var116:next(__var116).
      }
    }

    if self:async_result:errors:length > 0 {
      print(self:async_result).
      self:exit(self,5).
      return.
    }

    if not self:desugarer:atend {
      return.
    }

    set  self:outstring to  "".
    set  self:printer to  NodePrinter:new(NodePrinter,self:desugarer:value, {
      parameter str.
      //self->print(str);
      if self:outstring <> ""
        set  self:outstring to  self:outstring + char(10).
      set  self:outstring to  self:outstring + str.
    }).
    set  self:desugarer to  false.
    set  self:stage to  "print_stage".}.
set  prototype["print_stage"] to { parameter self.
    if not self:printer:done {
local  __var117 to 
      self:printer. __var117:next(__var117).
      return.
    } local outname to  "0:/kosos/kosos.ks". local outfile to  false.
	  if exists(outname) {
		  set  outfile to  open(outname).
	  } else {
  		set  outfile to  create(outname).
  	}

    if outfile:istype("boolean") {
      print("Unable to open output file: 0:/kosos/bin/" + self:filename + ".ks").
      self:exit(self,1).
    } else {
      outfile:clear().
      outfile:write(self:outstring).
      self:print(self,"Compliation succeded, assembling...").
      compile "0:/kosos/kosos".
      self:exit(self,0).
    }}.
return  result.}):call().
set  export to  KernelBuild.