local SleepProg to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","SleepProg").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.

    self:progSetup(self,id, input, output, args, env, fs, resultCallback).
    
    set self:sleepDone to  false.
    set self:sleepInterval to  0.

    if args:length < 2 {
      self:print(self,"Usage:").
      self:print(self,"  sleep <delay>").
      self:exit(self,1).
      return self.
    }
    if args[1] = "until" {
      self:exit(self,255).
    } else {
      set self:sleepInterval to  args[1]:toscalar(-1).
      set self:sleepDone to  true.
      if self:sleepInterval <= 0 {
        self:exit(self,0).
        return self.
      }
    }
local __var0 to fs:loop. __var0:sleep(__var0,self, self:sleepInterval).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["run"] to { parameter self.
    if self:interrupted {
      return.
    }
    if self:sleepDone {
      self:exit(self,0).
    }}.
return  result.}):call().

set export to  SleepProg.
