@lazyglobal off.
"kpp 1.1"."https://gitlab.com/thexa4/kos-kpp".
local Rendezvous to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,id, input, output, args, env, fs, resultCallback.local self to __cls:prototype:copy. set self:type to __cls.
    set self:pid to id.
    set self:output to output.
    set self:status to -1.
    set self:fs to fs.
    set self:callback to resultCallback.

    set self:stage to "coast".
    ({local __lhs to self:output. return __lhs:append(__lhs,"Coasting", Callback:null).}):call().
    set self:approaching to true.
    set self:targetDist to 200.
    set self:fired to false.

    if not hastarget {
      ({local __lhs to output. return __lhs:append(__lhs,"No target selected, unable to rendezvous.", Callback:null).}):call().
      set self:status to 1.
      ({local __lhs to fs:loop. return __lhs:defer(__lhs,resultCallback, id).}):call().
    } else {
      set self:target to target.
      set self:distance to (target:position - ship:position):mag.

      ({local __lhs to fs:loop. return __lhs:register(__lhs,self).}):call().
    } return self.}.

  set r:prototype:aim to {parameter self.
    local delta to self:target:position - ship:position.
    local speed to 0.
    if self:approaching {
      set speed to min(50, max(0, delta:mag - self:targetDist) / 50).
    }

    local velDiff to self:target:velocity:orbit - ship:velocity:orbit.
    set self:burnDir to velDiff + delta:normalized * speed.
    lock steering to self:burnDir.
    
    local alignment to self:burnDir:normalized * ship:facing:vector - 0.7.
    print alignment at (0, 1).
    print self:burnDir:mag at (0, 2).
  }.

  set r:prototype:run to {parameter self.
    self[self:stage](self).
  }.

  set r:prototype:coast to {parameter self.
    lock throttle to 0.
    local prevDist to self:distance.
    set self:distance to (target:position - ship:position):mag.

    if self:distance < self:targetDist {
      set self:stage to "stop".
      ({local __lhs to self:output. return __lhs:append(__lhs,"too close", Callback:null).}):call().
      return.
    }

    ({local __lhs to self. return __lhs:aim(__lhs).}):call().

    if prevDist >= self:distance
      return.

    set self:fired to false.
    set self:stage to "align".
    ({local __lhs to self:output. return __lhs:append(__lhs,"align", Callback:null).}):call().
  }.

  set r:prototype:align to {parameter self.
    ({local __lhs to self. return __lhs:aim(__lhs).}):call().

    local alignment to self:burnDir:normalized * ship:facing:vector - 0.7.
    if self:burnDir:mag < 0.2 or (alignment < 0 and self:fired) {
      lock throttle to 0.
      set self:stage to "coast".
      ({local __lhs to self:output. return __lhs:append(__lhs,"coast", Callback:null).}):call().
      return.
    }

    if alignment > 0
      set self:fired to true.
    lock throttle to self:burnDir:mag * alignment / 10.
  }.

  set r:prototype:stop to {parameter self.
    set self:approaching to false.

    ({local __lhs to self. return __lhs:aim(__lhs).}):call().

    if self:burnDir:mag < 0.1 {
      lock throttle to 0.
      unlock throttle.
      unlock steering.
      ({local __lhs to self:fs:loop. return __lhs:deregister(__lhs,self).}):call().
      
      set self:stage to "none".
      ({local __lhs to self:output. return __lhs:append(__lhs,"done", Callback:null).}):call().
      set self:status to 0.
      ({local __lhs to self:callback. return __lhs:call(__lhs,self:pid).}):call().
      return.
    }
    
    local alignment to self:burnDir:normalized * ship:facing:vector - 0.7.
    lock throttle to self:burnDir:mag * alignment / 10.
  }.

  set r:prototype:none to {parameter self.
    print("deregister failed").
  }.
}).
set export to Rendezvous.