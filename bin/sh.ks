local Shell to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Shell").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).
    set self:subProc to  false.
    self:readLine(self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onRead"] to { parameter self,line.
    if line:istype("Boolean") {
      self:exit(self,0).
      return.
    }
    self:print(self,line).
local __var0 to self:fs. __var0:exec(__var0,
      line,
      self:env,
      self:input,
      self:output,
      Callback:new(Callback,self, "onChildExit"),
      Callback:new(Callback,self, "onLoad")
    ).}.
set  prototype["onLoad"] to { parameter self,res.
    if res:istype("Boolean") {
      self:print(self,"No such program").
      self:onChildExit(self,0).
    }
    set self:subProc to  res.}.
set  prototype["onChildExit"] to { parameter self,_.
    set self:subProc to  false.
    self:readLine(self).}.
set  prototype["onSignal"] to { parameter self,signal.
    if self:subProc:istype("Boolean") {
      if signal = "End" {
        self:exit(self,9).
        return true.
      }
    } else {
local __var1 to self:subProc.
return  __var1:onSignal(__var1,signal).
    }}.
return  result.}):call().
set export to  Shell.