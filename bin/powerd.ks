local  PowerDaemon to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    self:progSetup(
    self,id, input, output, args, env, fs, resultCallback). set 

    self:check_interval to  60.
    if self:env:haskey("POWER_INTERVAL") and env:POWER_INTERVAL:toscalar(0) > 0 { set 
      self:check_interval to  env:POWER_INTERVAL:toscalar(0).
    } set 

    self:config_dir to  false. set 

    self:last_power to  100. set 
    self:up to  lex(). set 
    self:down to  lex().

    fs:mkdir(

    fs,"/etc/powerd", Callback: new( Callback,self, "onCreateDir")).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onCreateDir"] to { parameter self,dir. set 
    self:config_dir to  dir.
    if dir:istype("Boolean") {
local  __var20 to 
      self:output. __var20:append( __var20,"Unable to create config dir.", Callback:null).
      self:exit(
      self,1).
      return.
    }

    dir:read(

    dir, Callback:new( Callback,self, "onReadConfDir")).}.
set  prototype["onReadConfDir"] to { parameter self,files.
    for file in files { local parts to  file:split(".").
      if parts:length = 2 { local extension to  parts[parts:length - 1].
        if extension = "up" { local stop to  parts[0]:tonumber(-1).
          if stop > 0 and stop <= 99 {
            self:up:add(stop, "/etc/powerd/" + file).
          }
        }
        if extension = "down" { local stop to  parts[0]:tonumber(-1).
          if stop > 0 and stop <= 99 {
            self:down:add(stop, "/etc/powerd/" + file).
          }
        }
      }
    }

    self:start(

    self).}.
set  prototype["run"] to { parameter self. local electricCharge to  false.
    for resource in ship:resources {
      if resource:name = "ElectricCharge" { set 
        electricCharge to  resource.
      }
    }

    if electricCharge:istype("Boolean") {
      return.
    } local percentage to  floor(100 * electricCharge:amount / electricCharge:capacity).
    print("powerd: " + self:last_power + " => " + percentage).

    until self:last_power <= percentage {
      if self:up:haskey(percentage) { local command to  "kash < " + self:up[percentage].
        print(command).
        //self->fs->exec(
        //  command,
        //  self->env,
        //  self->input,
        //  self->output,
        //  Callback->null,
        //  Callback->null
        //);
      } set 
      self:last_power to  self:last_power - 1.
    }

    until self:last_power >= percentage { set 
      self:last_power to  self:last_power + 1.
      if self:down:haskey(percentage) { local command to  "kash < " + self:up[percentage].
        print(command).
        //self->fs->exec(
        //  command,
        //  self->env,
        //  self->input,
        //  self->output,
        //  Callback->null,
        //  Callback->null
        //);
      }
    }
local  __var21 to 

    self:fs:loop. __var21:sleep( __var21,self, self:check_interval).}.
return  result.}):call(). set 

export to  PowerDaemon.
