local  Env to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id.
    for key in env:keys {
      output:write(
      output,key + "=" + env[key], Callback:null).
    } set 
    self:status to  0.
local  __var10 to 
    fs:loop. __var10:defer( __var10,resultCallback, id).
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
export to  Env.