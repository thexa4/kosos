local  Echo to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id. set 
    self:status to  0.
    if ship:status = "prelaunch" {
      shutdown.
    }
local  __var103 to 
    fs:loop. __var103:defer( __var103,resultCallback, id).
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
export to  Echo.