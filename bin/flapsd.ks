local  DelegateFile to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,fs, readDelegate, writeDelegate.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:fs to  fs. set 
    self:readDelegate to  readDelegate. set 
    self:writeDelegate to  writeDelegate. set 
    self:stream to  false.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getType"] to { parameter self. return "file".}.
set  prototype["read"] to { parameter self,callback.
local  __var203 to  self:fs:loop. __var203:defer( __var203,callback, self:readDelegate()).}.
set  prototype["write"] to { parameter self,data, callback.
    if not data:istype("Boolean")
      self:writeDelegate(data).
local  __var204 to 
    self:fs:loop. __var204:defer( __var204,callback, true).}.
set  prototype["append"] to { parameter self,data, callback.
    self:write(
    self,data, callback).}.
return  result.}):call().
local  Flapsd to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id. set 
    self:status to  0. set 
    self:input to  input. set 
    self:output to  output. set 
    self:env to  env. set 
    self:fs to  fs. set 
    self:resultCallback to  resultCallback. set 
    self:running to  false. set 
    self:registered to  false. set 

    self:desiredState to  "none". set 
    self:currentState to  "unknown". set 

    self:flaps to  list(). local detectedState to  "".

    // FARControllableSurface
    // action: "increase flap deflection"
    // action: "decrease flap deflection"
    // field: "flap setting"
    // field: "flp/splr" -> true
    for part in ship:parts {
      if part:hasmodule("FARControllableSurface") { local module to  part:getmodule("FARControllableSurface").
        
        module:setfield("flp/splr", true).
        if module:hasfield("flap setting") {
          self:flaps:add(module). local curSetting to  "" + module:getfield("flap setting").
          if detectedState = "" set 
            detectedState to  curSetting.
          if curSetting <> detectedState { set 
            detectedState to  "inconsistent".
          }
        }
      }
    } set 

    self:currentState to  detectedState.

    if not career():CANDOACTIONS {
local  __var205 to 
      self:output. __var205:append( __var205,"Action Groups are not available yet", Callback:null). set 

      self:status to  1. set 
      self:exited to  true.
local  __var206 to 
      self:fs:loop. __var206:defer( __var206,self:resultCallback, self:pid).
      return self.
    }

    if self:flaps:length = 0 {
local  __var207 to 
      self:output. __var207:append( __var207,"No flaps found", Callback:null). set 

      self:status to  1. set 
      self:exited to  true.
local  __var208 to 
      self:fs:loop. __var208:defer( __var208,self:resultCallback, self:pid).
      return self.
    }

    self:startDaemon(

    self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["startDaemon"] to { parameter self. local configDir to  MemoryFilesystemDir: new( MemoryFilesystemDir).
    configDir:registerFile(
    configDir,"desired", DelegateFile: new( DelegateFile,self:fs, { return "" + self:desiredState. }, { parameter data. self:setDesired( self,data). })).
    configDir:registerFile(
    configDir,"current", DelegateFile: new( DelegateFile,self:fs, { return "" + self:currentState. }, { parameter data. })).
local  __var209 to 
    self:fs. __var209:mount( __var209,configDir, "/sys/class/flaps"). set 
    self:input:signalHandler to  Callback: new( Callback,self, "onSignal"). set 

    self:running to  true.}.
set  prototype["stopDaemon"] to { parameter self.
    if not self:running
      return.
local  __var210 to 

    self:fs. __var210:unmount( __var210,"/sys/class/flaps").
    if not self:exited { set 
      self:exited to  true.
    }

    if self:registered {
local  __var211 to 
      self:fs:loop. __var211:deregister( __var211,self).
    }}.
set  prototype["setDesired"] to { parameter self,state.
    if state <> "0" and state <> "1" and state <> "2" and state <> "3"
      return. set 
    
    self:desiredState to  state.
    if self:desiredState <> self:currentState {
      if not self:registered { set 
        self:registered to  true.
local  __var212 to 
        self:fs:loop. __var212:register( __var212,self).
      }
    }}.
set  prototype["run"] to { parameter self.
    if self:currentState = "inconsistent" {
      for module in self:flaps {
        if module:hasaction("decrease flap deflection") {
          module:doaction("decrease flap deflection", true).
        }
      }
    } else { local curInt to  self:currentState:tonumber(-1). local destInt to  self:desiredState:tonumber(-1).
      if curInt <> destInt {
        for module in self:flaps {
          if curInt < destInt {
            if module:hasaction("increase flap deflection")
              module:doaction("increase flap deflection", true).
          } else {
            if module:hasaction("decrease flap deflection")
              module:doaction("decrease flap deflection", true).
          }
        }
      }
    }
local detectedState to  "". 
    for module in self:flaps { 
      if module:hasfield("flap setting") { local curSetting to  "" + module:getfield("flap setting").
        if detectedState = "" set 
          detectedState to  curSetting.
        
        if curSetting <> detectedState { set 
          detectedState to  "inconsistent".
        }
      }
    }
	
	set self:currentState to "" + detectedState.

    if self:currentState = self:desiredState { set 
      self:registered to  false.
local  __var213 to 
      self:fs:loop. __var213:deregister( __var213,self).
    }}.
set  prototype["onSignal"] to { parameter self,signal.
    if not self:running
      return false.
    if signal = "End" {
      self:stopDaemon(
      self). set 
      self:status to  9. set 
      self:exited to  true.
local  __var214 to 
      self:fs:loop. __var214:defer( __var214,self:resultCallback, self:pid).
      return true.
    }}.
return  result.}):call(). set 
export to  Flapsd.
