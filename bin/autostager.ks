local AutoStager to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","AutoStager").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).

    set self:stages to  lexicon().
    local _engines to  ship:engines.
    local isActive to  false.
    for engine in _engines {
      if not self:stages:haskey(engine:stage)
        self:stages:add(engine:stage, list()).
      self:stages[engine:stage]:add(engine).
      if engine:maxthrust > 0
        set isActive to  true.
    }
    // Allow launch if no active engines and next stage enables engines.
    if not isActive and self:stages:haskey(stage:number - 1) {
      self:stages:add(stage:number, list(lexicon("flameout", true))).
    }

    self:start(self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["run"] to { parameter self.
    if stage:nextDecoupler = "None"
      return.
    if not stage:ready
      return.
    if not self:stages:haskey(stage:number - 1)
    {
      self:exit(self,0).
      return.
    }

    for engine in self:stages[stage:number] {
      if engine:flameout and throttle > 0 {
        stage.
        return.
      }
    }}.
return  result.}):call().

set export to  AutoStager.