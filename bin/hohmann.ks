local Hohmann to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Hohmann").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).

    set self:forceTime to  false.
    if args:length < 2 {
      set args to  list(args[0], "target").
    }
    if args[1] = "target" {
      if not hastarget {
        self:print(self,"No target selected!").
        self:exit(self,1).
        return self.
      }
      set self:target_obt to  target:obt.
    } else if args[1] = "alt" {
      if args:length <> 3 or args[2]:toscalar(-10000000) = -10000000 {
        self:print(self,"Missing altitude argument").
        self:exit(self,2).
        return self.
      }
      local desiredAlt to  args[2]:toscalar(-1).

      set self:target_obt to  Createorbit(ship:obt:inclination, 0, desiredAlt + ship:body:radius, ship:obt:lan, 0, 0, 0, ship:body).
      set self:forceTime to  300.
      if self:env:haskey("BURN_INTERVAL") {
        set self:forceTime to  self:env:BURN_INTERVAL:toscalar(self:forceTime).
      }
    } else {
      set self:target_obt to  Vessel(args[1]):obt.
    }

    if ship:obt:eccentricity > 0.01 or self:target_obt:eccentricity > 0.01 {
      self:print(self,"Bad eccentricity! Run with 'force' argument to continue!").
      if args[args:length - 1] <> "force" {
        self:exit(self,1).
        return self.
      }
    }
    
    local transferTime to  3.14159 * sqrt(((ship:obt:semimajoraxis + self:target_obt:semimajoraxis) / 2 ) ^ 3 / ship:body:mu). // time on transfer orbit.
    local targetSpeed to  360 / self:target_obt:period.  // 3
    local targetRotationAfterTransfer to  targetSpeed * transferTime. // 4
    local phaseAngleTransfer to  180 - targetRotationAfterTransfer. // 5

    if env:haskey("DEBUG") and env:debug:toscalar(0) > 0 {
      self:print(self,"phaseAngleTransfer: " + phaseAngleTransfer).
    }

    local speed to  360 / ship:obt:period. // 6    
    local phaseAngleTarget to  vang(ship:position - ship:body:position, self:target_obt:position - ship:body:position). // 7
    if(self:target_obt:position * ship:obt:velocity:orbit < 0) {
      set phaseAngleTarget to  360 - phaseAngleTarget.
    }
    
    if env:haskey("DEBUG") and env:debug:toscalar(0) > 0 {
      self:print(self,"phaseAngleTarget: " + phaseAngleTarget).
    }

    local phaseDelta to  phaseAngleTarget - phaseAngleTransfer.
    if speed - targetSpeed > 0 {
      if(phaseDelta < 0) {
        set phaseDelta to  phaseDelta + 360.
      }
    } else {
      if(phaseDelta > 0) {
        set phaseDelta to  phaseDelta - 360.
      }
    }
    
    if env:haskey("DEBUG") and env:debug:toscalar(0) > 0 {
      self:print(self,"phaseDelta: " + phaseDelta).
    }

    local intercept to  phaseDelta / (speed - targetSpeed). // 8
    local prograde_v to  sqrt(ship:body:mu / ship:obt:semimajoraxis) * (sqrt((2 * self:target_obt:semimajoraxis) / (ship:obt:semimajoraxis + self:target_obt:semimajoraxis)) - 1).

    if env:haskey("DEBUG") and env:debug:toscalar(0) > 0 {
      set self:arrows to  list().
      self:arrows:add(VECDRAW(
        { return target_obt:body:position. },
        { return target_obt:position - target_obt:body:position. },
         RGBA(1, 0, 0, 1), "", 1, true, 0.1, true)).
      self:arrows:add(VECDRAW(
        { return ship:obt:body:position. },
        { return ship:obt:position - ship:obt:body:position. },
         RGBA(0, 0, 1, 1),"", 1, true, 0.1, true)).
    }

    if self:forceTime:istype("Boolean") {
      add node(time:seconds + intercept, 0, 0, prograde_v).
    } else {
      add node(time:seconds + self:forceTime, 0, 0, prograde_v).
    }
    
    self:exit(self,0).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["run"] to { parameter self.}.
return  result.}):call().
set export to  Hohmann.