local  Make to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    PROGRAM_INIT.
    if args:length <> 2 {
      self:print(
      self,"Need target to compile.").
      self:print(
      self,"Usage:").
      self:print(
      self,"make <prog>").
      self:exit(
      self,1).
      return self.
    }

    run "0:/kpp/parser"(false). local infile to  "0:/kosos/bin/" + args[1] + ".kpp". local outfile to  "0:/kosos/bin/" + args[1] + ".ks".
    if args[1] = "kosos" { set 
      infile to  "0:/kosos/kosos.kpp". set 
      outfile to  "0:/kosos/kosos.ks".
    }
    if not fs:loop:framework:istype("Boolean") {
      self:print(
      self,"Error: another framework is already running, please wait for it to exit.").
      self:exit(
      self,1).
      return self.
    } local hasStdlib to  defined STDLIB_INCLUDED.
    self:print(
    self,"Stdlib available: " + hasStdlib). set 
    fs:loop:framework to  {
      declare parameter callback. local defines to  lex("NO_STDLIB", "1").
      if hasStdlib
        defines:add("STDLIB_KPP", 1).
      if args[1] = "kosos" set 
        defines to  lex().

      if not exists(infile) {
        self:print(
        self,"Program not found: " + infile).
        self:exit(
        self,1).
        return self.
      }
      if kpp_compile(infile, outfile, defines, callback) { set 
        self:status to  0.
        if args[1] = "kosos"
          compile "0:/kosos/kosos.ks" to "0:/kosos/kosos.ksm".
        else
          compile outfile to "0:/kosos/bin/" + args[1] + ".ksm".
      }

      self:exit(

      self,0).
    }.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
export to  Make.