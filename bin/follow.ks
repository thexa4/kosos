local Launch to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Launch").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).
    
    if args:length < 1 or args:length > 2   {
      self:print(self,"Usage: follow [<at>]").
      self:exit(self,1).
      return self.
    }

    if not hasTarget {
      self:print(self,"Target not set!").
      self:exit(self,1).
      return self.
    }

    set self:target to  target.

    local desiredKm to  0.
    if args:length = 2 {
      set desiredKm to  args[1]:tonumber(0).
    } else {
      set desiredKm to  -1.
    }
    if desiredKm = 0 {
      self:print(self,"Unable to follow inside target").
      self:exit(self,1).
      return self.
    }
    set self:followAt to  desiredKm / target:velocity:orbit:mag * 1000.
    self:print(self,"Following " + self:followAt + " seconds behind target.").

    set self:distance to  self:deltaPos(self):mag.
    set self:steering to  ship:facing:vector.
    set self:throttle to  0.
    set self:fired to  false.
    set self:stage to  "coast".
    self:print(self,"Coasting").

    lock steering to self:steering.
    lock throttle to self:throttle.
    rcs off.

    set self:arrow1 to  ship:position.
    set self:arrow2 to  positionat(self:target, time:seconds + self:followAt).
    set self:arrow to  vecdraw(self:arrow1, self:arrow2 , rgb(1, 0, 0), "target", 1, true).

    self:start(self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["run"] to { parameter self.   
    set self:arrow1 to  ship:position.
    set self:arrow2 to  positionat(self:target, time:seconds + self:followAt).

    self[self:stage](self).

    print "":padleft(terminal:width) at (0,1).
    print "":padleft(terminal:width) at (0,2).
    print "Pos: " + round(self:deltaPos(self):mag)  at (0, 1).
    print "Vel: " + round(self:deltaVel(self):mag, 2)  at (0, 2).}.
set  prototype["deltaPos"] to { parameter self.
    return positionat(self:target, time:seconds + self:followAt) - ship:position.}.
set  prototype["deltaVel"] to { parameter self.
    return velocityat(self:target, time:seconds + self:followAt):orbit - ship:velocity:orbit.}.
set  prototype["aim"] to { parameter self.
    local delta to  self:deltaPos(self).
    local speed to  min(50, max(0, delta:mag) / 50).

    local velDiff to  self:deltaVel(self).
    set self:burnDir to  velDiff + delta:normalized * speed.

    if self:burnDir:mag > 5 {
      set self:steering to  self:burnDir.
    }
    
    local alignment to  self:burnDir:normalized * ship:facing:vector - 0.7.
    print "":padleft(terminal:width) at (0,3).
    print "":padleft(terminal:width) at (0,4).
    print "Alignment: " + alignment at (0, 3).
    print "Burn: " + self:burnDir:mag at (0, 4).}.
set  prototype["coast"] to { parameter self.
    set self:throttle to  0.
    local prevDist to  self:distance.
    set self:distance to  self:deltaPos(self):mag.
    self:aim(self).

    if prevDist < self:distance {
      set self:fired to  false.
      set self:stage to  "align".
      self:print(self,"align").
    }}.
set  prototype["align"] to { parameter self.
    self:aim(self).
      
    if self:burnDir:mag < self:deltaPos(self):mag / 10000 {
      set self:throttle to  0.
      rcs off.
      set ship:control:fore to  0.
      set ship:control:starboard to  0.
      set ship:control:top to  0.
      set self:stage to  "coast".
      self:print(self,"coast").
      return.
    }

    if self:burnDir:mag <= 5 {
      
      rcs on.
      set self:throttle to  0.
      set self:fired to  true.

      set ship:control:fore to  self:burnDir * ship:facing:vector.
      set ship:control:starboard to  self:burnDir * ship:facing:starvector.
      set ship:control:top to  self:burnDir * ship:facing:topvector.
      
      if self:deltaPos(self):mag < 5 and self:deltaVel(self):mag < 0.01 {
       
        set ship:control:fore to  0.
        set ship:control:starboard to  0.
        set ship:control:top to  0.
        rcs off.

        set self:stage to  "none".
        self:print(self,"done!").
        self:exit(self,0).
        return.
      }

    } else {

      rcs off.
      local alignment to  self:burnDir:normalized * ship:facing:vector - 0.7.
      if alignment < 0 and self:fired {
        set self:throttle to  0.
        set self:stage to  "coast".
        self:print(self,"coast").
        return.
      }

      if alignment > 0
        set self:fired to  true.
      set self:throttle to  self:burnDir:mag * alignment / 10.
    }}.
set  prototype["none"] to { parameter self.
    print("none").}.
set  prototype["deconstruct"] to { parameter self,_.
    set ship:control:fore to  0.
    set ship:control:starboard to  0.
    set ship:control:top to  0.
    unlock throttle.
    unlock steering.
    set ship:control:neutralize to  true.
    clearvecdraws().}.
return  result.}):call().
set export to  Launch.