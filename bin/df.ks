local  Df to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id. set 
    self:output to  output. set 
    self:status to  -1. set 
    self:resultCallback to  resultCallback. set 
    self:fs to  fs. set 

    self:todo to  queue().
    for m in fs:mounts( fs) {
      self:todo:push(m).
    } set 
    self:current to  false.
local  __var126 to 

    self:output. __var126:append( __var126,"       Used Available Capacity Mounted on", Callback:null).
local  __var127 to 
    fs:loop. __var127:defer( __var127, Callback:new( Callback,self, "next"), 0).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["next"] to { parameter self,_.
    if self:todo:length = 0 { set 
      self:status to  0.
local  __var128 to 
      self:resultCallback. __var128:call( __var128,self:pid).
      return.
    } set 

    self:current to  self:todo:pop().
local  __var129 to 
    self:fs. __var129:open( __var129,self:current, Callback: new( Callback,self, "onOpen")).}.
set  prototype["onOpen"] to { parameter self,result.
    if result:istype("Boolean") or not result:haskey("df") {
local  __var130 to 
      self:output. __var130:append( __var130,"?":padleft(11) + " " + "?":padleft(11) + " " + "?":padleft(6) + " " + self:current, Callback:null).
    } else { local df to  result:df( result). local used to  df["used"]. local available to  df["available"]. local percent to  100.
      if available > 0 { set 
        percent to  round(used / available * 100).
      }
local  __var131 to 

      self:output. __var131:append( __var131,("" + used):padleft(11) + " " + ("" + available):padleft(11) + " " + ("" + percent):padleft(5) + "% " + self:current, Callback:null).
    }
local  __var132 to 
    self:fs:loop. __var132:defer( __var132, Callback:new( Callback,self, "next"), 0).}.
return  result.}):call(). set 
export to  Df.