@lazyglobal off.
"kpp 1.1"."https://gitlab.com/thexa4/kos-kpp".


local Position to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,v, body.local self to __cls:prototype:copy. set self:type to __cls.
    set self:v to v.
    set self:b to body.
    set self:abs to false.
    if body:istype("Boolean") or body = sun {
      // Sun is absolute
      set self:abs to true.
    } return self.}.

  set r:from_geo to {parameter self,geo, altitude.
    // START CRITICAL SECTION
    if opcodesleft < 24
      wait 0.
    local xyz to (geo:altitudePosition(altitude) - geo:body:position) * lookdirup(solarprimevector, v(0,1,0)):inverse.
    // END CRITICAL SECTION
    return Position:__new(Position,xyz, geo:body).
  }.

  set r:from_orbit to {parameter self,target, time.
    // START CRITICAL SECTION
    if opcodesleft < 24
      wait 0.
    local xyz to (positionat(target, time) - target:body:position) * lookdirup(solarprimevector, v(0,1,0)):inverse.
    // END CRITICAL SECTION
    return Position:__new(Position,xyz, target:body).
  }.

  set r:from_position to
  {parameter self,orbit.
    // START CRITICAL SECTION
    if opcodesleft < 24
      wait 0.
    local xyz to (orbit:position - orbit:body:position) * lookdirup(solarprimevector, v(0,1,0)):inverse.
    // END CRITICAL SECTION
    return Position:__new(Position,xyz, orbit:body).
  }.

  set r:from_velocity to {parameter self,s.
    // START CRITICAL SECTION (18 ops)
    if opcodesleft < 18
      wait 0.
    local xyz to s:velocity:orbit * lookdirup(solarprimevector, v(0,1,0)):inverse.
    // END CRITICAL SECTION
    return Position:__new(Position,xyz, false).
  }.

  set r:prototype:changeSOI to {parameter self,tgt, time.
    return false.
  }.

  set r:prototype:add to {parameter self,pos.
    if self:abs {
      return Position:__new(Position,self:v + pos:v, pos:b).
    }
    if pos:abs {
      return Position:__new(Position,self:v + pos:v, self:b).
    }
    if pos:b <> self:b {
      return false.
    }
    return Position:__new(Position,self:v + pos:v, self:b).
  }.
  set r:prototype:sub to {parameter self,pos.
    if pos:abs {
      return Position:__new(Position,self:v - pos:v, self:b).
    }
    if self:b <> pos:b {
      return false.
    }
    return Position:__new(Position,self:v - pos:v, false).
  }.

  set r:prototype:mult to {parameter self,s.
    return Position:__new(Position,self:v * s, self:b).
  }.

  set r:prototype:cross to {parameter self,o.
    if self:abs {
      return Position:__new(Position,vcrs(self:v, o:v), o:b).
    }
    if o:abs {
      return Position:__new(Position,vcrs(self:v, o:v), self:b).
    }
    if self:b <> o:b {
      return false.
    }
    return Position:__new(Position,vcrs(self:v, o:v), o:b).
  }.
  
  set r:prototype:dot to {parameter self,o.
    return self:v * o:v.
  }.

  set r:prototype:normalized to {parameter self.
    return Position:__new(Position,self:v:normalized, self:b).
  }.

  set r:prototype:to_geo to {parameter self,t.
    // START CRITICAL SECTION
    if opcodesleft < 31
      wait 0.
    local timeDelta to t - time:seconds.
    local geo to self:b:geopositionof(self:v * lookdirup(solarprimevector, v(0,1,0)) + self:b:position).
    // END CRITICAL SECTION
    //return geo;
    return self:b:geopositionlatlng(geo:lat, geo:lng - ( self:b:angularvel:mag * ( constant:pi/180 )) * timeDelta ).
  }.

  set r:fromString to {parameter self,s.
    if not s:startsWith("P;")
      return false.
    local parts to s:split(";").
    if parts:length <> 5
      return false.
    local body to false.
    if parts[1] <> "@"
      set body to Body(parts[1]).
    local x to parts[2]:tonumber(0).
    local y to parts[3]:tonumber(0).
    local z to parts[4]:tonumber(0).
    return Position:__new(Position,V(x,y,z), body).
  }.

  set r:prototype:to_s to {parameter self.
    local body to "@".
    if not self:abs {
      set body to self:b:name.
    }
    return "P;" + body + ";" + round(self:v:x,2) + ";" + round(self:v:y,2) + ";" + round(self:v:z,2).
  }.
}).

local Landing to Class:__new(Class, {parameter r.
  r:mix(r:prototype,Program:prototype).

  set r:__new to{parameter __cls,id, input, output, args, env, fs, resultCallback.local self to __cls:prototype:copy. set self:type to __cls. ({local __lhs to self. return __lhs:progSetup(__lhs,id, input, output, args, env, fs, resultCallback).}):call().

    set self:geo to ({local __lhs to self. return __lhs:landingSite(__lhs).}):call().
    if self:geo:istype("Boolean") {
        ({local __lhs to self. return __lhs:print(__lhs,"No landing site found!").}):call().
        ({local __lhs to self. return __lhs:exit(__lhs,1).}):call().
        return self.
    }
    set self:last to time:seconds.
         
    //vecdraw({ return self->geo:position + (up:vector * 100); } , { return - (up:vector * 100); } , rgb(1, 0, 0), "landing site", 1, true);
    vecdraw({ return self:geo:position + up:vector * 10000. }, up:vector * -10000, rgb(1, 0, 0), "landing site", 1, true, 0.05, false).
    //vecdraw({ return positionat(ship, time:seconds) + up:vector * 10000; }, up:vector * -10000, rgb(1, 1, 0), "", 1, true);
    //vecdraw({ return ship:body:geopositionof(positionat(ship, time:seconds)):position + up:vector * 10000; }, up:vector * -10000, rgb(0, 0, 1), "", 1, true, 0.05);

    ({local __lhs to self. return __lhs:start(__lhs).}):call(). return self.}.

  set r:prototype:run to {parameter self.
    if(self:last + 1 < time:seconds)
    {
        set self:geo to ({local __lhs to self. return __lhs:landingSite(__lhs).}):call(). 
        set self:last to time:seconds.
    }
  }.

  set r:prototype:find_impact_between to {parameter self,t_start, t_end, stepcount.
      local step_size to (t_end - t_start) / stepcount.
      print "step size: " + round(step_size, 1).

      for s in range(stepcount) {
          local step_time to t_start + s * step_size.
          local orbit_position to ({local __lhs to Position. return __lhs:from_orbit(__lhs,ship, step_time).}):call().
          local geo to ({local __lhs to orbit_position. return __lhs:to_geo(__lhs,step_time).}):call().
          local height to orbit_position:v:mag - (body:radius + geo:terrainheight).
          if height < 0 {
              return list(step_time, orbit_position).
          }
      }
      return false.
  }.

    set r:prototype:landingSite to
    {parameter self.
        local roughEstimate to ({local __lhs to self. return __lhs:find_impact_between(__lhs,time:seconds, time:seconds + (eta:periapsis * 1.1), 40).}):call().
        if roughEstimate:istype("Boolean") {
            return false.
        }
        local roughPos to roughEstimate[1].
        local roughTime to roughEstimate[0].
        
        local fineStep to eta:periapsis / 20.
        local fineEstimate to ({local __lhs to self. return __lhs:find_impact_between(__lhs,roughTime - fineStep, roughTime + fineStep, 40).}):call().
        if roughEstimate:istype("Boolean") {
            return false.
        }

        return ({local __lhs to fineEstimate[1]. return __lhs:to_geo(__lhs,fineEstimate[0]).}):call().
    }.

  set r:prototype:deconstruct to {parameter self,_.
    clearvecdraws().
  }.
}).
set export to Landing.