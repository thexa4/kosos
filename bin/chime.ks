local  Chime to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id. local notes to  list("B3", "D3", "G3", "B4", "D4"). local voiceStart to  10 - notes:length.
    for n in notes { local voice to  GetVoice(voiceStart). set 
      voiceStart to  voiceStart + 1. set 
      voice:wave to  "sine". set 
      voice:attack to  0.05. set 
      voice:decay to  0.1. set 
      voice:sustain to  0.9. set 
      voice:release to  0.9.
      voice:play(Note(n, 0.3)).
    } set 
    self:status to  0.
local  __var0 to 
    fs:loop. __var0:defer( __var0,resultCallback, id).
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
export to  Chime.