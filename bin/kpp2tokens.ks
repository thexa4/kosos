local  LexerToken to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,def, text, filename, lineNumber, charOffset, isWhitespace.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:def to  def. set 
    self:text to  text. set 
    self:filename to  filename. set 
    self:lineNumber to  lineNumber. set 
    self:charOffset to  charOffset. set 
    self:isWhitespace to  isWhitespace.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call().
local  LexerTokenDefSimple to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,domain, id, roughRegex, length.
local  self to  prototype:copy.
set  self["type"] to  result.
    parameter whitespace to false. set 
    self:domain to  domain. set 
    self:id to  id. set 
    self:roughRegex to  "^" + roughRegex. set 
    self:l to  length. set 
    self:whitespace to  whitespace.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["match"] to { parameter self,text.
    if text:matchesPattern(self:roughRegex) {
      return text:substring(0, self:l).
    }
    return false.}.
return  result.}):call().
local  LexerTokenDefDynamic to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,domain, id, roughRegex, refiner.
local  self to  prototype:copy.
set  self["type"] to  result.
    parameter whitespace to false. set 
    self:domain to  domain. set 
    self:id to  id. set 
    self:roughRegex to  "^" + roughRegex. set 
    self:refiner to  refiner. set 
    self:whitespace to  whitespace.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["match"] to { parameter self,text.
    if text:matchesPattern(self:roughRegex) { local length to  self:refiner(text).
      return text:substring(0, length).
    }
    return false.}.
return  result.}):call().
local  LexerWindow to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,l, offset.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:lexer to  l. set 
    self:offset to  offset. set 
    self:cache to  list(). set 
    self:atend to  l:atend.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["get"] to { parameter self,n. local i to  self:offset + n.
    until self:lexer:atend or self:cache:length > i {
      self:cache:add(self:lexer:value).
local  __var107 to 
      self:lexer. __var107:next( __var107).
    }

    if self:cache:length > i {
      return self:cache[i].
    }
    return false.}.
set  prototype["next"] to { parameter self.
    if self:atend {
      return false.
    } local hasNext to  self:get( self,2). local result to  self:copy. set 
    result:atend to  hasNext:istype("Boolean"). set 
    result:offset to  self:offset + 1.
    return result.}.
return  result.}):call().
local  Lexer to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,defs, domain, text, filename, noeof.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:defs to  list().
    for d in defs {
      if d:domain:find(domain) > -1 {
        self:defs:add(d).
      }
    } set 
    self:remainingText to  text. set 
    self:filename to  filename. set 
    self:lineNumber to  1. set 
    self:charOffset to  1. set 
    self:noeof to  noeof. set 
    self:atend to  false. set 
    self:error to  false. set 
    self:window to  LexerWindow: new( LexerWindow,self, 0).
    self:next(
    self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["next"] to { parameter self. local t to  self:remainingText.
    if t:length = 0 {
      if not self:noeof { set 
        self:value to  LexerToken: new( LexerToken,"EOF", "", self:filename, self:lineNumber, self:charOffset, false). set 
        self:noeof to  true.
        return.
      } set 
      self:value to  false. set 
      self:error to  self:filename + ": end of file". set 
      self:atend to  true.
      return.
    }

    for d in self:defs { local m to  d:match( d,t).
      if m:istype("string") { set 
        self:remainingText to  self:remainingText:remove(0, m:length). set 
        self:value to  LexerToken: new( LexerToken,d:id, m, self:filename, self:lineNumber, self:charOffset, d:whitespace). set 

        self:charOffset to  self:charOffset + m:length.
        if (d:id = "NEWLINE") { set 
          self:lineNumber to  self:lineNumber + 1. set 
          self:charOffset to  1.
        }
        return.
      }
    } set 
    
    self:error to  "Unexpected character found in file " + self:filename + ":" + self:lineNumber + ", char " + self:charOffset + ": '" + self:remainingText:substring(0, min(10, self:remainingText:length)) + "'...". set 
    self:atend to  true. set 
    self:value to  false.}.
return  result.}):call(). local LexerDefs to  list( LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "NEWLINE", "\n", 1, true), LexerTokenDefDynamic:
	new( LexerTokenDefDynamic,list("kos", "kpp"), "WHITESPACE", "(\s|\p{C})+", {
		// Try matching bigger identifiers until it no longer works
		declare parameter input.
		local length to 0.
		local matches to true.
		until matches = false {
			set length to length + 1.
			local regex to "^(\s|\p{C}){" + (length + 1) + "}".
			set matches to input:matchespattern(regex).
		}
		return length.
	}, true), LexerTokenDefDynamic:
  new( LexerTokenDefDynamic,list("kos", "kpp"), "COMMENTLINE", "//[^\n]*", {
			// Try matching bigger identifiers until it no longer works
			declare parameter input.
			local length to -1.
			local matches to true.
			until matches = false {
				set length to length + 1.
				local regex to "^//[^\n]{" + (length + 1) + "}".
				set matches to input:matchespattern(regex).
			}
			return length + 2.
		}, true), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "LAZYGLOBAL", "lazyglobal\b", 10), LexerTokenDefSimple:
	
		//Math
	new( LexerTokenDefSimple,list("kos", "kpp"), "ACCESSOR", "->", 2), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "PLUSMINUS", "(\+|-)", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "MULT", "\*", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "DIV", "/", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "POWER", "\^", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "E", "e((?=\d)|\b)", 1), LexerTokenDefSimple:

		//Logic
	new( LexerTokenDefSimple,list("kos", "kpp"), "NOT", "not\b", 3), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "AND", "and\b", 3), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "OR", "or\b", 2), LexerTokenDefDynamic:
	new( LexerTokenDefDynamic,list("kos", "kpp"), "TRUEFALSE", "(true\b|\bfalse\b)", {
		declare parameter input.
		return choose 4 if input:startswith("t") else 5.
	}), LexerTokenDefDynamic:
	new( LexerTokenDefDynamic,list("kos", "kpp"), "COMPARATOR", "(<>|>=|<=|==|>|<)", {
		declare parameter input.
		if input:startswith("<>") { return 2. }
		if input:startswith(">=") { return 2. }
		if input:startswith("<=") { return 2. }
		return 1.
	}), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "ASSIGNMENT", "=", 1), LexerTokenDefSimple:

		//Instructions tokens
	new( LexerTokenDefSimple,list("kos", "kpp"), "SET", "set\b", 3), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "TO", "to\b", 2), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "IS", "is\b", 2), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "IF", "if\b", 2), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "ELSE", "else\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "UNTIL", "until\b", 5), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "STEP", "step\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "DO", "do\b", 2), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "LOCK", "lock\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "UNLOCK", "unlock\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "PRINT", "print\b", 5), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "AT", "at\b", 2), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "ON", "on\b", 2), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "TOGGLE", "toggle\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "WAIT", "wait\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "WHEN", "when\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "THEN", "then\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "OFF", "off\b", 3), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "STAGE", "stage\b", 5), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "CLEARSCREEN", "clearscreen\b", 11), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "ADD", "add\b", 3), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "REMOVE", "remove\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "LOG", "log\b", 3), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "BREAK", "break\b", 5), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "PRESERVE", "preserve\b", 8), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "DECLARE", "declare\b", 7), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "DEFINED", "defined\b", 7), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "LOCAL", "local\b", 5), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "GLOBAL", "global\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "PARAMETER", "parameter\b", 9), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "FUNCTION", "function\b", 8), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "RETURN", "return\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "SWITCH", "switch\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "COPY", "copy\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "FROM", "from\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "RENAME", "rename\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "VOLUME", "volume\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "FILE", "file\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "DELETE", "delete\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "EDIT", "edit\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "RUN", "run\b", 3), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "RUNPATH", "runpath\b", 7), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "RUNONCEPATH", "runoncepath\b", 11), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "ONCE", "once\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "COMPILE", "compile\b", 7), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "LIST", "list\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "REBOOT", "reboot\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "SHUTDOWN", "shutdown\b", 8), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "FOR", "for\b", 3), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "UNSET", "unset\b", 5), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "CHOOSE", "choose\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "MIX", "mix\b", 3), LexerTokenDefSimple:

		//Generic
	new( LexerTokenDefSimple,list("kpp"), "INCLUDE", "#include\b", 8), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "MIFNDEF", "#ifndef\b", 7), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "MIFDEF", "#ifdef\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "MELSE", "#else\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "MENDIF", "#endif\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "MDEFINE", "#define\b", 7), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "MUNDEF", "#undef\b", 6), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "MPRAGMAONCE", "#pragma once\b", 12), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "BRACKETOPEN", "\(", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "BRACKETCLOSE", "\)", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "CURLYOPEN", "\{", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "CURLYCLOSE", "\}", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "SQUAREOPEN", "\[", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "SQUARECLOSE", "\]", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "COMMA", ",", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "COLON", ":", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "IN", "in\b", 2), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "ARRAYINDEX", "#", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "ALL", "all\b", 3), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "CLASS", "class\b", 5), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "FUNC", "func\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "INIT", "init\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "SELF", "self\b", 4), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "VAR", "var\b", 3), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "AWAIT", "await\b", 5), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "ASYNC", "async\b", 5), LexerTokenDefSimple:

	new( LexerTokenDefSimple,list("kpp"), "NEW", "new\b", 3), LexerTokenDefDynamic:
	new( LexerTokenDefDynamic,list("kos", "kpp"), "DOUBLE", "\d[_\d]*\.\d[_\d]*", {
		declare parameter input.
		local prefixlength to -1.
		local matches to true.

		until matches = false {
			set prefixlength to prefixlength + 1.
			local regex to "^[_\d]{" + (prefixlength) + "}".
			set matches to input:matchespattern(regex).
		}
		local rest to input:substring(prefixlength, input:length - prefixlength).

		local postfixlength to -1.
		set matches to true.
		until matches = false {
			set postfixlength to postfixlength + 1.
			local regex to "^[_\d]{" + (postfixlength) + "}".
			set matches to rest:matchespattern(regex).
		}

		return prefixlength + postfixlength - 1.
	}), LexerTokenDefDynamic:
	new( LexerTokenDefDynamic,list("kos", "kpp"), "INTEGER", "\d[_\d]*", {
		// Try matching bigger identifiers until it no longer works
		declare parameter input.
		local length to -1.
		local matches to true.
		until matches = false {
			set length to length + 1.
			local regex to "^\d[_\d]{" + (length + 1) + "}".
			set matches to input:matchespattern(regex).
		}
		return length + 1.
	}), LexerTokenDefDynamic:
	new( LexerTokenDefDynamic,list("kos", "kpp"), "STRING", "@?\" + char(34) + "(\" + char(34) + "\" + char(34) + "|[^\" + char(34) + "])*\" + char(34), {
		declare parameter input.
		local iterator to input:iterator.
		iterator:next.

		local foundquote to false.
		local length to 0.
		until(false) {
			set length to length + 1.
			iterator:next.
			
			if (iterator:atend and not foundquote) {
				print "Assertion failure, unterminated string found.".
				return 99999.
			}
			if (foundquote) {
				if (iterator:atend or iterator:value <> char(34)) {
					return length.
				} else {
					set foundquote to false.
				}
			} else {
				if (iterator:value = char(34)) {
					set foundquote to true.
				}
			}
		}
	}), LexerTokenDefDynamic:
	new( LexerTokenDefDynamic,list("kos", "kpp"), "IDENTIFIER", "[_\p{L}]\w*", {
		// Try matching bigger identifiers until it no longer works
		declare parameter input.
		local length to -1.
		local matches to true.
		until matches = false {
			set length to length + 1.
			local regex to "^[_\p{L}]\w{" + (length + 1) + "}".
			set matches to input:matchespattern(regex).
		}
		return length + 1.
	}), LexerTokenDefDynamic:
	new( LexerTokenDefDynamic,list("kos", "kpp"), "FILEIDENT", "[_\p{L}]\w*(\.[_\p{L}]\w*)*", {
		// Try matching bigger identifiers until it no longer works
		declare parameter input.
		local length to -1.
		local matches to true.
		until matches = false {
			set length to length + 1.
			local regex to "^[_\p{L}][\w\.\p{L}]{" + (length + 1) + "}".
			set matches to input:matchespattern(regex).
		}
		return length + 1.
	}), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos"), "EOI", "\.", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kpp"), "EOI", "(\.|;)", 1), LexerTokenDefSimple:
	new( LexerTokenDefSimple,list("kos", "kpp"), "ATSIGN", "@", 1)
).
local  Kpp2Tokens to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    PROGRAM_INIT. set 
    self:subProc to  false.
    self:readLine(
    self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onRead"] to { parameter self,line.
    if line:istype("Boolean") {
      self:exit(
      self,0).
      return.
    }
    self:print(
    self,line). local lexer to  Lexer: new( Lexer,LexerDefs, "kpp", line, "stdin", true).
    until lexer:atend {
      print lexer:value:def + ": """ + lexer:value:text + """".
      lexer:next(
      lexer).
    }
    if not lexer:error:istype("Boolean") {
      self:print(
      self,lexer:error).
      self:exit(
      self,1).
      return.
    }

    self:readLine(

    self).}.
return  result.}):call(). set 
export to  Kpp2Tokens.