local _Callback to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_Callback").
set  result["new"] to { parameter type,target, method.
local self to prototype:copy.
set  self["type"] to  result.
    set self:call to  { parameter self, args.
      return target[method]:call(target, args).
    }.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call().
set _Callback:null to  lex("call", { parameter s,_. }).
global Callback to _Callback.
local CheckRoute to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","CheckRoute").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:pid to  id.
    if args:length <> 1 {
      output:append(output,"Usage: checkroute", Callback:null).
      set self:status to  1.
local __var177 to fs:loop. __var177:defer(__var177,resultCallback, id).
      return self.
    }
    set self:output to  output.
    set self:status to  -1.
    set self:streaming to  true.
    set self:running to  false.
    set self:interrupted to  false.
    set self:resultCallback to  resultCallback.
    set self:fs to  fs.
    set self:waypoints to  queue().
    set self:from to  false.
    set self:to to  false.
    set self:dir to  false.
    set self:resolution to  100.
    set self:maxGrade to  0.3.
    set self:recomputeLength to  40.
    set self:recomputeFrom to  false.
    set self:purgatory to  queue().
    set self:failed to  false.
    set self:arrows to  list().
    clearvecdraws().
    set input:signalHandler to  Callback:new(Callback,self, "onSignal").
    set self:file to  input.
    input:read(input,Callback:new(Callback,self, "onRead")).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["geoToStr"] to { parameter self,geo.
    return "G;" + round(geo:lat, 3) + ";" + round(geo:lng, 3).}.
set  prototype["run"] to { parameter self.
    if self:to:istype("Boolean") {
      set self:to to  self:waypoints:pop().
local __var178 to self:output. __var178:append(__var178,self:to[0], Callback:null).
    }
    if self:waypoints:length = 0 {
      if not self:streaming {
        if not self:recomputeFrom:istype("Boolean") {
          self:unwindRewrite(self).
        }
        until self:purgatory:length = 0 {
local __var179 to self:output. __var179:append(__var179,self:geoToStr(self,self:purgatory:pop()), Callback:null).
        }
        set self:status to  0.
        if self:failed {
          set self:status to  1.
        }
        set self:running to  false.
local __var180 to self:fs:loop. __var180:deregister(__var180,self).
        set self:interrupted to  true.
local __var181 to self:fs:loop. __var181:defer(__var181,self:resultCallback, self:pid).
      }
      return.
    } else {
      set self:from to  self:to.
      set self:to to  self:waypoints:pop().
      local geoFrom to  self:from[1].
      local geoTo to  self:to[1].
      local posFrom to  geoFrom:altitudeposition(geoFrom:terrainheight).
      local posTo to  geoTo:altitudeposition(geoTo:terrainheight).
      local fullDir to  posTo - posFrom.
      local stepcount to  floor(fullDir:mag / self:resolution).
      local step to  fullDir / stepcount.

      local curPos to  posFrom.
      local prevHeight to  geoFrom:terrainheight.
      until stepcount = 0 {
        set stepcount to  stepcount - 1.

        local destRaw to  curPos + step.
        local destGeo to  geoFrom:body:geopositionof(destRaw).
        local destHeight to  destGeo:terrainheight.
        local dest to  destGeo:altitudeposition(destHeight).
        local localDir to  dest - curPos.
        local grade to  abs(destHeight - prevHeight) / localDir:mag.

        local terrainUp to  (destGeo:altitudePosition(1) - destGeo:altitudePosition(0)):normalized.
        local flat1 to  vcrs(terrainUp, destGeo:position:normalized).
        local flat2 to  vcrs(terrainUp, flat1).
        local g1 to  body:geopositionof(destGeo:position + flat1).
        local g2 to  body:geopositionof(destGeo:position + flat2).
        local p0 to  destGeo:altitudePosition(destGeo:terrainheight).
        local p1 to  g1:altitudePosition(g1:terrainheight).
        local p2 to  g2:altitudePosition(g2:terrainheight).
        local normal to  vcrs((p1 - p0):normalized, (p2 - p0):normalized).
        local normalGrade to  1 - terrainUp * normal.

        if grade > self:maxGrade or normalGrade > self:maxGrade {
          if self:recomputeFrom:istype("Boolean") {
            set self:recomputeFrom to  self:purgatory:pop().
          }
          set self:failed to  true.
          self:purgatory:clear().
          self:arrows:add(VecDraw(curPos, dest - curPos, RGB(1, 0, 0), "", 1, true)).
        } else {
          self:purgatory:push(destGeo).
          if not self:recomputeFrom:istype("Boolean") {
            if self:purgatory:length = 2 * self:recomputeLength {
              self:unwindRewrite(self).
            }
          } else {
            if self:purgatory:length > self:recomputeLength {
local __var182 to self:output. __var182:append(__var182,self:geoToStr(self,self:purgatory:pop()), Callback:null).
            }
          }
        }

        set curPos to  dest.
        set prevHeight to  destHeight.
      }
    }}.
set  prototype["unwindRewrite"] to { parameter self.
    until self:purgatory:length <= self:recomputeLength {
      self:purgatory:pop().
    }
    local rewriteDest to  self:to[1].
    if self:purgatory:length > 0 {
      set rewriteDest to  self:purgatory:pop().
    }
local __var183 to self:output. __var183:append(__var183,"REPATH:" + self:geoToStr(self,self:recomputeFrom) + ":" + self:geoToStr(self,rewriteDest), Callback:null).
    
    local recomputeFrom to  self:recomputeFrom:altitudePosition(self:recomputeFrom:terrainheight).
    local recomputeTo to  rewriteDest:altitudePosition(rewriteDest:terrainheight).
    self:arrows:add(VecDraw(recomputeFrom, recomputeTo - recomputeFrom, RGB(1, 1, 0), "", 1, true)).
    set self:recomputeFrom to  false.}.
set  prototype["onSignal"] to { parameter self,signal.
    if self:interrupted
      return false.
    if signal <> "End"
      return false.
    set self:interrupted to  true.
    set self:status to  9.
    if self:running {
      set self:running to  false.
local __var184 to self:fs:loop. __var184:deregister(__var184,self).
    }
local __var185 to self:fs:loop. __var185:defer(__var185,self:resultCallback, self:pid).
    return true.}.
set  prototype["onRead"] to { parameter self,data.
    if self:interrupted
      return.
    if data:istype("Boolean") {
      set self:streaming to  false.
      //self->output->write("#showroute: read failed");
      //self->status = 2;
      //clearvecdraws();
      //self->fs->loop->defer(self->resultCallback, self->pid);
      return.
    }
    local lines to  data:split(char(10)).
    for line in lines {
      self:handleLine(self,line).
    }
    if self:streaming {
local __var186 to self:file. __var186:read(__var186,Callback:new(Callback,self, "onRead")).
    }}.
set  prototype["handleLine"] to { parameter self,data.
    if data:trim:startsWith("#")
      return.
    if data:trim:startsWith("G;") {
      local parts to  data:trim:split(";").
      if parts:length <> 3 {
        output:append(output,"#Bad waypoint: " + data, Callback:null).
      } else {
        local lat to  parts[1]:tonumber(0).
        local lng to  parts[2]:tonumber(0).
        local geo to  latlng(lat, lng).
        self:waypoints:push(list(
          data,
          geo
        )).
        if not self:running {
          set self:running to  true.
local __var187 to self:fs:loop. __var187:register(__var187,self).
        }
      }
    } else {
local __var188 to self:output. __var188:append(__var188,"#Unknown waypoint: " + data, Callback:null).
    }}.
set  prototype["onClose"] to { parameter self,_.
    if self:interrupted or not self:streaming
      return false.
    set self:streaming to  false.
    return true.}.
return  result.}):call().
set export to  CheckRoute.
