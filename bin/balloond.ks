local DelegateFile to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","DelegateFile").
set  result["new"] to { parameter type,fs, readDelegate, writeDelegate.
local self to prototype:copy.
set  self["type"] to  result.
    set self:fs to  fs.
    set self:readDelegate to  readDelegate.
    set self:writeDelegate to  writeDelegate.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getType"] to { parameter self. return "file".}.
set  prototype["getReadHandle"] to { parameter self,loop. return StaticReadFileHandle:new(StaticReadFileHandle,self, loop).}.
set  prototype["_readAll"] to { parameter self,callback.
local __var56 to self:fs:loop. __var56:defer(__var56,callback, self:readDelegate()).}.
set  prototype["write"] to { parameter self,data, callback.
    if not data:istype("Boolean")
      self:writeDelegate(data).
local __var57 to self:fs:loop. __var57:defer(__var57,callback, true).}.
set  prototype["append"] to { parameter self,data, callback.
    self:write(self,data, callback).}.
return  result.}):call().
local Balloond to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Balloond").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:pid to  id.
    set self:status to  0.
    set self:input to  input.
    set self:output to  output.
    set self:env to  env.
    set self:fs to  fs.
    set self:resultCallback to  resultCallback.
    set self:running to  false.
    set self:targetAlt to  3000.
    set self:targetSpeed to  0.
    set self:targetHeading to  90.
    set self:targetPitch to  0.
    set self:groundAltReference to  "1".

    set self:desiredVertPid to  PidLoop(0.02, 0, 0.4).
    set self:desiredVertPid:maxoutput to  1.
    set self:desiredVertPid:minoutput to  -1.

    set self:speedPid to  PidLoop(0.1, 0.00001, 0.1).
    set self:speedPid:maxoutput to  1.
    set self:speedPid:minoutput to  0.
    set self:pitchPid to  PidLoop(0.1, 0.001, 0.05).
    set self:pitchPid:maxoutput to  1.
    set self:pitchPid:minoutput to  -1.

    set self:engineYawPid to  PidLoop(0.005, 0, 0).
    set self:engineYawPid:maxoutput to  0.9.
    set self:engineYawPid:minoutput to  -0.9.
    set self:controlYawPid to  PidLoop(0.1, 0, 0).
    set self:controlYawPid:maxoutput to  1.
    set self:controlYawPid:minoutput to  -1.
    set self:engineYaw2Pid to  PidLoop(0.1, 0, 0).
    set self:engineYaw2Pid:maxoutput to  1.
    set self:engineYaw2Pid:minoutput to  -1.

    set self:balloons to  list().
    set self:engines to  list().

    local vec_facing to  ship:facing:vector.
    local max_lever to  0.
    for module in ship:modulesnamed("HLEnvelopePartModule") {
      local part to  module:part.

      local leverage to  (part:position - ship:position) * vec_facing.
      if abs(leverage) > max_lever {
        set max_lever to  abs(leverage).
      }

      self:balloons:add(lex(
        "part", part,
        "module", module,
        "lever", leverage
      )).
    }
    for balloon in self:balloons {
      set balloon:lever to  balloon:lever / max_lever.
    }
local __var58 to self:output. __var58:append(__var58,"Found " + self:balloons:length + " balloons", Callback:null).

    set self:engineSwitchTime to  6.
    set self:engineState to  "forward".
    set self:engineDoneAt to  0.
    local vec_right to  vcrs(ship:up:vector, ship:facing:vector):normalized.
    for engine in ship:engines {
      local module to  engine:modulesnamed("ModuleAnimateGeneric")[0].

      set engine:thrustlimit to  100.
      if module:hasevent("disable thrust reverser") {
        module:doevent("disable thrust reverser").
        set self:engineDoneAt to  time:seconds + self:engineSwitchTime.
      }

      local leverage to  engine:position * vec_right.

      self:engines:add(lex(
        "engine", engine,
        "module", module,
        "lever", leverage
      )).
    }

    set self:state to  "unknown".
    set self:exited to  false.

    
    set self:takeoffAlt to  3000.

    if args:length = 1 {
      self:help(self).
      return self.
    }
    if args[1] = "detach" or args[1] = "foreground" {
      self:startDaemon(self).
      if args[1] = "detach" {
        set self:exited to  true.
local __var59 to self:fs:loop. __var59:defer(__var59,self:resultCallback, self:pid).
      }
      return self.
    }

    self:help(self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["help"] to { parameter self.
local __var60 to self:output. __var60:append(__var60,"Usage: balloond <subcommand>", Callback:null).
local __var61 to self:output. __var61:append(__var61,"", Callback:null).
local __var62 to self:output. __var62:append(__var62,"Subcommands available:", Callback:null).
local __var63 to self:output. __var63:append(__var63," - detach      Runs the plane management software in the background)", Callback:null).
local __var64 to self:output. __var64:append(__var64," - foreground  Runs in foreground)", Callback:null).
    set self:status to  1.
    set self:exited to  true.
local __var65 to self:fs:loop. __var65:defer(__var65,self:resultCallback, self:pid).}.
set  prototype["startDaemon"] to { parameter self.
    local configDir to  MemoryFilesystemDir:new(MemoryFilesystemDir).
    configDir:registerFile(configDir,"state", DelegateFile:new(DelegateFile,self:fs, { return self:state. }, { parameter data. self:setState(self,data). })).
    configDir:registerFile(configDir,"targetAlt", DelegateFile:new(DelegateFile,self:fs, { return "" + self:targetAlt. }, { parameter data. set self:targetAlt to  data:tonumber(self:targetAlt). })).
    configDir:registerFile(configDir,"targetSpeed", DelegateFile:new(DelegateFile,self:fs, { return "" + self:targetSpeed. }, { parameter data. set self:targetSpeed to  data:tonumber(self:targetSpeed). })).
    configDir:registerFile(configDir,"targetHeading", DelegateFile:new(DelegateFile,self:fs, { return "" + self:targetHeading. }, { parameter data. set self:targetHeading to  data:tonumber(self:targetHeading). })).
    configDir:registerFile(configDir,"targetPitch", DelegateFile:new(DelegateFile,self:fs, { return "" + self:targetPitch. }, { parameter data. set self:targetPitch to  data:tonumber(self:targetPitch). })).
    configDir:registerFile(configDir,"maxPitch", DelegateFile:new(DelegateFile,self:fs, { return "" + self:maxPitch. }, { parameter data. set self:maxPitch to  data:tonumber(self:maxPitch). })).
    configDir:registerFile(configDir,"groundAltReference", DelegateFile:new(DelegateFile,self:fs, { return self:groundAltReference. }, { parameter data. set self:groundAltReference to  data:trim. })).
    configDir:registerFile(configDir,"altP", DelegateFile:new(DelegateFile,self:fs, { return "" + self:desiredVertPid:Kp. }, { parameter data. set self:desiredVertPid:Kp to  data:trim:toscalar(self:desiredVertPid:Kp). })).
    configDir:registerFile(configDir,"altI", DelegateFile:new(DelegateFile,self:fs, { return "" + self:desiredVertPid:Ki. }, { parameter data. set self:desiredVertPid:Ki to  data:trim:toscalar(self:desiredVertPid:Ki). })).
    configDir:registerFile(configDir,"altD", DelegateFile:new(DelegateFile,self:fs, { return "" + self:desiredVertPid:Kd. }, { parameter data. set self:desiredVertPid:Kd to  data:trim:toscalar(self:desiredVertPid:Kd). })).
    configDir:registerFile(configDir,"speedP", DelegateFile:new(DelegateFile,self:fs, { return "" + self:speedPid:Kp. }, { parameter data. set self:speedPid:Kp to  data:trim:toscalar(self:speedPid:Kp). })).
    configDir:registerFile(configDir,"speedI", DelegateFile:new(DelegateFile,self:fs, { return "" + self:speedPid:Ki. }, { parameter data. set self:speedPid:Ki to  data:trim:toscalar(self:speedPid:Ki). })).
    configDir:registerFile(configDir,"speedD", DelegateFile:new(DelegateFile,self:fs, { return "" + self:speedPid:Kd. }, { parameter data. set self:speedPid:Kd to  data:trim:toscalar(self:speedPid:Kd). })).
    configDir:registerFile(configDir,"pitchP", DelegateFile:new(DelegateFile,self:fs, { return "" + self:pitchPid:Kp. }, { parameter data. set self:pitchPid:Kp to  data:trim:toscalar(self:pitchPid:Kp). })).
    configDir:registerFile(configDir,"pitchI", DelegateFile:new(DelegateFile,self:fs, { return "" + self:pitchPid:Ki. }, { parameter data. set self:pitchPid:Ki to  data:trim:toscalar(self:pitchPid:Ki). })).
    configDir:registerFile(configDir,"pitchD", DelegateFile:new(DelegateFile,self:fs, { return "" + self:pitchPid:Kd. }, { parameter data. set self:pitchPid:Kd to  data:trim:toscalar(self:pitchPid:Kd). })).
    configDir:registerFile(configDir,"engineYawP", DelegateFile:new(DelegateFile,self:fs, { return "" + self:engineYawPid:Kp. }, { parameter data. set self:engineYawPid:Kp to  data:trim:toscalar(self:engineYawPid:Kp). })).
    configDir:registerFile(configDir,"engineYawI", DelegateFile:new(DelegateFile,self:fs, { return "" + self:engineYawPid:Ki. }, { parameter data. set self:engineYawPid:Ki to  data:trim:toscalar(self:engineYawPid:Ki). })).
    configDir:registerFile(configDir,"engineYawD", DelegateFile:new(DelegateFile,self:fs, { return "" + self:engineYawPid:Kd. }, { parameter data. set self:engineYawPid:Kd to  data:trim:toscalar(self:engineYawPid:Kd). })).
    configDir:registerFile(configDir,"takeoffAlt", DelegateFile:new(DelegateFile,self:fs, { return "" + self:takeoffAlt. }, { parameter data. set self:takeoffAlt to  data:trim:toscalar(self:takeoffAlt). })).
local __var66 to self:fs. __var66:mount(__var66,configDir, "/sys/class/plane").
    set self:input:signalHandler to  Callback:new(Callback,self, "onSignal").

    local northPole to  latlng(90,0).
    set self:targetHeading to  mod(360 - northPole:bearing, 360).

    if ship:status = "LANDED" {
      set self:state to  "landed".
    }
    if ship:status = "FLYING" {
      set self:state to  "heading".
      set self:targetAlt to  alt:radar.
      set self:targetSpeed to  ship:velocity:surface:mag.
    }
local __var67 to self:fs:loop. __var67:register(__var67,self).
    set self:running to  true.}.
set  prototype["stopDaemon"] to { parameter self.
    if not self:running
      return.

    self:setState(self,"none").
local __var68 to self:fs. __var68:unmount(__var68,"/sys/class/plane").
    if not self:exited {
      set self:exited to  true.
local __var69 to self:fs:loop. __var69:deregister(__var69,self).
    }}.
set  prototype["setState"] to { parameter self,state.
    set self:state to  state.
    set ship:control:neutralize to  true.
    unlock throttle.
    unlock wheelsteering.
    rcs off.
    brakes off.
    if self:haskey("start" + state) {
      self["start" + state]:call(self).
    }}.
set  prototype["onSignal"] to { parameter self,signal.
    if not self:running
      return false.
    if signal = "End" {
      self:stopDaemon(self).
      return true.
    }}.
set  prototype["run"] to { parameter self.
    if self:haskey(self:state) {
      self[self:state]:call(self).
    }}.
set  prototype["ensureEngineState"] to { parameter self,state.
    if self:engineState = state {
      if self:engineDoneAt <> 0 {
        if time:seconds > self:engineDoneAt {
          set self:engineDoneAt to  0.
          return false.
        }
        return true.
      }
      return false.
    }

    set self:engineState to  state.
    set self:engineDoneAt to  time:seconds + self:engineSwitchTime.
    local new_throttle to  0.
    lock throttle to new_throttle.
    self:speedPid:reset().

    if state = "forward" {
      for engine in self:engines {
        set engine:engine:thrustlimit to  100.
        if engine:module:hasevent("disable thrust reverser")
          engine:module:doevent("disable thrust reverser").
      }
    }
    if state = "reverse" {
      for engine in self:engines {
        set engine:engine:thrustlimit to  100.
        if engine:module:hasevent("enable thrust reverser")
          engine:module:doevent("enable thrust reverser").
      }
    }
    if state = "rotateR" {
      for engine in self:engines {
        set engine:engine:thrustlimit to  100.
        if engine:lever > 0 {
          if engine:module:hasevent("enable thrust reverser")
            engine:module:doevent("enable thrust reverser").
        } else {
          if engine:module:hasevent("disable thrust reverser")
            engine:module:doevent("disable thrust reverser").
        }
      }
    }
    if state = "rotateL" {
      for engine in self:engines {
        set engine:engine:thrustlimit to  100.
        if engine:lever < 0 {
          if engine:module:hasevent("enable thrust reverser")
            engine:module:doevent("enable thrust reverser").
        } else {
          if engine:module:hasevent("disable thrust reverser")
            engine:module:doevent("disable thrust reverser").
        }
      }
    }

    return true.}.
set  prototype["balloonControl"] to { parameter self.
    local desiredVertSpeed to  (self:targetAlt - alt:radar) / 20.
    if desiredVertSpeed > 100 {
      set desiredVertSpeed to  100.
    }
    if desiredVertSpeed < -10 {
      set desiredVertSpeed to  -10.
    }

    if self:targetAlt = 0 {
      set desiredVertSpeed to  min(-1, desiredVertSpeed).
    }

    set self:desiredVertPid:setpoint to  desiredVertSpeed.
    
    local vertSpeed to  ship:velocity:surface * ship:up:vector.
    local buoyancy_increase to  self:desiredVertPid:update(time:seconds, vertSpeed).
    local pitch to  ship:facing:vector * up:vector.

    set self:pitchPid:setpoint to  self:targetPitch.
    local pitch_increase to  self:pitchPid:update(time:seconds, pitch).

    //print "buoy: " + buoyancy_increase + " " at (0, 0);
    //print "pitch: " + pitch_increase + " " at (0, 1);
    //print "error: " + (self->targetPitch - pitch) + " " at (0, 2); 
    
    for balloon in self:balloons {
      local delta to  buoyancy_increase + balloon:lever * pitch_increase.

      if delta > 0 {
        if delta < 0.1 {
          if delta * 10 - random() > 0 {
            balloon:module:doaction("buoyancy +", true).
          }
        } else {
          if delta - random() > 0 {
            balloon:module:doaction("buoyancy ++", true).
          } else {
            balloon:module:doaction("buoyancy +", true).
          }
        }
      } else {
        if delta > -0.1 {
          if delta * -10 - random() > 0 {
            balloon:module:doaction("buoyancy -", true).
          }
        } else {
          if delta + random() < 0 {
            balloon:module:doaction("buoyancy --", true).
          } else {
            balloon:module:doaction("buoyancy -", true).
          }
        }
      }
    }}.
set  prototype["startTakeoff"] to { parameter self.
    brakes off.
    set self:targetAlt to  self:takeoffAlt.}.
set  prototype["takeoff"] to { parameter self.
    self:balloonControl(self).
    if alt:radar > 10 {
      gear off.
      self:setState(self,"heading").
      set self:targetAlt to  self:takeoffAlt.
      set self:targetSpeed to  0.
    }}.
set  prototype["heading"] to { parameter self.
    local desiredForward to  heading(self:targetHeading, 0, 0):vector.
    local desiredGeo to  ship:body:geopositionof(ship:position + desiredForward * 100).

    if ship:status <> "LANDED" {
      self:balloonControl(self).
    }

    local var_steering to  desiredGeo:bearing.
    local cur_speed to  velocity:surface * ship:facing:vector.
    //print "state: " + self->engineState + " " at (0, 3);
    //print "steering: " + var_steering + " " at (0, 4);
    //print "speed: " + cur_speed + " " at (0, 5);
    
    set ship:control:yaw to  -self:controlYawPid:update(time:seconds, var_steering).

    if self:engineDoneAt > 0 {
      lock throttle to 0.
      if time:seconds > self:engineDoneAt {
        set self:engineDoneAt to  0.
      } else {
        return.
      }
    }

    if self:targetSpeed = 0 and cur_speed < 10 {
      lock throttle to 0.
      return.
    }

    if abs(var_steering) > 30 {
      if cur_speed > 8 {
        if self:engineState = "reverse" {
          set self:speedPid:setpoint to  0.
          set self:speedPid:minoutput to  0.
          set self:speedPid:maxoutput to  1.
          local new_throttle to  self:speedPid:update(time:seconds, -cur_speed).
          lock throttle to new_throttle.
        } else {
          set self:engineState to  "reverse".
          set self:engineDoneAt to  time:seconds + self:engineSwitchTime.
          for engine in self:engines {
            set engine:engine:thrustlimit to  100.
            if engine:module:hasevent("enable thrust reverser") {
              engine:module:doevent("enable thrust reverser").
            }
          }
        }
      } else {
        lock throttle to 0.
        if var_steering > 0 {
          if self:engineState = "rotateR" {
            set self:engineYawPid:maxoutput to  0.
            set self:engineYawPid:minoutput to  -1.
            local new_throttle to  -self:engineYawPid:update(time:seconds, var_steering).
            lock throttle to new_throttle.
          } else {
            set self:engineState to  "rotateR".
            set self:engineDoneAt to  time:seconds + self:engineSwitchTime.
            for engine in self:engines {
              set engine:engine:thrustlimit to  100.
              if engine:lever > 0 {
                if engine:module:hasevent("enable thrust reverser")
                  engine:module:doevent("enable thrust reverser").
              } else {
                if engine:module:hasevent("disable thrust reverser")
                  engine:module:doevent("disable thrust reverser").
              }
            }
          }
        } else {
          if self:engineState = "rotateL" {
            set self:engineYawPid:maxoutput to  1.
            set self:engineYawPid:minoutput to  0.
            local new_throttle to  self:engineYawPid:update(time:seconds, var_steering).
            lock throttle to new_throttle.
          } else {
            set self:engineState to  "rotateL".
            set self:engineDoneAt to  time:seconds + self:engineSwitchTime.
            for engine in self:engines {
              set engine:engine:thrustlimit to  100.
              if engine:lever < 0 {
                if engine:module:hasevent("enable thrust reverser")
                  engine:module:doevent("enable thrust reverser").
              } else {
                if engine:module:hasevent("disable thrust reverser")
                  engine:module:doevent("disable thrust reverser").
              }
            }
          }
        }
      }
    } else {
      if self:engineState = "forward" {
        local leftPercentage to  1.
        local rightPercentage to  1.
        local engineSteering to  self:engineYaw2Pid:update(time:seconds, var_steering).
        if engineSteering < 0 {
          set leftPercentage to  1 + engineSteering.
        } else {
          set rightPercentage to  1 - engineSteering.
        }
        for engine in self:engines {
          if engine:lever > 0 {
            set engine:engine:thrustlimit to  100 * leftPercentage.
          } else {
            set engine:engine:thrustlimit to  100 * rightPercentage.
          }
        }

        set self:speedPid:setpoint to  self:targetSpeed.
        set self:speedPid:minoutput to  -0.1.
        set self:speedPid:maxoutput to  1.
        local new_throttle to  self:speedPid:update(time:seconds, cur_speed).
        if new_throttle <= -0.09 {
          brakes on.
        } else {
          brakes off.
        }
        lock throttle to new_throttle.
      } else {
        set self:engineState to  "forward".
        set self:engineDoneAt to  time:seconds + self:engineSwitchTime.
        for engine in self:engines {
          set engine:engine:thrustlimit to  100.
          if engine:module:hasevent("disable thrust reverser")
            engine:module:doevent("disable thrust reverser").
        }
      }
    }}.
set  prototype["startDitch"] to { parameter self.
    set self:groundAltReference to  "1".
    set self:targetSpeed to  0.
    set self:landingDebounce to  0.
    gear on.}.
set  prototype["ditch"] to { parameter self.
    local cur_speed to  velocity:surface * ship:facing:vector.
    if cur_speed > 2 {
      if self:ensureEngineState(self,"reverse") {
        return.
      }

      set self:speedPid:setpoint to  0.
      local new_throttle to  self:speedPid:update(time:seconds, -cur_speed).
      lock throttle to new_throttle.
    }
    if cur_speed < -2 {
      if self:ensureEngineState(self,"forward") {
        return.
      }

      set self:speedPid:setpoint to  0.
      local new_throttle to  self:speedPid:update(time:seconds, cur_speed).
      lock throttle to new_throttle.
    }

    if ship:status = "LANDED" {
      if self:landingDebounce = 0 {
        set self:landingDebounce to  time:seconds + 10.
      } else {
        if self:landingDebounce < time:seconds {
          for balloon in self:balloons {
            balloon:module:doaction("buoyancy --", true).
          }
          self:ensureEngineState(self,"forward").
          self:setState(self,"none").
          set ship:control:pilotmainthrottle to  0.
          brakes on.
          return.
        }
      }
    } else {
      set self:landingDebounce to  0.
    }

    set self:targetAlt to  0.
    
    self:balloonControl(self).}.
return  result.}):call().

set export to  Balloond.
