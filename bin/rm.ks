local  _Callback to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,target, method.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:call to  { parameter self, args.
      return target[method]:call(target, args).
    }.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
_Callback:null to  lex("call", { parameter s,_. }).
global Callback to _Callback.
local  Rm to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id. set 
    self:status to  0.
    if args:length < 2 {
      self:help(
      self).
      return self.
    } set 

    self:resultCallback to  resultCallback. set 
    self:fs to  fs. set 
    self:env to  env. set 
    self:output to  output. set 

    self:todo to  Queue(). local sources to  args:sublist(1, args:length - 1).
    for s in sources {
      self:todo:push(s).
    }

    self:unlinkNext(

    self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["help"] to { parameter self.
local  __var0 to 
      self:output. __var0:append( __var0,"Usage: rm <file> [<file> ...]", Callback:null). set 
      self:status to  1.
local  __var1 to 
      self:fs:loop. __var1:defer( __var1,self:resultCallback, self:pid).}.
set  prototype["unlinkNext"] to { parameter self.
    if self:todo:empty { set 
      self:status to  0.
local  __var2 to 
      self:fs:loop. __var2:defer( __var2,self:resultCallback, self:pid).
      return.
    } set 

    self:path to  self:todo:pop().
    if not self:path:startsWith("/") { local pathParts to  self:env:CWD:split("/").
      for part in self:path:split("/") {
        pathParts:add(part).
      } set 
      self:path to  pathParts:join("/").
    } local parts to  self:path:split("/"). set 
    self:folder to  parts:sublist(0, parts:length - 1):join("/"). set 
    self:filename to  parts[parts:length - 1].
local  __var3 to 

    self:fs. __var3:open( __var3,self:folder, Callback: new( Callback,self, "onOpenFolder")).}.
set  prototype["onOpenFolder"] to { parameter self,file.
    if file:istype("Boolean") or file:getType( file) <> "dir" {
local  __var4 to 
      self:output. __var4:append( __var4,"Unable to find file " + self:folder + "/" + self:filename, Callback:null). set 
      self:status to  2.
local  __var5 to 
      self:fs:loop. __var5:defer( __var5,self:resultCallback, self:pid).
      return.
    }
    
    file:unlink(
    
    file,self:filename, Callback: new( Callback,self, "onUnlink")).}.
set  prototype["onUnlink"] to { parameter self,result.
    if not result {
local  __var6 to 
      self:output. __var6:append( __var6,"Unable to remove file " + self:folder + "/" + self:filename, Callback:null). set 
      self:status to  3.
local  __var7 to 
      self:fs:loop. __var7:defer( __var7,self:resultCallback, self:pid).
      return.
    }

    self:unlinkNext(

    self).}.
return  result.}):call(). set 
export to  Rm.