#include "0:/kosos/stdlib.kpp"
#include "0:/kpp/redux/parser.kpp"
#include "0:/kpp/redux/bottomUpStrategy.kpp"
#include "0:/kpp/redux/ParserDefs/ProgramNode.kpp"
#include "0:/kpp/redux/NodePrinter.kpp"

#define UNSAFE_ACCESSOR 1

class Kpp2Ks {
  mix Program

  init(id, input, output, args, env, fs, resultCallback) {
    self->progSetup(id, input, output, args, env, fs, resultCallback);

    if args:length < 2 {
      print("Usage:");
      print("kpp2ks <filename>");
      self->exit(1);
      return self;
    }

    self->priority = 50;
    if env:haskey("PRIORITY") {
      self->priority = env:PRIORITY:toscalar(50);
    }
    
    self->file = args[1];
    if not self->file:startswith("/") {
      self->file = env:CWD + self->file;
    }
    fs->open(self->file, new Callback(self, "onOpenMain"));

    self->include_filename = false;
    self->stage = "parse";
  }

  func onOpenMain(file) {
    if file:istype("Boolean") {
      print("Unable to open " + self->file + ", file not found.");
      self->exit(2);
      return;
    }

    file->read(new Callback(self, "onReadMain"));
  }

  func onReadMain(data) {
    if data:istype("Boolean") {
      print("Unable to open " + self->file + ", read error.");
      self->exit(3);
      return;
    }

    var lexer = new Lexer(LexerDefs, "kpp", data, self->file, false);
    self->parser = new Parser(lexer->window, LexerDefs, "kpp", {
      declare parameter filename.
      self->print("Compiling: " + filename);
      
      self->include_filename = filename;

      if not filename:startswith("/") {
        filename = self->env:CWD + filename;
      }

      self->fs->open(filename, new Callback(self, "onOpenInclude"));
      self->stop();
    }, ProgramNode, lex("KPP", "2"));

    self->start();
  }
  
  func onOpenInclude(file) {
    if file:istype("Boolean") {
      self->parser->onFileRead(self->include_filename, false);
      self->start();
      return;
    }
    file->read(new Callback(self, "onReadInclude"));
  }

  func onReadInclude(data) {
    self->parser->onFileRead(self->include_filename, data);
    self->start();
  }

  func run() {
    if self->stage = "parse"
      self->parse();
    if self->stage = "simplify"
      self->simplify();
    if self->stage = "desugar"
      self->desugar();
    if self->stage = "print_stage"
      self->print_stage();
  }

  func parse() {
    for i in range(self->priority) {
      if not self->parser->atend {
        self->parser->next();
      }
    }
    if not self->parser->atend {
      return;
    }
    
    if self->parser->value:istype("Boolean") {
      print("Parse failed:");
      print(self->parser->errors);
      self->exit(4);
      return;
    }

    self->stage = "simplify";
    self->simplifier = new BottomUpStrategy(self->parser->value, "simplify", false);
    self->parser = false;
  }

  func simplify() {
    for i in range(self->priority) {
      if not self->simplifier->atend {
        self->simplifier->next();
      }
    }
    if not self->simplifier->atend {
      return;
    }

    self->stage = "desugar";
    self->async_result = lex("errors", list());
    self->desugarer = new BottomUpStrategy(self->simplifier->value, "desugar", self->async_result);
    self->simplifier = false;
  }

  func desugar() {
    for i in range(self->priority) {
      if not self->desugarer->atend and self->async_result:errors:length = 0 {
        self->desugarer->next();
      }
    }

    if self->async_result:errors:length > 0 {
      print(self->async_result);
      self->exit(5);
      return;
    }

    if not self->desugarer->atend {
      return;
    }

    self->printer = new NodePrinter(self->desugarer->value, { parameter str. self->print(str). });
    self->desugarer = false;
    self->stage = "print_stage";
  }

  func print_stage() {
    if not self->printer->done {
      self->printer->next();
      return;
    }

    self->exit(0);
  }
}
export = Kpp2Ks;

#undef UNSAFE_ACCESSOR