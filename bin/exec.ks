local Exec to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Exec").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).

    set self:stage to  "coast".
    set self:rotationTime to  40.
    set self:forced to  false.
    if args:length >= 2 and args[1] = "force" {
      set self:forced to  true.
    }
    self:print(self,"Coasting").

    if not hasnode {
      self:print(self,"No next node, unable to execute.").
      self:exit(self,1).
    } else {
      if not self:forced and nextnode:eta < self:rotationTime * 2 {
        self:print(self,"Not enough time to execute.").
        self:exit(self,2).
      } else {
        set self:node to  nextnode.
        local acceleration to  ship:availablethrust / ship:mass.
        if acceleration = 0 {
          self:print(self,"No engines.").
          self:exit(self,3).
          return self.
        }
        set self:burntime to  nextnode:deltav:mag / acceleration.

        sas off.
        lock steering to self:node:deltav:normalized.

        self:start(self).
        //kuniverse:timewarp:warpto(self->node:time - self->rotationTime - self->burntime);
      }
    }
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["run"] to { parameter self.
    self[self:stage](self).}.
set  prototype["coast"] to { parameter self.
    if (time:seconds < self:node:time - self:rotationTime - self:burntime / 2)
      return.
    set self:stage to  "align".
    self:print(self,"align").
    rcs on.}.
set  prototype["align"] to { parameter self.
    if time:seconds < self:node:time - self:burntime / 2
      return.
    set self:stage to  "thrust".
    self:print(self,"thrust").}.
set  prototype["thrust"] to { parameter self.
    local alignment to  self:node:deltav:normalized * ship:facing:vector - 0.7.
    lock throttle to self:node:deltav:mag * alignment / 10.

    if self:node:deltav:mag > 0.1
      return.

    remove self:node.
    
    unlock throttle.
    unlock steering.
    rcs off.
    //kuniverse:timewarp:warpto(time:seconds + 5);
    set self:stage to  "none".
    
    self:print(self,"Node done").
    self:exit(self,0).}.
set  prototype["none"] to { parameter self.
    print("deregister failed").}.
set  prototype["deconstruct"] to { parameter self,_.
    unlock throttle.
    unlock steering.
    rcs off.}.
return  result.}):call().
set export to  Exec.