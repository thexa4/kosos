#include "0:/kosos/stdlib.kpp"

class MissionCtl {
  mix Program

  init(id, input, output, args, env, fs, resultCallback) {
    self->progSetup(id, input, output, args, env, fs, resultCallback);
    
    self->services = lex();
    self->servicePidLookup = lex();
    self->serviceTodoNames = lex();
    self->curProg = false;

    var command = "list";
    if args:length > 1 {
      command = args[1];
    }
    if command = "list" {
      self->open("/var/spool/mission", new Callback(self, "onOpenListSpool"));
    } else if command = "start" {
      self->start_server();
    } else if command = "queue" {
      self->create_job("run" + char(10) + args:sublist(2, args:length - 2):join(" "));
    } else if command = "service" {
      if args:length < 3 {
        self->help();
        self->exit(1);
        return self;
      }
      var subcommand = args[2];
      if subcommand = "start" {
        self->create_job("service start" + char(10) + args:sublist(3, args:length - 3):join(" "));
      } else if subcommand = "stop" {
        self->create_job("service stop" + char(10) + args:sublist(3, args:length - 3):join(" "));
      } else {
        self->help();
        self->exit(1);
      }
    } else if command = "reset" {
      self->open("/var/spool/mission", new Callback(self, "onOpenResetSpool"));
    } else {
      self->help();
      if command = "help" {
        self->exit(0);
      } else {
        self->exit(1);
      }
      return self;
    }
  }

  func help() {
    self->print("Usage:");
    self->print("  missionctl [list] - Shows remaining mission items");
    self->print("  missionctl help - Show this help");
    self->print("  missionctl start - Starts misison executor");
    self->print("  missionctl queue <command> - Queues a command");
    self->print("  missionctl service start <command> - Queues a service to be started");
    self->print("  missionctl service stop <command> - Queues a service to be stopped");
    self->print("  missionctl reset - Clears mission queue");
    self->print("");
    self->print("Maintains a queue of things to do. Assumes commands can be restarted at will until they exit.");
    self->print("Services have 5 seconds to initialize.");
  }

  func self:gen_filename() {
    var timepart = ("" + round(time:seconds * 10)):padleft(11):replace(" ", "0");
    var randpart = "" + floor(random() * 99);
    return timepart + "-" + randpart;
  }

  func create_job(contents) {
    self->jobdef = contents;
    self->fs->create("/var/spool/mission/" + self->type->gen_filename(), new Callback(self, "onCreateTaskfile"));
  }

  func onCreateTaskfile(file) {
    if file:istype("Boolean") {
      self->print("Unable to create task, does /var/spool/mission exist?");
      self->exit(2);
      return;
    }

    file->write(self->jobdef, new Callback(self, "ensureWriteAndExit"));
  }

  func ensureWriteAndExit(result) {
    if not result {
      self->print("Write failed");
      self->exit(3);
    } else {
      self->exit(0);
    }
  }

  func onOpenListSpool(dir) {
    if dir:istype("Boolean") or dir->getType() <> "dir" {
      self->print("# no tasks queued, system not configured");
      self->exit(0);
      return;
    }

    dir->read(new Callback(self, "onReadListSpool"));
  }
  func onReadListSpool(items) {
    if items:istype("Boolean") {
      self->print("# Unable to read spool directory");
      self->exit(4);
      return;
    }

    if items:length = 0 {
      self->print("# no tasks queued");
      self->exit(0);
      return;
    }

    self->listTodo = queue();
    for item in items {
      self->listTodo:push(item);
    }
    self->openNextListSpool();
  }
  func openNextListSpool() {
    if self->listTodo:length = 0 {
      self->exit(0);
      return;
    }
    self->open("/var/spool/mission/" + self->listTodo:pop(), new Callback(self, "onFileOpenListSpool"));
  }
  func onFileOpenListSpool(file) {
    if file:istype("Boolean") or file->getType() <> "file" {
      self->print("# broken item 1");
      self->openNextListSpool();
      return;
    }
    self->listCurFile = file;
    file->read(new Callback(self, "onFileReadTypeListSpool"));
  }
  func onFileReadTypeListSpool(line) {
    if line:istype("Boolean") {
      self->print("# broken item 2");
      self->openNextListSpool();
      return;
    }
    self->listCurType = line;
    self->listCurFile->read(new Callback(self, "onFileReadCommandListSpool"));    
  }
  func onFileReadCommandListSpool(line) {
    if line:istype("Boolean") {
      self->print("# broken item 3");
      self->openNextListSpool();
      return;
    }

    if self->listCurType = "run" {
      self->print(line);
    } else if self->listCurType = "service start" {
      self->print(line + " # Service start");
    } else if self->listCurType = "service stop" {
      self->print("# Stop service: " + line);
    } else {
      self->print("# invalid command");
    }
    self->openNextListSpool();
  }

  func onOpenResetSpool(dir) {
    if dir:istype("Boolean") or dir->getType() <> "dir" {
      self->exit(0);
      return;
    }

    self->resetDirectory = dir;
    dir->read(new Callback(self, "onReadResetSpoolDir"));
  }
  func onReadResetSpoolDir(items) {
    if items:istype("Boolean") or items:length = 0 {
      self->exit(0);
      return;
    }

    self->resetTodoLeft = items:length;
    self->resetSucces = true;
    for item in items {
      self->resetDirectory->unlink(item, new Callback(self, "onUnlinkReset"));
    }
  }
  func onUnlinkReset(result) {
    self->resetSucces = self->resetSucces and result;
    self->resetTodoLeft = self->resetTodoLeft - 1;

    if self->resetTodoLeft <= 0 {
      if self->resetSucces {
        self->exit(0);
      } else {
        self->print("# Unlink failed");
        self->exit(1);
      }
    }
  }

  func start_server() {
    self->open("/var/spool/mission", new Callback(self, "onServerStartDirOpen"));
  }
  func onServerStartDirOpen(dir) {
    if dir:istype("Boolean") or dir->getType() <> "dir" {
      self->print("System not configured for missions. Please create /var/spool/mission folder.");
      self->exit(2);
      return;
    }
    self->confdir = dir;
    self->executedTodos = list();
    self->currentTodoName = false;

    dir->read(new Callback(self, "onServerReadConfDir"));
  }
  func onServerReadConfDir(items) {
    if items:istype("Boolean") {
      self->print("Unable to get tasks");
      self->exit(2);
      return;
    }

    if items:length = 0 {
      self->sleepContinuation = "readConfDir";
      self->fs->loop->sleep(self, 10);
      return;
    }

    for item in items {
      if not self->executedTodos:contains(item) {
        self->currentTodoName = item;
        self->open("/var/spool/mission/" + item, new Callback(self, "onServerOpenTodo"));
        return;
      }
    }
  }
  func onServerOpenTodo(file) {
    if file:istype("Boolean") or file->getType() <> "file" {
      self->print("Bad todo " + self->currentTodoName);
      self->executedTodos:add(self->currentTodoName);
      self->confdir->read(new Callback(self, "onServerReadConfDir"));
      return;
    }

    self->currentTodoFile = file;
    file->read(new Callback(self, "onServerReadTodoType"));
  }
  func onServerReadTodoType(typeString) {
    if typeString:istype("Boolean") {
      self->print("Todo is truncated: " + self->currentTodoName);
      self->executedTodos:add(self->currentTodoName);
      self->confdir->read(new Callback(self, "onServerReadConfDir"));
      return;
    }

    if typeString = "run" {
      self->serverRunType = typeString;
    } else if typeString = "service start" {
      self->serverRunType = typeString;
    } else if typeString = "service stop" {
      self->serverRunType = typeString;
    } else {
      self->print("Todo " + self->currentTodoName + " has unknown action type: " + typeString);
      self->executedTodos:add(self->currentTodoName);
      self->confdir->read(new Callback(self, "onServerReadConfDir"));
      return;
    }

    self->currentTodoFile->read(new Callback(self, "onServerReadTodoCommand"));
  }
  func onServerReadTodoCommand(commandString) {
    if commandString:istype("Boolean") {
      self->print("Todo is truncated: " + self->currentTodoName);
      self->executedTodos:add(self->currentTodoName);
      self->confdir->read(new Callback(self, "onServerReadConfDir"));
      return;
    }

    var truncatedCommand = commandString:trim;
    if self->serverRunType = "service stop" {
      if self->services:haskey(truncatedCommand) {
        self->services[truncatedCommand]->onSignal("End");

        var todoItem = self->serviceTodoNames[truncatedCommand];
        self->confdir->unlink(todoItem, new Callback(self, "onServerServiceRemoveStartfile"));
        return;
      }

      self->print("Unable to find service to stop: " + truncatedCommand);
      self->print("" + self->services:keys);
      self->executedTodos:add(self->currentTodoName);
      self->confdir->read(new Callback(self, "onServerReadConfDir"));
      return;
    }

    if self->serverRunType = "service start" {
      if self->services:haskey(truncatedCommand) {
        self->print("Unable to start service, already running: " + truncatedCommand);
        self->executedTodos:add(self->currentTodoName);
        self->confdir->read(new Callback(self, "onServerReadConfDir"));
      } else {
        self->currentCommand = truncatedCommand;
        self->fs->exec(truncatedCommand, self->env, self->input, self->output, new Callback(self, "onServerServiceExit"), new Callback(self, "onServerServiceStart"));
        return;
      }
    }

    if self->serverRunType = "run" {
      self->currentCommand = truncatedCommand;
      self->fs->exec(truncatedCommand, self->env, self->input, self->output, new Callback(self, "onServerProgExit"), new Callback(self, "onServerProgStart"));
      return;
    }

    // Unknown type?
    self->exit(255);
  }
  func onServerServiceRemoveStartfile(result) {
    if not result {
      self->print("Unable to remove service start file: " + self->currentCommand);
      self->sleepContinuation = "readConfDir";
      self->fs->loop->sleep(self, 10);
      return;
    }

    self->confdir->unlink(self->currentTodoName, new Callback(self, "onServerServiceRemoveStopfile"));
  }
  func onServerServiceRemoveStopfile(result) {
    self->executedTodos:add(self->currentTodoName);
    self->confdir->read(new Callback(self, "onServerReadConfDir"));
  }
  func onServerServiceStart(prog) {
    if prog:istype("Boolean") {
      self->print("Service start failed: " + self->currentCommand);
      self->sleepContinuation = "readConfDir";
      self->fs->loop->sleep(self, 10);
      return;
    }
    self->print("Service start: " + self->currentCommand);
    prog:___MissionCtlServiceTag = self->currentCommand;
    self->services:add(self->currentCommand, prog);
    self->servicePidLookup:add(prog->pid, prog);
    self->serviceTodoNames:add(self->currentCommand, self->currentTodoName);
    self->executedTodos:add(self->currentTodoName);
    self->sleepContinuation = "readConfDir";
    self->fs->loop->sleep(self, 5);
  }
  func onServerServiceExit(pid) {
    if not self->servicePidLookup:haskey(pid) {
      self->print("Unknown service exit");
      return;
    }
    var prog = self->servicePidLookup[pid];
    self->servicePidLookup:remove(pid);
    if not prog:haskey("___MissionCtlServiceTag") {
      self->print("Service is missing service tag");
      return;
    }
    self->print("Service stop: " + prog:___MissionCtlServiceTag);
    if self->services:haskey(prog:___MissionCtlServiceTag) {
      self->services:remove(prog:___MissionCtlServiceTag);
    }
  }

  func onServerProgStart(prog) {
    if prog:istype("Boolean") {
      self->print("Prog start failed: " + self->currentCommand);
      self->sleepContinuation = "readConfDir";
      self->fs->loop->sleep(self, 10);
      return;
    }

    self->print("Run: " + self->currentCommand);
    self->curProg = prog;
  }
  func onServerProgExit(prog) {
    self->executedTodos:add(self->currentTodoName);
    self->print("Done: " + self->currentCommand);
    self->curProg = false;
    if not self->interrupted {
      self->confdir->unlink(self->currentTodoName, new Callback(self, "onServerUnlinkTodo"));
    }
  }
  func onServerUnlinkTodo(result) {
    if self->interrupted {
      return;
    }
    self->confdir->read(new Callback(self, "onServerReadConfDir"));
  }
  func onSignal(signal) {
    if signal = "End" {
      self->exit(9);
      if not self->curProg:istype("Boolean") {
        self->curProg->onSignal(signal);
      }
      for service in self->services:values {
        service->onSignal(signal);
      }
      return true;
    }
    return false;
  }

  func run() {
    if self->sleepContinuation = "readConfDir" {
      self->sleepContinuation = "";
      self->confdir->read(new Callback(self, "onServerReadConfDir"));
      return;
    }
    self->fs->loop->deregister(self);
  }
}
export = MissionCtl;
