local  ScienceDaemon to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    self:progSetup(
    self,id, input, output, args, env, fs, resultCallback). set 

    self:check_interval to  60.
    if self:env:haskey("SCIENCE_INTERVAL") and env:SCIENCE_INTERVAL:toscalar(0) > 0 { set 
      self:check_interval to  env:SCIENCE_INTERVAL:toscalar(0).
    } set 

    self:last_check to  time:seconds. set 
    self:min_science to  0.1. set 

    self:lab_module to  false.
    for module in ship:modulesnamed("ModuleScienceLab") { set 
      self:lab_module to  module.
    } set 
    self:container_modules to  false.
    for module in ship:modulesnamed("ModuleScienceContainer") {
      // Command cabs don't work
      if not module:part:name:startswith("WBI.") { set 
        self:container_module to  module.
      }
    } set 

    self:to_check to  list(). set 
    self:seen to  lex().
    self:start(
    self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["run"] to { parameter self.
    if time:seconds - self:last_check < self:check_interval / 2 {
      return.
    } set 
    self:last_check to  time:seconds.

    for k in self:seen:keys:copy {
      if not self:seen[k]:hasdata {
        self:seen:remove(k).
      }
    } local new_to_check to  list(). local to_reset to  list().
    for m in self:to_check {
      if not m:hasdata {
        new_to_check:add(m).
      } else { local total_science to  0. local total_transmit to  0.
        for d in m:data {
          if not self:seen:haskey(d:title) { set 
            total_science to  total_science + d:sciencevalue. set 
            total_transmit to  total_transmit + d:transmitvalue.
          }
        }
        if total_science <= self:min_science {
          to_reset:add(m).
        } else {
          if total_transmit > 0.01 and homeconnection:isconnected {
            if m:rerunnable or not self:lab_module:istype("Boolean") {
              m:transmit().
            } else {
              for d in m:data {
                self:seen:add(d:title, m).
              }
            }
          } else {
            for d in m:data {
              self:seen:add(d:title, m).
            }
          }
        }
      }
    }

    if not self:lab_module:istype("Boolean") {
      if not self:container_module:istype("Boolean") {
        self:container_module:collectall().
      }

      for eventname in self:lab_module:alleventnames {
        if eventname:startswith("clean experiments") {
          self:lab_module:doevent(eventname).
        }
      }
    }
    
    for m in to_reset {
      if not m:inoperable {
        m:reset().
      }
    } local modules to  ship:modulesnamed("ModuleScienceExperiment").
    for m in modules {
      if not m:inoperable {
        if not m:hasdata {
          m:deploy().
        }
        new_to_check:add(m).
      }
    } set 
    self:to_check to  new_to_check.}.
return  result.}):call(). set 
export to  ScienceDaemon.