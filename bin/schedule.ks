local Position to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Position").
set  result["new"] to { parameter type,vector, t_body.
local self to prototype:copy.
set  self["type"] to  result.
    set self:v to  vector.
    set self:b to  t_body.
    set self:abs to  false.
    if t_body:istype("Boolean") or t_body = sun {
      // Sun is absolute
      set self:abs to  true.
    }
return  self.}.
set  result["__new"] to  result["new"].
set  result["from_geo"] to { parameter self,geo, _altitude.
    // START CRITICAL SECTION
    if opcodesleft < 24
      wait 0.
    local xyz to  (geo:altitudePosition(_altitude) - geo:body:position) * lookdirup(solarprimevector, v(0,1,0)):inverse.
    // END CRITICAL SECTION
    return Position:new(Position,xyz, geo:body).}.
set  result["from_orbit"] to { parameter self,target, _time.
    // START CRITICAL SECTION
    if opcodesleft < 24
      wait 0.
    local xyz to  (positionat(target, _time) - target:body:position) * lookdirup(solarprimevector, v(0,1,0)):inverse.
    // END CRITICAL SECTION
    return Position:new(Position,xyz, target:body).}.
set  result["from_position"] to { parameter self,_orbit.
    // START CRITICAL SECTION
    if opcodesleft < 24
      wait 0.
    local xyz to  (_orbit:position - _orbit:body:position) * lookdirup(solarprimevector, v(0,1,0)):inverse.
    // END CRITICAL SECTION
    return Position:new(Position,xyz, _orbit:body).}.
set  result["from_velocity"] to { parameter self,s.
    // START CRITICAL SECTION (18 ops)
    if opcodesleft < 18
      wait 0.
    local xyz to  s:velocity:orbit * lookdirup(solarprimevector, v(0,1,0)):inverse.
    // END CRITICAL SECTION
    return Position:new(Position,xyz, false).}.
set  prototype["changeSOI"] to { parameter self,tgt, _time.
    return false.}.
set  prototype["add"] to { parameter self,pos.
    if self:abs {
      return Position:new(Position,self:v + pos:v, pos:b).
    }
    if pos:abs {
      return Position:new(Position,self:v + pos:v, self:b).
    }
    if pos:b <> self:b {
      return false.
    }
    return Position:new(Position,self:v + pos:v, self:b).}.
set  prototype["sub"] to { parameter self,pos.
    if pos:abs {
      return Position:new(Position,self:v - pos:v, self:b).
    }
    if self:b <> pos:b {
      return false.
    }
    return Position:new(Position,self:v - pos:v, false).}.
set  prototype["mult"] to { parameter self,s.
    return Position:new(Position,self:v * s, self:b).}.
set  prototype["cross"] to { parameter self,o.
    if self:abs {
      return Position:new(Position,vcrs(self:v, o:v), o:b).
    }
    if o:abs {
      return Position:new(Position,vcrs(self:v, o:v), self:b).
    }
    if self:b <> o:b {
      return false.
    }
    return Position:new(Position,vcrs(self:v, o:v), o:b).}.
set  prototype["dot"] to { parameter self,o.
    return self:v * o:v.}.
set  prototype["normalized"] to { parameter self.
    return Position:new(Position,self:v:normalized, self:b).}.
set  prototype["to_geo"] to { parameter self,t.
    // START CRITICAL SECTION
    if opcodesleft < 31
      wait 0.
    local timeDelta to  t - time:seconds.
    local geo to  self:b:geopositionof(self:v * lookdirup(solarprimevector, v(0,1,0)) + self:b:position).
    // END CRITICAL SECTION
    //return geo;
    return self:b:geopositionlatlng(geo:lat, geo:lng - ( self:b:angularvel:mag * ( constant:pi/180 )) * timeDelta ).}.
set  result["fromString"] to { parameter self,s.
    if not s:startsWith("P;")
      return false.
    local parts to  s:split(";").
    if parts:length <> 5
      return false.
    local _body to  false.
    if parts[1] <> "@"
      set _body to  Body(parts[1]).
    local x to  parts[2]:tonumber(0).
    local y to  parts[3]:tonumber(0).
    local z to  parts[4]:tonumber(0).
    return Position:new(Position,V(x,y,z), _body).}.
set  prototype["to_s"] to { parameter self.
    local _body to  "@".
    if not self:abs {
      set _body to  self:b:name.
    }
    return "P;" + _body + ";" + round(self:v:x,2) + ";" + round(self:v:y,2) + ";" + round(self:v:z,2).}.
return  result.}):call().
local Schedule to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Schedule").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:pid to  id.
    set self:output to  output.
    set self:fs to  fs.
    set self:resultCallback to  resultCallback.
    if args:length < 2 or args[1] = "help" {
      self:help(self).
      return self.
    }

    if args[1] <> "burn" or args:length < 4 or (args[2] <> "at" and args[2] <> "in") {
      self:help(self).
      return self.
    }

    local offset to  -1.
    if args[2] = "at" {
      local curOrbit to  ship:orbit.
      if args[3] = "apoapsis" {
        set offset to  eta:apoapsis.
        until offset < curOrbit:nextPatchEta {
          set curOrbit to  curOrbit:nextPatch.
          set offset to  curOrbit:eta:apoapsis.
        }
      }
      if args[3] = "periapsis" {
        set offset to  eta:periapsis.
        until offset < curOrbit:nextPatchEta {
          set curOrbit to  curOrbit:nextPatch.
          set offset to  curOrbit:eta:periapsis.
        }
      }
      if args[3] = "planechange" {
        set offset to  eta:periapsis.

        local tgtorbit to  false.
        if hastarget {
          set tgtorbit to  target:obt.
        } else {
          local robt to  ship:obt.
          set tgtorbit to  createorbit(0, robt:eccentricity, robt:semimajoraxis, robt:lan, robt:argumentofperiapsis, robt:meananomalyatepoch, robt:epoch, robt:body).
        }

        local upper_bound_time to  0.
        if ship:obt:eccentricity < 1 {
          set upper_bound_time to  ship:obt:period / 2.
        } else if ship:obt:hasnextpatch {
          set upper_bound_time to  ship:obt:nextpatcheta.
        } else {
local __var19 to self:output. __var19:append(__var19,"Current orbit too extreme!", Callback:null).
          set self:status to  1.
local __var20 to self:fs:loop. __var20:defer(__var20,self:resultCallback, self:pid).
          return self.
        }

        local intersect to  self:find_orbit_intersect(self,time:seconds, time:seconds + upper_bound_time, 20, ship, tgtorbit).
        if not intersect:istype("Boolean") {
          set intersect to  self:find_orbit_intersect(self,intersect:window_start, intersect:window_end, 20, ship, tgtorbit).
        }
        if not intersect:istype("Boolean") {
          set intersect to  self:find_orbit_intersect(self,intersect:window_start, intersect:window_end, 20, ship, tgtorbit).
        }
        if intersect:istype("Boolean") {
local __var21 to self:output. __var21:append(__var21,"No suitable plane change manouver could be found", Callback:null).
          set self:status to  1.
local __var22 to self:fs:loop. __var22:defer(__var22,self:resultCallback, self:pid).
          return self.
        }

        set offset to  intersect:time - time:seconds.
      }
    } else if args[2] = "in" {
      set offset to  args[3]:toscalar(-1).
      if args:length > 4 {
        if args[4] = "seconds" {
          set offset to  offset * 1.
        }
        if args[4] = "minutes" {
          set offset to  offset * 60.
        }
        if args[4] = "hours" {
          set offset to  offset * 60 * 60.
        }
      }
    }

    if offset < 0 {
      self:help(self).
      return self.
    }

    add node(time:seconds + offset, 0, 0, 0).
    set self:status to  0.
local __var23 to fs:loop. __var23:defer(__var23,resultCallback, id).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["help"] to { parameter self.
local __var24 to self:output. __var24:append(__var24,"Usage:", Callback:null).
local __var25 to self:output. __var25:append(__var25,"schedule burn at [apoapsis|periapsis|planechange]", Callback:null).
    set self:status to  1.
local __var26 to self:fs:loop. __var26:defer(__var26,self:resultCallback, self:pid).}.
set  prototype["find_orbit_intersect"] to { parameter self,t_start, t_end, stepcount, orbitable, target_orbit.

    if (orbitable:body <> target_orbit:body) {
      return false.
    }

    local step_size to  (t_end - t_start) / stepcount.
    print "step size: " + round(step_size, 1).
local __var27 to Position:from_position(Position,target_orbit).
local __var28 to __var27:cross(__var27,Position:from_velocity(Position,target_orbit)).

    local target_normal to  __var28:normalized(__var28).
local __var29 to Position:from_position(Position,orbitable).
local __var30 to __var29:cross(__var29,Position:from_velocity(Position,orbitable)).
    local orbitable_normal to  __var30:normalized(__var30).
local __var31 to target_normal:cross(target_normal,orbitable_normal).
    local intersect to  __var31:normalized(__var31).

    local lastAngle to  0.
    local downPhase to  true.
    for s in range(stepcount) {        
        local step_time to  t_start + s * step_size.       
        local orbit_position to  Position:from_orbit(Position,orbitable, step_time).
        local angle to  abs(intersect:dot(intersect,orbit_position:normalized(orbit_position))).
        print "testing at: " + timestamp(step_time):clock  + " found angle: " + angle.
        if s > 0 {
          if downPhase {
            print "down phase!".
          }
          if downPhase and lastAngle < angle {
            set downPhase to  false.
          }
          if not downPhase {
            print "up phase!".
          }
          if not downPhase and lastAngle >= angle {
            //https://ksp-kos.github.io/KOS/structures/misc/time.html#time-operators
            print "found: time:" + timestamp(step_time - step_size):clock + " window_start:" + timestamp(max(t_start, step_time - 2 * step_size)):clock + " window_end: "+ timestamp(step_time):clock.
            return lex("time", step_time - step_size, "window_start", max(t_start, step_time - 2 * step_size), "window_end", step_time).
          }
        } else {
          print "first phase!".
        }
        set lastAngle to  angle.
    }
    return false.}.
return  result.}):call().

set export to  Schedule.