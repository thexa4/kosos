local  MkdirProg to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id.
    if args:length < 2 {
      output:append(
      output,"Usage: mkdir <path>", Callback:null). set 
      self:status to  1.
local  __var5 to 
      fs:loop. __var5:defer( __var5,resultCallback, id).
      return self.
    } set 
    pathname to  args[1]. set 
    self:fs to  fs. set 
    self:resultCallback to  resultCallback. set 
    self:output to  output.
    fs:mkdir(
    fs,pathname, Callback: new( Callback,self, "onCreateDir")).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onCreateDir"] to { parameter self,dir.
    if dir:istype("Boolean") {
local  __var6 to 
      self:output. __var6:append( __var6,"Unable to create directory", Callback:null). set 
      self:status to  1.
    } else { set 
      self:status to  0.
    }
local  __var7 to 
    self:fs:loop. __var7:defer( __var7,self:resultCallback, self:pid).}.
return  result.}):call(). set 
export to  MkdirProg.
