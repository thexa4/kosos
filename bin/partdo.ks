local  PartDo to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id.
    if args:length <> 3 {
      output:append(
      output,"Usage: partdo <tag> <action_prefix>", Callback:null). set 
      self:status to  1.
local  __var9 to 
      fs:loop. __var9:defer( __var9,resultCallback, id).
      return.
    } local parts to  ship:partstagged(args[1]).
    if parts:length = 0 {
      output:append(
      output,"No parts with tag " + args[1] + " not found", Callback:null). set 
      self:status to  1.
local  __var10 to 
      fs:loop. __var10:defer( __var10,resultCallback, id).
      return.
    }
    
    for part in parts {
      for modulename in part:modules { local module to  part:getmodule(modulename).

        for eventname in module:alleventnames {
          if eventname:startswith(args[2]) {
            module:doevent(eventname).
          }
        }
      }
    } set 
    
    self:status to  0.
local  __var11 to 
    fs:loop. __var11:defer( __var11,resultCallback, id).
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
export to  PartDo.
