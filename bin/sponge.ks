local _Callback to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_Callback").
set  result["new"] to { parameter type,target, method.
local self to prototype:copy.
set  self["type"] to  result.
    set self:call to  { parameter self, args.
      return target[method]:call(target, args).
    }.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call().
set _Callback:null to  lex("call", { parameter s,_. }).
global Callback to _Callback.
local Sponge to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Sponge").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:pid to  id.
    if args:length <> 2 {
      output:write(output,"Usage: sponge <filename>").
      set self:status to  1.
local __var89 to fs:loop. __var89:defer(__var89,resultCallback, id).
      return self.
    }
    set self:output to  output.
    set self:data to  list().
    set self:status to  -1.
    set self:streaming to  false.
    set self:interrupted to  false.
    set self:resultCallback to  resultCallback.
    set self:fs to  fs.
    set self:file to  false.
    set self:input to  input.
    if input:haskey("closeHandler") {
      set self:streaming to  true.
      if not input:closeHandler:istype("Boolean") {
        print("sponge: double close handler detected, overwriting").
      }
      set input:closeHandler to  Callback:new(Callback,self, "onClose").
    }
    fs:create(fs,args[1], Callback:new(Callback,self, "onOpen")).
    set input:signalHandler to  Callback:new(Callback,self, "onSignal").
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onSignal"] to { parameter self,signal.
    if self:interrupted
      return false.
    if signal <> "End"
      return false.
    set self:interrupted to  true.
    if not self:file:istype("Boolean")
{
local __var90 to self:file. __var90:write(__var90,self:data:join(char(10)), Callback:null).
}
    set self:status to  9.
local __var91 to self:fs:loop. __var91:defer(__var91,self:resultCallback, self:pid).
    return true.}.
set  prototype["onOpen"] to { parameter self,file.
    if self:interrupted
      return.
    if file:istype("Boolean") {
      set self:interrupted to  true.
local __var92 to self:output. __var92:write(__var92,"sponge: no such file", Callback:null).
      set self:status to  1.
local __var93 to self:fs:loop. __var93:defer(__var93,self:resultCallback, self:pid).
      return.
    }

    set self:file to  file.
local __var94 to self:input. __var94:read(__var94,Callback:new(Callback,self, "onRead")).}.
set  prototype["onRead"] to { parameter self,data.
    if self:interrupted
      return.
    if data:istype("Boolean") {
      set self:interrupted to  true.
local __var95 to self:output. __var95:write(__var95,"sponge: read failed", Callback:null).
      set self:status to  2.
local __var96 to self:fs:loop. __var96:defer(__var96,self:resultCallback, self:pid).
      return.
    }
local __var97 to self:output. __var97:append(__var97,data, Callback:null).
    self:data:add(data).
    if self:streaming {
local __var98 to self:input. __var98:read(__var98,Callback:new(Callback,self, "onRead")).
    } else {
      print("non-stream").
      set self:interrupted to  true.
local __var99 to self:file. __var99:write(__var99,self:data:join(char(10)), Callback:null).
      set self:status to  0.
local __var100 to self:fs:loop. __var100:defer(__var100,self:resultCallback, self:pid).
    }}.
set  prototype["onClose"] to { parameter self,_.
    if self:interrupted
      return.
    set self:interrupted to  true.
    if not self:file:istype("Boolean")
{
local __var101 to self:file. __var101:write(__var101,self:data:join(char(10)), Callback:null).
}
    set self:status to  0.
local __var102 to self:fs:loop. __var102:defer(__var102,self:resultCallback, self:pid).
    return true.}.
return  result.}):call().
set export to  Sponge.