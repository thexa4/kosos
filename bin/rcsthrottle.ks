local  RCSThrottle to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    self:progSetup(
    self,id, input, output, args, env, fs, resultCallback).

    rcs on. local hasRcs to  false.
    for controller in ship:attitudecontrollers {
      if controller:controllertype = "RCS" { set 
        hasRcs to  true.
      }
    }
    
    if not hasRcs {
      self:print(
      self,"No RCS found").
      self:exit(
      self,1).
      return self.
    } set 
    self:hasLocked to  false.
    self:start(
    self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["run"] to { parameter self.
    if self:running { local fwd to  throttle.
      if fwd > 0.001 and fwd < 0.1 { set 
        fwd to  0.1.
      } set 
      ship:control:fore to  fwd.
    }}.
set  prototype["stop"] to { parameter self. set 
    ship:control:fore to  0. set 
    ship:control:neutralize to  true.
    if not self:running
      return. set 
    self:running to  false.
local  __var150 to 
    self:fs:loop. __var150:deregister( __var150,self).}.
return  result.}):call(). set 
export to  RCSThrottle.