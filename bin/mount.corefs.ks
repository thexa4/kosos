local DelegateFile to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","DelegateFile").
set  result["new"] to { parameter type,fs, readDelegate, writeDelegate.
local self to prototype:copy.
set  self["type"] to  result.
    set self:fs to  fs.
    set self:readDelegate to  readDelegate.
    set self:writeDelegate to  writeDelegate.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getType"] to { parameter self. return "file".}.
set  prototype["getReadHandle"] to { parameter self,loop. return StaticReadFileHandle:new(StaticReadFileHandle,self, loop).}.
set  prototype["_readAll"] to { parameter self,callback.
local __var0 to self:fs:loop. __var0:defer(__var0,callback, self:readDelegate()).}.
set  prototype["write"] to { parameter self,data, callback.
    if not data:istype("Boolean")
      self:writeDelegate(data).
local __var1 to self:fs:loop. __var1:defer(__var1,callback, true).}.
set  prototype["append"] to { parameter self,data, callback.
    self:write(self,data, callback).}.
return  result.}):call().
local CoreFsMount to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","CoreFsMount").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:pid to  id.
    set self:status to  0.
    if args:length <> 2 {
      set self:status to  1.
      output:append(output,"Usage: mount.corefs <dest>", Callback:null).
    } else {
      local bind to  MemoryFilesystemDir:new(MemoryFilesystemDir).
      bind:registerFile(bind,"bootfilename", DelegateFile:new(DelegateFile,fs, { return "" + core:bootfilename. }, { parameter data. set core:bootfilename to  data:trim. })).
      bind:registerFile(bind,"tag", DelegateFile:new(DelegateFile,fs, { return core:tag. }, { parameter data. set core:tag to  data:trim. })).
      bind:registerFile(bind,"ipu", DelegateFile:new(DelegateFile,fs, { return "" + config:ipu. }, { parameter data. })).
      
      // TODO: messagequeue
      fs:mount(fs,bind, args[1]).
    }
local __var2 to fs:loop. __var2:defer(__var2,resultCallback, id).
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call().
set export to  CoreFsMount.
