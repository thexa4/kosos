local  _Callback to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,target, method.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:call to  { parameter self, args.
      return target[method]:call(target, args).
    }.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
_Callback:null to  lex("call", { parameter s,_. }).
global Callback to _Callback.
local  BindFilesystemDir to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,source, fs.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:source to  source. set 
    self:fs to  fs. set 
    self:dir to  false. set 
    self:readCallbacks to  list(). set 
    self:openCallbacks to  list(). set 
    self:mkdirCallbacks to  list().
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["runFetch"] to { parameter self.
    if self:dir:istype("Boolean") {
local  __var0 to 
      self:fs. __var0:open( __var0,self:source, Callback: new( Callback,self, "onOpen")).
      return.
    }
    self:onOpen(
    self,self:dir).}.
set  prototype["onOpen"] to { parameter self,source.
    if source:istype("Boolean") {
      for c in self:readCallbacks {
local  __var1 to 
        self:fs:loop. __var1:defer( __var1,c, false).
      }
      self:readCallbacks:clear().
      for l in self:openCallbacks {
local  __var2 to 
        self:fs:loop. __var2:defer( __var2,l[1], false).
      }
      self:openCallbacks:clear().
      return.
    } set 
    self:dir to  source.
    for c in self:readCallbacks {
local  __var3 to 
      self:fs:loop. __var3:defer( __var3, Callback:new( Callback,source, "read"), c).
    }
    self:readCallbacks:clear(). local opens to  self:openCallbacks:copy.
    self:openCallbacks:clear().
    for l in opens {
      source:open(
      source,l[0], l[1]).
    } local mkdirs to  self:mkdirCallbacks:copy.
    self:mkdirCallbacks:clear().
    for mkdir in mkdirs {
local  __var4 to 
      self:dir. __var4:mkdir( __var4,mkdir[0], mkdir[1]).
    }}.
set  prototype["getType"] to { parameter self. return "dir".}.
set  prototype["read"] to { parameter self,callback.
    self:readCallbacks:add(callback).
    self:runFetch(
    self).}.
set  prototype["open"] to { parameter self,pathname, callback.
    self:openCallbacks:add(list(pathname, callback)).
    self:runFetch(
    self).}.
set  prototype["mkdir"] to { parameter self,pathname, callback.
    self:mkdirCallbacks:add(list(pathname, callback)).
    self:runFetch(
    self).}.
set  prototype["df"] to { parameter self.
    if self:dir:istype("Boolean") {
      return lex("used", 0, "available", 0).
    }
local  __var5 to  self:dir.
return  __var5:df( __var5).}.
return  result.}):call().
local  BindMount to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id. set 
    self:status to  0.
    if args:length <> 3 { set 
      self:status to  1.
      output:append(
      output,"Usage: mount.bind <source> <dest>", Callback:null).
    } else { local bind to  BindFilesystemDir: new( BindFilesystemDir,args[1], fs).
      fs:mount(
      fs,bind, args[2]).
    }
local  __var6 to 
    fs:loop. __var6:defer( __var6,resultCallback, id).
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
export to  BindMount.