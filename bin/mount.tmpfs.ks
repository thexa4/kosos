local  TmpFsMount to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id. set 
    self:status to  0.
    if args:length <> 2 { set 
      self:status to  1.
      output:append(
      output,"Usage: mount.tmpfs <dest>", Callback:null).
    } else { local bind to  MemoryFilesystemDir: new( MemoryFilesystemDir).
      fs:mount(
      fs,bind, args[1]).
    }
local  __var0 to 
    fs:loop. __var0:defer( __var0,resultCallback, id).
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
export to  TmpFsMount.