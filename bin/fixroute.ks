@lazyglobal off.
"kpp 1.1"."https://gitlab.com/thexa4/kos-kpp".


local FixRoute to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,id, input, output, args, env, fs, resultCallback.local self to __cls:prototype:copy. set self:type to __cls.
    set self:pid to id.
    if args:length <> 1 {
      output:write(output,"Usage: fixroute").
      set self:status to 1.
      fs:loop:defer(fs:loop,resultCallback, id).
      return self.
    }
    set self:env to env.
    set self:output to output.
    set self:status to -1.
    set self:interrupted to false.
    set self:resultCallback to resultCallback.
    set self:fs to fs.
    set self:reader to LineReader:__new(LineReader,input, fs).
    set self:subProc to false.
    clearvecdraws().
    set input:signalHandler to Callback:__new(Callback,self, "onSignal").
    self:reader:readLine(self:reader, Callback:__new(Callback,self, "handleLine")). return self.}.

  set r:prototype:onSignal to {parameter self,signal.
    if self:interrupted
      return false.
    if signal <> "End"
      return false.
    if not self:subProc:istype("Boolean") and not self:subProc:prog:istype("Boolean") {
      return self:subProc:prog:onSignal(self:subProc:prog,signal).
    }
    set self:interrupted to true.
    set self:status to 9.
    //clearvecdraws();
    self:fs:loop:defer(self:fs:loop,self:resultCallback, self:pid).
    return true.
  }.

  set r:prototype:handleLine to {parameter self,data.
    if self:interrupted = true {
      return.
    }
    if data:istype("Boolean") {
      set self:interrupted to true.
      set self:status to 0.
      //clearvecdraws();
      self:fs:loop:defer(self:fs:loop,self:resultCallback, self:pid).
      return.
    }

    local line to data:trim.
    if line:startsWith("#") {
      self:reader:readLine(self:reader, Callback:__new(Callback,self, "handleLine")).
      return.
    }
    if line:startsWith("G;") {
      self:output:write(self:output,line).
      self:reader:readLine(self:reader, Callback:__new(Callback,self, "handleLine")).
    } else if line:startsWith("REPATH:") {
      local parts to line:split(":").
      if parts:length <> 3 {
        self:output:write(self:output,"#Bad repath: " + line).
        self:reader:readLine(self:reader, Callback:__new(Callback,self, "handleLine")).
        return.
      }
      self:output:write(self:output,"#" + line).

      local res to "20".
      if self:env:haskey("res") {
        set res to self:env:res.
      }
      set self:subProc to ShellExec:__new(ShellExec,
        "nav from " + parts[1] + " to " + parts[2] + " res " + res,
        self:fs,
        self:env,
        false, Callback:__new(Callback,self, "onNavDone")
      ).
    } else {
      self:output:write(self:output,"#Unknown waypoint: " + line).
      self:reader:readLine(self:reader, Callback:__new(Callback,self, "handleLine")).
    }
  }.

  set r:prototype:onNavDone to {parameter self,exec.
    if exec:status <> 0 {
      self:output:write(self:output,"#Failed to execute").
      if not exec:output:istype("Boolean") {
        self:output:write(self:output,exec:output).
      }
      set self:status to 1.
      self:fs:loop:defer(self:fs:loop,self:resultCallback, self:pid).
      return.
    }
    self:output:write(self:output,exec:output).
    self:reader:readLine(self:reader, Callback:__new(Callback,self, "handleLine")).
  }.
}).
set export to FixRoute.