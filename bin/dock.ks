local  Dock to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    self:progSetup(
    self,id, input, output, args, env, fs, resultCallback). local hasRcs to  false.
    for controller in ship:attitudecontrollers {
      if controller:controllertype = "RCS" { set 
        hasRcs to  true.
      }
    }
    
    if not hasRcs {
      self:print(
      self,"No RCS found").
      self:exit(
      self,1).
      return self.
    }

    if not ship:controlpart:hasmodule("ModuleDockingNode") {
      self:print(
      self,"Control part is not a docking port.").
      self:exit(
      self,1).
      return self.
    }

    if not hastarget {
      self:print(
      self,"No target selected").
      self:exit(
      self,1).
      return self.
    }
    
    if not target:istype("Part") or not target:hasmodule("ModuleDockingNode") {
      self:print(
      self,"Target is not a docking port.").
      self:exit(
      self,1).
      return self.
    } set 

    self:target to  target. set 
    self:approachDist to  100. set 
    self:flyAroundDist to  100. set 
    self:maxSpeed to  5. set 
    self:dockSpeed to  0.1. set 
    self:desiredVelocity to  V(0,0,0). set 
    self:arrow to  vecdraw(ship:position, ship:facing:vector, rgb(1, 0, 0), "", 1, true). set 

    self:throttle to  0. set 
    self:heading to  ship:facing:vector. set 
    self:autoEngine to  false. set 

    self:referencePartCount to  ship:parts:length. set 
    self:lastDistance to  100.

    lock steering to self:heading.
    lock throttle to self:throttle.

    self:recomputeState(

    self).
    self:start(
    self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["recomputeState"] to { parameter self. local approachStart to  self:target:position + self:target:facing:vector * self:approachDist. local approachDir to  -self:target:facing:vector. local relPos to  self:target:position - ship:position. local planeDir to  vxcl(approachDir, relPos). local hemisphere to  approachDir * relPos.
    self:print(
    self,"Axis alignment: " + round(hemisphere)).
    self:print(
    self,"Plane alignment: " + round(planeDir:mag)).

    if hemisphere > self:approachDist {
      if planeDir:mag < 5 { set 
        self:stage to  "approach".
      } else { set 
        self:stage to  "align".
      }
    } else {
      if planeDir:mag < self:flyAroundDist { set 
        self:stage to  "flyAway".
      } else { set 
        self:stage to  "flyAround".
      }
    }
    self:print(
    self,"Performing " + self:stage).}.
set  prototype["run"] to { parameter self.
    self[self:stage](self).
    if self:autoEngine { local dv to  self:desiredVelocity - ship:velocity:orbit. set 
      self:heading to  dv:normalized. local alignment to  self:heading * ship:facing:vector. set 
      self:throttle to  ((alignment - 0.9) * 10) * (dv:mag / 15). // ~1.45 TWR
    } else { set 
      self:throttle to  0.
    }}.
set  prototype["stopAt"] to { parameter self,pos. local destDir to  pos - ship:position. local maxSpeed to  min(destDir:mag / 20, self:maxSpeed). local desiredVel to  destDir:normalized * maxSpeed. set 
    self:arrow:vec to  pos - ship:position. set 
    self:desiredVelocity to  desiredVel + self:target:ship:velocity:orbit.

    if destDir:mag < 1 { set 
      self:autoEngine to  false.
      self:recomputeState(
      self). set 
      self:desiredVelocity to  self:target:ship:velocity:orbit.
    }}.
set  prototype["flyAway"] to { parameter self. set 
    self:autoEngine to  true. local relPos to  self:target:position - ship:position. local planeDir to  vxcl(self:target:facing:vector, relPos). local distLeft to  (self:flyAroundDist + 10) - planeDir:mag.
    self:stopAt(
    self,ship:position - planeDir:normalized * distLeft).}.
set  prototype["flyAround"] to { parameter self. set 
    self:autoEngine to  true. local relPos to  self:target:position - ship:position. local planeDir to  vxcl(self:target:facing:vector, relPos). local dest to  relPos + self:target:facing:vector * (self:approachDist + 10) - planeDir:normalized * (self:flyAroundDist + 10).
    self:stopAt(
    self,dest).}.
set  prototype["align"] to { parameter self. set 
    self:autoEngine to  true. local relPos to  self:target:position - ship:position. local planeDir to  vxcl(self:target:facing:vector, relPos). local dest to  relPos + self:target:facing:vector * (self:approachDist + 10).
    self:stopAt(
    self,dest).}.
set  prototype["approach"] to { parameter self. set 
    self:autoEngine to  false. set 

    self:heading to  -self:target:facing:vector. local targetToShip to  ship:position - self:target:position. local error to  vxcl(self:target:facing:vector, targetToShip). local dest to  self:target:position. local desiredMovement to  dest - ship:position. set 
    self:arrow:vec to  desiredMovement. local progressTodo to  (targetToShip:mag - 5) / (self:approachDist - 5). local maxSpeed to  self:dockSpeed + progressTodo * max(0, 2 - self:dockSpeed). local desiredVel to  self:target:ship:velocity:orbit + desiredMovement:normalized * maxSpeed - error.

    if ship:facing:vector * self:heading > 0.95 {
      rcs on. local dv to  desiredVel - ship:velocity:orbit. set 
      ship:control:fore to  dv * ship:facing:vector. set 
      ship:control:starboard to  dv * ship:facing:starvector. set 
      ship:control:top to  dv * ship:facing:topvector.
    }

    if targetToShip:mag < 5 {
      self:print(
      self,"Coasting..."). set 
      self:stage to  "coast".
    }}.
set  prototype["coast"] to { parameter self. set 
    self:autoEngine to  false.
    unlock steering. set 
    ship:control:fore to  0. set 
    ship:control:starboard to  0. set 
    ship:control:top to  0. set 
    ship:control:pilotmainthrottle to  0.
    unlock throttle.
    unlock steering. set 
    ship:control:neutralize to  true.

    if ship:parts:length > self:referencePartCount {
      self:exit(
      self,0).
      return.
    } local targetToShip to  ship:position - self:target:position.
    if targetToShip:mag - self:lastDistance > 2 {
      self:print(
      self,"Docking failed").
      self:exit(
      self,1).
      return.
    }
    if targetToShip:mag < self:lastDistance { set 
      self:lastDistance to  targetToShip:mag.
    }}.
set  prototype["deconstruct"] to { parameter self,_. set 
    ship:control:fore to  0. set 
    ship:control:starboard to  0. set 
    ship:control:top to  0.
    unlock throttle.
    unlock steering. set 
    ship:control:neutralize to  true.
    clearvecdraws().}.
return  result.}):call(). set 
export to  Dock.