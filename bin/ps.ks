local  Ps to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id. set 
    self:status to  0. local services to  fs:loop:private()["s"].
    for pid in fs:cmds:keys { local s to  "wait".

      for s in services {
        if s["pid"] = pid { set 
          s to  "running".
        }
      }
      output:append(
      output,("" + pid):padleft(5) + " " + s:padright(7) + " " + fs:cmds[pid]:join(" "), Callback:null).
    }
local  __var0 to 

    fs:loop. __var0:defer( __var0,resultCallback, id).
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
export to  Ps.