@lazyglobal off.
"kpp 1.1"."https://gitlab.com/thexa4/kos-kpp".


local Chardump to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,id, input, output, args, env, fs, resultCallback.local self to __cls:prototype:copy. set self:type to __cls.
    set self:pid to id.
    set self:status to -1.
    set self:input to input.
    set self:output to output.
    set self:fs to fs.
    set self:resultCallback to resultCallback.

    //TODO: immediate mode?
    ({local __lhs to input. return __lhs:read(__lhs, Callback:__new(Callback,self, "onInput")).}):call(). return self.}.

  set r:prototype:onInput to {parameter self,data.
    ({local __lhs to self:output. return __lhs:append(__lhs,data, Callback:null).}):call().
    if data:length > 0
      print(unchar(data[0])).
    set self:status to 0.
    ({local __lhs to self:fs:loop. return __lhs:defer(__lhs,self:resultCallback, self:pid).}):call().
  }.
}).
set export to Chardump.