local  PropEmulator to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id. set 
    self:status to  0. set 
    self:input to  input. set 
    self:output to  output. set 
    self:env to  env. set 
    self:fs to  fs. set 
    self:resultCallback to  resultCallback. local parts to  list().
    list parts in parts.
    local engines to  list().
    local props to  list().
    for part in parts {
      if part:hasmodule("ModuleRoboticServoRotor") { local module to  part:getmodule("ModuleRoboticServoRotor").
        for child in part:children {
          if child:hasmodule("ModuleControlSurface") { local propModule to  child:getmodule("ModuleControlSurface").
            propModule:setfield("Deploy Angle", 90).
            props:add(propModule). set 
            self:rotorTipSpeed to  (child:bounds:abscenter:mag * 1.15) * 2 * 3.14 * 200 / 60.
          }
        }
        engines:add(module).
        module:setfield("motor", true).
        if part:tag <> "on" {
          module:doaction("Toggle Motor Power", true). set 
          part:tag to  "on".
        }
        module:setfield("motor", false).
      }
    } set 
    self:engines to  engines. set 
    self:props to  props.

    self:start(

    self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["run"] to { parameter self.
    for engine in self:engines {
      if throttle > 0 {
        engine:setfield("motor", true).
        if engine:hasfield("Torque Limit(%)")
          engine:setfield("Torque Limit(%)", throttle * 100).
      } else {
        engine:setfield("motor", false).
      }
    } local pitch to  min(90 - arctan(ship:airspeed / self:rotorTipSpeed), 89).
    for prop in self:props {
      prop:setfield("Deploy Angle", pitch).
    }}.
return  result.}):call(). set 

export to  PropEmulator.
