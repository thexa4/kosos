local  DelegateFile to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,fs, readDelegate, writeDelegate.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:fs to  fs.
    set  self:readDelegate to  readDelegate.
    set  self:writeDelegate to  writeDelegate.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getType"] to { parameter self. return "file".}.
set  prototype["getReadHandle"] to { parameter self,loop. return StaticReadFileHandle: new( StaticReadFileHandle,self, loop).}.
set  prototype["_readAll"] to { parameter self,callback.
local  __var0 to  self:fs:loop. __var0:defer( __var0,callback, self:readDelegate()).}.
set  prototype["write"] to { parameter self,data, callback.
    if not data:istype("Boolean")
      self:writeDelegate(data).
local  __var1 to 
    self:fs:loop. __var1:defer( __var1,callback, true).}.
set  prototype["append"] to { parameter self,data, callback.
    self:write(
    self,data, callback).}.
return  result.}):call().
local  Planed to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    set  self:pid to  id.
    set  self:status to  0.
    set  self:input to  input.
    set  self:output to  output.
    set  self:env to  env.
    set  self:fs to  fs.
    set  self:resultCallback to  resultCallback.
    set  self:running to  false.
    set  self:targetAlt to  500.
    set  self:groundAltReference to  "1".
    set  self:pitchLandingOffset to  0. local pitchP to  0.005.
    if self:env:haskey("PLANE_PITCH_P") and env:PLANE_PITCH_P:toscalar(0) > 0 {
      set  pitchP to  env:PLANE_PITCH_P:toscalar(0).
    } local pitchI to  0.
    if self:env:haskey("PLANE_PITCH_I") and env:PLANE_PITCH_I:toscalar(0) > 0 {
      set  pitchI to  env:PLANE_PITCH_I:toscalar(0).
    } local pitchD to  0.005.
    if self:env:haskey("PLANE_PITCH_D") and env:PLANE_PITCH_D:toscalar(0) > 0 {
      set  pitchD to  env:PLANE_PITCH_D:toscalar(0).
    }
    
    set  self:pitchOffset to  0.
    if self:env:haskey("PLANE_PITCH_OFFSET") and env:PLANE_PITCH_OFFSET:toscalar(0) > 0 {
      set  self:pitchOffset to  env:PLANE_PITCH_OFFSET:toscalar(0).
    }
    set  self:pitchSpeedMult to  0.
    if self:env:haskey("PLANE_PITCH_SPEED_MULT") and env:PLANE_PITCH_SPEED_MULT:toscalar(0) > 0 {
      set  self:pitchSpeedMult to  env:PLANE_PITCH_SPEED_MULT:toscalar(0).
    }
    set  self:pitchSpeedMax to  200.
    if self:env:haskey("PLANE_PITCH_SPEED_MAX") and env:PLANE_PITCH_SPEED_MAX:toscalar(0) > 0 {
      set  self:pitchSpeedMax to  env:PLANE_PITCH_SPEED_MAX:toscalar(0).
    }

    set  self:maxPitch to  0.3.
    if self:env:haskey("PLANE_PITCH_MAX") and env:PLANE_PITCH_MAX:toscalar(0) > 0 {
      set  self:maxPitch to  env:PLANE_PITCH_MAX:toscalar(0).
    }

    set  self:desiredPitchPid to  PidLoop(pitchP, pitchI, pitchD).
    set  self:desiredRollPid to  PidLoop(0.03, 0, 0).
    set  self:pitchPid to  PidLoop(1, 0.005, 0.2).
    set  self:pitchPid:maxoutput to  1.
    set  self:pitchPid:minoutput to  -1.
    set  self:rollPid to  PidLoop(1, 0, 0).
    set  self:speedPid to  PidLoop(0.03, 0.01, 0.01).
    set  self:speedPid:maxoutput to  1.
    set  self:speedPid:minoutput to  0.
    set  self:maxRoll to  0.5.
    if self:env:haskey("PLANE_ROLL_MAX") and env:PLANE_ROLL_MAX:toscalar(0) > 0 {
      set  self:maxRoll to  env:PLANE_ROLL_MAX:toscalar(0).
    }

    set  self:targetSpeed to  80.
    set  self:targetHeading to  90.
    set  self:state to  "unknown".
    set  self:exited to  false.
    
    set  self:minSpeed to  60.
    if self:env:haskey("PLANE_MIN_SPEED") and env:PLANE_MIN_SPEED:toscalar(0) > 0 {
      set  self:minSpeed to  env:PLANE_MIN_SPEED:toscalar(0).
    }

    set  self:takeoffAlt to  500.
    if self:env:haskey("PLANE_TAKEOFF_ALT") and env:PLANE_TAKEOFF_ALT:toscalar(0) > 0 {
      set  self:takeoffAlt to  env:PLANE_TAKEOFF_ALT:toscalar(0).
    }

    set  self:flapsSettingFile to  false.
    set  self:flapsMinSpeed to  100.
    if self:env:haskey("PLANE_FLAPS_SPEED") and env:PLANE_FLAPS_SPEED:toscalar(0) > 0 {
      set  self:flapsMinSpeed to  env:PLANE_FLAPS_SPEED:toscalar(100).
    }
    set  self:flapsMinHeight to  20000.
    if self:env:haskey("PLANE_FLAPS_HEIGHT") and env:PLANE_FLAPS_HEIGHT:toscalar(0) > 0 {
      set  self:flapsMinHeight to  env:PLANE_FLAPS_HEIGHT:toscalar(20000).
    }
    set  self:flapsState to  -1.

    if args:length = 1 {
      self:help(
      self).
      return self.
    }
    if args[1] = "detach" or args[1] = "foreground" {
      fs:open(
      fs,"/sys/class/flaps/desired", Callback: new( Callback,self, "onOpenFlapsFile")).
      self:startDaemon(
      self).
      if args[1] = "detach" {
        set  self:exited to  true.
local  __var2 to 
        self:fs:loop. __var2:defer( __var2,self:resultCallback, self:pid).
      }
      return self.
    }

    self:help(

    self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onOpenFlapsFile"] to { parameter self,file.
    set  self:flapsSettingFile to  file.
    if self:flapsSettingFile:istype("boolean") {
local  __var3 to 
      self:output. __var3:append( __var3,"Flaps not found", Callback:null).
    } else {
local  __var4 to 
      self:output. __var4:append( __var4,"Flaps found", Callback:null).
    }}.
set  prototype["help"] to { parameter self.
local  __var5 to 
    self:output. __var5:append( __var5,"Usage: planed <subcommand>", Callback:null).
local  __var6 to 
    self:output. __var6:append( __var6,"", Callback:null).
local  __var7 to 
    self:output. __var7:append( __var7,"Subcommands available:", Callback:null).
local  __var8 to 
    self:output. __var8:append( __var8," - detach      Runs the plane management software in the background)", Callback:null).
local  __var9 to 
    self:output. __var9:append( __var9," - foreground  Runs in foreground)", Callback:null).
    set  self:status to  1.
    set  self:exited to  true.
local  __var10 to 
    self:fs:loop. __var10:defer( __var10,self:resultCallback, self:pid).}.
set  prototype["startDaemon"] to { parameter self. local configDir to  MemoryFilesystemDir: new( MemoryFilesystemDir).
    configDir:registerFile(
    configDir,"state", DelegateFile: new( DelegateFile,self:fs, { return self:state. }, { parameter data. self:setState( self,data). })).
    configDir:registerFile(
    configDir,"targetAlt", DelegateFile: new( DelegateFile,self:fs, { return "" + self:targetAlt. }, { parameter data. set  self:targetAlt to  data:tonumber(self:targetAlt). })).
    configDir:registerFile(
    configDir,"targetSpeed", DelegateFile: new( DelegateFile,self:fs, { return "" + self:targetSpeed. }, { parameter data. set  self:targetSpeed to  data:tonumber(self:targetSpeed). })).
    configDir:registerFile(
    configDir,"targetHeading", DelegateFile: new( DelegateFile,self:fs, { return "" + self:targetHeading. }, { parameter data. set  self:targetHeading to  data:tonumber(self:targetHeading). })).
    configDir:registerFile(
    configDir,"maxPitch", DelegateFile: new( DelegateFile,self:fs, { return "" + self:maxPitch. }, { parameter data. set  self:maxPitch to  data:tonumber(self:maxPitch). })).
    configDir:registerFile(
    configDir,"groundAltReference", DelegateFile: new( DelegateFile,self:fs, { return self:groundAltReference. }, { parameter data. set  self:groundAltReference to  data:trim. })).
    configDir:registerFile(
    configDir,"pitchP", DelegateFile: new( DelegateFile,self:fs, { return "" + self:desiredPitchPid:Kp. }, { parameter data. set  self:desiredPitchPid:Kp to  data:trim:toscalar(self:desiredPitchPid:Kp). })).
    configDir:registerFile(
    configDir,"pitchI", DelegateFile: new( DelegateFile,self:fs, { return "" + self:desiredPitchPid:Ki. }, { parameter data. set  self:desiredPitchPid:Ki to  data:trim:toscalar(self:desiredPitchPid:Ki). })).
    configDir:registerFile(
    configDir,"pitchD", DelegateFile: new( DelegateFile,self:fs, { return "" + self:desiredPitchPid:Kd. }, { parameter data. set  self:desiredPitchPid:Kd to  data:trim:toscalar(self:desiredPitchPid:Kd). })).
local  __var11 to 
    self:fs. __var11:mount( __var11,configDir, "/sys/class/plane").
    set  self:input:signalHandler to  Callback: new( Callback,self, "onSignal").

    if ship:status = "LANDED" {
      set  self:state to  "landed".
    }
    if ship:status = "FLYING" {
      set  self:state to  "hold".
      set  self:targetAlt to  alt:radar.
      set  self:targetSpeed to  ship:velocity:surface:mag.
    }
local  __var12 to 

    self:fs:loop. __var12:register( __var12,self).
    set  self:running to  true.}.
set  prototype["stopDaemon"] to { parameter self.
    if not self:running
      return.

    self:setState(

    self,"none").
local  __var13 to 
    //clearvecdraws();
    self:fs. __var13:unmount( __var13,"/sys/class/plane").
    if not self:exited {
      set  self:exited to  true.
local  __var14 to 
      self:fs:loop. __var14:deregister( __var14,self).
    }}.
set  prototype["setState"] to { parameter self,state.
    set  self:state to  state.
    set  ship:control:roll to  0.
    set  ship:control:pitch to  0.
    unlock throttle.
    unlock wheelsteering.
    if self:haskey("start" + state) {
      self["start" + state]:call(self).
    }}.
set  prototype["onSignal"] to { parameter self,signal.
    if not self:running
      return false.
    if signal = "End" {
      self:stopDaemon(
      self).
      return true.
    }}.
set  prototype["run"] to { parameter self.
    if not self:flapsSettingFile:istype("boolean") {
      if ship:velocity:surface:mag < self:flapsMinSpeed {
        if self:flapsState <> 3 {
          set  self:flapsState to  3.
local  __var15 to 
          self:flapsSettingFile. __var15:write( __var15,"3", Callback:null).
        }
      } else {
        if ship:altitude > self:flapsMinHeight { local desiredFlap to  ceiling((ship:altitude - self:flapsMinHeight) / 1000).
          if desiredFlap > 2 {
            set  desiredFlap to  2.
          }
          if self:flapsState <> desiredFlap {
            set  self:flapsState to  desiredFlap.
local  __var16 to 
            self:flapsSettingFile. __var16:write( __var16,"" + desiredFlap, Callback:null).
            print("Flaps to " + desiredFlap).
          }
        } else {
          if self:flapsState <> 0 {
            set  self:flapsState to  0.
local  __var17 to 
            self:flapsSettingFile. __var17:write( __var17,"0", Callback:null).
          }
        }
      }
    }

    if self:haskey(self:state) {
      self[self:state]:call(self).
    }}.
set  prototype["startTakeoff"] to { parameter self.
    brakes off.
    lock throttle to 1.}.
set  prototype["takeoff"] to { parameter self.
    set  ship:control:pitch to  0.1 + self:pitchOffset + ship:control:pilotpitch.
    if alt:radar > 10 {
      gear off.
      self:setState(
      self,"heading").
      set  self:targetAlt to  self:takeoffAlt.
      set  self:targetSpeed to  2 * self:minSpeed.
    }}.
set  prototype["heading"] to { parameter self. local desiredForward to  heading(self:targetHeading, 0, 0):vector. local desiredGeo to  ship:body:geopositionof(ship:position + desiredForward * 100).

    self:doRoll(

    self,desiredGeo:bearing).
    self:hold(
    self,desiredGeo:bearing).}.
set  prototype["startDitch"] to { parameter self.
    set  self:groundAltReference to  "1".
    set  self:targetSpeed to  0.
    set  self:rollPid:setpoint to  0.
    gear on.
    brakes on.}.
set  prototype["ditch"] to { parameter self.
    set  self:targetAlt to  max(ship:velocity:surface:mag * 10 - 300, 200). local roll to  ship:facing:starvector * up:vector.
    set  ship:control:roll to  -self:rollPid:update(time:seconds, roll) + ship:control:pilotroll.
    self:hold(
    self).}.
set  prototype["startLand"] to { parameter self. local runwayStart to  latlng(-0.0486, -74.7247). local runwayEnd to  latlng(-0.0502, -74.4880). local approach to  ship:body:geopositionof(runwayStart:position + 1 * (runwayStart:position - runwayEnd:position)). local approachHeight to  approach:terrainheight + 200. local rotationNav to  ship:body:geopositionof(runwayStart:position + 10 * (runwayStart:position - runwayEnd:position)). local rotationHeight to  rotationNav:terrainheight + 1000. local sidewaysPos to  vcrs((runwayStart:position - runwayEnd:position):normalized, ship:up:vector):normalized * (runwayStart:position - runwayEnd:position):mag. local sidewaysNav to  ship:body:geopositionof(runwayStart:position + 12 * (runwayStart:position - runwayEnd:position) + 10 * sidewaysPos).

    set  self:waypoints to  list(
      list(sidewaysNav, rotationHeight),
      list(rotationNav, rotationHeight),
      list(approach, approachHeight),
      list(runwayStart, 100),
      list(runwayEnd, 100)
    ).
    set  self:groundAltReference to  "0".
    set  self:landingStartAlt to  alt:radar.}.
set  prototype["land"] to { parameter self. local aimpoint to  Planed:aimPoint( Planed,self:waypoints). local target to  self:waypoints[0][0]:altitudePosition(self:waypoints[0][1]). local flatTarget to  self:waypoints[0][0]:altitudePosition(0). local flatCurrent to  ship:geoposition:altitudePosition(0). local dist to  (flatCurrent - flatTarget):mag. local aimGeo to  ship:body:geopositionof(aimpoint).
    
    self:hold(
    
    self,aimGeo:bearing).

    //clearvecdraws();
    //vecdraw(ship:position, target - ship:position, RGB(1, 0, 0), "", 1, true);
    //vecdraw(ship:position, aimpoint - ship:position, RGB(0, 1, 0), "", 1, true);
    
    if ship:status <> "LANDED" {
      if self:waypoints:length = 0 {
        self:doRollFlat(
        self).
      } else {
        self:doRoll(
        self,aimGeo:bearing).
      }
    } else {
      set  ship:control:roll to  0.
      set  self:pitchLandingOffset to  0.
    }

    if dist < 2 * self:waypoints[0][1] {
      set  self:waypoints to  self:waypoints:sublist(1, self:waypoints:length - 1).
      set  self:landingStartAlt to  ship:altitude.
      
      if self:waypoints:length = 0 {
        set  self:targetAlt to  500.
        set  self:targetSpeed to  4 * self:minSpeed.
        set  self:pitchLandingOffset to  0.
        self:speedPid:reset().
        brakes off.
        self:setState(
        self,"takeoff").
        return.
      } else {
        set  self:landingStartDist to  (ship:position - self:waypoints[0][0]:altitudePosition(self:waypoints[0][1])):mag.
        if self:waypoints:length = 4 { local target to  self:minSpeed * 2.
          if self:targetSpeed > target
            set  self:targetSpeed to  target.
        } else if self:waypoints:length = 3 {
          set  self:targetSpeed to  self:minSpeed.
          brakes on.
        } else if self:waypoints:length = 2 {
          set  self:targetSpeed to  self:minSpeed.
          set  self:groundAltReference to  "1".
          self:speedPid:reset().
          brakes on.
          gear on.
        } else if self:waypoints:length = 1 {
          gear on.
          set  self:targetSpeed to  0.
          self:speedPid:reset().
          set  self:pitchLandingOffset to  0.05.
          brakes on.
        }
        set  self:targetAlt to  self:waypoints[0][1] - self:waypoints[0][0]:terrainheight.
      }
    }
    if self:waypoints:length <= 3 { local progress to  dist / self:landingStartDist.
      set  self:targetAlt to  self:landingStartAlt * progress + self:waypoints[0][1] * (1 - progress).
      lock wheelsteering to aimGeo:bearing.
    }}.
set  prototype["hold"] to { parameter self.
    parameter targetBearing to false. local pitch to  ship:facing:vector * up:vector.
    set  self:desiredPitchPid:setpoint to  self:targetAlt. local curAlt to  alt:radar.
    if self:groundAltReference <> "1" {
      set  curAlt to  ship:altitude.
    }
    set  self:desiredPitchPid:maxoutput to  self:maxPitch.
    set  self:desiredPitchPid:minoutput to  -self:maxPitch. local speed to  ship:velocity:surface:mag. local desiredPitch to  (self:targetAlt - curAlt) / ship:velocity:surface:mag / 2.
    if desiredPitch > self:maxPitch {
      set  desiredPitch to  self:maxPitch.
    }
    if desiredPitch < -self:maxPitch {
      set  desiredPitch to  -self:maxPitch.
    }

    set  self:pitchPid:setpoint to  desiredPitch.
    set  ship:control:pitch to  self:pitchPid:update(time:seconds, pitch) + self:pitchLandingOffset + self:pitchOffset + ship:control:pilotpitch. local dSpeed to  self:targetSpeed.
    if not targetBearing:istype("boolean") { local alignment to  1 - abs(targetBearing) / 180.
      set  alignment to  alignment * alignment * alignment.
      set  dSpeed to  dSpeed * alignment + min(self:minSpeed, dSpeed) * (1 - alignment).
    }

    set  self:speedPid:setpoint to  dSpeed. local desThrottle to  self:speedPid:update(time:seconds, ship:velocity:surface:mag).
    lock throttle to desThrottle.}.
set  result["aimPoint"] to { parameter self,waypoints. local target to  waypoints[0][0]:altitudePosition(waypoints[0][1]). local shipToTarget to  target - ship:position. local next to  target + shipToTarget.
    if waypoints:length > 1 {
      set  next to  waypoints[1][0]:altitudePosition(waypoints[1][1]).
    } local targetAxis to  (next - target):normalized. local progress to  (shipToTarget * targetAxis). local reprojected to  progress * targetAxis. local axisOffset to  shipToTarget - reprojected.

    if progress < 0 {
      // We're on the wrong side of the waypoint, fly parallel for a bit
      set  axisOffset to  -axisOffset.
    }

    return target + axisOffset.}.
set  prototype["doRoll"] to { parameter self,bearing. local roll to  ship:facing:starvector * up:vector.
    set  self:desiredRollPid:maxoutput to  self:maxRoll.
    set  self:desiredRollPid:minoutput to  -self:maxRoll. local desiredRoll to  self:desiredRollPid:update(time:seconds, bearing).
    set  self:rollPid:setpoint to  desiredRoll.
    set  ship:control:roll to  -self:rollPid:update(time:seconds, roll) + ship:control:pilotroll.}.
set  prototype["doRollFlat"] to { parameter self.
    set  self:rollPid:setpoint to  0.
    set  ship:control:roll to  -self:rollPid:update(time:seconds, roll) + ship:control:pilotroll.}.
return  result.}):call().
set  export to  Planed.