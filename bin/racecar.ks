local  _Callback to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,target, method.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:call to  { parameter self, args.
      return target[method]:call(target, args).
    }.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
_Callback:null to  lex("call", { parameter s,_. }).
global Callback to _Callback.
local  Racecar to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id. set 
    self:output to  output. set 
    self:status to  -1. set 
    self:fs to  fs. set 
    self:callback to  resultCallback. set 
    self:lastJump to  0. set 
    self:cruisecontrol to  0. set 
    self:output:signalHandler to  Callback: new( Callback,self, "onSignal"). set 

    self:stage to  "drive".

    if input:haskey("closeHandler") { set 
      self:streaming to  true. set 
      input:closeHandler to  Callback: new( Callback,self, "onClose").
    } set 
    
    self:registered to  true.
local  __var49 to 
    self:fs:loop. __var49:register( __var49,self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onSignal"] to { parameter self,type.
    if type = "End" { set 
      self:stage to  "done".
      return true.
    }
    return false.}.
set  prototype["run"] to { parameter self.
    self[self:stage](self).}.
set  prototype["wait"] to { parameter self.
    print("Deregister failed").}.
set  prototype["drive"] to { parameter self.
    if ship:status <> "LANDED" { set 
      self:lastJump to  time:seconds.
    } local isJumping to  true.
    if time:seconds - self:lastJump > 2 { set 
      isJumping to  false.
    } local bearing to  ship:control:pilotyaw * 90. local surfVelApprox to  vxcl(ship:up:vector, ship:velocity:surface). local velVec to  ship:velocity:surface.
    if velVec:mag < 1 { set 
      velVec to  ship:facing:vector.
    } set 
    velVec to  velVec:normalized. local shipRight to  vcrs(up:vector, velVec):normalized. local shipForward to  -vcrs(up:vector, shipRight):normalized. local terrainProbeCenterPos to  ship:position + shipForward * (surfVelApprox:mag + 5). local terrainProbeBackwdPos to  ship:position + shipForward * (surfVelApprox:mag + 4). local terrainProbeRightVec to  vcrs(up:vector, ship:facing:vector). local terrainProbeRightPos to  terrainProbeCenterPos + terrainProbeRightVec. local tg0 to  body:geopositionof(terrainProbeCenterPos). local tg1 to  body:geopositionof(terrainProbeBackwdPos). local tg2 to  body:geopositionof(terrainProbeRightPos). local p0 to  tg0:altitudeposition(tg0:terrainheight + 1). local p1 to  tg1:altitudeposition(tg1:terrainheight + 1). local p2 to  tg2:altitudeposition(tg2:terrainheight + 1). local tmpTerrainRight to  (p2 - p0):normalized. local tmpTerrainFwd to  (p0 - p1):normalized. local terrainUp to  vcrs(tmpTerrainFwd, tmpTerrainRight):normalized. local terrainRight to  vcrs(terrainUp, tmpTerrainFwd):normalized. local terrainFwd to  vcrs(terrainRight, terrainUp):normalized. local pitchDiff to  ship:facing:upvector * terrainFwd. local rollDiff to  ship:facing:upvector * terrainRight. local yawDiff to  ship:facing:vector * terrainRight. local unsafe to  isJumping.
    if abs(rollDiff) > 0.2 or abs(pitchDiff) > 0.2 or abs(pitchDiff) > 0.2 set 
      unsafe to  true. local alignment to  max(0.01, 1 - 2 * (abs(rollDiff) + abs(pitchDiff) + abs(yawDiff))). local targetSpeed to  50 * alignment + 1 * (1 - alignment).
    if ship:control:pilotpitch <> 0 { set 
      targetSpeed to  max(0, targetSpeed * -ship:control:pilotpitch). set 
      self:cruisecontrol to  floor(ship:velocity:surface:mag).
    } else { set 
      targetSpeed to  self:cruisecontrol.
    }

    //print("p: " + round(pitchDiff * 25, 4) + ", r: " + round(rollDiff * 25, 4) + ", y: " + round(yawDiff * 25, 4) + ", " + ship:status);
    if unsafe { set 
      ship:control:wheelsteer to  0.001. set 
      ship:control:wheelthrottle to  1.
    } else { local maxSteer to  1 / min(25, max(1, ship:velocity:surface:mag - 1)^1.8). set 
      ship:control:wheelsteer to  min(maxSteer, max(-maxSteer, bearing * -2)). set 
      ship:control:wheelthrottle to  targetSpeed - ship:velocity:surface:mag.
    } local desiredFwd to  terrainFwd - ship:control:wheelsteer * terrainRight * 10.
    if ship:velocity:surface:mag < 1 {
      lock steering to "kill". set 
      ship:control:wheelthrottle to  -ship:control:pilotpitch.
    } else { local desiredDir to  lookdirup(desiredFwd, terrainUp).
      lock steering to desiredDir.
    }

    if ship:velocity:surface:mag - targetSpeed > 0.5 and not unsafe
      brakes on.
    else
      brakes off.

    if brakes { set 
      ship:control:wheelthrottle to  0.
    }}.
set  prototype["done"] to { parameter self. set 
    ship:control:wheelsteer to  0. set 
    ship:control:wheelthrottle to  0.
    unlock steering.
    brakes on. set 
    self:stage to  "none".

    if self:registered { set 
      self:registered to  false.
local  __var50 to 
      self:fs:loop. __var50:deregister( __var50,self).
    } set 
    self:status to  0.
local  __var51 to 
    self:callback. __var51:call( __var51,self:pid).}.
set  prototype["none"] to { parameter self.
    print("deregister failed").}.
return  result.}):call(). set 
export to  Racecar.