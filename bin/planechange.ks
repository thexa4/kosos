local  Planechange to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result.
    self:progSetup(
    self,id, input, output, args, env, fs, resultCallback).

    if not hasnode {
      self:print(
      self,"Planechange needs a manouver node, please schedule one with `schedule`.").
      self:print(
      self,"Usage:").
      self:print(
      self,"planechange").
      self:exit(
      self,1).
      return self.
    } local activeNode to  allnodes[allnodes:length - 1]. local oldNode to  Node(activeNode:time, activeNode:radialout, activeNode:normal, activeNode:prograde). set  
    
    // zero active node to avoid funny stuff
    activeNode:prograde to  0. set 
    activeNode:normal to  0. set 
    activeNode:radialout to  0. local vat to  velocityat(ship, activeNode:time):orbit. local pat to  positionat(ship, activeNode:time). local distanceVec to  pat - ship:body:position. local upVec to  distanceVec:normalized. local progradeBasis to  vat:normalized. local normalBasis to  vcrs(progradeBasis, upVec):normalized. local radialBasis to  vcrs(normalBasis, progradeBasis):normalized. local nodedv to  progradeBasis * oldNode:prograde + 
                 normalBasis * oldNode:normal +
                 radialBasis * oldNode:radialout. local startVel to  vat + nodedv. local tgt_orbit to  false.
    if hastarget { set 
      tgt_orbit to  target:obt.
    } else { local robt to  ship:obt. set 
      tgt_orbit to  createorbit(0, robt:eccentricity, robt:semimajoraxis, robt:lan, robt:argumentofperiapsis, robt:meananomalyatepoch, robt:epoch, robt:body).
    } local desiredNormal to  vcrs(tgt_orbit:position - tgt_orbit:body:position, tgt_orbit:velocity:orbit):normalized. local desiredChange to  -(desiredNormal * startVel) * desiredNormal. local desiredDelta to  nodedv + desiredChange. set 

    activeNode:prograde to  desiredDelta * progradeBasis. set 
    activeNode:normal to  desiredDelta * normalBasis. set 
    activeNode:radialout to  desiredDelta * radialBasis.
    
    self:exit(
    
    self,0).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["run"] to { parameter self.}.
return  result.}):call(). set 
export to  Planechange.