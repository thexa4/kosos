local ShowRoute to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","ShowRoute").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:pid to  id.
    if args:length <> 1 {
      output:write(output,"Usage: simplifyroute").
      set self:status to  1.
local __var274 to fs:loop. __var274:defer(__var274,resultCallback, id).
      return self.
    }
    set self:output to  output.
    set self:status to  -1.
    set self:interrupted to  false.
    set self:resultCallback to  resultCallback.
    set self:fs to  fs.
    set self:origin to  false.
    set self:reference to  false.
    set self:lastPoint to  false.
    set self:lastGeo to  false.
    set self:reader to  LineReader:new(LineReader,input, fs).
local __var275 to self:reader. __var275:readLine(__var275,Callback:new(Callback,self, "onWaypoint")).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["parseGeo"] to { parameter self,str.
    local parts to  str:trim:split(";").
    if parts:length <> 3 {
      return false.
    } else {
      local lat to  parts[1]:tonumber(0).
      local lng to  parts[2]:tonumber(0).
      return latlng(lat, lng).
    }}.
set  prototype["onWaypoint"] to { parameter self,line.
    if self:interrupted
      return.
    if line:istype("Boolean") {
      if not self:lastPoint:istype("Boolean") {
local __var276 to self:output. __var276:append(__var276,self:lastPoint, Callback:null).
      }
      set self:interrupted to  true.
      set self:status to  0.
local __var277 to self:fs:loop. __var277:defer(__var277,self:resultCallback, self:pid).
      return.
    }

    if line:startswith("#") {
local __var278 to self:reader. __var278:readLine(__var278,Callback:new(Callback,self, "onWaypoint")).
      return.
    }

    local geo to  self:parseGeo(self,line).
    if geo:istype("Boolean") {
local __var279 to self:reader. __var279:readLine(__var279,Callback:new(Callback,self, "onWaypoint")).
      return.
    }
    if self:origin:istype("Boolean") {
local __var280 to self:output. __var280:append(__var280,line, Callback:null).
      set self:origin to  geo:position.
local __var281 to self:reader. __var281:readLine(__var281,Callback:new(Callback,self, "onWaypoint")).
      return.
    }

    if self:reference:istype("Boolean") {
      set self:reference to  geo:position - self:origin.
      set self:lastGeo to  geo.
      set self:lastPoint to  line.
local __var282 to self:reader. __var282:readLine(__var282,Callback:new(Callback,self, "onWaypoint")).
      return.
    }

    local projection to  (geo:position - self:origin) * self:reference:normalized.
    local reProject to  self:origin + self:reference:normalized * projection.
    local rePos to  body:geopositionof(reProject):position.

    local delta to  (geo:position - rePos):mag.
    if delta > 10 {
local __var283 to self:output. __var283:append(__var283,self:lastPoint, Callback:null).
      set self:origin to  self:lastGeo:position.
      set self:reference to  geo:position - self:origin.
      set self:lastPoint to  line.
      set self:lastGeo to  geo.
local __var284 to self:reader. __var284:readLine(__var284,Callback:new(Callback,self, "onWaypoint")).
      return.
    }
    set self:lastPoint to  line.
    set self:lastGeo to  geo.
local __var285 to self:reader. __var285:readLine(__var285,Callback:new(Callback,self, "onWaypoint")).}.
set  prototype["onSignal"] to { parameter self,signal.
    if self:interrupted
      return false.
    if signal <> "End"
      return false.
    set self:interrupted to  true.
    set self:status to  0.
local __var286 to self:fs:loop. __var286:defer(__var286,self:resultCallback, self:pid).
    return true.}.
return  result.}):call().
set export to  ShowRoute.
