local Balloon to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Balloon").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:pid to  id.
    set self:status to  0.
    set self:input to  input.
    set self:output to  output.
    set self:env to  env.
    set self:fs to  fs.
    set self:resultCallback to  resultCallback.
    set self:running to  false.
    if args:length = 1 or args[1] = "help" {
      self:help(self).
      return self.
    }

    local cmds to  lex("takeoff", "takeoff", "heading", "heading", "ditch", "ditch", "waypoint", "heading", "geo", "heading", "land", "heading", "set", false).
    if not cmds:haskey(args[1]) {
      self:help(self).
      return self.
    }
    set self:chosenCommand to  args[1].
    set self:desiredState to  cmds[args[1]].
    set self:desiredAlt to  false.
    set self:desiredHeading to  false.
    set self:desiredSpeed to  false.
    set self:desiredGroundRef to  false.
    set self:waitUntilDone to  false.
    set self:chosenGeo to  false.

    set self:waypointAccuracy to  500.
    if self:env:haskey("WAYPOINT_ACCURACY") and env:WAYPOINT_ACCURACY:toscalar(0) > 0 {
      set self:waypointAccuracy to  env:WAYPOINT_ACCURACY:toscalar(0).
    }

    local toParse to  args:sublist(2, args:length - 2).
    until toParse:length = 0 {
      if toParse[0] = "wait" {
        set self:waitUntilDone to  true.
        set toParse to  toParse:sublist(1, toParse:length).
      } else {
        if toParse:length < 2 {
          self:help(self).
          return self.
        }
        local param to  toParse[0].
        if param = "alt" {
          set self:desiredAlt to  toParse[1]:tonumber(-1000).
          if self:desiredAlt = -1000 {
            self:help(self).
            return self.
          }
        } else if param = "heading" {
          set self:desiredHeading to  toParse[1]:tonumber(-1000).
          if self:desiredHeading = -1000 {
            self:help(self).
            return self.
          }
        } else if param = "speed" {
          set self:desiredSpeed to  toParse[1]:tonumber(-1000).
          if self:desiredSpeed = -1000 {
            self:help(self).
            return self.
          }
        } else if param = "groundref" {
          set self:desiredGroundRef to  toParse[1]:tonumber(-1000).
          if self:desiredGroundRef = -1000 {
            self:help(self).
            return self.
          }
        } else if param = "geo" {
          if not toParse[1]:startswith("G;") {
            self:help(self).
            return self.
          }
          local parts to  toParse[1]:split(";").
          if parts:length <> 3 {
            self:help(self).
            return self.
          }
          local lat to  parts[1]:tonumber(-1000).
          local lng to  parts[2]:tonumber(-1000).
          if lat < -90 or lat > 90 or lng < -180 or lng > 180 {
            self:help(self).
            return self.
          }
          set self:chosenGeo to  latlng(lat, lng).
        } else {
          output:append(output,"Unable to set property " + param, Callback:null).
          self:help(self).
          return self.
        }

        set toParse to  toParse:sublist(2, toParse:length).
      }
    }
    if self:chosenCommand = "waypoint" or self:chosenCommand = "land" {
      if self:chosenGeo:istype("Boolean") {
        for waypoint in allwaypoints() {
          if waypoint:isselected {
            set self:chosenGeo to  waypoint:geoposition.
          }
        }
      }
    }
    
    if self:chosenCommand = "waypoint" or self:chosenCommand = "geo" or self:chosenCommand = "land" {
      set self:waitUntilDone to  true.
      if self:chosenCommand = "land" {
        set self:desiredSpeed to  "" + 0.
        set self:desiredAlt to  "" + MAX(alt:radar, 500).
        set self:desiredGroundRef to  "1".

        set self:rcsFwdPid to  PidLoop(1, 0, 2).
        set self:rcsFwdPid:minoutput to  -1.
        set self:rcsFwdPid:maxoutput to  1.
        set self:rcsSidePid to  PidLoop(3, 0, 3).
        set self:rcsSidePid:minoutput to  -1.
        set self:rcsSidePid:maxoutput to  1.
        set self:landingState to  "rough_align".
      }
      if self:chosenGeo:istype("Boolean") {
        output:append(output,"No waypoint selected, unable to navigate", Callback:null).
        self:help(self).
        return self.
      } else {
        set self:currentSpeed to  self:desiredSpeed.
        set self:currentHeading to  round(self:chosenGeo:heading).
        set self:desiredHeading to  self:currentHeading.
        if self:desiredSpeed:istype("Boolean") {
          set self:desiredSpeed to  100.
          set self:currentSpeed to  100.
        }
      }
    }

    set self:stateFile to  false.
    set self:altFile to  false.
    set self:headingFile to  false.
    set self:speedFile to  false.
    set self:groundRefFile to  false.
    set self:toRead to  0.
    set self:landingDebounce to  0.

    set self:loading to  true.
    set self:exited to  false.

    if not self:desiredState:istype("Boolean") {
      set self:toRead to  self:toRead + 1.
    }
    if not self:desiredAlt:istype("Boolean") {
      set self:toRead to  self:toRead + 1.
    }
    if not self:desiredHeading:istype("Boolean") {
      set self:toRead to  self:toRead + 1.
    }
    if not self:desiredSpeed:istype("Boolean") {
      set self:toRead to  self:toRead + 1.
    }
    if not self:desiredGroundRef:istype("Boolean") {
      set self:toRead to  self:toRead + 1.
    }
    if not self:desiredState:istype("Boolean") {
      fs:modify(fs,"/sys/class/plane/state", Callback:new(Callback,self, "onOpenState")).
    }
    if not self:desiredAlt:istype("Boolean") {
      fs:modify(fs,"/sys/class/plane/targetAlt", Callback:new(Callback,self, "onOpenAlt")).
    }
    if not self:desiredHeading:istype("Boolean") {
      fs:modify(fs,"/sys/class/plane/targetHeading", Callback:new(Callback,self, "onOpenHeading")).
    }
    if not self:desiredSpeed:istype("Boolean") {
      fs:modify(fs,"/sys/class/plane/targetSpeed", Callback:new(Callback,self, "onOpenSpeed")).
    }
    if not self:desiredGroundRef:istype("Boolean") {
      fs:modify(fs,"/sys/class/plane/groundAltReference", Callback:new(Callback,self, "onOpenGroundRef")).
    }
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onOpenState"] to { parameter self,file.
    if not self:loading
      return.
    
    if file:istype("Boolean") {
      return self:helpNotRunning(self).
    }

    set self:stateFile to  file.
    set self:toRead to  self:toRead - 1.
    if self:toRead = 0 {
      self:onAllLoaded(self).
    }}.
set  prototype["onOpenAlt"] to { parameter self,file.
    if not self:loading
      return.
    
    if file:istype("Boolean") {
      return self:helpNotRunning(self).
    }

    set self:altFile to  file.
    set self:toRead to  self:toRead - 1.
    if self:toRead = 0 {
      self:onAllLoaded(self).
    }}.
set  prototype["onOpenHeading"] to { parameter self,file.
    if not self:loading
      return.
    
    if file:istype("Boolean") {
      return self:helpNotRunning(self).
    }

    set self:headingFile to  file.
    set self:toRead to  self:toRead - 1.
    if self:toRead = 0 {
      self:onAllLoaded(self).
    }}.
set  prototype["onOpenSpeed"] to { parameter self,file.
    if not self:loading
      return.
    
    if file:istype("Boolean") {
      return self:helpNotRunning(self).
    }

    set self:speedFile to  file.
    set self:toRead to  self:toRead - 1.
    if self:toRead = 0 {
      self:onAllLoaded(self).
    }}.
set  prototype["onOpenGroundRef"] to { parameter self,file.
    if not self:loading
      return.
    
    if file:istype("Boolean") {
      return self:helpNotRunning(self).
    }

    set self:groundRefFile to  file.
    set self:toRead to  self:toRead - 1.
    if self:toRead = 0 {
      self:onAllLoaded(self).
    }}.
set  prototype["onAllLoaded"] to { parameter self.
    set self:loading to  false.

    if not self:desiredState:istype("Boolean") {
local __var70 to self:stateFile. __var70:write(__var70,"" + self:desiredState, Callback:null).
    }
    if not self:desiredAlt:istype("Boolean") {
local __var71 to self:altFile. __var71:write(__var71,"" + self:desiredAlt, Callback:null).
    }
    if not self:desiredHeading:istype("Boolean") {
local __var72 to self:headingFile. __var72:write(__var72,"" + self:desiredHeading, Callback:null).
    }
    if not self:desiredSpeed:istype("Boolean") {
local __var73 to self:speedFile. __var73:write(__var73,"" + self:desiredSpeed, Callback:null).
    }
    if not self:desiredGroundRef:istype("Boolean") {
local __var74 to self:groundRefFile. __var74:write(__var74,"" + self:desiredGroundRef, Callback:null).
    }
    if self:waitUntilDone {
      
      if self:chosenCommand = "takeoff" or self:chosenCommand = "land" or self:chosenCommand = "ditch" {
        if self:chosenCommand <> "takeoff" and self:chosenCommand <> "land" {
          print("todo: implement waiting on " + self:chosenCommand).
          
          set self:status to  1.
local __var75 to self:fs:loop. __var75:defer(__var75,self:resultCallback, self:pid).
          return.
        }
local __var76 to self:fs:loop. __var76:register(__var76,self).
        set self:running to  true.
        return.
      }
      if self:chosenCommand = "waypoint" or self:chosenCommand = "land" {
local __var77 to self:fs:loop. __var77:register(__var77,self).
        set self:running to  true.
        return.
      }
      set self:status to  0.
local __var78 to self:fs:loop. __var78:defer(__var78,self:resultCallback, self:pid).
      return.
      
    } else {
      set self:status to  0.
local __var79 to self:fs:loop. __var79:defer(__var79,self:resultCallback, self:pid).
    }}.
set  prototype["helpNotRunning"] to { parameter self.
local __var80 to self:output. __var80:callback(__var80,"Unable to open planed config files.", Callback:null).
local __var81 to self:output. __var81:callback(__var81,"Is balloond running?", Callback:null).
local __var82 to self:output. __var82:callback(__var82,"If not, try starting with 'balloond detach'", Callback:null).
    
    set self:status to  1.
    set self:loading to  false.
local __var83 to self:fs:loop. __var83:defer(__var83,self:resultCallback, self:pid).}.
set  prototype["help"] to { parameter self.
local __var84 to self:output. __var84:append(__var84,"Usage: balloon <subcommand> [wait] [alt <altitude>] [heading <heading>] [speed <speed>] [groundref <0|1>]", Callback:null).
local __var85 to self:output. __var85:append(__var85,"", Callback:null).
local __var86 to self:output. __var86:append(__var86,"Subcommands available:", Callback:null).
local __var87 to self:output. __var87:append(__var87," - help      Shows this help", Callback:null).
local __var88 to self:output. __var88:append(__var88," - set       Set flight parameters", Callback:null).
local __var89 to self:output. __var89:append(__var89," - takeoff   Launch plane", Callback:null).
local __var90 to self:output. __var90:append(__var90," - heading   Keep constant heading", Callback:null).
local __var91 to self:output. __var91:append(__var91," - waypoint  Go to waypoint", Callback:null).
local __var92 to self:output. __var92:append(__var92," - ditch     Land immediately", Callback:null).
local __var93 to self:output. __var93:append(__var93," - land      Land on waypoint", Callback:null).
    set self:status to  1.
    set self:running to  false.
local __var94 to self:fs:loop. __var94:defer(__var94,self:resultCallback, self:pid).}.
set  prototype["onSignal"] to { parameter self,signal.
    if not self:running {
      return false.
    }
    if signal = "End" {
      self:stopDaemon(self).
      return true.
    }}.
set  prototype["stopDaemon"] to { parameter self.
    if not self:running
      return.

    if not self:exited {
      set self:exited to  true.
local __var95 to self:fs:loop. __var95:deregister(__var95,self).
      set self:status to  0.
      set self:exited to  true.
      set self:running to  false.
local __var96 to self:fs:loop. __var96:defer(__var96,self:resultCallback, self:pid).
    }}.
set  prototype["run"] to { parameter self.
    if self:chosenCommand = "takeoff" and ship:status <> "LANDED" {
      set self:status to  0.
      set self:exited to  true.
      set self:running to  false.
local __var97 to self:fs:loop. __var97:deregister(__var97,self).
local __var98 to self:fs:loop. __var98:defer(__var98,self:resultCallback, self:pid).
    }
    
    if self:chosenCommand = "waypoint" {
      local new_heading to  round(self:chosenGeo:heading).
      if new_heading <> self:currentHeading {
        set self:currentHeading to  new_heading.
local __var99 to self:headingFile. __var99:write(__var99,"" + new_heading, Callback:null).
      }

      local distance to  (ship:geoposition:altitudeposition(0) - self:chosenGeo:altitudeposition(0)):mag.
      if distance < self:waypointAccuracy {
        set self:status to  0.
        set self:exited to  true.
        set self:running to  false.
local __var100 to self:fs:loop. __var100:deregister(__var100,self).
local __var101 to self:fs:loop. __var101:defer(__var101,self:resultCallback, self:pid).
      }
      local new_speed to  round(distance / 240).
      if new_speed < self:currentSpeed {
        set self:currentSpeed to  new_speed.
local __var102 to self:speedFile. __var102:write(__var102,"" + new_speed, Callback:null).
      }
    }

    if self:chosenCommand = "land" {
      rcs on.

      local cur_offset to  self:chosenGeo:altitudeposition(0) - ship:geoposition:altitudeposition(0).
      local fwd_offset to  ship:facing:vector * cur_offset.
      local side_offset to  ship:facing:starvector * cur_offset.

      local speed_multiplier to  0.9 * min(500, alt:radar) / 500 + 0.1.

      local desired_fwd to  -fwd_offset * speed_multiplier / 4.
      local desired_side to  -side_offset * speed_multiplier / 4.
      if desired_fwd > 10 {
        set desired_fwd to  10.
      }
      if desired_fwd < -10 {
        set desired_fwd to  -10.
      }
      if desired_side > 10 {
        set desired_side to  10.
      }
      if desired_side < -10 {
        set desired_side to  -10.
      }

      local cur_fwd to  ship:velocity:surface * -ship:facing:vector.
      local cur_side to  ship:velocity:surface * -ship:facing:starvector.

      set self:rcsFwdPid:setpoint to  desired_fwd.
      set self:rcsSidePid:setpoint to  desired_side.
      local fwd_thrust to  -self:rcsFwdPid:update(time:seconds, cur_fwd).
      local side_thrust to  -self:rcsSidePid:update(time:seconds, cur_side).

      set ship:control:fore to  fwd_thrust.
      set ship:control:starboard to  side_thrust.

      if ship:status = "landed" {
        if self:landingDebounce = 0 {
          set self:landingDebounce to  time:seconds + 5.
        }
        if self:landingDebounce <= time:seconds {
local __var103 to self:stateFile. __var103:write(__var103,"land", Callback:null).
          gear on.
          brakes on.
          set self:status to  0.
          set self:exited to  true.
          set self:running to  false.
local __var104 to self:fs:loop. __var104:deregister(__var104,self).
local __var105 to self:fs:loop. __var105:defer(__var105,self:resultCallback, self:pid).
          return.
        }
      } else {
        set self:landingDebounce to  0.
      }

      if self:landingState = "rough_align" {
        if cur_offset:mag < 300 {
local __var106 to self:output. __var106:append(__var106,"fine align, " + round(cur_offset:mag) + "m", Callback:null).
          set self:landingState to  "fine_align".
local __var107 to self:altFile. __var107:write(__var107,"300", Callback:null).
          return.
        }
      }
      if self:landingState = "fine_align" {
        if alt:radar < 320 and alt:radar > 250 and cur_offset:mag < 100 {
local __var108 to self:output. __var108:append(__var108,"drop, " + round(cur_offset:mag) + "m", Callback:null).
          set self:landingState to  "drop".
local __var109 to self:altFile. __var109:write(__var109,"50", Callback:null).
        }
      }
      if self:landingState = "drop" {
        if alt:radar < 70 and alt:radar > 30 and cur_offset:mag < 10 {
local __var110 to self:stateFile. __var110:write(__var110,"ditch", Callback:null).
          return.
        }
      }
    }}.
return  result.}):call().

set export to  Balloon.
