local  MountVolume to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id.
    if args:length <> 3 {
      output:append(
      output,"Usage: mount.volume <tag> <dest>", Callback:null). set 
      self:status to  1.
local  __var0 to 
      fs:loop. __var0:defer( __var0,resultCallback, id).
      return self.
    } local tag to  args[1]. local processors to  ship:partstagged(tag).
    if processors:length = 0 {
      output:append(
      output,"Processor " + tag + " not found.", Callback:null).
      output:append(
      output,"Usage: mount.volume <tag> <dest>", Callback:null). set 
      self:status to  1.
local  __var1 to 
      fs:loop. __var1:defer( __var1,resultCallback, id).
      return self.
    } local cpu to  processors[0]:getmodule("kOSProcessor").
    fs:mount(
    fs, VolumeFilesystemDir:new( VolumeFilesystemDir,cpu:volume:name, "/", cpu:volume:name + ":", cpu:connection, false, fs), args[2]). set 
    self:status to  0.
local  __var2 to 
    fs:loop. __var2:defer( __var2,resultCallback, id).
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call(). set 
export to  MountVolume.