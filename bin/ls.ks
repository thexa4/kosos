local Ls to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Ls").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:pid to  id.
    set self:fs to  fs.
    set self:resultCallback to  resultCallback.
    set self:status to  0.
    set self:output to  output.
    if args:length > 2 {
      output:append(output,"Usage: ls [folder]", Callback:null).
      set self:status to  1.
local __var0 to fs:loop. __var0:defer(__var0,resultCallback, id).
      return self.
    }
    local relative to  "".
    if args:length = 2
      set relative to  args[1].
    local pathname to  Util:absPath(Util,relative, env).
    
    local prefixes to  fs:mounts(fs).
    set self:extra to  list().
    for prefix in prefixes {
      if prefix:startsWith(pathname) {
        local rest to  prefix:substring(pathname:length, prefix:length - pathname:length).
        if rest:length > 0 and rest[0] = "/" {
          local entry to  rest:substring(1, rest:length - 1).
          if entry:endsWith("/")
            set entry to  entry:substring(0, entry:length - 1).
          if not entry:contains("/") and entry:length > 0
            self:extra:add(entry).
        }
      }
    }

    fs:open(fs,pathname, Callback:new(Callback,self, "onOpen")).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onOpen"] to { parameter self,dir.
    if not dir:istype("Lexicon") {
      set self:status to  1.
local __var1 to self:output. __var1:append(__var1,"No such directory", Callback:null).
local __var2 to self:fs:loop. __var2:defer(__var2,self:resultCallback, self:pid).
      return.
    }

    if dir:getType(dir) <> "dir" { 
      set self:status to  1.
local __var3 to self:output. __var3:append(__var3,"Not a directory", Callback:null).
local __var4 to self:fs:loop. __var4:defer(__var4,self:resultCallback, self:pid).
      return.
    }

    dir:read(dir,Callback:new(Callback,self, "onRead")).}.
set  prototype["onRead"] to { parameter self,files.
    if files:istype("Boolean") {
      set self:status to  1.
local __var5 to self:output. __var5:append(__var5,"Unable to fetch directory contents.", Callback:null).
local __var6 to self:fs:loop. __var6:defer(__var6,self:resultCallback, self:pid).
      return.
    }

    local items to  list().
    for file in self:extra {
      if not file:trim:startswith(".") {
        items:add(file).
      }
    }
    for file in files {
      if not file:trim:startswith(".") {
        items:add(file).
      }
    }
    set items to  Util:sort(Util,items).
    for item in items {
local __var7 to self:output. __var7:append(__var7,item, Callback:null).
    }
    set self:status to  0.
local __var8 to self:fs:loop. __var8:defer(__var8,self:resultCallback, self:pid).}.
return  result.}):call().
set export to  Ls.