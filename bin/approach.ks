local  Approach to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id. set 
    self:output to  output. set 
    self:status to  -1. set 
    self:fs to  fs. set 
    self:callback to  resultCallback. set 

    self:stage to  "coast".
local  __var151 to 
    self:output. __var151:append( __var151,"Coasting", Callback:null). set 
    self:approaching to  true. set 
    self:targetDist to  200. set 
    self:fired to  false. set 
    self:steering to  ship:facing:vector. set 
    self:throttle to  0.

    lock steering to self:steering.
    lock throttle to self:throttle.

    if not hastarget {
      output:append(
      output,"No target selected, unable to rendezvous.", Callback:null). set 
      self:status to  1.
local  __var152 to 
      fs:loop. __var152:defer( __var152,resultCallback, id).
    } else { set 
      self:target to  target. set 
      self:distance to  (target:position - ship:position):mag.
local  __var153 to 

      fs:loop. __var153:register( __var153,self).
    }
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["aim"] to { parameter self. local delta to  self:target:position - ship:position. local speed to  0.
    if self:approaching { set 
      speed to  min(50, max(0, delta:mag - self:targetDist) / 50).
    } local velDiff to  self:target:velocity:orbit - ship:velocity:orbit. set 
    self:burnDir to  velDiff + delta:normalized * speed. set 
    self:steering to  self:burnDir. local alignment to  self:burnDir:normalized * ship:facing:vector - 0.7.
    print alignment at (0, 1).
    print self:burnDir:mag at (0, 2).}.
set  prototype["run"] to { parameter self.
    self[self:stage](self).}.
set  prototype["coast"] to { parameter self. set 
    self:throttle to  0. local prevDist to  self:distance. set 
    self:distance to  (self:target:position - ship:position):mag.

    if self:distance < self:targetDist { set 
      self:stage to  "stop".
local  __var154 to 
      self:output. __var154:append( __var154,"too close", Callback:null).
      return.
    }

    self:aim(

    self).

    if prevDist >= self:distance
      return. set 

    self:fired to  false. set 
    self:stage to  "align".
local  __var155 to 
    self:output. __var155:append( __var155,"align", Callback:null).}.
set  prototype["align"] to { parameter self.
    self:aim(
    self). local alignment to  self:burnDir:normalized * ship:facing:vector - 0.7.
    if self:burnDir:mag < 0.2 or (alignment < 0 and self:fired) { set 
      self:throttle to  0. set 
      self:stage to  "coast".
local  __var156 to 
      self:output. __var156:append( __var156,"coast", Callback:null).
      return.
    }
    if self:distance < self:targetDist { set 
      self:stage to  "stop".
local  __var157 to 
      self:output. __var157:append( __var157,"too close", Callback:null).
      return.
    }

    if alignment > 0
      set self:fired to true. set 
    self:throttle to  self:burnDir:mag * alignment / 10.}.
set  prototype["stop"] to { parameter self. set 
    self:approaching to  false.

    self:aim(

    self).

    if self:burnDir:mag < 0.1 { set 
      self:throttle to  0.
      unlock throttle.
      unlock steering.
local  __var158 to 
      self:fs:loop. __var158:deregister( __var158,self). set 
      
      self:stage to  "none".
local  __var159 to 
      self:output. __var159:append( __var159,"done", Callback:null). set 
      self:status to  0.
local  __var160 to 
      self:callback. __var160:call( __var160,self:pid).
      return.
    } local alignment to  self:burnDir:normalized * ship:facing:vector - 0.7. set 
    self:throttle to  self:burnDir:mag * alignment / 10.}.
set  prototype["none"] to { parameter self.
    print("deregister failed").}.
return  result.}):call(). set 
export to  Approach.