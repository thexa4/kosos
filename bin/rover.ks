local _Callback to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_Callback").
set  result["new"] to { parameter type,target, method.
local self to prototype:copy.
set  self["type"] to  result.
    set self:call to  { parameter self, args.
      return target[method]:call(target, args).
    }.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call().
set _Callback:null to  lex("call", { parameter s,_. }).
global Callback to _Callback.
local Rover to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Rover").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:pid to  id.
    set self:output to  output.
    set self:status to  -1.
    set self:fs to  fs.
    set self:callback to  resultCallback.
    set self:desiredSpeed to  10.
    set self:waypoints to  Queue().
    set self:lowestPath to  lex().
    set self:lastJump to  0.
    set self:dist to  0.
    set self:lastPos to  ship:position.
    set self:output:signalHandler to  Callback:new(Callback,self, "onSignal").
    set self:traveled to  0.
    set self:startTime to  time:seconds.
    set self:lastDistPos to  body:geopositionof(ship:position).
    set self:crewCount to  ship:crew:length.
    set self:arrow to  false.
    if env:haskey("debug") and env:debug = "1" {
      set self:arrow to  vecdraw(ship:position, up:vector, rgb(1,1,1), "", 1, false, 0.2, false).
    }
    set self:lasers to  list().
    local laserParts to  ship:partsTagged("forward").
    for p in laserParts {
      local mods to  p:modulesnamed("LaserDistModule").
      if mods:length > 0 {
        self:lasers:add(mods[0]).
      }
    }
    output:append(output,"" + self:lasers:length + " lasers registered", Callback:null).

    set self:stage to  "wait".
    set self:shouldResume to  false.
    if args:length = 2 and args[1] = "resume" {
      set self:shouldResume to  true.
    }

    if env:haskey("speed") {
      set self:desiredSpeed to  env:speed:tonumber(self:desiredSpeed).
    }

    set self:roverConf to  "".
    if exists("1:/rover.conf")
      set self:roverConf to  open("1:/rover.conf"):readall:string.
    
    set self:streaming to  true.
    set self:registered to  false.
    set self:input to  input.
    input:read(input,Callback:new(Callback,self, "onRead")).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onSignal"] to { parameter self,type.
    if type = "End" {
      set self:stage to  "done".
local __var100 to self:output. __var100:append(__var100,"Interrupted", Callback:null).
      return true.
    }
    return false.}.
set  prototype["onClose"] to { parameter self,_.
    set self:streaming to  false.
    if self:stage = "wait" {
      set self:stage to  "done".
    }
    return true.}.
set  prototype["onRead"] to { parameter self,data.
    if data:istype("Boolean") {
local __var101 to self:output. __var101:append(__var101,"Distance: " + ceiling(self:dist / 1000) + "km", Callback:null).
local __var102 to self:output. __var102:append(__var102,"ETA: " + ceiling(self:dist / (self:desiredSpeed * 0.66) / 60) + " mins", Callback:null).
      return.
    }

    local lines to  data:trim:split(char(10)).
    for line in lines {
      self:onReadLine(self,line).
    }
local __var103 to self:input. __var103:read(__var103,Callback:new(Callback,self, "onRead")).}.
set  prototype["onReadLine"] to { parameter self,data.
    if data:trim:startsWith("#")
      return.
    if data:trim:startsWith("G;") {
      local parts to  data:trim:split(";").
      if parts:length <> 3 {
        output:append(output,"Bad waypoint: " + data, Callback:null).
      } else {
        local lat to  parts[1]:tonumber(0).
        local lng to  parts[2]:tonumber(0).
        local geo to  latlng(lat, lng).
        local pos to  geo:altitudeposition(geo:terrainheight + 1).
        set self:dist to  self:dist + (self:lastPos - pos):mag.
        set self:lastPos to  pos.
        if data:trim = self:roverConf and self:shouldResume {
local __var104 to self:output. __var104:append(__var104,"Resuming from: " + data:trim, Callback:null).
          self:waypoints:clear().
          set self:dist to  0.
        }
        self:waypoints:push(list(geo, data:trim)).
        if self:stage = "wait" {
          set self:stage to  "drive".
          set self:registered to  true.
local __var105 to self:fs:loop. __var105:register(__var105,self).
        }
      }
    } else {
local __var106 to self:output. __var106:append(__var106,"Unknown waypoint: " + data, Callback:null).
    }}.
set  prototype["run"] to { parameter self.
    self[self:stage](self).}.
set  prototype["wait"] to { parameter self.
    print("Deregister failed").}.
set  prototype["drive"] to { parameter self.
    if self:waypoints:length = 0 {
      if self:streaming {
        if not self:arrow:istype("Boolean") {
          set self:arrow:show to  false.
        }
        set self:stage to  "wait".
        set self:registered to  false.
local __var107 to self:fs:loop. __var107:deregister(__var107,self).
local __var108 to self:output. __var108:append(__var108,"Buffering...", Callback:null).
      } else {
        set self:stage to  "done".
local __var109 to self:output. __var109:append(__var109,"Path finished", Callback:null).
      }
      return.
    }
    if ship:crew:length <> self:crewCount {
local __var110 to self:output. __var110:append(__var110,"Crew count changed, did we lose someone?", Callback:null).
      set self:stage to  "done".
      return.
    }

    if self:waypoints:length = 0 {
      set self:stage to  "wait".
      set self:registered to  false.
local __var111 to self:fs:loop. __var111:deregister(__var111,self).
      brakes on.
      return.
    }
    local curDest to  self:waypoints:peek().
    local dir to  curDest[0]:position - ship:position.
    if not self:arrow:istype("Boolean") {
      set self:arrow:start to  ship:position.
      set self:arrow:vec to  dir.
      set self:arrow:show to  true.
    }
    if dir:mag < 20 {
      self:waypoints:pop().
      local nextWaypoint to  self:waypoints:peek().
      if nextWaypoint[1] <> self:roverConf {
        local conf to  false.
        if exists("1:/rover.conf") {
          set conf to  open("1:/rover.conf").
        } else {
          set conf to  create("1:/rover.conf").
        }
        conf:clear().
        conf:write(nextWaypoint[1]).
      }
      local extraDist to  (curDest[0]:position - self:lastDistPos:position):mag.
      set self:lastDistPos to  curDest[0].
      set self:traveled to  self:traveled + extraDist.
      if mod(self:traveled, 1000) - extraDist <= 0 {
        local left to  self:dist - self:traveled.
        local speed to  self:traveled / (time:seconds - self:startTime).
        local _eta to  left / speed.
        print("Driving: " + ceiling(left / 1000) + "km @" + round(speed, 1) + "m/s: " + ceiling(_eta / 60) + " mins left. Next: " + nextWaypoint[1]).
      }
    }

    if ship:status <> "LANDED" {
      set self:lastJump to  time:seconds.
    }
    local isJumping to  true.
    if time:seconds - self:lastJump > 2 {
      set isJumping to  false.
    }

    local bearing to  ship:facing:rightvector * dir:normalized.
    local quad to  ship:facing:vector * dir:normalized.
    if quad < 0
      set bearing to  bearing * 100.

    local surfVelApprox to  vxcl(ship:up:vector, ship:velocity:surface).
    local velVec to  ship:velocity:surface.
    if velVec:mag < 1 {
      set velVec to  ship:facing:vector.
    }
    set velVec to  velVec:normalized.

    local shipRight to  vcrs(up:vector, velVec):normalized.
    local shipForward to  -vcrs(up:vector, shipRight):normalized.

    local terrainProbeCenterPos to  ship:position + shipForward * (surfVelApprox:mag + 5).
    local terrainProbeBackwdPos to  ship:position + shipForward * (surfVelApprox:mag + 4).
    local terrainProbeRightVec to  vcrs(up:vector, ship:facing:vector).
    local terrainProbeRightPos to  terrainProbeCenterPos + terrainProbeRightVec.
    
    local tg0 to  body:geopositionof(terrainProbeCenterPos).
    local tg1 to  body:geopositionof(terrainProbeBackwdPos).
    local tg2 to  body:geopositionof(terrainProbeRightPos).
    local p0 to  tg0:altitudeposition(tg0:terrainheight + 1).
    local p1 to  tg1:altitudeposition(tg1:terrainheight + 1).
    local p2 to  tg2:altitudeposition(tg2:terrainheight + 1).

    local tmpTerrainRight to  (p2 - p0):normalized.
    local tmpTerrainFwd to  (p0 - p1):normalized.
    local terrainUp to  vcrs(tmpTerrainFwd, tmpTerrainRight):normalized.
    local terrainRight to  vcrs(terrainUp, tmpTerrainFwd):normalized.
    local terrainFwd to  vcrs(terrainRight, terrainUp):normalized.

    local pitchDiff to  ship:facing:upvector * terrainFwd.
    local rollDiff to  ship:facing:upvector * terrainRight.
    local yawDiff to  ship:facing:vector * terrainRight.

    local unsafe to  isJumping.
    if abs(rollDiff) > 0.2 or abs(pitchDiff) > 0.2 or abs(pitchDiff) > 0.2
      set unsafe to  true.
    
    local alignment to  max(0, quad)^3 * max(0.01, 1 - 2 * (abs(rollDiff) + abs(pitchDiff) + abs(yawDiff))).
    local targetSpeed to  self:desiredSpeed * alignment + 1 * (1 - alignment).
    if dir:mag < 220 {
      set targetSpeed to  max(1, targetSpeed * ((dir:mag - 20) / 250 + 0.2)).
    }

    for laser in self:lasers {
      if laser:getfield("layer") <> "<none>" and laser:getfield("layer") <> "Local Scenery" {
        set bearing to  90.
        set targetSpeed to  0.
      }
    }

    //print("p: " + round(pitchDiff * 25, 4) + ", r: " + round(rollDiff * 25, 4) + ", y: " + round(yawDiff * 25, 4) + ", " + ship:status);
    if unsafe {
      set ship:control:wheelsteer to  0.
      set ship:control:wheelthrottle to  1.
    } else {
      local maxSteer to  1 / min(25, max(1, ship:velocity:surface:mag - 1)^1.8).
      set ship:control:wheelsteer to  min(maxSteer, max(-maxSteer, bearing * -2)).
      set ship:control:wheelthrottle to  targetSpeed - ship:velocity:surface:mag.
    }
    
    local desiredFwd to  terrainFwd - ship:control:wheelsteer * terrainRight * 5.
    if ship:velocity:surface:mag < 1 {
      unlock steering.
    } else {
      local desiredDir to  lookdirup(desiredFwd, terrainUp).
      lock steering to desiredDir.
    }

    if ship:velocity:surface:mag - targetSpeed > 0.5 and not unsafe
      brakes on.
    else
      brakes off.

    if brakes {
      set ship:control:wheelthrottle to  0.
    }}.
set  prototype["done"] to { parameter self.
    set ship:control:wheelsteer to  0.
    set ship:control:wheelthrottle to  0.
    unlock steering.
    brakes on.
local __var112 to self:output. __var112:append(__var112,"Arrived", Callback:null).
    set self:stage to  "none".

    if self:registered {
      set self:registered to  false.
local __var113 to self:fs:loop. __var113:deregister(__var113,self).
    }
    if not self:arrow:istype("Boolean") {
      set self:arrow:show to  false.
    }
    set self:status to  0.
local __var114 to self:callback. __var114:call(__var114,self:pid).}.
set  prototype["none"] to { parameter self.
    print("deregister failed").}.
return  result.}):call().
set export to  Rover.