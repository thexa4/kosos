local  Reboot to ({
local  prototype to  lex().
local  result to  lex("prototype", prototype).
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local  self to  prototype:copy.
set  self["type"] to  result. set 
    self:pid to  id. set 
    self:fs to  fs. set 
    self:timeout to  30. local shouldWait to  false.
    for pid in fs:progs:keys {
      print pid. local prog to  fs:progs[pid].

      if prog:haskey("onSignal") {
local  __var14 to 
        fs:loop. __var14:defer( __var14, Callback:new( Callback,prog, "onSignal"), "End"). set 
        shouldWait to  true.
      }
    }

    if shouldWait {
      output:append(
      output,"Waiting for applications to quit...", Callback:null).
local  __var15 to 
      fs:loop. __var15:register( __var15,self).
    } else {
      reboot.
    }
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["run"] to { parameter self. set 
    self:timeout to  self:timeout - 1.
    if self:timeout <= 0 {
      reboot.
    }}.
return  result.}):call(). set 
export to  Reboot.