@lazyglobal off.
"kpp 1.1"."https://gitlab.com/thexa4/kos-kpp".


local LexerToken to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,def, text, filename, lineNumber, charOffset, isWhitespace.local self to __cls:prototype:copy. set self:type to __cls.
    set self:def to def.
    set self:text to text.
    set self:filename to filename.
    set self:lineNumber to lineNumber.
    set self:charOffset to charOffset.
    set self:isWhitespace to isWhitespace. return self.}.
}).

local LexerTokenDefSimple to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,domain, id, roughRegex, length.local self to __cls:prototype:copy. set self:type to __cls.
    parameter whitespace to false.
    set self:domain to domain.
    set self:id to id.
    set self:roughRegex to "^" + roughRegex.
    set self:l to length.
    set self:whitespace to whitespace. return self.}.

  set r:prototype:match to {parameter self,text.
    if text:matchesPattern(self:roughRegex) {
      return text:substring(0, self:l).
    }
    return false.
  }.
}).

local LexerTokenDefDynamic to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,domain, id, roughRegex, refiner.local self to __cls:prototype:copy. set self:type to __cls.
    parameter whitespace to false.
    set self:domain to domain.
    set self:id to id.
    set self:roughRegex to "^" + roughRegex.
    set self:refiner to refiner.
    set self:whitespace to whitespace. return self.}.

  set r:prototype:match to {parameter self,text.
    if text:matchesPattern(self:roughRegex) {
      local length to self:refiner(text).
      return text:substring(0, length).
    }
    return false.
  }.
}).

local LexerDefs to list( LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "NEWLINE", "\n", 1, true), LexerTokenDefDynamic:__new(LexerTokenDefDynamic,list("kos", "kpp"), "WHITESPACE", "(\s|\p{C})+", {
		// Try matching bigger identifiers until it no longer works
		declare parameter input.
		local length to 0.
		local matches to true.
		until matches = false {
			set length to length + 1.
			local regex to "^(\s|\p{C}){" + (length + 1) + "}".
			set matches to input:matchespattern(regex).
		}
		return length.
	}, true), LexerTokenDefDynamic:__new(LexerTokenDefDynamic,list("kos", "kpp"), "COMMENTLINE", "//[^\n]*", {
			// Try matching bigger identifiers until it no longer works
			declare parameter input.
			local length to -1.
			local matches to true.
			until matches = false {
				set length to length + 1.
				local regex to "^//[^\n]{" + (length + 1) + "}".
				set matches to input:matchespattern(regex).
			}
			return length + 2.
		}, true), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "LAZYGLOBAL", "lazyglobal\b", 10), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "ACCESSOR", "->", 2), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "PLUSMINUS", "(\+|-)", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "MULT", "\*", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "DIV", "/", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "POWER", "\^", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "E", "e((?=\d)|\b)", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "NOT", "not\b", 3), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "AND", "and\b", 3), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "OR", "or\b", 2), LexerTokenDefDynamic:__new(LexerTokenDefDynamic,list("kos", "kpp"), "TRUEFALSE", "(true\b|\bfalse\b)", {
		declare parameter input.
		return choose 4 if input:startswith("t") else 5.
	}), LexerTokenDefDynamic:__new(LexerTokenDefDynamic,list("kos", "kpp"), "COMPARATOR", "(<>|>=|<=|==|>|<)", {
		declare parameter input.
		if input:startswith("<>") { return 2. }
		if input:startswith(">=") { return 2. }
		if input:startswith("<=") { return 2. }
		return 1.
	}), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "ASSIGNMENT", "=", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "SET", "set\b", 3), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "TO", "to\b", 2), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "IS", "is\b", 2), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "IF", "if\b", 2), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "ELSE", "else\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "UNTIL", "until\b", 5), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "STEP", "step\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "DO", "do\b", 2), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "LOCK", "lock\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "UNLOCK", "unlock\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "PRINT", "print\b", 5), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "AT", "at\b", 2), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "ON", "on\b", 2), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "TOGGLE", "toggle\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "WAIT", "wait\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "WHEN", "when\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "THEN", "then\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "OFF", "off\b", 3), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "STAGE", "stage\b", 5), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "CLEARSCREEN", "clearscreen\b", 11), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "ADD", "add\b", 3), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "REMOVE", "remove\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "LOG", "log\b", 3), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "BREAK", "break\b", 5), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "PRESERVE", "preserve\b", 8), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "DECLARE", "declare\b", 7), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "DEFINED", "defined\b", 7), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "LOCAL", "local\b", 5), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "GLOBAL", "global\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "PARAMETER", "parameter\b", 9), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "FUNCTION", "function\b", 8), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "RETURN", "return\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "SWITCH", "switch\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "COPY", "copy\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "FROM", "from\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "RENAME", "rename\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "VOLUME", "volume\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "FILE", "file\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "DELETE", "delete\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "EDIT", "edit\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "RUN", "run\b", 3), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "RUNPATH", "runpath\b", 7), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "RUNONCEPATH", "runoncepath\b", 11), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "ONCE", "once\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "COMPILE", "compile\b", 7), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "LIST", "list\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "REBOOT", "reboot\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "SHUTDOWN", "shutdown\b", 8), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "FOR", "for\b", 3), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "UNSET", "unset\b", 5), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "CHOOSE", "choose\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "MIX", "mix\b", 3), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "INCLUDE", "#include\b", 8), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "MIFNDEF", "#ifndef\b", 7), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "MIFDEF", "#ifdef\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "MELSE", "#else\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "MENDIF", "#endif\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "MDEFINE", "#define\b", 7), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "MUNDEF", "#undef\b", 6), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "MPRAGMAONCE", "#pragma once\b", 12), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "BRACKETOPEN", "\(", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "BRACKETCLOSE", "\)", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "CURLYOPEN", "\{", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "CURLYCLOSE", "\}", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "SQUAREOPEN", "\[", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "SQUARECLOSE", "\]", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "COMMA", ",", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "COLON", ":", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "IN", "in\b", 2), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "ARRAYINDEX", "#", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "ALL", "all\b", 3), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "CLASS", "class\b", 5), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "FUNC", "func\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "INIT", "init\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "SELF", "self\b", 4), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "VAR", "var\b", 3), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "AWAIT", "await\b", 5), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "ASYNC", "async\b", 5), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "NEW", "new\b", 3), LexerTokenDefDynamic:__new(LexerTokenDefDynamic,list("kos", "kpp"), "DOUBLE", "\d[_\d]*\.\d[_\d]*", {
		declare parameter input.
		local prefixlength to -1.
		local matches to true.

		until matches = false {
			set prefixlength to prefixlength + 1.
			local regex to "^[_\d]{" + (prefixlength) + "}".
			set matches to input:matchespattern(regex).
		}
		local rest to input:substring(prefixlength, input:length - prefixlength).

		local postfixlength to -1.
		set matches to true.
		until matches = false {
			set postfixlength to postfixlength + 1.
			local regex to "^[_\d]{" + (postfixlength) + "}".
			set matches to rest:matchespattern(regex).
		}

		return prefixlength + postfixlength - 1.
	}), LexerTokenDefDynamic:__new(LexerTokenDefDynamic,list("kos", "kpp"), "INTEGER", "\d[_\d]*", {
		// Try matching bigger identifiers until it no longer works
		declare parameter input.
		local length to -1.
		local matches to true.
		until matches = false {
			set length to length + 1.
			local regex to "^\d[_\d]{" + (length + 1) + "}".
			set matches to input:matchespattern(regex).
		}
		return length + 1.
	}), LexerTokenDefDynamic:__new(LexerTokenDefDynamic,list("kos", "kpp"), "STRING", "@?\" + char(34) + "(\" + char(34) + "\" + char(34) + "|[^\" + char(34) + "])*\" + char(34), {
		declare parameter input.
		local iterator to input:iterator.
		iterator:next.

		local foundquote to false.
		local length to 0.
		until(false) {
			set length to length + 1.
			iterator:next.
			
			if (iterator:atend and not foundquote) {
				print "Assertion failure, unterminated string found.".
				return 99999.
			}
			if (foundquote) {
				if (iterator:atend or iterator:value <> char(34)) {
					return length.
				} else {
					set foundquote to false.
				}
			} else {
				if (iterator:value = char(34)) {
					set foundquote to true.
				}
			}
		}
	}), LexerTokenDefDynamic:__new(LexerTokenDefDynamic,list("kos", "kpp"), "IDENTIFIER", "[_\p{L}]\w*", {
		// Try matching bigger identifiers until it no longer works
		declare parameter input.
		local length to -1.
		local matches to true.
		until matches = false {
			set length to length + 1.
			local regex to "^[_\p{L}]\w{" + (length + 1) + "}".
			set matches to input:matchespattern(regex).
		}
		return length + 1.
	}), LexerTokenDefDynamic:__new(LexerTokenDefDynamic,list("kos", "kpp"), "FILEIDENT", "[_\p{L}]\w*(\.[_\p{L}]\w*)*", {
		// Try matching bigger identifiers until it no longer works
		declare parameter input.
		local length to -1.
		local matches to true.
		until matches = false {
			set length to length + 1.
			local regex to "^[_\p{L}][\w\.\p{L}]{" + (length + 1) + "}".
			set matches to input:matchespattern(regex).
		}
		return length + 1.
	}), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos"), "EOI", "\.", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kpp"), "EOI", "(\.|;)", 1), LexerTokenDefSimple:__new(LexerTokenDefSimple,list("kos", "kpp"), "ATSIGN", "@", 1)
).

local VarIdentifiers to list(
		"IDENTIFIER", "NOT", "AND", "OR", "TRUEFALSE", "SET", "TO",
		"IS", "IF", "ELSE", "UNTIL", "STEP", "DO", "LOCK", "UNLOCK",
		"PRINT", "AT", "ON", "TOGGLE", "WAIT", "WHEN", "THEN", "OFF",
		"STAGE", "CLEARSCREEN", "ADD", "REMOVE", "LOG", "BREAK",
		"PRESERVE", "DECLARE", "DEFINED", "LOCAL", "GLOBAL", "PARAMETER",
		"FUNCTION", "RETURN", "SWITCH", "COPY", "FROM", "RENAME",
		"VOLUME", "FILE", "DELETE", "EDIT", "RUN", "RUNPATH",
		"RUNONCEPATH", "ONCE", "COMPILE", "LIST", "REBOOT", "SHUTDOWN",
		"FOR", "UNSET", "CHOOSE", "LAZYGLOBAL", "IN", "ALL", "CLASS",
		"FUNC", "INIT", "SELF", "VAR", "NEW", "MIX", "AWAIT", "ASYNC"
	).

local ParserNode to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls.local self to __cls:prototype:copy. set self:type to __cls. return self.}.

  set r:prototype:children to {parameter self.
    // Must be returned in the order they are read
    print "Unimplemented children in node!".
    return list().
  }.
  
  set r:prototype:toSExpr to {parameter self.
    local result to "(" + self:type:name(self:type).
    for child in self:children(self) {
      set result to result + ", " + child:toSExpr(child).
    }
    set result to result + ")".
    return result.
  }.

  set r:prototype:string to {parameter self.
    local result to "".
    for c in self:children(self) {
      set result to result + c:string(c).
    }
    return result.
  }.

  set r:prototype:update_state to {parameter self,parser.
  }.

  set r:prototype:simplify to {parameter self,context.
    return self.
  }.

  set r:prototype:desugar to {parameter self,context.
    return self.
  }.
}).

local NoneNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "None".
  }.

  set r:prototype:children to {parameter self.
    return list().
  }.
  
  set r:prototype:toString to {parameter self.
    return "".
  }.
}).

local LiteralNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:create to {parameter self,identifier, whitespace, text.
    local textToken to LexerToken:__new(LexerToken,identifier, text, "<builtin>", -1, -1, false).
    local whitespacelist to list().
    if not whitespace:istype("boolean")
      whitespacelist:add( LexerToken:__new(LexerToken,"WHITESPACE", whitespace, "<builtin>", -1, -1, true)).
    
    return LiteralNode:__new(LiteralNode,textToken, whitespacelist).
  }.

  set r:name to {parameter self.
    return "LiteralNode".
  }.
  set r:__new to{parameter __cls,token, whitespace.local self to __cls:prototype:copy. set self:type to __cls.
    set self:whitespace to whitespace.
    set self:token to token. return self.}.

  set r:prototype:children to {parameter self.
    return list().
  }.

  set r:prototype:toSExpr to {parameter self.
    return """" + self:token:text + """".
  }.

  set r:prototype:string to {parameter self.
    local result to "".
    for w in self:whitespace {
      set result to result + w:text.
    }
    return result + self:token:text.
  }.

  set r:prototype:debug to {parameter self.
    return self:token:text + " on " + self:token:filename + ":" + self:token:lineNumber + ":" + self:token:charOffset + " [" + self:token:def + "].".
  }.

  set r:prototype:desugar to {parameter self,context.
    if self:token:def = "EOI" {
      local newToken to self:token:copy.
      local result to self:copy.
      set newToken:text to ".".
      set result:token to newToken.
      return result.
    }
    return self.
  }.
}).

local ParserState to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,pWindow, definitions, nodePrototype.local self to __cls:prototype:copy. set self:type to __cls.
    set self:window to pWindow.
    set self:definitions to definitions.
    set self:parts to list().
    set self:errors to list().
    set self:warnings to list().
    set self:matched to false.
    set self:fatal to false.
    set self:nodePrototype to nodePrototype.
    set self:todo to nodePrototype:format(nodePrototype):copy.
    set self:alternatives to queue().
    set self:value to false.
    set self:blocked to false.
    set self:metaStack to stack().

    for t in self:todo[0] {
      self:alternatives:push(t).
    }

    set self:nextToken to false.
    self:eatToken(self). return self.}.

  set r:prototype:eatToken to {parameter self.
    set self:prev_window to self:window.

    local whitespace to list().
    set self:nextToken to false.

    until(self:window:value:istype("Boolean")) {
      local current to self:window:value.
      if current:istype("Boolean") {
        set self:atend to true.
        set self:fatal to true.
        print("lexer error").
        self:errors:add(self:window:error).
        return.
      }
      set self:window to self:window:next(self:window).
      if current:isWhitespace {
        whitespace:add(current).
      } else {
        set self:nextToken to LiteralNode:__new(LiteralNode,current, whitespace).
        return.
      }
    }
  }.

  set r:prototype:recordFail to {parameter self.
    local result to self:copy.
    set result:alternatives to result:alternatives:copy.
    local lastAlt to result:alternatives:pop().
    if not lastAlt:istype("string") {
      set lastAlt to lastAlt:name(lastAlt).
    }

    if result:alternatives:length = 0 {
      set result:atend to true.
      if self:parts:length > 0 {
        local todo to self:todo[self:parts:length].
        local error to "Error while trying to parse " + self:nodePrototype:name(self:nodePrototype) + ", expected " + lastAlt + " but reached the end of the file.".
        if not self:nexttoken:istype("Boolean")
          set error to "Error while trying to parse " + self:nodePrototype:name(self:nodePrototype) + ", expected " + lastAlt + " but found: " + self:nextToken:debug(self:nextToken).
        set result to result:with_fatal(result,error).
      }
    }
    return result.
  }.

  set r:prototype:recordMatch to {parameter self,nextState.
    local result to nextState:copy.
    set result:parts to self:parts:copy.
    result:parts:add(result:value).
    set result:value to false.
    set result:todo to self:todo.
    set result:nodePrototype to self:nodePrototype.
    set result:matched to false.

    set result:alternatives to queue().
    if result:parts:length < result:todo:length {
      for t in result:todo[result:parts:length] {
        result:alternatives:push(t).
      }
    } else {
      result:completeMatch(result).
    }
    return result.
  }.

  set r:prototype:matchToken to {parameter self,id.
    if self:nextToken:istype("Boolean") {
      return self:recordFail(self).
    }
    if self:nextToken:token:def = id {
      local result to self:copy.
      set result:parts to self:parts:copy.
      result:parts:add(self:nextToken).
      set result:alternatives to queue().
      if result:parts:length < result:todo:length {
        for t in result:todo[result:parts:length] {
          result:alternatives:push(t).
        }
      } else {
        result:completeMatch(result).
      }
      result:eatToken(result).
      return result.
    } else {
      return self:recordFail(self).
    }
  }.

  set r:prototype:completeMatch to {parameter self.
    set self:matched to true.
    local prototype to self:nodePrototype.
    //print("matched: " + prototype->name());
    set self:value to prototype:__new(prototype,self:parts).
  }.

  set r:prototype:branchTodo to {parameter self.
    local result to self:copy.
    
    set result:nodePrototype to self:nextTodo(self).
    set result:todo to result:nodePrototype:format(result:nodePrototype):copy.
    set result:alternatives to queue().
    set result:value to false.
    set result:parts to list().
    //print("try: " + result->nodePrototype->name());
    //print(result->nextToken);

    if result:todo:length > 0 {
      for t in result:todo[0] {
        result:alternatives:push(t).
      }
    } else {
      result:completeMatch(result).
    }
    return result.
  }.

  set r:prototype:nextTodo to {parameter self.
    if self:alternatives:length = 0 {
      return false.
    }
    return self:alternatives:peek().
  }.

  set r:prototype:currentLexerToken to {parameter self.
    return self:nextToken.
  }.

  set r:prototype:include to {parameter self,lWindow.
    local result to self:copy.
    set result:window to ParserWindow:__new(ParserWindow,lWindow, self:prev_window).
    set result:nextToken to false.
    result:eatToken(result).
    return result.
  }.

  set r:prototype:handle_meta to {parameter self.
    local definition to self:nextToken:token:def.

    if self:metaStack:length > 0 and definition <> "EOF" {
      local result to self:copy.

      if definition = "MELSE" {
        set result:metaStack to self:metaStack:copy.
        local current to result:metaStack:pop().
        result:metaStack:push(not current).
        result:eatToken(result).
        return result.
      }

      if definition = "MENDIF" {
        set result:metaStack to self:metaStack:copy.
        result:metaStack:pop().
        result:eatToken(result).
        return result.
      }

      local shouldSkip to false.
      for s in self:metaStack {
        set shouldSkip to shouldSkip or s.
      }
      
      if shouldSkip {

        if definition = "MIFDEF" or definition = "MIFNDEF" {
          set result:metaStack to result:metaStack:copy.
          result:metaStack:push(false).
        }

        result:eatToken(result).

        return result.
      }
    }

    if definition = "MDEFINE" {
      local result to self:copy.

      result:eatToken(result).
      local identifier to result:nextToken:token.

      result:eatToken(result).
      local value to result:nextToken:token.
      if result:definitions:haskey(identifier:text)
        result:definitions:remove(identifier:text).
      result:definitions:add(identifier:text, value).

      result:eatToken(result).
      return result.
    }
    if definition = "MUNDEF" {
      local result to self:copy.

      result:eatToken(result).
      local identifier to result:nextToken:token.

      result:definitions:remove(identifier:text).

      result:eatToken(result).
      return result.
    }
    if definition = "MIFDEF" or definition = "MIFNDEF" {
      local result to self:copy.
      
      result:eatToken(result).
      local identifier to result:nextToken:token.

      set result:metaStack to result:metaStack:copy.
      local shouldSkip to result:definitions:haskey(identifier:text).
      if definition = "MINFDEF"
        set shouldSkip to not shouldSkip.
      result:metaStack:push(shouldSkip).

      result:eatToken(result).
      return result.
    }

    if self:definitions:haskey(self:nextToken:token:text) {
      local result to self:copy.
      set result:nextToken to self:nextToken:copy.
      set result:nextToken:token to self:definitions[self:nextToken:token:text].
      return result.
    }

    return self.
  }.

  set r:prototype:with_fatal to {parameter self,msg.
    local result to self:copy.
    set result:errors to self:errors:copy.
    result:errors:add(msg).
    set result:fatal to true.
    return result.
  }.

  set r:prototype:skip to {parameter self,n.
    // copy
    // advance window n
  }.

  set r:prototype:define to {parameter self,id, value.
    // copy
    // add define
  }.

  set r:prototype:undefine to {parameter self,id.
    // copy
    // drop define
  }.

  set r:prototype:addWarning to {parameter self,str.
    // copy
    // add warning
  }.
}).

local LexerWindow to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,l, offset.local self to __cls:prototype:copy. set self:type to __cls.
    set self:lexer to l.
    set self:offset to offset.
    set self:cache to list().
    set self:atend to l:atend. return self.}.

  set r:prototype:get to {parameter self,n.
    local i to self:offset + n.
    until self:lexer:atend or self:cache:length > i {
      self:cache:add(self:lexer:value).
      self:lexer:next(self:lexer).
    }

    if self:cache:length > i {
      return self:cache[i].
    }
    return false.
  }.

  set r:prototype:next to {parameter self.
    if self:atend {
      return false.
    }
    local hasNext to self:get(self,2).
    local result to self:copy.
    set result:atend to hasNext:istype("Boolean").
    set result:offset to self:offset + 1.
    return result.
  }.
}).

local Lexer to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,defs, domain, text, filename, noeof.local self to __cls:prototype:copy. set self:type to __cls.
    set self:defs to list().
    for d in defs {
      if d:domain:find(domain) > -1 {
        self:defs:add(d).
      }
    }
    set self:remainingText to text.
    set self:filename to filename.
    set self:lineNumber to 1.
    set self:charOffset to 1.
    set self:noeof to noeof.
    set self:atend to false.
    set self:error to false.
    set self:window to LexerWindow:__new(LexerWindow,self, 0).
    self:next(self). return self.}.

  set r:prototype:next to {parameter self.
    local t to self:remainingText.
    if t:length = 0 {
      if not self:noeof {
        set self:value to LexerToken:__new(LexerToken,"EOF", "", self:filename, self:lineNumber, self:charOffset, false).
        set self:noeof to true.
        return.
      }
      set self:value to false.
      set self:error to self:filename + ": end of file".
      set self:atend to true.
      return.
    }

    for d in self:defs {
      local m to d:match(d,t).
      if m:istype("string") {
        set self:remainingText to self:remainingText:remove(0, m:length).
        set self:value to LexerToken:__new(LexerToken,d:id, m, self:filename, self:lineNumber, self:charOffset, d:whitespace).

        set self:charOffset to self:charOffset + m:length.
        if (d:id = "NEWLINE") {
          set self:lineNumber to self:lineNumber + 1.
          set self:charOffset to 1.
        }
        return.
      }
    }
    
    set self:error to "Unexpected character found in file " + self:filename + ":" + self:lineNumber + ", char " + self:charOffset + ": '" + self:remainingText:substring(0, min(10, self:remainingText:length)) + "'...".
    set self:atend to true.
    set self:value to false.
  }.
}).

local ParserWindow to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,lWindow, continueWith.local self to __cls:prototype:copy. set self:type to __cls.
    set self:window to lWindow.
    set self:continueWith to continueWith.
    set self:atend to false.
    if continueWith:istype("Boolean") {
      set self:atend to lWindow:atend.
    }
    set self:value to ({local __lhs to lWindow. return __lhs:get(__lhs,0).}):call(). return self.}.

  set r:prototype:next to {parameter self.
    if self:window:atend {
      if not self:continueWith:istype("Boolean") {
        return self:continueWith.
      }
      if self:value:istype("Boolean") {
        return self.
      }
      local result to self:copy.
      set result:error to "Premature end of file".
      set result:value to false.
      return result.
    }
    local result to self:copy.
    set result:window to ({local __lhs to self:window. return __lhs:next(__lhs).}):call().
    set result:value to ({local __lhs to result:window. return __lhs:get(__lhs,0).}):call().
    if result:window:atend and self:continueWith:istype("Boolean") {
      set result:atend to true.
    }
    return result.
  }.
}).

local BottomUpStrategyTodo to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,node.local self to __cls:prototype:copy. set self:type to __cls.
    set self:constructor to node:type.
    set self:sourceNodes to ({local __lhs to node. return __lhs:children(__lhs).}):call().
    set self:attributes to lex().
    if node:haskey("attributes")
      set self:attributes to node:attributes.
    set self:replacementNodes to list(). return self.}.

  set r:prototype:isDone to {parameter self.
    return self:replacementNodes:length = self:sourceNodes:length.
  }.

  set r:prototype:current to {parameter self.
    return self:sourceNodes[self:replacementNodes:length].
  }.

  set r:prototype:addReplaced to {parameter self,replacement.
    self:replacementNodes:add(replacement).
  }.

  set r:prototype:compute to {parameter self,methodname, context.
    local type to self:constructor.
    local temp to type:__new(type,self:replacementNodes).
    set temp:attributes to self:attributes.
    return temp[methodname]:call(temp, context).
  }.
}).

local BottomUpStrategy to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,node, methodname, context.local self to __cls:prototype:copy. set self:type to __cls.
    set self:stack to stack().
    set self:todo to BottomUpStrategyTodo:__new(BottomUpStrategyTodo,node).
    set self:methodname to methodname.

    set self:atend to false.
    set self:value to false.
    set self:context to context. return self.}.

  set r:prototype:next to {parameter self.
    if ({local __lhs to self:todo. return __lhs:isDone(__lhs).}):call() {
      local transformed to ({local __lhs to self:todo. return __lhs:compute(__lhs,self:methodname, self:context).}):call().
      if self:stack:length = 0 {
        set self:value to transformed.
        set self:atend to true.
      } else {
        set self:todo to self:stack:pop().
        ({local __lhs to self:todo. return __lhs:addReplaced(__lhs,transformed).}):call().
      }
      return.
    }
    local current to ({local __lhs to self:todo. return __lhs:current(__lhs).}):call().
    if ({local __lhs to current:type. return __lhs:name(__lhs).}):call() = "LiteralNode" {
      if current:haskey(self:methodname)
        set current to current[self:methodname]:call(current, self:context).
      ({local __lhs to self:todo. return __lhs:addReplaced(__lhs,current).}):call().
    } else {
      local newTodo to BottomUpStrategyTodo:__new(BottomUpStrategyTodo,current).
      self:stack:push(self:todo).
      set self:todo to newTodo.
    }
  }.
}).

local Parser to Class:__new(Class, {parameter r.
  set r:__new to{parameter __cls,lexWindow, lexerDefs, domain, readFileFn, startSymbol, definitions.local self to __cls:prototype:copy. set self:type to __cls.
    set self:stack to stack().
    set self:current to ParserState:__new(ParserState, ParserWindow:__new(ParserWindow,lexWindow, false),
      definitions,
      startSymbol
     ).
    
    set self:lexerDefs to lexerDefs.
    set self:domain to domain.
    set self:readFileFn to readFileFn.

    set self:value to false.
    set self:atend to false.
    set self:errors to list().
    set self:warnings to list().
    set self:file_cache to lex(). return self.}.

  set r:prototype:next to {parameter self.
    if self:atend or self:current:blocked
      return.

    local new_state to self:current:handle_meta(self:current).
    if new_state <> self:current {
      set self:current to new_state.
      return.
    }

    local next_todo to self:current:nextTodo(self:current).
    if next_todo:istype("Boolean") {
      // None matched.
      if self:stack:length = 0 {
        set self:atend to true.
        return.
      }

      set self:current to self:stack:pop().
      set self:current to self:current:recordFail(self:current).

      if self:current:fatal {
        set self:errors to self:current:errors.
        set self:warnings to self:current:warnings.
        set self:atend to true.
      }
      return.
    }

    if next_todo:istype("String") {
      set self:current to self:current:matchToken(self:current,next_todo).
    } else {
      self:stack:push(self:current).
      set self:current to self:current:branchTodo(self:current).
    }

    self:_unwind_matches(self).
  }.

  set r:prototype:_unwind_matches to {parameter self.
    until (not self:current:matched) or self:current:blocked
    {
      if self:current:fatal {
        set self:errors to self:current:errors.
        set self:warnings to self:current:warnings.
        set self:atend to true.
        return.
      }
    
      self:current:value:update_state(self:current:value,self).
      if self:current:blocked {
        return.
      }

      if self:stack:length = 0 {
        set self:value to self:current:value.
        set self:atend to true.
        return.
      } else {
        local prev to self:current.
        set self:current to self:stack:pop().
        set self:current to self:current:recordMatch(self:current,prev).
      }
    }
    if self:current:fatal {
      set self:errors to self:current:errors.
      set self:warnings to self:current:warnings.
      set self:atend to true.
      return.
    }
  }.

  set r:prototype:onFileRead to {parameter self,filename, content.
    if content:istype("Boolean") {
      self:file_cache:add(filename, false).
    } else {
      local l to Lexer:__new(Lexer,self:lexerDefs, self:domain, content, filename, true).
      local lwindow to LexerWindow:__new(LexerWindow,l, 0).
      self:file_cache:add(filename, lwindow).
    }

    if self:current:blocked {
      self:current:value:update_state(self:current:value,self).
    }
    self:_unwind_matches(self).
  }.
}).

local EmptyStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "EmptyStmt".
  }.
  set r:format to {parameter self.
    return list(list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:eoi to parts[0]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:eoi).
  }.
}).

local NoneNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "None".
  }.
  set r:format to {parameter self.
    return list().
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls. return self.}.

  set r:prototype:children to {parameter self.
    return list().
  }.
}).

local NumberNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "NumberNode".
  }.
  set r:format to {parameter self.
    return list(list("INTEGER", "DOUBLE")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:literal to parts[0]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:literal).
  }.
}).

local SciNumberTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "SciNumberNodeTrailer".
  }.
  set r:format to {parameter self.
    return list(list("E"), list("PLUSMINUS", NoneNode), list("INTEGER")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:eliteral to parts[0].
    set self:plusminus to parts[1].
    set self:exponent to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:eliteral, self:plusminus, self:exponent).
  }.
}).

local SciNumberNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "SciNumber".
  }.
  set r:format to {parameter self.
    return list(list(NumberNode), list(SciNumberTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:number to parts[0].
    set self:trailer to parts[1]. return self.}.

  set r:prototype:simplify to {parameter self,context.
    local tailType to self:trailer:type.
    if ({local __lhs to tailType. return __lhs:name(__lhs).}):call() = "None" {
      return self:number.
    }
    return self.
  }.

  set r:prototype:children to {parameter self.
    return list(self:number, self:trailer).
  }.
}).

local TernaryExprNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "TernaryExpr".
  }.
  set r:format to {parameter self.
    return list(list("CHOOSE"), list(ExprNode), list("IF"), list(ExprNode), list("ELSE"), list(ExprNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:chooseLiteral to parts[0].
    set self:trueExpr to parts[1].
    set self:ifLiteral to parts[2].
    set self:testExpr to parts[3].
    set self:elseLiteral to parts[4].
    set self:elseExpr to parts[5]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:chooseLiteral, self:trueExpr, self:ifLiteral, self:testExpr, self:elseLiteral, self:elseExpr).
  }.
}).

local FactorTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "FactorTrailer".
  }.
  set r:format to {parameter self.
    return list(list("POWER"), list(SuffixNode), list(FactorTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:power to parts[0].
    set self:suffix to parts[1].
    set self:tail to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:power, self:suffix, self:tail).
  }.
}).

local FactorNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "Factor".
  }.
  set r:format to {parameter self.
    return list(list(SuffixNode), list(FactorTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:suffix to parts[0].
    set self:tail to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:suffix, self:tail).
  }.

  set r:prototype:simplify to {parameter self,context.
    local tailType to self:tail:type.
    if ({local __lhs to tailType. return __lhs:name(__lhs).}):call() = "None" {
      return self:suffix.
    }
    return self.
  }.
}).

local UnaryExprInnerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "UnaryExprInner".
  }.
  set r:format to {parameter self.
    return list(list("PLUSMINUS", "NOT", "DEFINED"), list(FactorNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:operator to parts[0].
    set self:operand to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:operator, self:operand).
  }.
}).

local UnaryExprNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "UnaryExpr".
  }.
  set r:format to {parameter self.
    return list(list(UnaryExprInnerNode, FactorNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:inner to parts[0]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:inner).
  }.

  set r:prototype:simplify to {parameter self,context.
    return self:inner.
  }.
}).

local MultDivExprTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "MultDivExprTrailer".
  }.
  set r:format to {parameter self.
    return list(list("MULT", "DIV"), list(UnaryExprNode), list(MultDivExprTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:operator to parts[0].
    set self:operand to parts[1].
    set self:tail to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:operator, self:operand, self:tail).
  }.
}).

local MultDivExprNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "MultDivExpr".
  }.
  set r:format to {parameter self.
    return list(list(UnaryExprNode), list(MultDivExprTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:expr to parts[0].
    set self:tail to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:expr, self:tail).
  }.

  set r:prototype:simplify to {parameter self,context.
    local tailType to self:tail:type.
    if ({local __lhs to tailType. return __lhs:name(__lhs).}):call() = "None" {
      return self:expr.
    }
    return self.
  }.
}).

local PlusMinusExprTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "PlusMinusExprTrailer".
  }.
  set r:format to {parameter self.
    return list(list("PLUSMINUS"), list(MultDivExprNode), list(PlusMinusExprTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:operator to parts[0].
    set self:operand to parts[1].
    set self:tail to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:operator, self:operand, self:tail).
  }.

  set r:prototype:desugar to {parameter self,context.
    local operand to self:operand.
    local tail to self:tail.

    local instructions to list().
    if ({local __lhs to operand:type. return __lhs:name(__lhs).}):call() = "ExprBreakupPropagation" {
      set instructions to operand:statements:copy.
      set operand to operand:inner.
    }
    if ({local __lhs to tail:type. return __lhs:name(__lhs).}):call() = "ExprBreakupPropagation" {
      for i in tail:statements {
        instructions:add(i).
      }
      set tail to tail:inner.
    }

    if instructions:length = 0
      return self.
    
    local inner to PlusMinusExprTrailerNode:__new(PlusMinusExprTrailerNode,list(self:operator, operand, tail)).
    return ExprBreakupPropagationNode:__new(ExprBreakupPropagationNode,list(inner, instructions)).
  }.
}).

local PlusMinusExprNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "PlusMinusExpr".
  }.
  set r:format to {parameter self.
    return list(list(MultDivExprNode), list(PlusMinusExprTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:expr to parts[0].
    set self:tail to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:expr, self:tail).
  }.

  set r:prototype:simplify to {parameter self,context.
    local tailType to self:tail:type.
    if ({local __lhs to tailType. return __lhs:name(__lhs).}):call() = "None" {
      return self:expr.
    }
    return self.
  }.

  set r:prototype:desugar to {parameter self,context.
    local expr to self:expr.
    local tail to self:tail.

    local instructions to list().
    if ({local __lhs to expr:type. return __lhs:name(__lhs).}):call() = "ExprBreakupPropagation" {
      set instructions to expr:statements:copy.
      set expr to expr:inner.
    }
    if ({local __lhs to tail:type. return __lhs:name(__lhs).}):call() = "ExprBreakupPropagation" {
      for i in tail:statements {
        instructions:add(i).
      }
      set tail to tail:inner.
    }

    if instructions:length = 0
      return self.
    
    local inner to PlusMinusExprNode:__new(PlusMinusExprNode,list(expr, tail)).
    return ExprBreakupPropagationNode:__new(ExprBreakupPropagationNode,list(inner, instructions)).
  }.
}).

local CompareExprTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "CompareExprTrailer".
  }.
  set r:format to {parameter self.
    return list(list("COMPARATOR", "ASSIGNMENT"), list(PlusMinusExprNode), list(compareExprTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:operator to parts[0].
    set self:operand to parts[1].
    set self:tail to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:operator, self:operand, self:tail).
  }.
}).

local CompareExprNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "CompareExpr".
  }.
  set r:format to {parameter self.
    return list(list(PlusMinusExprNode), list(CompareExprTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:expr to parts[0].
    set self:tail to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:expr, self:tail).
  }.

  set r:prototype:simplify to {parameter self,context.
    local tailType to self:tail:type.
    if ({local __lhs to tailType. return __lhs:name(__lhs).}):call() = "None" {
      return self:expr.
    }
    return self.
  }.
}).

local AndExprTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "AndExprTrailer".
  }.
  set r:format to {parameter self.
    return list(list("AND"), list(CompareExprNode), list(AndExprTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:operator to parts[0].
    set self:operand to parts[1].
    set self:tail to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:operator, self:operand, self:tail).
  }.
}).

local AndExprNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "AndExpr".
  }.
  set r:format to {parameter self.
    return list(list(CompareExprNode), list(AndExprTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:expr to parts[0].
    set self:tail to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:expr, self:tail).
  }.

  set r:prototype:simplify to {parameter self,context.
    local tailType to self:tail:type.
    if ({local __lhs to tailType. return __lhs:name(__lhs).}):call() = "None" {
      return self:expr.
    }
    return self.
  }.
}).

local OrExprTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "OrExprTrailer".
  }.
  set r:format to {parameter self.
    return list(list("OR"), list(AndExprNode), list(OrExprTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:operator to parts[0].
    set self:operand to parts[1].
    set self:tail to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:operator, self:operand, self:tail).
  }.
}).

local OrExprNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "OrExpr".
  }.
  set r:format to {parameter self.
    return list(list(AndExprNode), list(OrExprTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:expr to parts[0].
    set self:tail to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:expr, self:tail).
  }.

  set r:prototype:simplify to {parameter self,context.
    local tailType to self:tail:type.
    if ({local __lhs to tailType. return __lhs:name(__lhs).}):call() = "None" {
      return self:expr.
    }
    return self.
  }.
}).

local InstructionBlockStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "InstructionBlockStmt".
  }.
  set r:format to {parameter self.
    return list(list("CURLYOPEN"), list(InstructionListNode, NoneNode), list("CURLYCLOSE")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:open to parts[0].
    set self:instructions to parts[1].
    set self:close to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:open, self:instructions, self:close).
  }.
}).

local ExprNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "Expr".
  }.
  set r:format to {parameter self.
    return list(list(TernaryExprNode, OrExprNode, InstructionBlockStmtNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:inner to parts[0]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:inner).
  }.

  set r:prototype:simplify to {parameter self,context.
    return self:inner.
  }.
}).

local BracketExprAtom to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "BracketExprAtom".
  }.
  set r:format to {parameter self.
    return list(list("BRACKETOPEN"), list(ExprNode), list("BRACKETCLOSE")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:open to parts[0].
    set self:expr to parts[1].
    set self:close to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:open, self:expr, self:close).
  }.

  set r:prototype:desugar to {parameter self,context.
    if ({local __lhs to self:expr:type. return __lhs:name(__lhs).}):call() = "ExprBreakupPropagation" {
      local destructured to BracketExprAtom:__new(BracketExprAtom,self:open, self:expr:inner, self:close).
      local parts to self:expr:statements:copy.
      parts:insert(0, destructured).
      return ExprBreakupPropagationNode:__new(ExprBreakupPropagationNode,parts).
    }
    
    return self.
  }.

  set r:create to {parameter self,expr.
    return BracketExprAtom:__new(BracketExprAtom,list(
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"BRACKETOPEN", "", "(").}):call(),
      expr,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"BRACKETCLOSE", "", ")").}):call()
    )).
  }.
}).

local AtomNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "Atom".
  }.
  set r:format to {parameter self.
    local allowed to VarIdentifiers:copy.
    allowed:insert(0, "TRUEFALSE").
    allowed:insert(0, SciNumberNode).
    allowed:add("FILEIDENT").
    allowed:add(BracketExprAtom).
    allowed:add("STRING").
    return list(allowed).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:literal to parts[0]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:literal).
  }.

  set r:prototype:simplify to {parameter self,context.
    local type to self:literal:type.
    if ({local __lhs to type. return __lhs:name(__lhs).}):call() = "SciNumber" or ({local __lhs to type. return __lhs:name(__lhs).}):call() = "BracketExprAtom" {
      return self:literal.
    }
    return self.
  }.
}).

local ArrayAccessNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ArrayAccess".
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:subject to parts[0].
    set self:bopen to parts[1].
    set self:index to parts[2].
    set self:bclose to parts[3]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:subject, self:bopen, self:index, self:bclose).
  }.

  set r:create to {parameter self,subject, key.
    return ArrayAccessNode:__new(ArrayAccessNode,list(
      subject,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"SQUAREOPEN", "", "[").}):call(),
      key,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"SQUARECLOSE", "", "]").}):call()
    )).
  }.
}).

local ArrayBracketTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ArrayBracketTrailer".
  }.
  set r:format to {parameter self.
    return list(list("SQUAREOPEN"), list(ExprNode), list("SQUARECLOSE")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:open to parts[0].
    set self:expr to parts[1].
    set self:close to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:open, self:expr, self:close).
  }.
  
  set r:prototype:suffix_simplify to {parameter self,subject.
    return ArrayAccessNode:__new(ArrayAccessNode,list(subject, self:open, self:expr, self:close)).
  }.
}).

local ArrayHashTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ArrayHashTrailer".
  }.
  set r:format to {parameter self.
    return list(list("ARRAYINDEX"), list("IDENTIFIER", "INTEGER")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:hash to parts[0].
    set self:identifier to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:hash, self:identifier).
  }.
  
  set r:prototype:suffix_simplify to {parameter self,subject.
    return ArrayAccessNode:__new(ArrayAccessNode,list(subject, ({local __lhs to LiteralNode. return __lhs:create(__lhs,"SQUAREOPEN", "", "[").}):call(), self:identifier, ({local __lhs to LiteralNode. return __lhs:create(__lhs,"SQUARECLOSE", "", "]").}):call())).
  }.
}).


local ArrayTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ArrayTrailer".
  }.
  set r:format to {parameter self.
    return list(list(ArrayBracketTrailerNode, ArrayHashTrailerNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:trailer to parts[0]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:trailer).
  }.
  
  set r:prototype:suffix_simplify to {parameter self,subject.
    return ({local __lhs to self:trailer. return __lhs:suffix_simplify(__lhs,subject).}):call().
  }.
}).

local ArgListTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ArgListTrailer".
  }.
  set r:format to {parameter self.
    return list(list("COMMA"), list(ExprNode), list(ArgListTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:comma to parts[0].
    set self:expr to parts[1].
    set self:trailer to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:comma, self:expr, self:trailer).
  }.

  set r:prototype:to_list to {parameter self,current.
    local result to current:copy.
    result:add(self:comma).
    result:add(self:expr).

    if ({local __lhs to self:trailer:type. return __lhs:name(__lhs).}):call() = "None"
      return result.
    return ({local __lhs to self:trailer. return __lhs:to_list(__lhs,result).}):call().
  }.

  set r:prototype:to_parameter_list to {parameter self.
    local rest to NoneNode:__new(NoneNode,list()).
    if ({local __lhs to self:trailer:type. return __lhs:name(__lhs).}):call() <> "None" {
      set rest to ({local __lhs to self:trailer. return __lhs:to_parameter_list(__lhs).}):call().
    }
    return DeclareParameterClauseTrailer:__new(DeclareParameterClauseTrailer,list(
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"COMMA", "", ",").}):call(),
      self:expr, NoneNode:__new(NoneNode,list()),
      rest
    )).
  }.
}).

local ArgListNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ArgList".
  }.
  set r:format to {parameter self.
    return list(list(ExprNode), list(ArgListTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:expr to parts[0].
    set self:trailer to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:expr, self:trailer).
  }.

  set r:prototype:to_list to {parameter self.
    local result to list(self:expr).
    if ({local __lhs to self:trailer:type. return __lhs:name(__lhs).}):call() = "None"
      return result.
    return ({local __lhs to self:trailer. return __lhs:to_list(__lhs,result).}):call().
  }.

  set r:prototype:to_parameter_list to {parameter self.
    local rest to NoneNode:__new(NoneNode,list()).
    if ({local __lhs to self:trailer:type. return __lhs:name(__lhs).}):call() <> "None" {
      set rest to ({local __lhs to self:trailer. return __lhs:to_parameter_list(__lhs).}):call().
    }
    return DeclareParameterClauseTrailer:__new(DeclareParameterClauseTrailer,list(
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"COMMA", "", ",").}):call(),
      self:expr, NoneNode:__new(NoneNode,list()),
      rest
    )).
  }.
}).

local NonSingleStatementGuardNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "NonSingleStatementGuard".
  }.
  set r:format to {parameter self.
    return list(list(InstructionListNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:instructions to parts[0]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:instructions).
  }.
}).

local ExprBreakupPropagationNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ExprBreakupPropagation".
  }.

  set r:gen_variable to {parameter self.
    if not self:prototype:haskey("counter")
      set self:prototype:counter to 0.
    local token to LexerToken:__new(LexerToken,"VARIDENTIFIER", "__var" + self:prototype:counter, "<builtin>", -1, -1, false).
    local varname to LiteralNode:__new(LiteralNode,token, list( LexerToken:__new(LexerToken,"SPACE", " ", "<builtin>", -1, -1, true))).
    set self:prototype:counter to self:prototype:counter + 1.
    return varname.
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:inner to parts[0].
    set self:statements to parts:sublist(1, parts:length - 1). return self.}.

  set r:prototype:children to {parameter self.
    local result to self:statements:copy.
    result:insert(0, self:inner).
    return result.
  }.

  set r:prototype:to_block to {parameter self,new_inner.
    function recursion {
      parameter list, inner.
      if list:length = 0
        return inner.
      
      local current to list[0].
      local rest to list:sublist(1, list:length - 1).
      return InstructionListNode:__new(InstructionListNode,list(current, recursion(rest, inner))).
    }
    local instructionList to recursion(self:statements, new_inner).
    return NonSingleStatementGuardNode:__new(NonSingleStatementGuardNode,list(instructionList)).
  }.
}).

local DeclareFunctionClause to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "DeclareFunctionClause".
  }.
  set r:format to {parameter self.
    return list(
      list("FUNCTION"),
      list("IDENTIFIER"),
      list(InstructionBlockStmtNode),
      list("EOI", NoneNode)
    ).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:function to parts[0].
    set self:identifier to parts[1].
    set self:instruction_list to parts[2].
    set self:eoi to parts[3]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:function, self:identifier, self:instruction_list, self:eoi).
  }.
}).

local DeclareIdentifierClause to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "DeclareIdentifierClause".
  }.
  set r:format to {parameter self.
    return list(VarIdentifiers, list("TO", "IS"), list(ExprNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:identifier to parts[0].
    set self:to to parts[1].
    set self:expr to parts[2].
    set self:eoi to parts[3]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:identifier, self:to, self:expr, self:eoi).
  }.
}).

local DeclareLockClause to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "DeclareLockClause".
  }.
  set r:format to {parameter self.
    return list(
      list("LOCK"),
      VarIdentifiers,
      list("TO"),
      list(exprNode),
      list("EOI")
    ).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:lock to parts[0].
    set self:identifier to parts[1].
    set self:to to parts[2].
    set self:expr to parts[3].
    set self:eoi to parts[4]. return self.}.

  set r:prototype:children to {parameter self.
    return list(
      self:lock,
      self:identifier,
      self:to,
      self:expr,
      self:eoi
    ).
  }.
}).

local DeclareParameterClauseDefaultPart to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "DeclareParameterClauseDefaultPart".
  }.
  set r:format to {parameter self.
    return list(list("TO", "IS"), list(ExprNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:to to parts[0].
    set self:expr to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:to, self:expr).
  }.
}).

local DeclareParameterClauseTrailer to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "DeclareParameterClauseTrailer".
  }.
  set r:format to {parameter self.
    return list(list("COMMA"), VarIdentifiers, list(DeclareParameterClauseDefaultPart, NoneNode), list(DeclareParameterClauseTrailer, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:comma to parts[0].
    set self:identifier to parts[1].
    set self:default to parts[2].
    set self:tail to parts[3]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:comma, self:identifier, self:default, self:tail).
  }.
}).

local DeclareParameterClause to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "DeclareParameterClause".
  }.
  set r:format to {parameter self.
    return list(
      list("PARAMETER"),
      VarIdentifiers,
      list(DeclareParameterClauseDefaultPart, NoneNode),
      list(DeclareParameterClauseTrailer, NoneNode),
      list("EOI")
    ).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:parameter to parts[0].
    set self:identifier to parts[1].
    set self:defaults to parts[2].
    set self:trailer to parts[3].
    set self:eoi to parts[4]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:parameter, self:identifier, self:defaults, self:trailer, self:eoi).
  }.
}).

local DeclareStmtScopeNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "DeclareScope".
  }.
  set r:format to {parameter self.
    return list(
      list("LOCAL", "GLOBAL")
    ).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:scope to parts[0]. return self.}.

  set r:prototype:children to {parameter self.
    return list(
      self:scope
    ).
  }.
}).

local DeclareStmtDeclarePrefixNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "DeclareDeclarePrefix".
  }.
  set r:format to {parameter self.
    return list(
      list("DECLARE"),
      list(DeclareStmtScopeNode, NoneNode)
    ).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:declare to parts[0].
    set self:scope to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(
      self:declare,
      self:scope
    ).
  }.
}).

local DeclareStmtScopedDeclarationNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "DeclareScopedDeclaration".
  }.
  set r:format to {parameter self.
    return list(
      list(DeclareStmtDeclarePrefixNode, DeclareStmtScopeNode),
      list(DeclareParameterClause, DeclareFunctionClause, DeclareIdentifierclause, DeclareLockClause)
    ).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:prefix to parts[0].
    set self:definition to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(
      self:prefix,
      self:definition
    ).
  }.

  set r:create_local to {parameter self,identifier, value.
    local prefix to DeclareStmtScopeNode:__new(DeclareStmtScopeNode,list(({local __lhs to LiteralNode. return __lhs:create(__lhs,"LOCAL", char(10), "local ").}):call())).
    local definition to DeclareIdentifierClause:__new(DeclareIdentifierClause,list(identifier, ({local __lhs to LiteralNode. return __lhs:create(__lhs,"TO", " ", "to ").}):call(), value, ({local __lhs to LiteralNode. return __lhs:create(__lhs,"EOI", "", ".").}):call())).
    return DeclareStmtScopedDeclarationNode:__new(DeclareStmtScopedDeclarationNode,list(prefix, definition)).
  }.
  
  set r:create_global to {parameter self,identifier, value.
    local prefix to DeclareStmtScopeNode:__new(DeclareStmtScopeNode,list(({local __lhs to LiteralNode. return __lhs:create(__lhs,"GLOBAL", char(10), "global ").}):call())).
    local definition to DeclareIdentifierClause:__new(DeclareIdentifierClause,list(identifier, ({local __lhs to LiteralNode. return __lhs:create(__lhs,"TO", " ", "to ").}):call(), value, ({local __lhs to LiteralNode. return __lhs:create(__lhs,"EOI", "", ".").}):call())).
    return DeclareStmtScopedDeclarationNode:__new(DeclareStmtScopedDeclarationNode,list(prefix, definition)).
  }.
}).

local DeclareStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "Declare".
  }.
  set r:format to {parameter self.
    return list(
      list(
        DeclareParameterClause,
        DeclareFunctionClause,
        DeclareLockClause,
        DeclareStmtScopedDeclarationNode
      )
    ).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:definition to parts[0]. return self.}.

  set r:prototype:children to {parameter self.
    return list(
      self:definition
    ).
  }.
}).

local FunctionCallNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "FunctionCall".
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:subject to parts[0].
    set self:bopen to parts[1].
    set self:args to parts:copy.
    self:args:remove(0).
    self:args:remove(0).
    self:args:remove(self:args:length - 1).
    set self:bclose to parts[parts:length - 1].

    set self:attributes to lex().
    set self:attributes:is_object_call to false.
    if ({local __lhs to self:subject:type. return __lhs:name(__lhs).}):call() = "StructureAccess" {
      if ({local __lhs to self:subject. return __lhs:is_object_accessor(__lhs).}):call()
        set self:attributes:is_object_call to true.
    } return self.}.

  set r:prototype:children to {parameter self.
    local result to self:args:copy.
    result:insert(0, self:subject).
    result:insert(1, self:bopen).
    result:add(self:bclose).
    return result.
  }.

  set r:prototype:desugar to {parameter self,context.
    local call to self.
    local precursors to list().

    if ({local __lhs to call:subject:type. return __lhs:name(__lhs).}):call() = "ExprBreakupPropagation" {
      for stmt in call:subject:statements {
        precursors:add(stmt).
      }
      local parts to list(call:subject:inner, call:bopen).
      for a in call:args {
        parts:add(a).
      }
      parts:add(call:bclose).
      set call to FunctionCallNode:__new(FunctionCallNode,parts).
      set call:attributes to self:attributes.
    }

    local newArgs to list().
    local isModified to false.
    for arg in call:args {
      if ({local __lhs to arg:type. return __lhs:name(__lhs).}):call() = "ExprBreakupPropagation" {
        set isModified to true.

        for stmt in arg:statements {
          precursors:add(stmt).
        }
        newArgs:add(arg:inner).
      } else {
        newArgs:add(arg).
      }
    }
    if isModified {
      local parts to list(call:subject, call:bopen).
      for a in newArgs {
        parts:add(a).
      }
      parts:add(call:bclose).
      set call to FunctionCallNode:__new(FunctionCallNode,parts).
      set call:attributes to self:attributes.
    }

    if call:attributes:is_object_call {
      if ({local __lhs to call:subject:type. return __lhs:name(__lhs).}):call() = "StructureAccess" {
        local shouldCapture to true.
        local variable to call:subject:subject.

        if ({local __lhs to call:subject:subject:type. return __lhs:name(__lhs).}):call() = "Atom" {
          set shouldCapture to false.
        }

        if shouldCapture {
          set variable to ({local __lhs to ExprBreakupPropagationNode. return __lhs:gen_variable(__lhs).}):call().

          local capturedDef to ({local __lhs to DeclareStmtScopedDeclarationNode. return __lhs:create_local(__lhs,variable, call:subject:subject).}):call().
          precursors:add(capturedDef).
        }

        local functionSubject to ({local __lhs to StructureAccessNode. return __lhs:create(__lhs,variable, call:subject:suffix).}):call().

        local comma to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"COMMA", "", ",").}):call().

        local parts to list(functionSubject, call:bopen, variable).
        if call:args:length > 0
          parts:add(comma).
        
        for a in call:args {
          parts:add(a).
        }
        parts:add(call:bclose).
        set call to FunctionCallNode:__new(FunctionCallNode,parts).
        set call:attributes to self:attributes.
      }
    }
    if precursors:length = 0 {
      return call.
    }

    precursors:insert(0, call).
    return ExprBreakupPropagationNode:__new(ExprBreakupPropagationNode,precursors).
  }.

  set r:create to {parameter self,subject, args.
    local parts to list(
      subject,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"BRACKETOPEN", "", "(").}):call()
    ).
    local first to true.
    for a in args {
      if first {
        set first to false.
      } else {
        parts:add(({local __lhs to LiteralNode. return __lhs:create(__lhs,"COMMA", "", ",").}):call()).
      }
      parts:add(a).
    }
    parts:add(({local __lhs to LiteralNode. return __lhs:create(__lhs,"BRACKETCLOSE", "", ")").}):call()).
    return FunctionCallNode:__new(FunctionCallNode,parts).
  }.
}).

local FunctionCallTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "FunctionCallTrailer".
  }.
  set r:format to {parameter self.
    return list(list("BRACKETOPEN"), list(ArgListNode, NoneNode), list("BRACKETCLOSE")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:open to parts[0].
    set self:args to parts[1].
    set self:close to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:open, self:args, self:close).
  }.

  set r:prototype:suffix_simplify to {parameter self,subject.
    local arglist to list().
    if ({local __lhs to self:args:type. return __lhs:name(__lhs).}):call() <> "None"
      set arglist to ({local __lhs to self:args. return __lhs:to_list(__lhs).}):call().
    
    arglist:insert(0, subject).
    arglist:insert(1, self:open).
    arglist:add(self:close).
    return FunctionCallNode:__new(FunctionCallNode,arglist).
  }.

  set r:prototype:to_parameters to {parameter self.
    parameter selfName to "self".
    local params to NoneNode:__new(NoneNode,list()).
    if ({local __lhs to self:args:type. return __lhs:name(__lhs).}):call() <> "None" {
      set params to ({local __lhs to self:args. return __lhs:to_parameter_list(__lhs).}):call().
    }
    return DeclareParameterClause:__new(DeclareParameterClause,list(
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"PARAMETER", " ", "parameter").}):call(),
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"IDENTIFIER", " ", selfName).}):call(), NoneNode:__new(NoneNode,list()),
      params,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"EOI", "", ".").}):call()
    )).
  }.
}).

local FunctionCaptureTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "FunctionCaptureTrailer".
  }.
  set r:format to {parameter self.
    return list(list("ATSIGN")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:atsign to parts[0]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:atsign).
  }.
}).

local SuffixtermTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "SuffixtermTrailer".
  }.
  set r:format to {parameter self.
    return list(list(FunctionCallTrailerNode, ArrayTrailerNode, FunctionCaptureTrailerNode), list(SuffixtermTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:action to parts[0].
    set self:tail to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:action, self:tail).
  }.
  
  set r:prototype:suffix_simplify to {parameter self,subject.
    local new_subject to subject.
    set new_subject to ({local __lhs to self:action. return __lhs:suffix_simplify(__lhs,new_subject).}):call().
    if ({local __lhs to self:tail:type. return __lhs:name(__lhs).}):call() = "SuffixtermTrailer" {
      set new_subject to ({local __lhs to self:tail. return __lhs:suffix_simplify(__lhs,new_subject).}):call().
    }
    return new_subject.
  }.
}).

local ObjectConstructionNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ObjectConstruction".
  }.
  set r:format to {parameter self.
    return list(list("NEW"), list(AtomNode), list("BRACKETOPEN"), list(ArgListNode, NoneNode), list("BRACKETCLOSE")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:newLiteral to parts[0].
    set self:typeIdentifier to parts[1].
    set self:open to parts[2].
    set self:args to parts[3].
    set self:close to parts[4]. return self.}.

  set r:prototype:simplify to {parameter self,context.
    local access to StructureAccessNode:__new(StructureAccessNode,list(self:typeIdentifier, ({local __lhs to LiteralNode. return __lhs:create(__lhs,"ACCESSOR", "", "->").}):call(), self:newLiteral)).

    local parts to list(access, self:open).
    if ({local __lhs to self:args:type. return __lhs:name(__lhs).}):call() <> "None" {
      for arg in ({local __lhs to self:args. return __lhs:to_list(__lhs).}):call() {
        parts:add(arg).
      }
    }
    parts:add(self:close).
    return FunctionCallNode:__new(FunctionCallNode,parts).
  }.

  set r:prototype:children to {parameter self.
    return list(self:newLiteral, self:typeIdentifier, self:open, self:args, self:close).
  }.
}).

local SuffixtermNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "Suffixterm".
  }.
  set r:format to {parameter self.
    return list(list(ObjectConstructionNode, AtomNode), list(SuffixtermTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:subject to parts[0].
    set self:tail to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:subject, self:tail).
  }.

  set r:prototype:is_function_call to {parameter self.
    if ({local __lhs to self:tail:action:type. return __lhs:name(__lhs).}):call() = "FunctionCallTrailerNode"
      return true.
    return false.
  }.

  set r:prototype:suffix_simplify to {parameter self,subject.
    if ({local __lhs to self:tail:type. return __lhs:name(__lhs).}):call() = "None"
      return subject.
    
    return ({local __lhs to self:tail. return __lhs:suffix_simplify(__lhs,subject).}):call().
  }.
}).

local StructureAccessNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "StructureAccess".
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:subject to parts[0].
    set self:colon to parts[1].
    set self:suffix to parts[2]. return self.}.

  set r:prototype:is_object_accessor to {parameter self.
    return self:colon:token:def = "ACCESSOR".
  }.

  set r:prototype:children to {parameter self.
    return list(self:subject, self:colon, self:suffix).
  }.

  set r:prototype:desugar to {parameter self,context.
    local result to self.
    local propagation to list().

    if ({local __lhs to self:subject:type. return __lhs:name(__lhs).}):call() = "ExprBreakupPropagation" {
      set propagation to self:subject:statements:copy.
      set result to StructureAccessNode:__new(StructureAccessNode,list(self:subject:inner, self:colon, self:suffix)).
    }

    if ({local __lhs to self. return __lhs:is_object_accessor(__lhs).}):call() {
      local colon to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"COLON", "", ":").}):call().
      set result to StructureAccessNode:__new(StructureAccessNode,list(result:subject, colon, result:suffix)).
    }

    if propagation:length = 0
      return result.

    propagation:insert(0, result).
    return ExprBreakupPropagationNode:__new(ExprBreakupPropagationNode,propagation).
  }.

  set r:create to {parameter self,subject, suffix.
    return StructureAccessNode:__new(StructureAccessNode,list(
      subject,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"COLON", "", ":").}):call(),
      suffix
    )).
  }.
}).

local SuffixTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "SuffixTrailer".
  }.
  set r:format to {parameter self.
    return list(list("COLON", "ACCESSOR"), list(SuffixtermNode), list(SuffixTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:colon to parts[0].
    set self:suffix to parts[1].
    set self:tail to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:colon, self:suffix, self:tail).
  }.

  set r:prototype:suffix_simplify to {parameter self,subject.
    local new_subject to StructureAccessNode:__new(StructureAccessNode,list(subject, self:colon, self:suffix:subject)).
    set new_subject to ({local __lhs to self:suffix. return __lhs:suffix_simplify(__lhs,new_subject).}):call().
    if ({local __lhs to self:tail:type. return __lhs:name(__lhs).}):call() <> "None" {
      set new_subject to ({local __lhs to self:tail. return __lhs:suffix_simplify(__lhs,new_subject).}):call().
    }
    return new_subject.
  }.
}).

local SuffixNormalNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "SuffixNormal".
  }.
  set r:format to {parameter self.
    return list(list(SuffixtermNode), list(SuffixTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:term to parts[0].
    set self:trailer to parts[1]. return self.}.

  set r:prototype:simplify to {parameter self,context.
    local subject to ({local __lhs to self:term. return __lhs:suffix_simplify(__lhs,self:term:subject).}):call().
    if ({local __lhs to self:trailer:type. return __lhs:name(__lhs).}):call() <> "None" {
      set subject to ({local __lhs to self:trailer. return __lhs:suffix_simplify(__lhs,subject).}):call().
    }
    return subject.
  }.

  set r:prototype:children to {parameter self.
    return list(self:term, self:trailer).
  }.
}).

local AsyncResolveStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "AsyncResolveStmt".
  }.

  set r:format to {parameter self.
    return list(list("VAR"), VarIdentifiers, list("ASSIGNMENT"), list("AWAIT"), VarIdentifiers, list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:varLiteral to parts[0].
    set self:identifier to parts[1].
    set self:assignment to parts[2].
    set self:awaitStmt to parts[3].
    set self:promiseVar to parts[4].
    set self:eoi to parts[5]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:varLiteral, self:identifier, self:assignment, self:awaitStmt, self:promiseVar, self:eoi).
  }.
}).

local VarDeclarationStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "VarDeclarationStmt".
  }.

  set r:format to {parameter self.
    return list(list("VAR"), VarIdentifiers, list("ASSIGNMENT"), list(ExprNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:varLiteral to parts[0].
    set self:identifier to parts[1].
    set self:assignment to parts[2].
    set self:value to parts[3].
    set self:eoi to parts[4]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:varLiteral, self:identifier, self:assignment, self:value, self:eoi).
  }.

  set r:prototype:as_kos to {parameter self,value.
    local localToken to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"LOCAL", " ", "local").}):call().
    local toToken to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"TO", " ", "to ").}):call().
    local scopeNode to DeclareStmtScopeNode:__new(DeclareStmtScopeNode,list(localToken)).
    local identifierDeclarationNode to DeclareIdentifierClause:__new(DeclareIdentifierClause,list(self:identifier, toToken, value, self:eoi)).
    return DeclareStmtScopedDeclarationNode:__new(DeclareStmtScopedDeclarationNode,list(scopeNode, identifierDeclarationNode)).
  }.

  set r:prototype:desugar to {parameter self,context.
    if ({local __lhs to self:value:type. return __lhs:name(__lhs).}):call() = "ExprBreakupPropagation" {
      local inner to false.
      if ({local __lhs to self:value:inner:type. return __lhs:name(__lhs).}):call() = "SuffixAwait" {
        set inner to AsyncResolveStmtNode:__new(AsyncResolveStmtNode,list(self:varLiteral, self:identifier, self:assignment, self:value:inner:asyncStmt, self:value:inner:suffix, self:eoi)).
      } else {
        set inner to ({local __lhs to self. return __lhs:as_kos(__lhs,self:value:inner).}):call().
      }
      return ({local __lhs to self:value. return __lhs:to_block(__lhs,inner).}):call().
    } else {
      return ({local __lhs to self. return __lhs:as_kos(__lhs,self:value).}):call().
    }
  }.
}).

local SuffixAwaitNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "SuffixAwait".
  }.
  set r:format to {parameter self.
    return list(list("AWAIT"), list(SuffixNormalNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:asyncStmt to parts[0].
    set self:suffix to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:asyncStmt, self:suffix).
  }.

  set r:prototype:desugar to {parameter self,context.
    if ({local __lhs to self:suffix:type. return __lhs:name(__lhs).}):call() = "LiteralNode"
      return self.

    local spaceToken to LexerToken:__new(LexerToken,"SPACE", " ", "<builtin>", -1, -1, true).
    local newLineToken to LexerToken:__new(LexerToken,"NEWLINE", char(10), "<builtin>", -1, -1, true).
    local varToken to LexerToken:__new(LexerToken,"VAR", "var", "<builtin>", -1, -1, false).
    local equalsToken to LexerToken:__new(LexerToken,"EQUALS", "=", "<builtin>", -1, -1, false).
    local eoiToken to LexerToken:__new(LexerToken,"EOI", ";", "<builtin>", -1, -1, false).

    local varname to ({local __lhs to ExprBreakupPropagationNode. return __lhs:gen_variable(__lhs).}):call().

    local setStmt to VarDeclarationStmtNode:__new(VarDeclarationStmtNode,list( LiteralNode:__new(LiteralNode,varToken, list(newLineToken)),
      varname, LiteralNode:__new(LiteralNode,equalsToken, list(spaceToken)),
      self:suffix, LiteralNode:__new(LiteralNode,eoiToken, list())
    )).
    
    local inner to SuffixAwaitNode:__new(SuffixAwaitNode,list(self:asyncStmt, varname)).
    return ExprBreakupPropagationNode:__new(ExprBreakupPropagationNode,list(inner, setStmt)).
  }.
}).

local SuffixNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "Suffix".
  }.
  set r:format to {parameter self.
    return list(list(SuffixAwaitNode, SuffixNormalNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:inner to parts[0]. return self.}.

  set r:prototype:simplify to {parameter self,context.
    return self:inner.
  }.

  set r:prototype:children to {parameter self.
    return list(self:inner).
  }.
}).

local SetStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "SetStmt".
  }.
  set r:format to {parameter self.
    return list(list("SET"), list(SuffixNode), list("TO"), list(ExprNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:set to parts[0].
    set self:lvalue to parts[1].
    set self:to to parts[2].
    set self:expr to parts[3].
    set self:eoi to parts[4]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:set, self:lvalue, self:to, self:expr, self:eoi).
  }.

  set r:create to {parameter self,subject, value.
    return SetStmtNode:__new(SetStmtNode,list(
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"SET", char(10), "set ").}):call(),
      subject,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"TO", " ", "to ").}):call(),
      value,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"EOI", "", ".").}):call()
    )).
  }.
}).

local IfElseTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "IfElseTrailerStmt".
  }.
  set r:format to {parameter self.
    return list(list("ELSE"), list(InstructionNode), list("EOI", NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:else to parts[0].
    set self:instruction to parts[1].
    set self:eoi to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:else, self:instruction, self:eoi).
  }.
}).

local IfStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "IfStmt".
  }.
  set r:format to {parameter self.
    return list(list("IF"), list(ExprNode), list(InstructionNode), list("EOI", NoneNode), list(IfElseTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:if to parts[0].
    set self:expr to parts[1].
    set self:instruction to parts[2].
    set self:eoi to parts[3].
    set self:else to parts[4]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:if, self:expr, self:instruction, self:eoi, self:else).
  }.

  set r:prototype:desugar to
  {parameter self,context.
    local arg to self:expr.
    local result to self.
    local propagation to list().

    if ({local __lhs to arg:type. return __lhs:name(__lhs).}):call() = "ExprBreakupPropagation" {
      set propagation to arg:statements:copy.
      set arg to arg:inner.
      set result to IfStmtNode:__new(IfStmtNode,list(self:if, arg, self:instruction, self:eoi, self:else)).
    }

    if ({local __lhs to self:instruction:type. return __lhs:name(__lhs).}):call() = "NonSingleStatementGuard" {
      local c_open to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"CURLYOPEN", char(10), "{").}):call().
      local c_close to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"CURLYCLOSE", char(10), "}").}):call().
      local block to InstructionBlockStmtNode:__new(InstructionBlockStmtNode,list(c_open, self:instruction:instructions, c_close)).
      set result to IfStmtNode:__new(IfStmtNode,list(self:if, arg, block, self:eoi, self:else)).
    }

    if propagation:length = 0
      return result.
    
    propagation:insert(0, result).
    local breakup to ExprBreakupPropagationNode:__new(ExprBreakupPropagationNode,propagation).

    return ({local __lhs to breakup. return __lhs:to_block(__lhs,result).}):call().
  }.

  set r:create to {parameter self,expr, instruction.
    return IfStmtNode:__new(IfStmtNode,list(
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"IF", char(10), "if").}):call(),
      expr,
      instruction, NoneNode:__new(NoneNode,list()), NoneNode:__new(NoneNode,list())
    )).
  }.
}).

local UntilStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "UntilStmt".
  }.
  set r:format to {parameter self.
    return list(list("UNTIL"), list(ExprNode), list(InstructionNode), list("EOI", NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:until to parts[0].
    set self:expr to parts[1].
    set self:instruction to parts[2].
    set self:eoi to parts[3]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:until, self:expr, self:instruction, self:eoi).
  }.
}).

local FromLoopStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "FromLoopStmt".
  }.
  set r:format to {parameter self.
    return list(
      list("FROM"),
      list(InstructionBlockStmtNode),
      list("UNTIL"),
      list(ExprNode),
      list("STEP"),
      list(InstructionBlockStmtNode),
      list("DO"),
      list(InstructionNode),
      list("EOI", NoneNode)
    ).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:from to parts[0].
    set self:setup to parts[1].
    set self:untilLiteral to parts[2].
    set self:until to parts[3].
    set self:stepLiteral to parts[4].
    set self:step to parts[5].
    set self:doLiteral to parts[6].
    set self:do to parts[7].
    set self:eoi to parts[8]. return self.}.

  set r:prototype:children to {parameter self.
    return list(
      self:from,
      self:setup,
      self:untilLiteral,
      self:until,
      self:stepLiteral,
      self:step,
      self:doLiteral,
      self:do,
      self:eoi
    ).
  }.
}).

local UnlockStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "UnlockStmt".
  }.
  set r:format to {parameter self.
    return list(list("UNLOCK"), VarIdentifiers, list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:unlock to parts[0].
    set self:identifier to parts[1].
    set self:eoi to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:unlock, self:identifier, self:eoi).
  }.
}).

local PrintPositionNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "PrintPosition".
  }.
  set r:format to {parameter self.
    return list(
      list("AT"),
      list("BRACKETOPEN"),
      list(ExprNode),
      list("COMMA"),
      list(exprNode),
      list("BRACKETCLOSE")
    ).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:at to parts[0].
    set self:bopen to parts[1].
    set self:column to parts[2].
    set self:comma to parts[3].
    set self:line to parts[4].
    set self:bclose to parts[5]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:at, self:bopen, self:column, self:comma, self:line, self:bclose).
  }.
}).

local PrintStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "PrintStmt".
  }.
  set r:format to {parameter self.
    return list(list("PRINT"), list(ExprNode), list(PrintPositionNode, NoneNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:print to parts[0].
    set self:expr to parts[1].
    set self:position to parts[2].
    set self:eoi to parts[3]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:print, self:expr, self:position, self:eoi).
  }.
}).

local OnStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "OnStmt".
  }.
  set r:format to {parameter self.
    return list(list("ON"), list("VARIDENTIFIER"), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:on to parts[0].
    set self:identifier to parts[1].
    set self:eoi to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:on, self:identifier, self:eoi).
  }.
}).

local ToggleStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ToggleStmt".
  }.
  set r:format to {parameter self.
    return list(list("TOGGLE"), list("VARIDENTIFIER"), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:toggle to parts[0].
    set self:identifier to parts[1].
    set self:eoi to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:toggle, self:identifier, self:eoi).
  }.
}).

local WaitStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "WaitStmt".
  }.
  set r:format to {parameter self.
    return list(list("WAIT"), list("UNTIL", noneNode), list(exprNode), list("EOI", noneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:wait to parts[0].
    set self:until to parts[1].
    set self:expr to parts[2].
    set self:eoi to parts[3]. return self.}.

  set r:prototype:children to {parameter self.
    return list(
      self:wait,
      self:until,
      self:expr,
      self:eoi
    ).
  }.
}).

local WhenStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "WhenStmt".
  }.
  set r:format to {parameter self.
    return list(list("WHEN"), list(exprNode), list("THEN"), list(instructionNode), list("EOI", noneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:when to parts[0].
    set self:expr to parts[1].
    set self:then to parts[2].
    set self:instruction to parts[3].
    set self:eoi to parts[4]. return self.}.

  set r:prototype:children to {parameter self.
    return list(
      self:when,
      self:expr,
      self:then,
      self:instruction,
      self:eoi
    ).
  }.
}).

local StageStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "StageStmt".
  }.
  set r:format to {parameter self.
    return list(list("STAGE"), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:stage to parts[0].
    set self:eoi to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:stage, self:eoi).
  }.
}).

local ClearscreenStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ClearscreenStmt".
  }.
  set r:format to {parameter self.
    return list(list("CLEARSCREEN"), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:clearscreen to parts[0].
    set self:eoi to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:clearscreen, self:eoi).
  }.
}).

local AddStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "AddStmt".
  }.
  set r:format to {parameter self.
    return list(list("ADD"), list(ExprNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:add to parts[0].
    set self:expr to parts[1].
    set self:eoi to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:add, self:expr, self:eoi).
  }.
}).

local RemoveStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "RemoveStmt".
  }.
  set r:format to {parameter self.
    return list(list("REMOVE"), list(ExprNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:remove to parts[0].
    set self:expr to parts[1].
    set self:eoi to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:remove, self:expr, self:eoi).
  }.
}).

local LogStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "LogStmt".
  }.
  set r:format to {parameter self.
    return list(list("LOG"), list(ExprNode), list("TO"), list(ExprNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:logToken to parts[0].
    set self:value to parts[1].
    set self:toToken to parts[2].
    set self:filename to parts[3].
    set self:eoi to parts[4]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:logToken, self:value, self:toToken, self:filename, self:eoi).
  }.
}).

local BreakStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "BreakStmt".
  }.
  set r:format to {parameter self.
    return list(list("BREAK"), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:breakToken to parts[0].
    set self:eoi to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:breakToken, self:eoi).
  }.
}).

local PreserveStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "PreserveStmt".
  }.
  set r:format to {parameter self.
    return list(list("PRESERVE"), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:preserveToken to parts[0].
    set self:eoi to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:preserveToken, self:eoi).
  }.
}).

local ReturnStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ReturnStmt".
  }.
  set r:format to {parameter self.
    return list(list("RETURN"), list(ExprNode, NoneNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:returnToken to parts[0].
    set self:result to parts[1].
    set self:eoi to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:returnToken, self:result, self:eoi).
  }.

  set r:prototype:desugar to {parameter self,context.
    if ({local __lhs to self:result:type. return __lhs:name(__lhs).}):call() <> "ExprBreakupPropagation" {
      return self.
    }

    return ({local __lhs to self:result. return __lhs:to_block(__lhs,({local __lhs to ReturnStmtNode. return __lhs:create(__lhs,self:result:inner).}):call()).}):call().
  }.

  set r:create to {parameter self,subject.
    local returnToken to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"RETURN", char(10), "return ").}):call().
    local eoiToken to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"EOI", "", ".").}):call().
    return ReturnStmtNode:__new(ReturnStmtNode,list(returnToken, subject, eoiToken)).
  }.
}).

local SwitchStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "SwitchStmt".
  }.
  set r:format to {parameter self.
    return list(list("SWITCH"), list("TO"), list(ExprNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:switchToken to parts[0].
    set self:toToken to parts[1].
    set self:identifier to parts[2].
    set self:eoi to parts[3]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:switchToken, self:toToken, self:identifier, self:eoi).
  }.
}).

local CopyStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "CopyStmt".
  }.
  set r:format to {parameter self.
    return list(list("COPY"), list(ExprNode), list("FROM", "TO"), list(ExprNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:copyToken to parts[0].
    set self:sourceIdentifier to parts[1].
    set self:fromToIdentifier to parts[2].
    set self:destIdentifier to parts[4].
    set self:eoi to parts[5]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:copyToken, self:sourceIdentifier, self:fromToIdentifier, self:destIdentifier, self:eoi).
  }.
}).

local RenameStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "RenameStmt".
  }.
  set r:format to {parameter self.
    return list(list("RENAME"), list("VOLUME", "FILE", NoneNode), list(ExprNode), list("TO"), list(ExprNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:renameToken to parts[0].
    set self:filter to parts[1].
    set self:fromIdentifier to parts[2].
    set self:toToken to parts[3].
    set self:destIdentifier to parts[4].
    set self:eoi to parts[5]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:renameToken, self:filter, self:fromIdentifier, self:toToken, self:destIdentifier, self:eoi).
  }.
}).

local DeleteTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "DeleteTrailer".
  }.
  set r:format to {parameter self.
    return list(list("FROM"), list(ExprNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:fromToken to parts[0].
    set self:expr to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:fromToken, self:expr).
  }.
}).

local DeleteStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "DeleteStmt".
  }.
  set r:format to {parameter self.
    return list(list("DELETE"), list(ExprNode), list(DeleteTrailerNode, NoneNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:deleteToken to parts[0].
    set self:expr to parts[1].
    set self:from to parts[2].
    set self:eoi to parts[3]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:compileToken, self:expr, self:from, self:eoi).
  }.
}).

local EditStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "EditStmt".
  }.
  set r:format to {parameter self.
    return list(list("EDIT"), list(ExprNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:editToken to parts[0].
    set self:expr to parts[1].
    set self:eoi to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:editToken, self:expr, self:eoi).
  }.
}).

local RunArgListNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "RunArgList".
  }.
  set r:format to {parameter self.
    return list(list("BRACKETOPEN"), list(ArgListNode), list("BRACKETCLOSE")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:bopen to parts[0].
    set self:arglist to parts[1].
    set self:bclose to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:bopen, self:arglist, self:bclose).
  }.
}).

local RunStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "RunPathStmt".
  }.
  set r:format to {parameter self.
    return list(list("RUN"), list("ONCE", NoneNode), list("FILEIDENT", "STRING"), list(RunArgListNode, NoneNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:runToken to parts[0].
    set self:onceToken to parts[1].
    set self:identifier to parts[2].
    set self:arglist to parts[3].
    set self:eoi to parts[4]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:runToken, self:onceToken, self:identifier, self:arglist, self:eoi).
  }.
}).

local RunPathTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "RunPathTrailer".
  }.
  set r:format to {parameter self.
    return list(list("COMMA"), list(ArgListNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:comma to parts[0].
    set self:arglist to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:comma, self:arglist).
  }.
}).

local RunPathStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "RunPathStmt".
  }.
  set r:format to {parameter self.
    return list(list("RUNPATH", "RUNONCEPATH"), list("BRACKETOPEN"), list(ExprNode), list(RunPathTrailerNode, NoneNode), list("BRACKETCLOSE"), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:runPathToken to parts[0].
    set self:bopen to parts[1].
    set self:expr to parts[2].
    set self:args to parts[3].
    set self:bclose to parts[4].
    set self:eoi to parts[5]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:runPathToken, self:bopen, self:expr, self:args, self:bclose, self:eoi).
  }.
}).

local CompileTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "CompileTrailer".
  }.
  set r:format to {parameter self.
    return list(list("TO"), list(ExprNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:toToken to parts[0].
    set self:expr to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:toToken, self:expr).
  }.
}).

local CompileStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "CompileStmt".
  }.
  set r:format to {parameter self.
    return list(list("COMPILE"), list(ExprNode), list(CompileTrailerNode, NoneNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:compileToken to parts[0].
    set self:expr to parts[1].
    set self:to to parts[2].
    set self:eoi to parts[3]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:compileToken, self:expr, self:to, self:eoi).
  }.
}).

local ListDestinationTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ListDestinationTrailer".
  }.
  set r:format to {parameter self.
    return list(list("IN"), list("IDENTIFIER")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:inToken to parts[0].
    set self:identifier to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:inToken, self:identifier).
  }.
}).

local ListIdentifierTrailerNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ListIdentifierTrailer".
  }.
  set r:format to {parameter self.
    return list(list("IDENTIFIER"), list(ListDestinationTrailerNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:identifier to parts[0].
    set self:trailer to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:identifier, self:trailer).
  }.
}).

local ListStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ListStmt".
  }.
  set r:format to {parameter self.
    return list(list("LIST"), list(ListIdentifierTrailerNode, NoneNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:listToken to parts[0].
    set self:trailer to parts[1].
    set self:eof to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:listToken, self:trailer, self:eof).
  }.
}).

local RebootStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "RebootStmt".
  }.
  set r:format to {parameter self.
    return list(list("REBOOT"), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:rebootToken to parts[0].
    set self:eoi to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:rebootToken, self:eoi).
  }.
}).

local ShutdownStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ShutdownStmt".
  }.
  set r:format to {parameter self.
    return list(list("SHUTDOWN"), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:shutdownToken to parts[0].
    set self:eoi to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:shutdownToken, self:eoi).
  }.
}).

local ForStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ForStmt".
  }.
  set r:format to {parameter self.
    return list(list("FOR"), VarIdentifiers, list("IN"), list(SuffixNode), list(InstructionNode), list("EOI", NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:forToken to parts[0].
    set self:identifier to parts[1].
    set self:inToken to parts[2].
    set self:collection to parts[3].
    set self:instruction to parts[4].
    set self:eoi to parts[5]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:forToken, self:identifier, self:inToken, self:collection, self:instruction, self:eoi).
  }.

  set r:create to {parameter self,identifier, collection, instruction.
    return ForStmtNode:__new(ForStmtNode,list(
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"FOR", char(10), "for ").}):call(),
      identifier,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"IN", " ", "in ").}):call(),
      collection,
      instruction, NoneNode:__new(NoneNode,list())
    )).
  }.
}).

local UnsetStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "UnsetStmt".
  }.
  set r:format to {parameter self.
    return list(list("UNSET"), list("IDENTIFIER", "ALL"), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:unsetToken to parts[0].
    set self:identifier to parts[1].
    set self:eoi to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:unsetToken, self:identifier, self:eoi).
  }.
}).

local ClassInitMemberNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ClassInitMember".
  }.
  
  set r:format to {parameter self.
    return list(list("INIT"), list(FunctionCallTrailerNode), list("ASYNC", NoneNode), list("CURLYOPEN"), list(InstructionListNode, NoneNode), list("CURLYCLOSE")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:init to parts[0].
    set self:args to parts[1].
    set self:asyncStmt to parts[2].
    set self:open to parts[3].
    set self:instructions to parts[4].
    set self:close to parts[5]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:init, self:args, self:asyncStmt, self:open, self:instructions, self:close).
  }.

  set r:prototype:to_instructions to {parameter self,instructions, resultIdentifier, prototypeIdentifier.
    local legacyIdentifier to char(34) + "__new" + char(34).
    local stringIdentifier to char(34) + "new" + char(34).
    local resultEntry to ArrayAccessNode:__new(ArrayAccessNode,list(
      resultIdentifier,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"SQUAREOPEN", "", "[").}):call(),
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"STRING", "", stringIdentifier).}):call(),
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"SQUARECLOSE", "", "]").}):call()
    )).
    local legacyEntry to ArrayAccessNode:__new(ArrayAccessNode,list(
      resultIdentifier,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"SQUAREOPEN", "", "[").}):call(),
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"STRING", "", legacyIdentifier).}):call(),
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"SQUARECLOSE", "", "]").}):call()
    )).
    local legacyStmt to ({local __lhs to SetStmtNode. return __lhs:create(__lhs,legacyEntry, resultEntry).}):call().
    set instructions to InstructionListNode:__new(InstructionListNode,list(legacyStmt, instructions)).

    local parameters to ({local __lhs to self:args. return __lhs:to_parameters(__lhs,"type").}):call().
    
    local selfIdentifier to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"IDENTIFIER", " ", "self").}):call().
    local returnStmt to ({local __lhs to ReturnStmtNode. return __lhs:create(__lhs,selfIdentifier).}):call().
    local innerInstructions to false.
    if ({local __lhs to self:instructions:type. return __lhs:name(__lhs).}):call() = "InstructionList" {
      set innerInstructions to ({local __lhs to self:instructions. return __lhs:append(__lhs,list(returnStmt)).}):call().
    } else {
      set innerInstructions to InstructionListNode:__new(InstructionListNode,list(self:instructions, returnStmt)).
    }
    local selfcopyStmt to ({local __lhs to DeclareStmtScopedDeclarationNode. return __lhs:create_local(__lhs,selfIdentifier, StructureAccessNode:__new(StructureAccessNode,list(
      prototypeIdentifier,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"COLON", "", ":").}):call(),
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"IDENTIFIER", "", "copy").}):call()
    ))).}):call().
    set innerInstructions to InstructionListNode:__new(InstructionListNode,list(selfcopyStmt, innerInstructions)).
    set innerInstructions to InstructionListNode:__new(InstructionListNode,list(parameters, innerInstructions)).

    local functionDef to InstructionBlockStmtNode:__new(InstructionBlockStmtNode,list(
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"BRACKETOPEN", "", "{").}):call(),
      innerInstructions,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"BRACKETCLOSE", "", "}").}):call()
    )).
    local setStmt to ({local __lhs to SetStmtNode. return __lhs:create(__lhs,resultEntry, functionDef).}):call().

    set instructions to InstructionListNode:__new(InstructionListNode,list(setStmt, instructions)).
    return instructions.
  }.
}).

local ClassFunctionStaticMemberIdentifierNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ClassFunctionStaticMemberIdentifier".
  }.
  
  set r:format to {parameter self.
    return list(list("SELF"), list("COLON"), VarIdentifiers).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:selfToken to parts[0].
    set self:colonToken to parts[1].
    set self:identifier to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:selfToken, self:colonToken, self:identifier).
  }.
}).

local ClassFunctionMemberNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ClassFunctionMember".
  }.
  
  set r:format to {parameter self.
    local allowedIdentifiers to VarIdentifiers:copy.
    allowedIdentifiers:insert(0, ClassFunctionStaticMemberIdentifierNode).
    return list(list("FUNC"), allowedIdentifiers, list(FunctionCallTrailerNode), list("CURLYOPEN"), list(InstructionListNode, NoneNode), list("CURLYCLOSE")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:func to parts[0].
    set self:identifier to parts[1].
    set self:args to parts[2].
    set self:open to parts[3].
    set self:instructions to parts[4].
    set self:close to parts[5]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:func, self:identifier, self:args, self:open, self:instructions, self:close).
  }.

  set r:prototype:to_instructions to {parameter self,instructions, resultIdentifier, prototypeIdentifier.
    local destination to prototypeIdentifier.
    local identifier to self:identifier.
    if ({local __lhs to self:identifier:type. return __lhs:name(__lhs).}):call() = "ClassFunctionStaticMemberIdentifier" {
      set destination to resultIdentifier.
      set identifier to self:identifier:identifier.
    }

    local stringIdentifier to char(34) + identifier:token:text + char(34).
    local prototypeEntry to ArrayAccessNode:__new(ArrayAccessNode,list(
      destination,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"SQUAREOPEN", "", "[").}):call(),
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"STRING", "", stringIdentifier).}):call(),
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"SQUARECLOSE", "", "]").}):call()
    )).
    local parameters to ({local __lhs to self:args. return __lhs:to_parameters(__lhs).}):call().
    
    local functionDef to InstructionBlockStmtNode:__new(InstructionBlockStmtNode,list(
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"BRACKETOPEN", "", "{").}):call(), InstructionListNode:__new(InstructionListNode,list(parameters, self:instructions)),
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"BRACKETCLOSE", "", "}").}):call()
    )).
    local setStmt to ({local __lhs to SetStmtNode. return __lhs:create(__lhs,prototypeEntry, functionDef).}):call().

    set instructions to InstructionListNode:__new(InstructionListNode,list(setStmt, instructions)).
    return instructions.
  }.
}).

local ClassMixNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ClassMix".
  }.
  
  set r:format to {parameter self.
    return list(list("MIX"), VarIdentifiers).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:mixToken to parts[0].
    set self:identifier to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:mixToken, self:identifier).
  }.

  set r:prototype:to_instructions to {parameter self,instructions, resultIdentifier, prototypeIdentifier.
    //for k in m:keys
    //  if not k:startsWith("__")
    //    set s[k] to m[k].

    local sourceIdentifier to ({local __lhs to StructureAccessNode. return __lhs:create(__lhs,self:identifier, ({local __lhs to LiteralNode. return __lhs:create(__lhs,"IDENTIFIER", "", "prototype").}):call()).}):call().
    local destIdentifier to prototypeIdentifier.
    local keyIdentifier to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"IDENTIFIER", "", "k").}):call().
    local innerCopy to ({local __lhs to SetStmtNode. return __lhs:create(__lhs,({local __lhs to ArrayAccessNode. return __lhs:create(__lhs,destIdentifier, keyIdentifier).}):call(), ({local __lhs to ArrayAccessNode. return __lhs:create(__lhs,sourceIdentifier, keyIdentifier).}):call()).}):call().

    local underscoreString to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"STRING", "", char(34)+"__"+char(34)).}):call().
    local startsWithCall to ({local __lhs to FunctionCallNode. return __lhs:create(__lhs,({local __lhs to StructureAccessNode. return __lhs:create(__lhs,keyIdentifier, ({local __lhs to LiteralNode. return __lhs:create(__lhs,"IDENTIFIER", "", "startsWith").}):call()).}):call(), list(underscoreString)).}):call().
    local negatedStartsWith to UnaryExprInnerNode:__new(UnaryExprInnerNode,list(({local __lhs to LiteralNode. return __lhs:create(__lhs,"NOT", " ", "not ").}):call(), startsWithCall)).
    local ifStmt to ({local __lhs to IfStmtNode. return __lhs:create(__lhs,negatedStartsWith, innerCopy).}):call().

    local keysLookup to ({local __lhs to StructureAccessNode. return __lhs:create(__lhs,
      sourceIdentifier,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"IDENTIFIER", "", "keys").}):call()
    ).}):call().
    local forLoop to ({local __lhs to ForStmtNode. return __lhs:create(__lhs,keyIdentifier, keysLookup, ifStmt).}):call().

    return InstructionListNode:__new(InstructionListNode,list(forLoop, instructions)).
  }.
}).

local ClassMemberListNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ClassMemberList".
  }.
  
  set r:format to {parameter self.
    return list(list(ClassInitMemberNode, ClassFunctionMemberNode, ClassMixNode), list(ClassMemberListNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:member to parts[0].
    set self:tail to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:member, self:tail).
  }.

  set r:prototype:to_list to {parameter self,out.
    out:add(self:member).
    if ({local __lhs to self:tail:type. return __lhs:name(__lhs).}):call() <> "None" {
      ({local __lhs to self:tail. return __lhs:to_list(__lhs,out).}):call().
    }
  }.
}).

local ClassDefinitionStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "ClassDefinitionStmt".
  }.
  set r:format to {parameter self.
    local allowedIdentifiers to VarIdentifiers:copy.
    return list(list("CLASS"), allowedIdentifiers, list("CURLYOPEN"), list(ClassMemberListNode, NoneNode), list("CURLYCLOSE")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:class to parts[0].
    set self:identifier to parts[1].
    set self:open to parts[2].
    set self:defs to parts[3].
    set self:close to parts[4]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:class, self:identifier, self:open, self:defs, self:close).
  }.

  set r:prototype:members to {parameter self.
    if ({local __lhs to self:defs:type. return __lhs:name(__lhs).}):call() = "None" {
      return list().
    }
    local result to list().
    ({local __lhs to self:defs. return __lhs:to_list(__lhs,result).}):call().
    return result.
  }.

  set r:prototype:desugar to {parameter self,context.
    local resultIdentifier to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"IDENTIFIER", " ", "result").}):call().
    local prototypeIdentifier to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"IDENTIFIER", " ", "prototype").}):call().
    local instructions to ({local __lhs to ReturnStmtNode. return __lhs:create(__lhs,resultIdentifier).}):call().

    local hasConstructor to false.
    local todo to ({local __lhs to self. return __lhs:members(__lhs).}):call().

    until todo:length = 0 {
      local member to todo[todo:length - 1].
      set todo to todo:sublist(0, todo:length - 1).

      if ({local __lhs to member:type. return __lhs:name(__lhs).}):call() = "ClassInitMember"
        set hasConstructor to true.
      
      set instructions to ({local __lhs to member. return __lhs:to_instructions(__lhs,instructions, resultIdentifier, prototypeIdentifier).}):call().
    }

    //local typename to ({
    //  local prototype to lex().
    //  local result to lex("prototype", lex()).
    //  
    //  
    //  return result.
    //}):call().

    local lexToken to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"IDENTIFIER", " ", "lex").}):call().
    local prototypeVal to ({local __lhs to FunctionCallNode. return __lhs:create(__lhs,lexToken, list()).}):call().
    local resultVal to ({local __lhs to FunctionCallNode. return __lhs:create(__lhs,lexToken, list(({local __lhs to LiteralNode. return __lhs:create(__lhs,"STRING", "", char(34)+"prototype"+char(34)).}):call(), prototypeIdentifier)).}):call().
    local prototypeDef to ({local __lhs to DeclareStmtScopedDeclarationNode. return __lhs:create_local(__lhs,prototypeIdentifier, prototypeVal).}):call().
    local resultDef to ({local __lhs to DeclareStmtScopedDeclarationNode. return __lhs:create_local(__lhs,resultIdentifier, resultVal).}):call().
    set instructions to InstructionListNode:__new(InstructionListNode,list(prototypeDef, InstructionListNode:__new(InstructionListNode,list(resultDef, instructions)))).

    local block to ({local __lhs to BracketExprAtom. return __lhs:create(__lhs, InstructionBlockStmtNode:__new(InstructionBlockStmtNode,list(
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"BRACKETOPEN", "", "{").}):call(),
      instructions,
      ({local __lhs to LiteralNode. return __lhs:create(__lhs,"BRACKETCLOSE", "", "}").}):call()
    ))).}):call().
    local evalStmt to ({local __lhs to FunctionCallNode. return __lhs:create(__lhs, StructureAccessNode:__new(StructureAccessNode,list(
        block,
        ({local __lhs to LiteralNode. return __lhs:create(__lhs,"COLON", "", ":").}):call(),
        ({local __lhs to LiteralNode. return __lhs:create(__lhs,"IDENTIFIER", "", "call").}):call()
      )),
      list()
    ).}):call().
    return ({local __lhs to DeclareStmtScopedDeclarationNode. return __lhs:create_local(__lhs,self:identifier, evalStmt).}):call().
  }.
}).

local IdentifierLedExprNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "IdentifierLedExpr".
  }.
  set r:format to {parameter self.
    return list(list("ON", "OFF", NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:tail to parts[0]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:tail).
  }.

  set r:prototype:simplify to {parameter self,context.
    local tailType to self:tail:type.
    if ({local __lhs to tailType. return __lhs:name(__lhs).}):call() = "None" {
      return self:tail.
    }
    return self.
  }.
}).

local IdentifierLedAssignmentNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "IdentifierLedAssignment".
  }.
  set r:format to {parameter self.
    return list(list("ASSIGNMENT"), list(ExprNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:assignment to parts[0].
    set self:value to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:assignment, self:value).
  }.

  set r:prototype:simplify to {parameter self,context.
    return self.
  }.
}).

local IdentifierLedStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "IdentifierLedStmt".
  }.
  set r:format to {parameter self.
    return list(list(SuffixNode), list(IdentifierLedAssignmentNode, IdentifierLedExprNode), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:suffix to parts[0].
    set self:expr to parts[1].
    set self:eoi to parts[2]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:suffix, self:expr, self:eoi).
  }.

  set r:prototype:to_kos to {parameter self,subject, value.
    if ({local __lhs to self:expr:type. return __lhs:name(__lhs).}):call() = "IdentifierLedAssignment" {
      return SetStmtNode:__new(SetStmtNode,list(
        ({local __lhs to LiteralNode. return __lhs:create(__lhs,"SET", " ", "set ").}):call(),
        subject,
        ({local __lhs to LiteralNode. return __lhs:create(__lhs,"TO", " ", "to ").}):call(),
        value
      )).
    }

    return self.
  }.

  set r:prototype:desugar to {parameter self,context.
    local subject to self:suffix.
    local propagation to list().

    if ({local __lhs to subject:type. return __lhs:name(__lhs).}):call() = "ExprBreakupPropagation" {
      for stmt in subject:statements {
        propagation:add(stmt).
      }
      set subject to subject:inner.
    }

    if ({local __lhs to self:expr:type. return __lhs:name(__lhs).}):call() = "IdentifierLedAssignment" {
      local value to self:expr:value.
      if ({local __lhs to value:type. return __lhs:name(__lhs).}):call() = "ExprBreakupPropagation" {
        for stmt in value:statements {
          propagation:add(stmt).
        }
        set value to value:inner.
      }

      if ({local __lhs to value:type. return __lhs:name(__lhs).}):call() = "SuffixAwait" {
        local varname to ({local __lhs to ExprBreakupPropagationNode. return __lhs:gen_variable(__lhs).}):call().
        local var_literal to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"VAR", char(10), "var").}):call().
        local assignment_literal to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"ASSIGNMENT", " ", "=").}):call().
        local inner to AsyncResolveStmtNode:__new(AsyncResolveStmtNode,list(var_literal, varname, assignment_literal, value:asyncStmt, value:suffix, self:eoi)).
        if propagation:length = 0
          return inner.

        local breakup to ExprBreakupPropagationNode:__new(ExprBreakupPropagationNode,list(inner, propagation)).
        return ({local __lhs to breakup. return __lhs:to_block(__lhs,inner).}):call().
      }

      local set_literal to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"SET", " ", "set ").}):call().
      local to_literal to ({local __lhs to LiteralNode. return __lhs:create(__lhs,"TO", " ", "to ").}):call().
      local stmt to SetStmtNode:__new(SetStmtNode,list(set_literal, subject, to_literal, value, self:eoi)).

      if propagation:length = 0
        return stmt.

      propagation:insert(0, stmt).
      local breakup to ExprBreakupPropagationNode:__new(ExprBreakupPropagationNode,propagation).
      return ({local __lhs to breakup. return __lhs:to_block(__lhs,stmt).}):call().
    }
    
    local expr to self:expr.
    if ({local __lhs to expr:type. return __lhs:name(__lhs).}):call() = "ExprBreakupPropagation" {
      for stmt in expr:statements {
        propagation:add(stmt).
      }
      set expr to expr:inner.
    }
    local stmt to IdentifierLedStmtNode:__new(IdentifierLedStmtNode,list(subject, expr, self:eoi)).

    if propagation:length = 0
      return stmt.

    propagation:insert(0, stmt).
    local breakup to ExprBreakupPropagationNode:__new(ExprBreakupPropagationNode,propagation).
    return ({local __lhs to breakup. return __lhs:to_block(__lhs,stmt).}):call().
  }.
}).

local DirectiveNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "Directive".
  }.
  set r:format to {parameter self.
    return list(list("ATSIGN"), list("LAZYGLOBAL"), list("ON", "OFF"), list("EOI")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:atsign to parts[0].
    set self:lazyglobal to parts[1].
    set self:status to parts[2].
    set self:eoi to parts[3]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:atsign, self:lazyglobal, self:status, self:eoi).
  }.
}).

local IncludeStmtNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "IncludeStmt".
  }.
  set r:format to {parameter self.
    return list(list("INCLUDE"), list("STRING")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:include to parts[0].
    set self:target to parts[1].

    set self:unexecuted to true. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:include, self:target).
  }.

  set r:prototype:filename to {parameter self.
    local full to self:target:token:text.
    local stripped to full:substring(1, full:length - 2).
    if stripped:startswith("0:") {
      return "/archive/" + stripped:substring(3, stripped:length - 3).
    }
    return stripped.
  }.

  set r:prototype:update_state to {parameter self,parser.
    set parser:current:blocked to true.
    local filename to ({local __lhs to self. return __lhs:filename(__lhs).}):call().
    if parser:file_cache:haskey(filename) {
      local lwindow to parser:file_cache[filename].
      set parser:current:blocked to false.
      if lwindow:istype("Boolean") {
        set parser:current to ({local __lhs to parser:current. return __lhs:with_fatal(__lhs,"Unable to open for inclusion: " + filename).}):call().
      } else {
        // check if we already ran inclusion
        if self:unexecuted {
          set self:unexecuted to false.
          set parser:current to ({local __lhs to parser:current. return __lhs:include(__lhs,lwindow).}):call().
        }
      }
      return.
    }

    parser:readFileFn(filename).
  }.

  set r:prototype:simplify to {parameter self,context.
    return NoneNode:__new(NoneNode,list()).
  }.
}).

local InstructionNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "Instruction".
  }.
  set r:format to {parameter self.
    return list(list(
      EmptyStmtNode,
      SetStmtNode,
      IfStmtNode,
      UntilStmtNode,
      FromLoopStmtNode,
      UnlockStmtNode,
      PrintStmtNode,
      OnStmtNode,
      ToggleStmtNode,
      WaitStmtNode,
      WhenStmtNode,
      StageStmtNode,
      ClearscreenStmtNode,
      AddStmtNode,
      RemoveStmtNode,
      LogStmtNode,
      BreakStmtNode,
      PreserveStmtNode,
      DeclareStmtNode,
      ReturnStmtNode,
      SwitchStmtNode,
      CopyStmtNode,
      RenameStmtNode,
      DeleteStmtNode,
      EditStmtNode,
      RunStmtNode,
      RunPathStmtNode,
      CompileStmtNode,
      ListStmtNode,
      RebootStmtNode,
      ShutdownStmtNode,
      ForStmtNode,
      UnsetStmtNode,
      InstructionBlockStmtNode,
      VarDeclarationStmtNode,
      ClassDefinitionStmtNode,
      IdentifierLedStmtNode,
      DirectiveNode,
      IncludeStmtNode
    )).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:instruction to parts[0]. return self.}.

  set r:prototype:simplify to {parameter self,context.
    return self:instruction.
  }.

  set r:prototype:children to {parameter self.
    return list(self:instruction).
  }.
}).

local InstructionListNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "InstructionList".
  }.
  set r:format to {parameter self.
    return list(list(InstructionNode), list(InstructionListNode, NoneNode)).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:instruction to parts[0].
    set self:tail to parts[1]. return self.}.

  set r:prototype:simplify to {parameter self,context.
    local tailType to self:tail:type.
    if ({local __lhs to tailType. return __lhs:name(__lhs).}):call() = "None" {
      return self:instruction.
    }
    return self.
  }.

  set r:prototype:children to {parameter self.
    return list(self:instruction, self:tail).
  }.

  set r:prototype:append to {parameter self,instruction_list.
    local tailType to ({local __lhs to self:tail:type. return __lhs:name(__lhs).}):call().
    if tailType <> "InstructionList" {
      if instruction_list:length = 0
        return self:copy.
      
      local last to instruction_list[instruction_list:length - 1].
      local rest to instruction_list:sublist(0, instruction_list:length - 1).
      
      until rest:length = 0 {
        local current to rest[rest:length - 1].
        set rest to rest:sublist(0, rest:length - 1).
        set last to InstructionListNode:__new(InstructionListNode,list(current, last)).
      }

      if tailType <> "None" {
        set last to InstructionListNode:__new(InstructionListNode,list(self:tail, last)).
      }

      return InstructionListNode:__new(InstructionListNode,list(self:instruction, last)).
    } else {
      local tail to ({local __lhs to self:tail. return __lhs:append(__lhs,instruction_list).}):call().
      return InstructionListNode:__new(InstructionListNode,list(self:instruction, tail)).
    }
  }.
}).

local ProgramNode to Class:__new(Class, {parameter r.
  r:mix(r:prototype,ParserNode:prototype).

  set r:name to {parameter self.
    return "Program".
  }.
  set r:format to {parameter self.
    return list(list(InstructionListNode, NoneNode), list("EOF")).
  }.

  set r:__new to{parameter __cls,parts.local self to __cls:prototype:copy. set self:type to __cls.
    set self:instructionList to parts[0].
    set self:eof to parts[1]. return self.}.

  set r:prototype:children to {parameter self.
    return list(self:instructionList, self:eof).
  }.

  set r:prototype:desugar to {parameter self,context.
    if ({local __lhs to self:instructionList:type. return __lhs:name(__lhs).}):call() = "NonSingleStatementGuard" {
      return ProgramNode:__new(ProgramNode,list(self:instructionList:instructions, self:eof)).
    }

    return self.
  }.
}).

local Kpp2ParseTree to Class:__new(Class, {parameter r.
  r:mix(r:prototype,Program:prototype).

  set r:__new to{parameter __cls,id, input, output, args, env, fs, resultCallback.local self to __cls:prototype:copy. set self:type to __cls.
    PROGRAM_INIT.
    set self:subProc to false.
    self:readLine(self). return self.}.

  set r:prototype:onRead to {parameter self,line.
    if line:istype("Boolean") {
      self:exit(self,0).
      return.
    }
    self:print(self,"Parsing (expr): " + line).
    local lexer to Lexer:__new(Lexer,LexerDefs, "kpp", line, "stdin", false).
    local parser to Parser:__new(Parser,lexer:window, LexerDefs, "kpp", {}, ProgramNode, {}).
    until parser:atend {
      parser:next(parser).
    }
    if parser:value:istype("Boolean") {
      print("Parse failed:").
      print(parser:errors).
      self:readLine(self).
      return.
    }
    
    local simplifier to BottomUpStrategy:__new(BottomUpStrategy,parser:value, "simplify", false).
    until simplifier:atend {
      simplifier:next(simplifier).
    }
    
    local desugarer to simplifier.
    if not self:env:haskey("NO_DESUGAR") {
      local async_result to lex("errors", list()).
      set desugarer to BottomUpStrategy:__new(BottomUpStrategy,simplifier:value, "desugar", async_result).
      until desugarer:atend or async_result:errors:length > 0 {
        desugarer:next(desugarer).
      }
      if async_result:errors:length > 0
      {
        print(async_result).
        self:readLine(self).
        return.
      }
    }

    print(desugarer:value:toSExpr(desugarer:value)).
    print(desugarer:value:string(desugarer:value)).

    self:readLine(self).
  }.
}).
set export to Kpp2ParseTree.