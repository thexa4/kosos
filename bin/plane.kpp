#include "0:/kosos/stdlib.kpp"

#define UNSAFE_ACCESSOR 1

class Plane {
  init(id, input, output, args, env, fs, resultCallback) {
    self->pid = id;
    self->status = 0;
    self->input = input;
    self->output = output;
    self->env = env;
    self->fs = fs;
    self->resultCallback = resultCallback;
    self->running = false;
    if args:length = 1 or args[1] = "help" {
      self->help();
      return self;
    }

    var cmds = lex("takeoff", "takeoff", "heading", "heading", "hold", "hold", "ditch", "ditch", "land", "land", "set", false);
    if not cmds:haskey(args[1]) {
      self->help();
      return self;
    }
    self->chosenCommand = args[1];
    self->desiredState = cmds[args[1]];
    self->desiredAlt = false;
    self->desiredHeading = false;
    self->desiredSpeed = false;
    self->desiredGroundRef = false;
    self->waitUntilDone = false;

    var toParse = args:sublist(2, args:length - 2);
    until toParse:length = 0 {
      if toParse[0] = "wait" {
        self->waitUntilDone = true;
        toParse = toParse:sublist(1, toParse:length);
      } else {
        if toParse:length < 2 {
          self->help();
          return self;
        }
        var param = toParse[0];
        if param = "alt" {
          self->desiredAlt = toParse[1]:tonumber(-1000);
          if self->desiredAlt = -1000 {
            self->help();
            return self;
          }
        } else if param = "heading" {
          self->desiredHeading = toParse[1]:tonumber(-1000);
          if self->desiredHeading = -1000 {
            self->help();
            return self;
          }
        } else if param = "speed" {
          self->desiredSpeed = toParse[1]:tonumber(-1000);
          if self->desiredSpeed = -1000 {
            self->help();
            return self;
          }
        } else if param = "groundref" {
          self->desiredGroundRef = toParse[1]:tonumber(-1000);
          if self->desiredGroundRef = -1000 {
            self->help();
            return self;
          }
        } else {
          output->append("Unable to set property " + param, Callback->null);
          self->help();
          return self;
        }

        toParse = toParse:sublist(2, toParse:length);
      }
    }

    self->stateFile = false;
    self->altFile = false;
    self->headingFile = false;
    self->speedFile = false;
    self->groundRefFile = false;
    self->toRead = 0;

    self->loading = true;

    if not self->desiredState:istype("Boolean") {
      self->toRead = self->toRead + 1;
    }
    if not self->desiredAlt:istype("Boolean") {
      self->toRead = self->toRead + 1;
    }
    if not self->desiredHeading:istype("Boolean") {
      self->toRead = self->toRead + 1;
    }
    if not self->desiredSpeed:istype("Boolean") {
      self->toRead = self->toRead + 1;
    }
    if not self->desiredGroundRef:istype("Boolean") {
      self->toRead = self->toRead + 1;
    }
    if not self->desiredState:istype("Boolean") {
      fs->modify("/sys/class/plane/state", new Callback(self, "onOpenState"));
    }
    if not self->desiredAlt:istype("Boolean") {
      fs->modify("/sys/class/plane/targetAlt", new Callback(self, "onOpenAlt"));
    }
    if not self->desiredHeading:istype("Boolean") {
      fs->modify("/sys/class/plane/targetHeading", new Callback(self, "onOpenHeading"));
    }
    if not self->desiredSpeed:istype("Boolean") {
      fs->modify("/sys/class/plane/targetSpeed", new Callback(self, "onOpenSpeed"));
    }
    if not self->desiredGroundRef:istype("Boolean") {
      fs->modify("/sys/class/plane/groundAltReference", new Callback(self, "onOpenGroundRef"));
    }
  }

  func onOpenState(file) {
    if not self->loading
      return;
    
    if file:istype("Boolean") {
      return self->helpNotRunning();
    }

    self->stateFile = file;
    self->toRead = self->toRead - 1;
    if self->toRead = 0 {
      self->onAllLoaded();
    }
  }
  func onOpenAlt(file) {
    if not self->loading
      return;
    
    if file:istype("Boolean") {
      return self->helpNotRunning();
    }

    self->altFile = file;
    self->toRead = self->toRead - 1;
    if self->toRead = 0 {
      self->onAllLoaded();
    }
  }
  func onOpenHeading(file) {
    if not self->loading
      return;
    
    if file:istype("Boolean") {
      return self->helpNotRunning();
    }

    self->headingFile = file;
    self->toRead = self->toRead - 1;
    if self->toRead = 0 {
      self->onAllLoaded();
    }
  }
  func onOpenSpeed(file) {
    if not self->loading
      return;
    
    if file:istype("Boolean") {
      return self->helpNotRunning();
    }

    self->speedFile = file;
    self->toRead = self->toRead - 1;
    if self->toRead = 0 {
      self->onAllLoaded();
    }
  }
  func onOpenGroundRef(file) {
    if not self->loading
      return;
    
    if file:istype("Boolean") {
      return self->helpNotRunning();
    }

    self->groundRefFile = file;
    self->toRead = self->toRead - 1;
    if self->toRead = 0 {
      self->onAllLoaded();
    }
  }

  func onAllLoaded() {
    self->loading = false;

    if not self->desiredState:istype("Boolean") {
      self->stateFile->write("" + self->desiredState, Callback->null);
    }
    if not self->desiredAlt:istype("Boolean") {
      self->altFile->write("" + self->desiredAlt, Callback->null);
    }
    if not self->desiredHeading:istype("Boolean") {
      self->headingFile->write("" + self->desiredHeading, Callback->null);
    }
    if not self->desiredSpeed:istype("Boolean") {
      self->speedFile->write("" + self->desiredSpeed, Callback->null);
    }
    if not self->desiredGroundRef:istype("Boolean") {
      self->groundRefFile->write("" + self->desiredGroundRef, Callback->null);
    }
    if self->waitUntilDone {
      
      if self->chosenCommand = "takeoff" or self->chosenCommand = "land" or self->chosenCommand = "ditch" {
        if self->chosenCommand <> "takeoff" {
          print("todo: implement waiting on " + self->chosenCommand);
          
          self->status = 1;
          self->fs->loop->defer(self->resultCallback, self->pid);
          return;
        }
        self->fs->loop->register(self);
        self->running = true;
        return;
      }
      self->status = 0;
      self->fs->loop->defer(self->resultCallback, self->pid);
      return;
      
    } else {
      self->status = 0;
      self->fs->loop->defer(self->resultCallback, self->pid);
    }
  }

  func helpNotRunning() {
    self->output->callback("Unable to open planed config files.", Callback->null);
    self->output->callback("Is planed running?", Callback->null);
    self->output->callback("If not, try starting with 'planed detach'", Callback->null);
    
    self->status = 1;
    self->loading = false;
    self->fs->loop->defer(self->resultCallback, self->pid);
  }

  func help() {
    self->output->append("Usage: plane <subcommand> [wait] [alt <altitude>] [heading <heading>] [speed <speed>] [groundref <0|1>]", Callback->null);
    self->output->append("", Callback->null);
    self->output->append("Subcommands available:", Callback->null);
    self->output->append(" - help      Shows this help", Callback->null);
    self->output->append(" - set       Set flight parameters", Callback->null);
    self->output->append(" - takeoff   Launch plane", Callback->null);
    self->output->append(" - heading   Keep constant heading", Callback->null);
    self->output->append(" - hold      Keep plane level, free roll", Callback->null);
    self->output->append(" - ditch     Try landing immediately", Callback->null);
    self->output->append(" - land      Land at KSC runway", Callback->null);
    self->status = 1;
    self->running = false;
    self->fs->loop->defer(self->resultCallback, self->pid);
  }

  func onSignal(signal) {
    if not self->running {
      return false;
    }
    if signal = "End" {
      self->stopDaemon();
      return true;
    }
  }

  func run() {
    if self->chosenCommand = "takeoff" and ship:status = "LANDED" {
      self->status = 0;
      self->exited = true;
      self->running = false;
      self->fs->loop->deregister(self);
      self->fs->loop->defer(self->resultCallback, self->pid);
    }
  }

}
export = Plane;

#undef UNSAFE_ACCESSOR