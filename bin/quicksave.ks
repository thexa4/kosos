local Echo to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Echo").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:pid to  id.
    //if kuniverse:canquicksave {
      kuniverse:quicksave().
      set self:status to  0.
local __var76 to fs:loop. __var76:defer(__var76,resultCallback, id).
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call().
set export to  Echo.
