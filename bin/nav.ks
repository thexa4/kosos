local _Callback to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_Callback").
set  result["new"] to { parameter type,target, method.
local self to prototype:copy.
set  self["type"] to  result.
    set self:call to  { parameter self, args.
      return target[method]:call(target, args).
    }.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call().
set _Callback:null to  lex("call", { parameter s,_. }).
global Callback to _Callback.
local PriorityQueue to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","PriorityQueue").
set  result["new"] to { parameter type.
local self to prototype:copy.
set  self["type"] to  result.
    set self:items to  list().
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["find"] to { parameter self,_q.
    local start to  0.
    local end to  self:items:length - 1.
    if end = -1
      return 0.
    
    local mid to  floor((start + end) / 2).

    local midQ to  self:items[mid][0].
    local last to  end.
    until start >= end or midQ = _q {
      if midQ < _q {
        set end to  max(0, mid - 1).
      } else {
        set start to  min(last, mid + 1).
      }
      set mid to  floor((start + end) / 2).
      set midQ to  self:items[mid][0].
    }
    //print("s: " + start + ", e: " + end + ", m: " + mid + ", q: " + floor(q) + ", mq: " + floor(midQ)).
    if _q > midQ
      return mid + 1.
    return mid.}.
set  prototype["push"] to { parameter self,_q, item.
    local i to  self:find(self,_q).
    self:items:insert(i, list(_q, item)).}.
set  prototype["peek"] to { parameter self.
    if self:items:length = 0
      return false.
    return self:items[self:items:length - 1][1].}.
set  prototype["pop"] to { parameter self.
    if self:items:length = 0
      return false.
    local result to  self:items[self:items:length - 1][1].
    self:items:remove(self:items:length - 1).
    return result.}.
set  prototype["empty"] to { parameter self.
    self:items:clear().}.
return  result.}):call().
local Nav to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Nav").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:pid to  id.
    set self:output to  output.
    set self:status to  -1.
    set self:fs to  fs.
    set self:callback to  resultCallback.
    
    set self:resolution to  2.
    set self:maxGrade to  0.3.
    set self:heightPenalty to  0.02.
    if env:haskey("COST_HEIGHT") {
      set self:heightPenalty to  env:COST_HEIGHT:toscalar(self:heightPenalty).
    }
    set self:normalPenalty to  10.
    set self:gradeDiffPenalty to  10.
    set self:checkpoints to  Stack().
    set self:lowestPath to  lex().
    set self:grades to  lex().
    set self:debug to  0.
    set self:approachesLeft to  8.
    set self:suppressExit to  false.
    set self:lastPercentage to  0.
    if env:haskey("DEBUG") {
      if env:debug = "1"
        set self:debug to  1.
      if env:debug = "2"
        set self:debug to  2.
    }
    set self:priority to  5.
    if env:haskey("priority") {
      set self:priority to  env:priority:tonumber(self:priority).
    }
local __var250 to fs:loop. __var250:register(__var250,self).

    set self:targetGeo to  false.
    set self:startGeo to  ship:geoposition.
    local partsLeft to  args:sublist(1, args:length - 1).
    until partsLeft:length = 0 {
      if partsLeft[0] = "from" {
        local parsed to  false.
        if partsLeft:length > 1 {
          set parsed to  self:parseGeo(self,partsLeft[1]).
        }
        if parsed:istype("Boolean") {
          output:append(output,"#Bad from arg", Callback:null).
local __var251 to fs:loop. __var251:defer(__var251,Callback:new(Callback,self, "help"), 0).
          return self.
        }
        set self:startGeo to  parsed.
        set partsLeft to  partsLeft:sublist(2, partsLeft:length - 2).
      } else if partsLeft[0] = "to" {
        local parsed to  false.
        if partsLeft:length > 1 {
          set parsed to  self:parseGeo(self,partsLeft[1]).
        }
        if parsed:istype("Boolean") {
          output:append(output,"#Bad to arg", Callback:null).
local __var252 to fs:loop. __var252:defer(__var252,Callback:new(Callback,self, "help"), 0).
          return self.
        }
        set self:targetGeo to  parsed.
        set partsLeft to  partsLeft:sublist(2, partsLeft:length - 2).
      } else if partsLeft[0] = "res" {
        local parsed to  -1.
        if partsLeft:length > 1 {
          set parsed to  partsLeft[1]:tonumber(-1).
        }
        if parsed <= 0 {
          output:append(output,"#Bad res arg", Callback:null).
local __var253 to fs:loop. __var253:defer(__var253,Callback:new(Callback,self, "help"), 0).
          return self.
        }
        set self:resolution to  parsed.
        set partsLeft to  partsLeft:sublist(2, partsLeft:length - 2).
      } else {
        output:append(output,"#Unrecognised arg " + partsLeft[0], Callback:null).
local __var254 to fs:loop. __var254:defer(__var254,Callback:new(Callback,self, "help"), 0).
        return self.
      }
    }

    output:append(output,"G;" + round(self:startGeo:lat, 3) + ";" + round(self:startGeo:lng, 3), Callback:null).
    set self:startCoord to  self:geoToCoord(self,self:startGeo).
    if self:targetGeo:istype("Boolean") {
      local _waypoint to  Nav:findWaypoint(Nav).
      if _waypoint:istype("Boolean") {
        output:append(output,"#No target waypoint selected.", Callback:null).
local __var255 to fs:loop. __var255:defer(__var255,Callback:new(Callback,self, "help"), 0).
        return self.
      }
      set self:targetGeo to  _waypoint:geoposition.
    }
    set self:targetCoord to  self:geoToCoord(self,self:targetGeo).

    set self:pathQueue to  PriorityQueue:new(PriorityQueue).
    local estimate to  self:estimateDist(self,self:startCoord, self:targetCoord).
local __var256 to self:pathQueue. __var256:push(__var256,estimate, lex(
      "dist", 0,
      "pos", self:startCoord,
      "est", estimate,
      "id", self:coordToStr(self,self:startCoord),
      "prevUp", up:vector
    )).
    set self:maxdist to  estimate.

    output:append(output,"#from: " + self:coordToStr(self,self:startCoord), Callback:null).
    output:append(output,"#to: " + self:coordToStr(self,self:targetCoord), Callback:null).
    output:append(output,"#res: " + self:resolution, Callback:null).
    set self:stage to  "precompute".

    clearVecDraws().
    set self:destVec to  false.
    if self:debug > 0 {
      set self:destVec to  self:drawArrow(self,self:startCoord, self:targetCoord, RGB(1, 0, 0)).
      set self:progressArrows to  list().
      set self:progressPtr to  0.
      local x to  0.
      until x >= 30 {
        self:progressArrows:add(vecdraw(ship:position, up:vector * 1000, rgb(0, 1, 0), "", 1, true, 0.2, false)).
        set x to  x + 1.
      }
    }
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["help"] to { parameter self,_.
local __var257 to self:output. __var257:append(__var257,"#usage: nav [from <geocoord>] [to <geocoord>] [res <resolution>]", Callback:null).
    clearVecDraws().
    set self:stage to  "none".
local __var258 to self:fs:loop. __var258:deregister(__var258,self).
    set self:status to  1.
local __var259 to self:callback. __var259:call(__var259,self:pid).}.
set  prototype["parseGeo"] to { parameter self,str.
    local parts to  str:trim:split(";").
    if parts:length <> 3 {
      return false.
    } else {
      local lat to  parts[1]:tonumber(0).
      local lng to  parts[2]:tonumber(0).
      return latlng(lat, lng).
    }}.
set  prototype["onSignal"] to { parameter self,type.
    if type = "End" {
local __var260 to self:output. __var260:append(__var260,"#Interrupted", Callback:null).
      clearVecDraws().
      set self:stage to  "none".
local __var261 to self:fs:loop. __var261:deregister(__var261,self).
      set self:status to  9.
local __var262 to self:callback. __var262:call(__var262,self:pid).
      return true.
    }
    return false.}.
set  result["findWaypoint"] to { parameter self.
    for point in allwaypoints() {
      if point:isSelected and point:body = ship:body {
        return point.
      }
    }
    return false.}.
set  prototype["geoGrade"] to { parameter self,geo, id, p0.
    if self:grades:haskey(id)
      return self:grades[id].
    
    local terrainUp to  (geo:altitudePosition(1) - geo:altitudePosition(0)):normalized.
    local flat1 to  vcrs(terrainUp, geo:position:normalized).
    local flat2 to  vcrs(terrainUp, flat1).
    local g1 to  body:geopositionof(geo:position + flat1).
    local g2 to  body:geopositionof(geo:position + flat2).
    local p1 to  g1:altitudePosition(g1:terrainheight).
    local p2 to  g2:altitudePosition(g2:terrainheight).
    local normal to  vcrs((p1 - p0):normalized, (p2 - p0):normalized).
    local grade to  terrainUp * normal.
    return list(1 - grade, normal).}.
set  prototype["geoToCoord"] to { parameter self,geo.
    return list(round(geo:lat * self:resolution), round(geo:lng * self:resolution)).}.
set  prototype["coordToGeo"] to { parameter self,coord.
    return latlng(coord[0] / self:resolution, coord[1] / self:resolution).}.
set  prototype["coordToStr"] to { parameter self,coord.
    return coord[0] + ":" + coord[1].}.
set  prototype["coordToPos"] to { parameter self,coord.
    declare parameter offset to 0.
    local geo to  self:coordToGeo(self,coord).
    return geo:altitudeposition(geo:terrainheight + offset).}.
set  prototype["drawArrow"] to { parameter self,c1, c2, color.
    local p1 to  self:coordToPos(self,c1, 1).
    local p2 to  self:coordToPos(self,c2, 1).
    return vecdraw(p1, p2-p1, color, "", 1.0, true, 0.1, false).}.
set  prototype["estimateDist"] to { parameter self,c1, c2.
    local dist to  self:coordToPos(self,c1) - self:coordToPos(self,c2).
    return dist:mag * 1.05.}.
set  prototype["coordCost"] to { parameter self,c1, c2, maxGrade.
    local g1 to  self:coordToGeo(self,c1).
    local g2 to  self:coordToGeo(self,c2).
    local p1 to  g1:altitudePosition(g1:terrainheight).
    local p2 to  g2:altitudePosition(g2:terrainheight).
    local s1 to  g1:position.
    local s2 to  g2:position.
    local cost to  (p1 - p2):mag + 0.01.
    local seaDist to  (s1 - s2):mag + 0.01.
    local heightDiff to  abs(g1:terrainheight - g2:terrainheight).
    local grade to  heightDiff / seaDist.
    set cost to  cost + heightDiff * heightDiff * self:heightPenalty.
    if grade > maxGrade
      set cost to  cost + 99999999.
    return cost.}.
set  prototype["run"] to { parameter self.
    self[self:stage](self).}.
set  prototype["precompute"] to { parameter self.
    from {local x to  0.} until x = self:priority or self:stage <> "precompute" step {set x to  x + 1.} do self:pathing(self).}.
set  prototype["pathing"] to { parameter self.
local __var263 to self:pathQueue.
    local next to  __var263:pop(__var263).
    if next:istype("Boolean") {
local __var264 to self:output. __var264:append(__var264,"#No path found", Callback:null).
      set self:stage to  "wait".
      return.
    }
    local coord to  next:pos.
    local progress to  next:dist / (next:est + 0.01).
    set progress to  progress * progress.
    set progress to  floor(progress * 100 / 5) * 5.
    if self:lastPercentage < progress {
      print("#Pathing: " + progress + "%").
      set self:lastPercentage to  progress.
    }
    if self:debug > 0 {
      set self:progressArrows[self:progressPtr]:start to  self:coordToPos(self,coord).
      set self:progressPtr to  self:progressPtr + 1.
      if self:progressPtr >= self:progressArrows:length {
        set self:progressPtr to  0.
      }
    }

    if coord[0] = self:targetCoord[0] and coord[1] = self:targetCoord[1] {
local __var265 to self:output. __var265:append(__var265,"#Planning finished", Callback:null).
local __var266 to self:pathQueue. __var266:empty(__var266).
      self:lowestPath:clear().
      local dist to  0.
      local prevPos to  self:targetGeo:position.

      self:checkpoints:push(self:targetGeo).
      local unwind to  next.
      until not unwind:haskey("prev") {
        self:checkpoints:push(self:coordToGeo(self,unwind:pos)).
        local nextPos to  self:coordToPos(self,unwind:pos).
        set dist to  dist + (prevPos - nextPos):mag.
        set prevPos to  nextPos.
        
        set unwind to  unwind:prev.
      }
local __var267 to self:output. __var267:append(__var267,"#Distance: " + ceiling(dist / 1000) + "km", Callback:null).

      until self:checkpoints:length = 0 {
        local next to  self:checkpoints:pop().
local __var268 to self:output. __var268:append(__var268,"G;" + round(next:lat, 3) + ";" + round(next:lng, 3), Callback:null).
      }
local __var269 to self:output. __var269:append(__var269,"G;" + round(self:targetGeo:lat, 3) + ";" + round(self:targetGeo:lng, 3), Callback:null).
      set self:stage to  "wait".
      clearVecDraws().
      return.
    }
    if next:haskey("arrow") {
      set next:arrow:show to  false.
      next:remove("arrow").
    }

    local normalGrade to  self:geoGrade(self,self:coordToGeo(self,coord), next:id, self:coordToPos(self,next:pos)).
    if normalGrade[0] > 0.3 {
      return.
    }

    local gradeDiffCost to  (1 - (next:prevUp * normalGrade[1])) * self:gradeDiffPenalty.

    local coordId to  self:coordToStr(self,coord).
    if not self:lowestPath:haskey(coordId) {
      self:lowestPath:add(coordId, next:dist).
    } else {
      if self:lowestPath[coordId] <= next:dist {
        return. // already visited with lower cost
      } else {
        self:lowestPath:remove(coordId).
        self:lowestPath:add(coordId, next:dist).
      }
    }
    
    local neighbours to  list().
    neighbours:add(list(coord[0] - 1, coord[1] - 1)).
    neighbours:add(list(coord[0], coord[1] - 1)).
    neighbours:add(list(coord[0] + 1, coord[1] - 1)).
    neighbours:add(list(coord[0] - 1, coord[1])).
    neighbours:add(list(coord[0] + 1, coord[1])).
    neighbours:add(list(coord[0] - 1, coord[1] + 1)).
    neighbours:add(list(coord[0], coord[1] + 1)).
    neighbours:add(list(coord[0] + 1, coord[1] + 1)).

    for den in neighbours {
      local n to  self:geoToCoord(self,self:coordToGeo(self,den)).
      local id to  self:coordToStr(self,n).
      local dist to  self:coordCost(self,coord, n, self:maxGrade).
      if dist < 99999999 {
        local cost to  next:dist + dist + normalGrade[0] * self:normalPenalty + gradeDiffCost.
        local rest to  self:estimateDist(self,n, self:targetCoord).
        local priority to  cost + rest.
        local nextItem to  lex(
          "dist", cost,
          "pos", n,
          "prev", next,
          "est", priority,
          "id", id,
          "prevUp", normalGrade[1]
        ).
        if self:debug > 2
          set nextItem:arrow to  self:drawArrow(self,coord, n, RGB(priority / self:maxdist / 2, 0, 1)).
local __var270 to self:pathQueue. __var270:push(__var270,priority, nextItem).
      } else if n[0] = self:targetCoord[0] and n[1] = self:targetCoord[1] {
        set self:approachesLeft to  self:approachesLeft - 1.
        print "Rejected approach, " + self:approachesLeft + " left.".
        if self:approachesLeft = 0 {
local __var271 to self:output. __var271:append(__var271,"#No path found", Callback:null).
          set self:stage to  "wait".
          return.
        }
      }
    }}.
set  prototype["wait"] to { parameter self.
    clearVecDraws().
    set self:stage to  "none".
local __var272 to self:fs:loop. __var272:deregister(__var272,self).
    set self:status to  0.
local __var273 to self:callback. __var273:call(__var273,self:pid).}.
set  prototype["none"] to { parameter self.
    if self:suppressExit {
      print("deregister failed").
    }
    set self:suppressExit to  true.}.
return  result.}):call().
set export to  Nav.