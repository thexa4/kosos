local PlaneLaunchProg to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","PlaneLaunchProg").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).
    
    if args:length < 3 or args[1] <> "target" {
      self:print(self,"Usage:").
      self:print(self,"  planelaunch target <alt> [warp]").
      self:print(self,"").
      self:print(self,"Interval from optimal launch time can be set using LAUNCH_INTERVAL. Defaults to 45.").
      self:exit(self,1).
      return self.
    }

    if not hastarget {
      self:print(self,"no target selected").
      self:exit(self,1).
      return self.
    }
    set self:target_obt to  target:obt.
    set self:target_alt to  args[2]:toscalar(-1).
    set self:subproc to  false.

    local lat to  ship:latitude.
    if abs(lat) > abs(self:target_obt:inclination) or self:target_alt < 0 {
      self:print(self,"Target orbit is not reachable from this position").
      self:exit(self,2).
      return self.
    }
    
    local launch_interval to  45.
    if env:haskey("LAUNCH_INTERVAL") {
      set launch_interval to  env:LAUNCH_INTERVAL:toscalar(launch_interval).
    }
    local eta_seconds to  round(self:launch_eta(self) - launch_interval).
    set self:azimuth to  self:launch_azimuth(self).

    local eta_date to  ("" + floor(eta_seconds/3600)):padleft(2):replace(" ", "0") +
      ":" + ("" + floor(mod(eta_seconds, 3600) / 60)):padleft(2):replace(" ", "0") +
      ":" + ("" + floor(mod(eta_seconds, 60) / 60)):padleft(2):replace(" ", "0").
    
    self:print(self,"In " + eta_date + ", launch at " + round(self:azimuth) + " degrees").
local __var16 to fs:loop. __var16:sleep(__var16,self, eta_seconds).

    if args:length >= 4 and args[3] = "warp" {
      self:print(self,"Warping until one minute before launch").
      kuniverse:timewarp:warpto(time:seconds + eta_seconds - 60).
    }
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["launch_eta"] to { parameter self.
    local relative_longitude to  arcsin(tan(ship:geoposition:lat) / tan(self:target_obt:inclination)).

    local g_lan to  mod(self:target_obt:lan + relative_longitude - ship:body:rotationangle + 360, 360).
    local node_angle to  mod(g_lan - ship:geoposition:lng + 360, 360).
    return (node_angle / 360) * ship:body:rotationperiod.}.
set  prototype["launch_azimuth"] to { parameter self.
    local targetSMA to  self:target_obt:semimajoraxis.
    local bodyInertia to  max(-1, min(cos(self:target_obt:inclination)/cos(ship:latitude), 1)).
    local orbitalVelocity to  sqrt(ship:body:mu / targetSMA).
    local equatorialVelocity to  2 * Constant:PI * ship:body:radius / ship:body:rotationperiod.

    local azimuth to  mod(arctan2(orbitalVelocity * bodyInertia - equatorialVelocity * cos(ship:latitude), orbitalVelocity * sqrt(1 - bodyInertia^2)) + 360, 360).
    return azimuth - 90.}.
set  prototype["run"] to { parameter self.
    set self:running to  true.
    if self:interrupted {
      return.
    }
    self:stop(self).

    local command to  "launch " + self:target_alt + " " + self:azimuth.
    self:print(self,"Executing: " + command).

    set self:subproc to  ShellExec:new(ShellExec,
      command,
      self:fs,
      self:env,
      self:input,
      Callback:new(Callback,self, "onLaunchDone")
    ).}.
set  prototype["onSignal"] to { parameter self,signal.
    if not self:subproc:istype("Boolean") {
      if not self:subproc:proc:istype("Boolean") {
local __var17 to self:subproc:proc.
return  __var17:onSignal(__var17,signal).
      }
    }

    if signal = "End" {
      self:exit(self,9).
      return true.
    }
    return false.}.
set  prototype["onLaunchDone"] to { parameter self,proc.
    self:exit(self,proc:status).}.
return  result.}):call().

set export to  PlaneLaunchProg.
