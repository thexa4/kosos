local MissionCtl to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","MissionCtl").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).
    
    set self:services to  lex().
    set self:servicePidLookup to  lex().
    set self:serviceTodoNames to  lex().
    set self:curProg to  false.

    local command to  "list".
    if args:length > 1 {
      set command to  args[1].
    }
    if command = "list" {
      self:open(self,"/var/spool/mission", Callback:new(Callback,self, "onOpenListSpool")).
    } else if command = "start" {
      self:start_server(self).
    } else if command = "queue" {
      self:create_job(self,"run" + char(10) + args:sublist(2, args:length - 2):join(" ")).
    } else if command = "service" {
      if args:length < 3 {
        self:help(self).
        self:exit(self,1).
        return self.
      }
      local subcommand to  args[2].
      if subcommand = "start" {
        self:create_job(self,"service start" + char(10) + args:sublist(3, args:length - 3):join(" ")).
      } else if subcommand = "stop" {
        self:create_job(self,"service stop" + char(10) + args:sublist(3, args:length - 3):join(" ")).
      } else {
        self:help(self).
        self:exit(self,1).
      }
    } else if command = "reset" {
      self:open(self,"/var/spool/mission", Callback:new(Callback,self, "onOpenResetSpool")).
    } else {
      self:help(self).
      if command = "help" {
        self:exit(self,0).
      } else {
        self:exit(self,1).
      }
      return self.
    }
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["help"] to { parameter self.
    self:print(self,"Usage:").
    self:print(self,"  missionctl [list] - Shows remaining mission items").
    self:print(self,"  missionctl help - Show this help").
    self:print(self,"  missionctl start - Starts misison executor").
    self:print(self,"  missionctl queue <command> - Queues a command").
    self:print(self,"  missionctl service start <command> - Queues a service to be started").
    self:print(self,"  missionctl service stop <command> - Queues a service to be stopped").
    self:print(self,"  missionctl reset - Clears mission queue").
    self:print(self,"").
    self:print(self,"Maintains a queue of things to do. Assumes commands can be restarted at will until they exit.").
    self:print(self,"Services have 5 seconds to initialize.").}.
set  result["gen_filename"] to { parameter self.
    local timepart to  ("" + round(time:seconds * 10)):padleft(11):replace(" ", "0").
    local randpart to  "" + floor(random() * 99).
    return timepart + "-" + randpart.}.
set  prototype["create_job"] to { parameter self,contents.
    set self:jobdef to  contents.
local __var0 to self:type.
local __var1 to self:fs. __var1:create(__var1,"/var/spool/mission/" + __var0:gen_filename(__var0), Callback:new(Callback,self, "onCreateTaskfile")).}.
set  prototype["onCreateTaskfile"] to { parameter self,file.
    if file:istype("Boolean") {
      self:print(self,"Unable to create task, does /var/spool/mission exist?").
      self:exit(self,2).
      return.
    }

    file:write(file,self:jobdef, Callback:new(Callback,self, "ensureWriteAndExit")).}.
set  prototype["ensureWriteAndExit"] to { parameter self,result.
    if not result {
      self:print(self,"Write failed").
      self:exit(self,3).
    } else {
      self:exit(self,0).
    }}.
set  prototype["onOpenListSpool"] to { parameter self,dir.
    if dir:istype("Boolean") or dir:getType(dir) <> "dir" {
      self:print(self,"# no tasks queued, system not configured").
      self:exit(self,0).
      return.
    }

    dir:read(dir,Callback:new(Callback,self, "onReadListSpool")).}.
set  prototype["onReadListSpool"] to { parameter self,items.
    if items:istype("Boolean") {
      self:print(self,"# Unable to read spool directory").
      self:exit(self,4).
      return.
    }

    if items:length = 0 {
      self:print(self,"# no tasks queued").
      self:exit(self,0).
      return.
    }

    set self:listTodo to  queue().
    for item in items {
      self:listTodo:push(item).
    }
    self:openNextListSpool(self).}.
set  prototype["openNextListSpool"] to { parameter self.
    if self:listTodo:length = 0 {
      self:exit(self,0).
      return.
    }
    self:open(self,"/var/spool/mission/" + self:listTodo:pop(), Callback:new(Callback,self, "onFileOpenListSpool")).}.
set  prototype["onFileOpenListSpool"] to { parameter self,file.
    if file:istype("Boolean") or file:getType(file) <> "file" {
      self:print(self,"# broken item 1").
      self:openNextListSpool(self).
      return.
    }
    set self:listCurFile to  file.
    file:read(file,Callback:new(Callback,self, "onFileReadTypeListSpool")).}.
set  prototype["onFileReadTypeListSpool"] to { parameter self,line.
    if line:istype("Boolean") {
      self:print(self,"# broken item 2").
      self:openNextListSpool(self).
      return.
    }
    set self:listCurType to  line.
local __var2 to self:listCurFile. __var2:read(__var2,Callback:new(Callback,self, "onFileReadCommandListSpool")).}.
set  prototype["onFileReadCommandListSpool"] to { parameter self,line.
    if line:istype("Boolean") {
      self:print(self,"# broken item 3").
      self:openNextListSpool(self).
      return.
    }

    if self:listCurType = "run" {
      self:print(self,line).
    } else if self:listCurType = "service start" {
      self:print(self,line + " # Service start").
    } else if self:listCurType = "service stop" {
      self:print(self,"# Stop service: " + line).
    } else {
      self:print(self,"# invalid command").
    }
    self:openNextListSpool(self).}.
set  prototype["onOpenResetSpool"] to { parameter self,dir.
    if dir:istype("Boolean") or dir:getType(dir) <> "dir" {
      self:exit(self,0).
      return.
    }

    set self:resetDirectory to  dir.
    dir:read(dir,Callback:new(Callback,self, "onReadResetSpoolDir")).}.
set  prototype["onReadResetSpoolDir"] to { parameter self,items.
    if items:istype("Boolean") or items:length = 0 {
      self:exit(self,0).
      return.
    }

    set self:resetTodoLeft to  items:length.
    set self:resetSucces to  true.
    for item in items {
local __var3 to self:resetDirectory. __var3:unlink(__var3,item, Callback:new(Callback,self, "onUnlinkReset")).
    }}.
set  prototype["onUnlinkReset"] to { parameter self,result.
    set self:resetSucces to  self:resetSucces and result.
    set self:resetTodoLeft to  self:resetTodoLeft - 1.

    if self:resetTodoLeft <= 0 {
      if self:resetSucces {
        self:exit(self,0).
      } else {
        self:print(self,"# Unlink failed").
        self:exit(self,1).
      }
    }}.
set  prototype["start_server"] to { parameter self.
    self:open(self,"/var/spool/mission", Callback:new(Callback,self, "onServerStartDirOpen")).}.
set  prototype["onServerStartDirOpen"] to { parameter self,dir.
    if dir:istype("Boolean") or dir:getType(dir) <> "dir" {
      self:print(self,"System not configured for missions. Please create /var/spool/mission folder.").
      self:exit(self,2).
      return.
    }
    set self:confdir to  dir.
    set self:executedTodos to  list().
    set self:currentTodoName to  false.

    dir:read(dir,Callback:new(Callback,self, "onServerReadConfDir")).}.
set  prototype["onServerReadConfDir"] to { parameter self,items.
    if items:istype("Boolean") {
      self:print(self,"Unable to get tasks").
      self:exit(self,2).
      return.
    }

    if items:length = 0 {
      set self:sleepContinuation to  "readConfDir".
local __var4 to self:fs:loop. __var4:sleep(__var4,self, 10).
      return.
    }

    for item in items {
      if not self:executedTodos:contains(item) {
        set self:currentTodoName to  item.
        self:open(self,"/var/spool/mission/" + item, Callback:new(Callback,self, "onServerOpenTodo")).
        return.
      }
    }}.
set  prototype["onServerOpenTodo"] to { parameter self,file.
    if file:istype("Boolean") or file:getType(file) <> "file" {
      self:print(self,"Bad todo " + self:currentTodoName).
      self:executedTodos:add(self:currentTodoName).
local __var5 to self:confdir. __var5:read(__var5,Callback:new(Callback,self, "onServerReadConfDir")).
      return.
    }

    set self:currentTodoFile to  file.
    file:read(file,Callback:new(Callback,self, "onServerReadTodoType")).}.
set  prototype["onServerReadTodoType"] to { parameter self,typeString.
    if typeString:istype("Boolean") {
      self:print(self,"Todo is truncated: " + self:currentTodoName).
      self:executedTodos:add(self:currentTodoName).
local __var6 to self:confdir. __var6:read(__var6,Callback:new(Callback,self, "onServerReadConfDir")).
      return.
    }

    if typeString = "run" {
      set self:serverRunType to  typeString.
    } else if typeString = "service start" {
      set self:serverRunType to  typeString.
    } else if typeString = "service stop" {
      set self:serverRunType to  typeString.
    } else {
      self:print(self,"Todo " + self:currentTodoName + " has unknown action type: " + typeString).
      self:executedTodos:add(self:currentTodoName).
local __var7 to self:confdir. __var7:read(__var7,Callback:new(Callback,self, "onServerReadConfDir")).
      return.
    }
local __var8 to self:currentTodoFile. __var8:read(__var8,Callback:new(Callback,self, "onServerReadTodoCommand")).}.
set  prototype["onServerReadTodoCommand"] to { parameter self,commandString.
    if commandString:istype("Boolean") {
      self:print(self,"Todo is truncated: " + self:currentTodoName).
      self:executedTodos:add(self:currentTodoName).
local __var9 to self:confdir. __var9:read(__var9,Callback:new(Callback,self, "onServerReadConfDir")).
      return.
    }

    local truncatedCommand to  commandString:trim.
    if self:serverRunType = "service stop" {
      if self:services:haskey(truncatedCommand) {
local __var10 to self:services[truncatedCommand]. __var10:onSignal(__var10,"End").

        local todoItem to  self:serviceTodoNames[truncatedCommand].
local __var11 to self:confdir. __var11:unlink(__var11,todoItem, Callback:new(Callback,self, "onServerServiceRemoveStartfile")).
        return.
      }

      self:print(self,"Unable to find service to stop: " + truncatedCommand).
      self:print(self,"" + self:services:keys).
      self:executedTodos:add(self:currentTodoName).
local __var12 to self:confdir. __var12:read(__var12,Callback:new(Callback,self, "onServerReadConfDir")).
      return.
    }

    if self:serverRunType = "service start" {
      if self:services:haskey(truncatedCommand) {
        self:print(self,"Unable to start service, already running: " + truncatedCommand).
        self:executedTodos:add(self:currentTodoName).
local __var13 to self:confdir. __var13:read(__var13,Callback:new(Callback,self, "onServerReadConfDir")).
      } else {
        set self:currentCommand to  truncatedCommand.
local __var14 to self:fs. __var14:exec(__var14,truncatedCommand, self:env, self:input, self:output, Callback:new(Callback,self, "onServerServiceExit"), Callback:new(Callback,self, "onServerServiceStart")).
        return.
      }
    }

    if self:serverRunType = "run" {
      set self:currentCommand to  truncatedCommand.
local __var15 to self:fs. __var15:exec(__var15,truncatedCommand, self:env, self:input, self:output, Callback:new(Callback,self, "onServerProgExit"), Callback:new(Callback,self, "onServerProgStart")).
      return.
    }

    // Unknown type?
    self:exit(self,255).}.
set  prototype["onServerServiceRemoveStartfile"] to { parameter self,result.
    if not result {
      self:print(self,"Unable to remove service start file: " + self:currentCommand).
      set self:sleepContinuation to  "readConfDir".
local __var16 to self:fs:loop. __var16:sleep(__var16,self, 10).
      return.
    }
local __var17 to self:confdir. __var17:unlink(__var17,self:currentTodoName, Callback:new(Callback,self, "onServerServiceRemoveStopfile")).}.
set  prototype["onServerServiceRemoveStopfile"] to { parameter self,result.
    self:executedTodos:add(self:currentTodoName).
local __var18 to self:confdir. __var18:read(__var18,Callback:new(Callback,self, "onServerReadConfDir")).}.
set  prototype["onServerServiceStart"] to { parameter self,prog.
    if prog:istype("Boolean") {
      self:print(self,"Service start failed: " + self:currentCommand).
      set self:sleepContinuation to  "readConfDir".
local __var19 to self:fs:loop. __var19:sleep(__var19,self, 10).
      return.
    }
    self:print(self,"Service start: " + self:currentCommand).
    set prog:___MissionCtlServiceTag to  self:currentCommand.
    self:services:add(self:currentCommand, prog).
    self:servicePidLookup:add(prog:pid, prog).
    self:serviceTodoNames:add(self:currentCommand, self:currentTodoName).
    self:executedTodos:add(self:currentTodoName).
    set self:sleepContinuation to  "readConfDir".
local __var20 to self:fs:loop. __var20:sleep(__var20,self, 5).}.
set  prototype["onServerServiceExit"] to { parameter self,pid.
    if not self:servicePidLookup:haskey(pid) {
      self:print(self,"Unknown service exit").
      return.
    }
    local prog to  self:servicePidLookup[pid].
    self:servicePidLookup:remove(pid).
    if not prog:haskey("___MissionCtlServiceTag") {
      self:print(self,"Service is missing service tag").
      return.
    }
    self:print(self,"Service stop: " + prog:___MissionCtlServiceTag).
    if self:services:haskey(prog:___MissionCtlServiceTag) {
      self:services:remove(prog:___MissionCtlServiceTag).
    }}.
set  prototype["onServerProgStart"] to { parameter self,prog.
    if prog:istype("Boolean") {
      self:print(self,"Prog start failed: " + self:currentCommand).
      set self:sleepContinuation to  "readConfDir".
local __var21 to self:fs:loop. __var21:sleep(__var21,self, 10).
      return.
    }

    self:print(self,"Run: " + self:currentCommand).
    set self:curProg to  prog.}.
set  prototype["onServerProgExit"] to { parameter self,prog.
    self:executedTodos:add(self:currentTodoName).
    self:print(self,"Done: " + self:currentCommand).
    set self:curProg to  false.
    if not self:interrupted {
local __var22 to self:confdir. __var22:unlink(__var22,self:currentTodoName, Callback:new(Callback,self, "onServerUnlinkTodo")).
    }}.
set  prototype["onServerUnlinkTodo"] to { parameter self,result.
    if self:interrupted {
      return.
    }
local __var23 to self:confdir. __var23:read(__var23,Callback:new(Callback,self, "onServerReadConfDir")).}.
set  prototype["onSignal"] to { parameter self,signal.
    if signal = "End" {
      self:exit(self,9).
      if not self:curProg:istype("Boolean") {
local __var24 to self:curProg. __var24:onSignal(__var24,signal).
      }
      for service in self:services:values {
        service:onSignal(service,signal).
      }
      return true.
    }
    return false.}.
set  prototype["run"] to { parameter self.
    if self:sleepContinuation = "readConfDir" {
      set self:sleepContinuation to  "".
local __var25 to self:confdir. __var25:read(__var25,Callback:new(Callback,self, "onServerReadConfDir")).
      return.
    }
local __var26 to self:fs:loop. __var26:deregister(__var26,self).}.
return  result.}):call().
set export to  MissionCtl.
