local Circularize to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Circularize").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).

    if not hasnode {
      self:print(self,"Circularize needs a manouver node, please schedule one with `schedule`.").
      self:print(self,"Usage:").
      self:print(self,"circularize").
      self:exit(self,1).
      return self.
    }

    local activeNode to  allnodes[allnodes:length - 1].
    local oldNode to  Node(activeNode:time, activeNode:radialout, activeNode:normal, activeNode:prograde). 
    
    // zero active node to avoid funny stuff
    set activeNode:prograde to  0.
    set activeNode:normal to  0.
    set activeNode:radialout to  0.
    
    local vat to  velocityat(ship, activeNode:time):orbit.
    if activeNode:orbit:body <> ship:body {
      set vat to  vat - velocityAt(activeNode:orbit:body, activeNode:time):orbit.
    }
    local pat to  positionat(ship, activeNode:time).

    local distanceVec to  pat - activeNode:orbit:body:position.
    local upVec to  distanceVec:normalized.

    local progradeBasis to  vat:normalized.
    local normalBasis to  vcrs(progradeBasis, upVec):normalized.
    local radialBasis to  vcrs(normalBasis, progradeBasis):normalized.

    local nodedv to  progradeBasis * oldNode:prograde + 
                 normalBasis * oldNode:normal +
                 radialBasis * oldNode:radialout.
    
    local startVel to  vat + nodedv.
    local progradeVec to  startVel:normalized.
    local horizonVec to  vxcl(upVec, progradeVec):normalized.
    local desiredSpeed to  sqrt(activeNode:orbit:body:MU / distanceVec:mag).
    local desiredVelocity to  horizonVec * desiredSpeed.
    local desiredDelta to  desiredVelocity - vat.

    set activeNode:prograde to  desiredDelta * progradeBasis.
    set activeNode:normal to  desiredDelta * normalBasis.
    set activeNode:radialout to  desiredDelta * radialBasis.
    
    self:exit(self,0).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["run"] to { parameter self.}.
return  result.}):call().
set export to  Circularize.