local Cat to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Cat").
for k in  Program:prototype:keys
if not k:startsWith("__")
set  prototype[k] to  Program:prototype[k].
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    self:progSetup(self,id, input, output, args, env, fs, resultCallback).

    if args:length > 2 {
      self:print(self,"Usage: cat [filename]").
      self:exit(self,1).
      return self.
    }
    self:readLine(self).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onRead"] to { parameter self,data.
    if data:istype("Boolean") {
      self:exit(self,0).
      return.
    }
    self:print(self,data).
    self:readLine(self).}.
return  result.}):call().
set export to  Cat.
