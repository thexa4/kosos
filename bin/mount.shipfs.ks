local DelegateFile to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","DelegateFile").
set  result["new"] to { parameter type,fs, readDelegate, writeDelegate.
local self to prototype:copy.
set  self["type"] to  result.
    set self:fs to  fs.
    set self:readDelegate to  readDelegate.
    set self:writeDelegate to  writeDelegate.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getType"] to { parameter self. return "file".}.
set  prototype["getReadHandle"] to { parameter self,loop. return StaticReadFileHandle:new(StaticReadFileHandle,self, loop).}.
set  prototype["_readAll"] to { parameter self,callback.
local __var3 to self:fs:loop. __var3:defer(__var3,callback, self:readDelegate()).}.
set  prototype["write"] to { parameter self,data, callback.
    if not data:istype("Boolean")
      self:writeDelegate(data).
local __var4 to self:fs:loop. __var4:defer(__var4,callback, true).}.
set  prototype["append"] to { parameter self,data, callback.
    self:write(self,data, callback).}.
return  result.}):call().
local ShipFsMount to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","ShipFsMount").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set self:pid to  id.
    set self:status to  0.
    if args:length <> 2 {
      set self:status to  1.
      output:append(output,"Usage: mount.shipfs <dest>", Callback:null).
    } else {
      local bind to  MemoryFilesystemDir:new(MemoryFilesystemDir).
      bind:registerFile(bind,"name", DelegateFile:new(DelegateFile,fs, { return "" + ship:name. }, { parameter data. if data <> "" { set ship:name to  "" + data. } })).
      bind:registerFile(bind,"geo", DelegateFile:new(DelegateFile,fs, { 
          return "G;" + round(ship:geoposition:lat, 4) + ";" + round(ship:geoposition:lng, 4).
        }, { parameter data. })
      ).
      bind:registerFile(bind,"alt", DelegateFile:new(DelegateFile,fs, { return "" + round(ship:altitude). }, { parameter data. })).
      bind:registerFile(bind,"body", DelegateFile:new(DelegateFile,fs, { return "" + ship:body:name. }, { parameter data. })).
      bind:registerFile(bind,"status", DelegateFile:new(DelegateFile,fs, { return "" + ship:status. }, { parameter data. })).
      bind:registerFile(bind,"target", DelegateFile:new(DelegateFile,fs, { return self:getTarget(self). }, { parameter data. self:setTarget(self,data). })).

      fs:mount(fs,bind, args[1]).
    }
local __var5 to fs:loop. __var5:defer(__var5,resultCallback, id).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getTarget"] to { parameter self.
    if target:istype("Vessel") {
      return "S;" + target:name.
    }
    if target:istype("Part") {
      return "P;" + target:ship:name + ";" + target:cid.
    }
    if target:istype("Body") {
      return "B;" + target:name.
    }
    return "".}.
set  prototype["setTarget"] to { parameter self,tgt.
    if KUniverse:activevessel <> ship {
      return.
    }
    local parts to  tgt:split(";").
    if parts:length < 2 {
      return.
    }
    if parts[0] = "S" {
      set target to  Vessel(parts[1]).
      return.
    }
    if parts[0] = "B" {
      set target to  Body(parts[1]).
      return.
    }
    if parts[0] = "P" {
      if parts:length < 3 {
        return.
      }
      local tship to  Vessel(parts[1]).
      for part in tship:parts {
        if part:cid = parts[2] {
          set target to  part.
          return.
        }
      }
    }}.
return  result.}):call().
set export to  ShipFsMount.
