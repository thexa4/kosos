if not exists("1:/boot/kosos.ksm")
  copypath("0:/kosos/kosos.ksm", "1:/boot/kosos.ksm").
if not exists("1:/bin") {
  createdir("1:/bin").
}
if not exists("1:/mnt") {
  createdir("1:/mnt").
}
declare parameter apps to list(
  "sh",
  "mount.volume",
  "mount.bind",
  "exec",
  "land",
  "launch",
  "circularize",
  "autostager",
  "planechange",
  "schedule",
  "kash",
  "ls",
  "cp",
  "cat",
  "reboot",
  "scienced",
  "env"
).
set todo to list().
set bind to "mount.bind".
set path to "/bin".
set hdc to unchar("a").
for app in apps {
  if not exists("1:/bin/" + app + ".ksm") {
    set file to open("0:/kosos/bin/" + app + ".ksm").
    if file:istype("Boolean")
      print app + " not found.".
	else {
		set size to file:size.
		if core:volume:freespace - size <= 250 or not copypath("0:/kosos/bin/" + app + ".ksm", "1:/bin/" + app + ".ksm") {
		  todo:add(app).
		}
	}
  }
}
if not exists("1:/init.rc") {
  local cpus to ship:modulesnamed("kOSProcessor").
  for cpu in cpus {
    if cpu:part <> core:part {
      if cpu:bootfilename = "None" {
        if cpu:tag = "" {
          set cpu:tag to "sd" + char(hdc) + "1".
          set cpu:volume:name to cpu:tag.
          set hdc to hdc + 1.
        }
        local tag to cpu:volume:name.
        log "mount.volume " + tag + " /mnt/" + tag to "1:/init.rc".
        set path to path + ":/mnt/" + tag.
        set apps to todo:copy.
        todo:clear().
        for app in apps {
          if not exists(tag + ":/" + app + ".ksm") {
            if not copypath("0:/kosos/bin/" + app + ".ksm", tag + ":/" + app + ".ksm") {
              todo:add(app).
            } else if app = "mount.bind" {
              set bind to "/mnt/" + tag + "/" + app.
            } else {
              print("Copy " + app + " to " + tag).
            }
          }
        }
      }
    } else {
      if cpu:tag = "" {
        set cpu:tag to "probe".
      }
    }
  }
  log bind + " /archive/kosos /usr" to "1:/init.rc".
  log "chime" to "1:/init.rc".
  log "PATH=" + path + ":/usr/bin kash" to "1:/init.rc".
  log "PATH=" + path + ":/usr/bin sh" to "1:/init.rc".
}
set core:bootfilename to "/boot/kosos.ksm".
deletepath("1:/boot/probe.ks").
print "not copied:".
print todo.
reboot.