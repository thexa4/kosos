parameter command_override to false.
local _Callback to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_Callback").
set  result["new"] to { parameter type,target, method.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:call to  { parameter self, args.
      return target[method]:call(target, args).
    }.
return  self.}.
set  result["__new"] to  result["new"].
return  result.}):call().
set  _Callback:null to  lex("call", { parameter s,_. }).
global Callback to _Callback.
local EventLoop to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","EventLoop").
set  result["new"] to { parameter type.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:stopped to  true.
    set  self:framework to  false.
    set  self:nextTimer to  false.
    set  self:private to  ({
      local services to  list().
      local callbacks to  list().
      local result to  lex("s", services, "c", callbacks, "t", list()).
      return {
        return result.
      }.
    }):call().
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["register"] to { parameter self,service.
    self:private()["s"]:add(service).}.
set  prototype["deregister"] to { parameter self,service.
    self:defer(self,Callback:new(Callback,self, "_deregister"), service).}.
set  prototype["_deregister"] to { parameter self,service.
    local services to  self:private()["s"].
    local c to  0.
    for s in services {
      if s = service {
        services:remove(c).
        return.
      }
      set  c to  c + 1.
    }}.
set  prototype["sleep"] to { parameter self,service, duration.
    if duration <= 0 {
      return.
    }
    if duration > 10000 {
      set  duration to  10000.
    }
    self:deregister(self,service).
    self:private()["t"]:add(list(service, time:seconds + duration)).
    self:_recomputeTimers(self).}.
set  prototype["_recomputeTimers"] to { parameter self.
    set  self:nextTimer to  false.
    for timer in self:private()["t"] {
      if self:nextTimer:istype("Boolean") {
        set  self:nextTimer to  timer[1].
      }
      if timer[1] < self:nextTimer {
        set  self:nextTimer to  timer[1].
      }
    }}.
set  prototype["defer"] to { parameter self,callback, result.
    self:private()["c"]:add(list(callback, result)).}.
set  prototype["start"] to { parameter self.
    set  self:stopped to  false.
    until self:stopped {
      if not self:framework:istype("Boolean") {
        local me to  self.
        self:framework({me:runLoop(me).}).
        set  self:framework to  false.
      }
      local start to  time:seconds.
      self:runLoop(self).
      wait 0.1 - time:seconds + start.
    }
    print("Bye").}.
set  prototype["runLoop"] to { parameter self.
    local callbacks to  self:private()["c"].
    local services to  self:private()["s"]:copy.
    
    for service in services
      service:run(service).
    
    local clist to  callbacks:copy.
    callbacks:clear().
    until clist:length = 0 {
      for c in clist {
        c[0]:call(c[0],c[1]).
      }
      set  clist to  callbacks:copy.
      callbacks:clear().
    }

    local nextTimer to  self:nextTimer.
    if not nextTimer:istype("Boolean") {
      if time:seconds >= nextTimer {
        local removeList to  list().
        local timers to  self:private()["t"].
        local c to  0.
        for timer in timers {
          set  timer to  timers[c].
          if time:seconds >= timer[1] {
            removeList:insert(0, c).
            self:register(self,timer[0]).
          }
          set  c to  c + 1.
        }

        for index in removeList {
          timers:remove(index).
        }

        self:_recomputeTimers(self).
      }
    }}.
set  prototype["stop"] to { parameter self,_.
    set  self:stopped to  true.}.
return  result.}):call().
global STDLIB_INCLUDED to 1.
local _Util to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_Util").
set  result["new"] to { parameter type.
local self to prototype:copy.
set  self["type"] to  result.
return  self.}.
set  result["__new"] to  result["new"].
set  result["splitArgs"] to { parameter self,cmd.
    local quote to  char(34).
    local stringParts to  cmd:split(quote).

    local parsedParts to  list().
    local isString to  false.
    local toAdd to  "".
    for part in stringParts {
      if isString {
        parsedParts:add(toAdd + part).
        set  toAdd to  "".
      } else {
        local words to  part:split(" ").
        for word in words {
          if toAdd <> ""
            parsedParts:add(toAdd).
          set  toAdd to  word.
        }
      }
      set  isString to  not isString.
    }
    if toAdd <> ""
      parsedParts:add(toAdd).
    return parsedParts.}.
set  result["absPath"] to { parameter self,pathname, env.
    local cwd to  "".

    if not pathname:startsWith("/") {
      if env:haskey("CWD")
        set  cwd to  env["CWD"].
      
      if cwd:endsWith("/")
        set  cwd to  cwd:substring(1, cwd:length - 1).
    }

    for part in pathname:split("/") {
      if part:length > 0 {
        if part = ".." {
          local back to  cwd:split("/").
          if back:length > 0 {
            set  cwd to  back:sublist(0, back:length - 1):join("/").
          }
        } else {
          set  cwd to  cwd + "/" + part.
        }
      }
    }
    return cwd.}.
set  result["sort"] to { parameter self,arr.
    set  arr to  arr:copy.
    local i to  1.
    until i >= arr:length {
      local j to  i.
      until j <= 0 or arr[j - 1] <= arr[j] {
        local cur to  arr[j].
        set  arr[j] to  arr[j-1].
        set  arr[j-1] to  cur.

        set  j to  j - 1.
      }
      set  i to  i + 1.
    }
    return arr.}.
return  result.}):call().
global Util to _Util.
local _Pipe to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_Pipe").
set  result["new"] to { parameter type.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:buffer to  list().
    set  self:waiting to  false.
    set  self:recvCallback to  false.
    set  self:sendCallback to  false.
    set  self:closeHandler to  false.
    set  self:signalHandler to  false.
    set  self:closed to  false.
    set  self:identifier to  0.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getType"] to { parameter self. return "file".}.
set  prototype["read"] to { parameter self,callback.
    if self:buffer:length > 0 {
      local _r to  self:buffer[0].
      self:buffer:remove(0).
      callback:call(callback,_r[0]).
local __var0 to _r[1]. __var0:call(__var0,true).
      return.
    }
    set  self:recvCallback to  callback.
    set  self:waiting to  true.
    if not self:sendCallback:istype("Boolean") {
local __var1 to self:sendCallback. __var1:call(__var1,self:identifier).
    }}.
set  prototype["write"] to { parameter self,data, callback.
    self:append(self,data, callback).
    self:close(self,0).}.
set  prototype["append"] to { parameter self,data, callback.
    if self:closed {
      callback:call(callback,false).
      return.
    }
    if self:waiting {
      set  self:waiting to  false.
local __var2 to self:recvCallback. __var2:call(__var2,data).
      callback:call(callback,true).
      return.
    }
    self:buffer:add(list(data, callback)).}.
set  prototype["close"] to { parameter self,_.
    if self:closed
      return false.
    set  self:closed to  true.
    if not self:closeHandler:istype("Boolean") {
local __var3 to self:closeHandler.
return  __var3:call(__var3,_).
    }
    return false.}.
set  prototype["signal"] to { parameter self,type.
    local handled to  false.
    if not self:signalHandler:istype("Boolean") {
local __var4 to self:signalHandler.
      set  handled to  __var4:call(__var4,type).
    }
    return handled.}.
return  result.}):call().
global Pipe to _Pipe.
local _LineReader to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_LineReader").
set  result["new"] to { parameter type,input, fs.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:fs to  fs.
    set  self:isReading to  false.
    set  self:lineBuffer to  Queue().
    set  self:isClosed to  false.
    if input:istype("lexicon")
      set  input:closeHandler to  Callback:new(Callback,self, "onClose").
    else
      set  self:isClosed to  true.
    set  self:private to  ({
      local lineCallback to  list(false).
      return {
        return lex("i", input, "l", lineCallback).
      }.
    }):call().
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["readLine"] to { parameter self,c.
    local p to  self:private().
    if self:isReading {
      print("LineReader: double readLine, ignoring").
      return.
    }
    p:l:clear().
    p:l:add(c).
    if self:lineBuffer:length > 0 {
local __var5 to self:fs:loop. __var5:defer(__var5,c, self:lineBuffer:pop()).
      return.
    }
    if self:isClosed {
local __var6 to self:fs:loop. __var6:defer(__var6,p:l[0], false).
      return.
    }
    set  self:isReading to  true.
local __var7 to self:fs:loop. __var7:defer(__var7,Callback:new(Callback,p:i, "read"), Callback:new(Callback,self, "onRead")).}.
set  prototype["onRead"] to { parameter self,data.
    local p to  self:private().
    set  self:isReading to  false.
    if data:istype("Boolean") {
      set  self:isClosed to  true.
local __var8 to self:fs:loop. __var8:defer(__var8,p:l[0], false).
      return.
    }
    local lines to  data:split(char(10)).
    for line in lines {
      self:lineBuffer:push(line).
    }
local __var9 to self:fs:loop. __var9:defer(__var9,p:l[0], self:lineBuffer:pop()).}.
set  prototype["onClose"] to { parameter self,_.
    set  self:isClosed to  true.}.
return  result.}):call().
global LineReader to _LineReader.
local _NullFile to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_NullFile").
set  result["new"] to { parameter type.
local self to prototype:copy.
set  self["type"] to  result.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getType"] to { parameter self. return "file".}.
set  prototype["read"] to { parameter self,callback. callback:call(callback,"").}.
set  prototype["write"] to { parameter self,data, callback. callback:call(callback,true).}.
set  prototype["append"] to { parameter self,data, callback. callback:call(callback,true).}.
return  result.}):call().
global NullFile to _NullFile.
local _ShellExec to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_ShellExec").
set  result["new"] to { parameter type,cmd, fs, env, input, doneCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:cmd to  cmd.
    set  self:fs to  fs.
    set  self:env to  env.
    set  self:input to  input.
    if input:istype("Boolean") {
      set  self:input to  NullFile:new(NullFile).
    }
    set  self:doneCallback to  doneCallback.
    set  self:pipe to  Pipe:new(Pipe).
local __var10 to self:pipe. __var10:read(__var10,Callback:new(Callback,self, "onWrite")).
    set  self:output to  false.
    set  self:prog to  false.
    set  self:status to  -1.

    set  self:is_terminated to  false.

    fs:exec(fs,cmd, env, input, self:pipe, Callback:new(Callback,self, "onExit"), Callback:new(Callback,self, "onStart")).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onWrite"] to { parameter self,data.
    if self:output:istype("Boolean") {
      set  self:output to  data.
    } else {
      set  self:output to  self:output + char(10) + data.
    }
local __var11 to self:pipe. __var11:read(__var11,Callback:new(Callback,self, "onWrite")).}.
set  prototype["onExit"] to { parameter self,_.
    if not self:prog:istype("Boolean")
      set  self:status to  self:prog:status.
local __var12 to self:doneCallback. __var12:call(__var12,self).}.
set  prototype["onStart"] to { parameter self,prog.
    set  self:prog to  prog.
    if prog:istype("Boolean") {
local __var13 to self:doneCallback. __var13:call(__var13,self).
    }
    if self:is_terminated {
      prog:onSignal(prog,"End").
    }}.
set  prototype["terminate"] to { parameter self.
    set  self:is_terminated to  true.
    if not prog:istype("Boolean") {
      prog:onSignal(prog,"End").
    }}.
return  result.}):call().
global ShellExec to _ShellExec.
local MemoryFilesystemFile to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","MemoryFilesystemFile").
set  result["new"] to { parameter type.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:data to  "".
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getType"] to { parameter self. return "file".}.
set  prototype["getReadHandle"] to { parameter self,loop. return StaticReadFileHandle:new(StaticReadFileHandle,self, loop).}.
set  prototype["_readAll"] to { parameter self,callback. callback:call(callback,self:data).}.
set  prototype["write"] to { parameter self,data, callback.
    set  self:data to  "" + data.
    callback:call(callback,true).}.
set  prototype["append"] to { parameter self,data, callback.
    set  self:data to  self:data + char(10) + data.
    callback:call(callback,true).}.
return  result.}):call().
local _MemoryFilesystemDir to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_MemoryFilesystemDir").
set  result["new"] to { parameter type.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:entries to  lex().
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getType"] to { parameter self. return "dir".}.
set  prototype["read"] to { parameter self,callback. callback:call(callback,Util:sort(Util,self:entries:keys)).}.
set  prototype["open"] to { parameter self,pathname, callback.
    if self:entries:haskey(pathname) {
      local result to  self:entries[pathname].
      callback:call(callback,result).
    } else {
      callback:call(callback,false).
    }}.
set  prototype["create"] to { parameter self,pathname, callback.
    local result to  MemoryFilesystemFile:new(MemoryFilesystemFile).
    if self:entries:haskey(pathname) {
      self:entries:remove(pathname).
    }
    self:entries:add(pathname, result).
    callback:call(callback,result).}.
set  prototype["registerFile"] to { parameter self,pathname, file.
    self:entries:add(pathname, file).}.
set  prototype["unlink"] to { parameter self,pathname, callback.
    if not self:entries:haskey(pathname) {
      callback:call(callback,false).
      return.
    }
    self:entries:remove(pathname).
    callback:call(callback,true).}.
set  prototype["mkdir"] to { parameter self,pathname, callback.
    if self:entries:haskey(pathname) {
      callback:call(callback,self:entries[pathname]).
      return.
    }
    local result to  MemoryFilesystemDir:new(MemoryFilesystemDir).
    self:entries:add(pathname, result).
    callback:call(callback,result).}.
set  prototype["df"] to { parameter self.
    return lex("used", 0, "available", 64 * 1024 * 1024 * 1024).}.
return  result.}):call().
global MemoryFilesystemDir to _MemoryFilesystemDir.
local VolumeFilesystemFile to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","VolumeFilesystemFile").
set  result["new"] to { parameter type,kosvolume, file, pathname, root.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:file to  file.
    set  self:program to  false.
    set  self:path to  pathname.
    set  self:root to  root.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getType"] to { parameter self. return "file".}.
set  prototype["getReadHandle"] to { parameter self,loop. return StaticReadFileHandle:new(StaticReadFileHandle,self, loop).}.
set  prototype["_readAll"] to { parameter self,callback.
local __var14 to self:root.
    if __var14:retry(__var14,self, "_readAll", list(callback), callback)
      return.
local __var15 to self:root:fs:loop. __var15:defer(__var15,callback, self:file:readall:string).}.
set  prototype["write"] to { parameter self,data, callback.
local __var16 to self:root.
    if __var16:retry(__var16,self, "write", list(data, callback), callback)
      return.
    self:file:clear().
    self:file:write(data).
local __var17 to self:root:fs:loop. __var17:defer(__var17,callback, true).}.
set  prototype["append"] to { parameter self,data, callback.
local __var18 to self:root.
    if __var18:retry(__var18,self, "append", list(data, callback), callback)
      return.
    self:file:write(data + char(10)).
local __var19 to self:root:fs:loop. __var19:defer(__var19,callback, true).}.
set  prototype["isExecutable"] to { parameter self.
    if self:file:extension = "ks" or self:file:extension = "ksm" {
      return true.
    }
    return false.}.
set  prototype["exec"] to { parameter self.
    if not self:isExecutable(self) {
      return false.
    }

    if self:program:istype("Boolean") {
      global export to false.
      runpath(self:path).
      set  self:program to  export.
      unset export.
    }
    return self:program.}.
return  result.}):call().
local _VolumeFilesystemDir to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_VolumeFilesystemDir").
set  result["new"] to { parameter type,volumeId, dir, pathname, connection, root, fs.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:volumeId to  volumeId.
    set  self:volume to  false.
    set  self:dirPath to  dir.
    set  self:dir to  false.
    set  self:cache to  lexicon().
    set  self:path to  pathname.
    set  self:connection to  connection.
    set  self:root to  root.
    if root:istype("Boolean") {
      set  self:root to  self.
    }
    set  self:fs to  fs.
    set  self:retryList to  list().
    set  self:failed to  false.
    set  self:retrying to  false.
    set  self:retryAt to  0.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["retry"] to { parameter self,victim, method, args, callback.
    if self:connection:isconnected
    {
      set  self:failed to  false.
      return false.
    }

    if self:failed {
local __var20 to self:fs:loop. __var20:defer(__var20,callback, false).
      return true.
    }

    self:retryList:add(lex("victim", victim, "method", method, "args", args, "callback", callback)).
    if self:retrying
      return true.
    
    set  self:retryAt to  time:seconds.
    set  self:retrying to  true.
local __var21 to self:fs:loop. __var21:register(__var21,self).
    
    return true.}.
set  prototype["run"] to { parameter self.
    if self:connection:isconnected {
      set  self:retrying to  false.
local __var22 to self:fs:loop. __var22:deregister(__var22,self).

      local todo to  self:retryList:copy.
      self:retryList:clear().
      for rerun in todo {
        if rerun:args:length = 1 {
          rerun:victim[rerun:method]:call(rerun:victim, rerun:args[0]).
        } else {
          rerun:victim[rerun:method]:call(rerun:victim, rerun:args[0], rerun:args[1]).
        }
      }
    } else {
      if time:seconds - self:retryAt > 2 {
        set  self:retrying to  false.
local __var23 to self:fs:loop. __var23:deregister(__var23,self).
        set  self:failed to  true.
        local todo to  self:retryList:copy.
        self:retryList:clear().
        for rerun in todo {
local __var24 to self:fs:loop. __var24:defer(__var24,rerun:callback, false).
        }
      }
    }}.
set  prototype["setup"] to { parameter self.
    if not self:connection:isconnected
      return false.
    if self:volume:istype("Boolean")
      set  self:volume to  volume(self:volumeId).
    
    if self:dir:istype("Boolean")
      set  self:dir to  self:volume:open(self:dirPath).
    if self:dir:istype("Boolean") {
      print("Error: dir " + self:dirPath + " on volume " + self:volume:name + " does not exist.").
      return false.
    }
    return true.}.
set  prototype["getType"] to { parameter self. return "dir".}.
set  prototype["read"] to { parameter self,callback.
local __var25 to self:root.
    if __var25:retry(__var25,self, "read", list(callback), callback)
      return.
    if not self:setup(self) {
      callback:call(callback,false).
      return.
    }
    callback:call(callback,Util:sort(Util,self:dir:list():keys)).}.
set  prototype["open"] to { parameter self,pathname, callback.
local __var26 to self:root.
    if __var26:retry(__var26,self, "open", list(pathname, callback), callback)
      return.
    if not self:setup(self) {
      callback:call(callback,false).
      return.
    }
    for item in self:dir:list():values {
      if item:name = pathname {
        local key to  item:name.
        if item:isfile {
          set  key to  key + "@" + item:size.
        } else {
          set  key to  key + "@dir".
        }
        if self:cache:haskey(key) {
          callback:call(callback,self:cache[key]).
          return.
        }
        local result to  false.
        local newPath to  self:path + "/" + pathname.
        local newDir to  self:dirPath + "/" + pathname.
        if item:isfile {
          set  result to  VolumeFilesystemFile:new(VolumeFilesystemFile,self:volumeId, item, newPath, self:root).
        } else {
          set  result to  VolumeFilesystemDir:new(VolumeFilesystemDir,self:volumeId, newDir, newPath, self:connection, self:root, self:fs).
        }
        self:cache:add(key, result).
        callback:call(callback,result).
        return.
      }
    }
    callback:call(callback,false).
    return.}.
set  prototype["create"] to { parameter self,pathname, callback.
local __var27 to self:root.
    if __var27:retry(__var27,self, "create", list(pathname, callback), callback)
      return.
    if not self:setup(self) {
      callback:call(callback,false).
      return.
    }
    local newPath to  self:dirPath + "/" + pathname.
    local item to  self:volume:create(newPath).
    local result to  VolumeFilesystemFile:new(VolumeFilesystemFile,self:volumeId, item, self:path + "/" + pathname, self:root).
    if self:cache:haskey(pathname) {
      self:cache:remove(pathname).
    }
    self:cache:add(pathname, result).
    callback:call(callback,result).}.
set  prototype["df"] to { parameter self.
    if self:volume:capacity < 0 {
      return lex("used", 0, "available", 64 * 1024 * 1024 * 1024).
    }
    return lex("used", self:volume:capacity - self:volume:freespace, "available", self:volume:capacity).}.
set  prototype["unlink"] to { parameter self,pathname, callback.
local __var28 to self:root.
    if __var28:retry(__var28,self, "unlink", list(pathname, callback), callback)
      return.
    if not self:setup(self) {
      callback:call(callback,false).
      return.
    }
    local newPath to  self:dirPath + "/" + pathname.
    local result to  self:volume:exists(newPath).
    self:volume:delete(newPath).
    callback:call(callback,result).}.
set  prototype["mkdir"] to { parameter self,pathname, callback.
local __var29 to self:root.
    if __var29:retry(__var29,self, "mkdir", list(pathname, callback), callback)
      return.
    if not self:setup(self) {
      callback:call(callback,false).
      return.
    }
    local newPath to  self:dirPath + "/" + pathname.

    local dir to  false.
    if self:volume:exists(newPath) {
      set  dir to  self:volume:open(newPath).
      if dir:isfile {
        callback:call(callback,false).
        return.
      }
    } else {
      set  dir to  self:volume:createdir(newPath).
    }

    local newDir to  self:dirPath + "/" + pathname.
    local result to  VolumeFilesystemDir:new(VolumeFilesystemDir,self:volumeId, newDir, newPath, self:connection, self:root, self:fs).
    self:cache:add(pathname, result).
    callback:call(callback,result).}.
return  result.}):call().
global VolumeFilesystemDir to _VolumeFilesystemDir.
local _Program to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_Program").
set  result["new"] to { parameter type.
local self to prototype:copy.
set  self["type"] to  result.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["progSetup"] to { parameter self,id, in, out, args, env, fs, c.
    set  self:pid to  id.
    set  self:input to  in.
    set  self:output to  out.

    set  self:fs to  fs.
    set  self:env to  env.
    set  self:status to  -1.
    set  self:interrupted to  false.
    set  self:running to  false.
    set  self:_ to  lex(
      "res", c,
      "reader", LineReader:new(LineReader,in, fs),
      "wc", Callback:new(Callback,self, "onWrite"),
      "rc", Callback:new(Callback,self, "onReadDone"),
      "eof", false,
      "r", false
    ).

    if not in:istype("lexicon") {
      fail_input_is_not_object().
    }
    set  in:signalHandler to  Callback:new(Callback,self, "onSignal").}.
set  prototype["start"] to { parameter self.
    if self:running or self:interrupted
      return.
    set  self:running to  true.
local __var30 to self:fs:loop. __var30:register(__var30,self).}.
set  prototype["deconstruct"] to { parameter self,_.}.
set  prototype["stop"] to { parameter self.
    if not self:running
      return.
    set  self:running to  false.
local __var31 to self:fs:loop. __var31:deregister(__var31,self).}.
set  prototype["onReadDone"] to { parameter self,data.
    if self:interrupted
      return.
    set  self:_:r to  false.
    if data:istype("Boolean") {
      set  self:_:eof to  true.
    }
    self:onRead(self,data).}.
set  prototype["readLine"] to { parameter self.
    if self:_:eof {
      self:exit(self,8).
      return.
    }
    if self:_:r
      return.
    set  self:_:r to  true.
local __var32 to self:_:reader. __var32:readLine(__var32,self:_:rc).}.
set  prototype["exit"] to { parameter self,statusCode.
    if self:interrupted
      return.
    set  self:interrupted to  true.
    self:stop(self).
    set  self:status to  statusCode.
local __var33 to self:fs:loop. __var33:defer(__var33,self:_:res, self:pid).
local __var34 to self:fs:loop. __var34:defer(__var34,Callback:new(Callback,self, "deconstruct"), 0).}.
set  prototype["onSignal"] to { parameter self,signal.
    if signal = "End" {
      self:exit(self,9).
      return true.
    }
    return false.}.
set  prototype["print"] to { parameter self,str.
local __var35 to self:output. __var35:append(__var35,str, self:_:wc).}.
set  prototype["onWriteDone"] to { parameter self,res.
    if not res {
      self:exit(self,7).
      return.
    }
    self:onWrite(self,res).}.
set  prototype["onWrite"] to { parameter self,_.}.
set  prototype["run"] to { parameter self.
    print "Please add run() function.".
    self:exit(self,1).}.
set  prototype["onRead"] to { parameter self,_.
    print "Please add onReadLine() function.".
    self:exit(self,1).}.
set  prototype["cwd"] to { parameter self,newPath.
    if not self:env:haskey("CWD") {
      self:env:add("CWD", "/").
    }
    local newPath to  Util:absPath(Util,newPath, self:env).
    if self:env:haskey("CWD") {
      self:env:remove("CWD").
    }
    self:env:add("CWD", newPath).}.
set  prototype["open"] to { parameter self,pathname, callback.
local __var36 to self:fs. __var36:open(__var36,Util:absPath(Util,pathname, self:env), callback).}.
set  prototype["mkdir"] to { parameter self,pathname, callback.
local __var37 to self:fs. __var37:mkdir(__var37,Util:absPath(Util,pathname, self:env), callback).}.
return  result.}):call().
global Program to _Program.
local Init to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","Init").
set  result["new"] to { parameter type,id, input, output, args, env, fs, resultCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:pid to  id.
    set  self:fs to  fs.
    set  self:env to  env.
    set  self:callback to  resultCallback.
    set  self:status to  0.
    set  self:input to  input.
    set  self:output to  output.
    local data to  core:volume:open("init.rc").
    if data:istype("Boolean") {
      print("No init.rc found on local disk. Falling back to shell.").
      set  data to  "sh".
    } else {
      set  data to  data:readall:string.
    }
    set  self:queue to  queue().
    set  self:lastCommand to  false.
    set  self:childProc to  false.

    local lines to  queue().
    for line in data:split(char(10))
      lines:push(line).
    
    until lines:length = 0 {
      local command to  "\".
      local lastCommand to  "\".
      until not lastCommand:endsWith("\") or lines:length = 0 {
        set  lastCommand to  lines:pop().
        set  command to  command:substring(0, command:length - 1) + char(10) + lastCommand.
      }
      set  command to  command:substring(1, command:length - 1).
      if command:trim:length > 0
        self:queue:push(command).
    }
local __var38 to self:fs:loop. __var38:defer(__var38,Callback:new(Callback,self, "runNext"), 0).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["runNext"] to { parameter self,_.
    if not self:childProc:istype("Boolean") {
      set  self:status to  self:childProc:status.
    }

    if self:status <> 0 {
      print("Error while running init.rc. Exit code " + self:status + " while running:").
      print(self:lastCommand).
    }

    if self:queue:length = 0 {
local __var39 to self:fs:loop. __var39:defer(__var39,self:callback, self:pid).
      return.
    }

    local command to  self:queue:pop().
    if command:trim:startswith("export ") {
      local assignmentParts to  command:trim:split(" ")[1]:split("=").
      if assignmentParts:length < 2 {
        self:runNext(self,0).
        return.
      }

      local key to  assignmentParts[0].
      local value to  assignmentParts:sublist(1, assignmentParts:length - 1):join("=").
      if self:env:haskey(key) {
        self:env:remove(key).
      }
      self:env:add(key, value).
      self:runNext(self,0).
      return.
    }

    set  self:lastCommand to  command.
    set  self:childProc to  false.
    set  self:status to  0.
    local onExit to  Callback:new(Callback,self, "runNext").
    set  self:isBg to  command:trim:endswith("&").
    if self:isBg {
      set  command to  command:trim.
      set  command to  command:substring(0, command:length - 1).
      set  onExit to  Callback:null.
    }
local __var40 to self:fs. __var40:exec(__var40,
      command,
      self:env,
      self:input,
      self:output,
      onExit,
      Callback:new(Callback,self, "onExec")
    ).}.
set  prototype["onExec"] to { parameter self,program.
    if program:istype("Boolean") {
      print("Error while running init.rc. Command not found while executing:").
      print(self:lastCommand).
      self:runNext(self,0).
      return.
    }
    if self:isBg {
local __var41 to self:fs:loop. __var41:defer(__var41,Callback:new(Callback,self, "runNext"), 0).
    } else {
      set  self:childProc to  program.
    }}.
return  result.}):call().
local TerminalFile to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","TerminalFile").
set  result["new"] to { parameter type,loop.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:pid to  0.
    set  self:line to  "".
    set  self:buffer to  list().
    set  self:requests to  list().
    loop:register(loop,self).
    set  self:signalHandler to  false.
    set  self:loop to  loop.
    set  self:history to  list().
    set  self:curBuffer to  "".
    set  self:historyPos to  0.
    set  self:blinkFull to  true.
    set  self:cursorPos to  0.
    set  self:lastBlink to  time:seconds.
    set  self:prompt to  "".
    set  self:completionHandler to  false.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getType"] to { parameter self. return "file".}.
set  prototype["read"] to { parameter self,callback.
    self:requests:add(callback).}.
set  prototype["write"] to { parameter self,data, callback. print(data).
local __var42 to self:loop. __var42:defer(__var42,callback, true).}.
set  prototype["append"] to { parameter self,data, callback. self:write(self,data, callback).}.
set  prototype["setHistory"] to { parameter self,h.
    set  self:history to  h:copy.
    set  self:historyPos to  h:length.}.
set  prototype["run"] to { parameter self.
    until self:requests:length = 0 or self:buffer:length = 0 {
local __var43 to self:loop. __var43:defer(__var43,self:requests[0], self:buffer[0]).
      self:requests:remove(0).
      self:buffer:remove(0).
    }
    local tWidth to  terminal:width - 1.
    until not terminal:input:haschar {
      local ccode to  terminal:input:getchar.
      set  self:lastBlink to  time:seconds.
      set  self:blinkFull to  true.
      local id to  unchar(ccode).
      if id = 57351 {
        // Up
        if self:historyPos > 0 {
          if self:historyPos = self:history:length {
            set  self:curBuffer to  self:line.
          }
          set  self:historyPos to  self:historyPos - 1.
          set  self:line to  self:history[self:historyPos].
          set  self:cursorPos to  self:line:length.
        }
      }
      else if id = 57352 {
        // Down
        if self:historyPos < self:history:length {
          set  self:historyPos to  self:historyPos + 1.
          if self:historyPos = self:history:length {
            set  self:line to  self:curBuffer.
          } else {
            set  self:line to  self:history[self:historyPos].
          }
          set  self:cursorPos to  self:line:length.
        }
      }
      else if id = 57353 {
        // Left
        if self:cursorPos > 0
          set self:cursorPos to self:cursorPos - 1.
      }
      else if id = 57354 {
        // Right
        if self:cursorPos < self:line:length
          set self:cursorPos to self:cursorPos + 1.
      }
      else if id = 57356 or unchar(ccode) = 57355 {
        // End / Home
        local handled to  false.
        if not self:signalHandler:isType("Boolean") {
          local signal to  "End".
          if unchar(ccode) = 57355
            set  signal to  "Home".
local __var44 to self:signalHandler.
          set  handled to  __var44:call(__var44,signal).
        }
        if not handled:isType("Boolean") or handled = false {
          print("Warning: unhandled signal").
        }
      }
      else if id = 8 {
        // Backspace
        if (self:cursorPos > 0) {
          set  self:cursorPos to  self:cursorPos - 1.
          set  self:line to  self:line:substring(0,self:cursorPos) + self:line:substring(self:cursorPos + 1, self:line:length - self:cursorPos - 1).
        }
      } else if id = 9 {
        // Tab
        if not self:completionHandler:istype("Boolean") {
local __var45 to self:completionHandler. __var45:call(__var45,self).
        }
      }
      else if id = 13 {
        // Newline
        self:buffer:add(self:line).
        set  self:last to  self:line.
        set  self:line to  "".
        set  self:prompt to  "".
        set  self:curBuffer to  "".
        set  self:history to  list().
        set  self:cursorPos to  0.
        set  self:completionHandler to  false.
        if self:requests:length > 0 {
          print "":padright(terminal:width - 1) at (0, terminal:height - 1).
        }
      } else {
        set  self:line to  self:line:substring(0, self:cursorPos) + ccode + self:line:substring(self:cursorPos, self:line:length - self:cursorPos).
        set  self:cursorPos to  self:cursorPos + 1.
        set  self:historyPos to  self:history:length.
      }
    }

    if self:requests:length > 0 {
      local size to  ceiling((self:prompt:length + self:line:length + 2) / tWidth).
      local prompted to  self:prompt + self:line.
      local padded to  prompted:padright(size * tWidth).
      if self:blinkFull {
        set  padded to  padded:substring(0, self:prompt:length + self:cursorPos) + char(9608) + padded:substring(self:prompt:length + self:cursorPos + 1, padded:length - self:cursorPos - self:prompt:length - 2).
      }
      local line to  floor((self:prompt:length + self:cursorPos) / tWidth).
      set  padded to  padded:substring(line * tWidth, tWidth - 1).
      if time:seconds - self:lastBlink > 0.8 {
        set  self:lastBlink to  time:seconds.
        set  self:blinkFull to  not self:blinkFull.
      }

      print padded at (0, terminal:height - 1).
    }}.
set  prototype["close"] to { parameter self,_.
    // Ignore
    return false.}.
return  result.}):call().
local _StaticReadFileHandle to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","_StaticReadFileHandle").
set  result["new"] to { parameter type,file, loop.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:loop to  loop.
    set  self:file to  file.
    set  self:readQueue to  queue().
    set  self:initialized to  false.
    set  self:lines to  false.
    file:_readAll(file,Callback:new(Callback,self, "onReadAll")).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["getType"] to { parameter self.
local __var46 to self:file.
return  __var46:getType(__var46).}.
set  prototype["onReadAll"] to { parameter self,data.
    set  self:lines to  queue().
    for line in data:split(char(10)) {
      self:lines:push(line).
    }
    set  self:initialized to  true.

    for callback in self:readQueue {
      self:read(self,callback).
    }
    set  self:readQueue to  queue().}.
set  prototype["read"] to { parameter self,callback.
    if not self:initialized {
      self:readQueue:push(callback).
      return.
    }
    if self:lines:empty {
local __var47 to self:loop. __var47:defer(__var47,callback, false).
    } else {
local __var48 to self:loop. __var48:defer(__var48,callback, self:lines:pop()).
    }}.
set  prototype["rewind"] to { parameter self.
    set  self:readQueue to  queue().
    set  self:initialized to  false.
    set  self:lines to  false.
local __var49 to self:file. __var49:_readAll(__var49,Callback:new(Callback,self, "onReadAll")).}.
return  result.}):call().
global StaticReadFileHandle to _StaticReadFileHandle.
local OpenState to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","OpenState").
set  result["new"] to { parameter type,root, parts, callback, method, loop.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:parts to  parts.
    set  self:callback to  callback.
    set  self:prev to  root.
    set  self:currentName to  false.
    set  self:method to  method.
    set  self:loop to  loop.
    self:onOpen(self,root).
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onOpen"] to { parameter self,dir.
    if self:parts:length = 0 and not dir:istype("Boolean") {
      if self:method = "r" and dir:getType(dir) = "file" {
local __var50 to self:callback. __var50:call(__var50,dir:getReadHandle(dir,self:loop)).
      } else {
local __var51 to self:callback. __var51:call(__var51,dir).
      }
      return.
    }

    if not dir:istype("Lexicon") {
      if self:method = "r" or self:parts:length > 0 {
local __var52 to self:callback. __var52:call(__var52,false).
        return.
      } else {
        // Try to create
        if not self:prev:haskey("create") {
local __var53 to self:callback. __var53:call(__var53,false).
          return.
        }
        if self:method = "w+" {
local __var54 to self:prev. __var54:create(__var54,self:currentName, Callback:new(Callback,self, "onOpen")).
        } else {
local __var55 to self:callback. __var55:call(__var55,false).
        }
        return.
      }
    }

    if dir:getType(dir) <> "dir" and self:parts:length > 1 {
local __var56 to self:callback. __var56:call(__var56,false).
      return.
    }

    if self:method = "d" and self:parts:length = 1 {
      dir:mkdir(dir,self:parts[0], self:callback).
      return.
    }

    local next to  self:parts[0].
    set  self:currentName to  next.
    set  self:prev to  dir.
    set  self:parts to  self:parts:sublist(1, self:parts:length - 1).
    dir:open(dir,next, Callback:new(Callback,self, "onOpen")).}.
return  result.}):call().
local ProgramCleaner to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","ProgramCleaner").
set  result["new"] to { parameter type,fs, pid, callback.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:fs to  fs.
    set  self:pid to  pid.
    set  self:callback to  callback.
    set  self:done to  false.
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onExit"] to { parameter self,arg.
    set  self:done to  true.
    if self:fs:progs:haskey(self:pid) {
      self:fs:progs:remove(self:pid).
      self:fs:cmds:remove(self:pid).
    }
local __var57 to self:callback. __var57:call(__var57,arg).}.
return  result.}):call().
local LoaderState to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","LoaderState").
set  result["new"] to { parameter type,args, input, output, inputFile, outputFile, env, searchPaths, fs, resultCallback, progCallback.
local self to prototype:copy.
set  self["type"] to  result.
    set  self:args to  args.
    set  self:input to  input.
    if not input:istype("lexicon") {
      fail_input_is_not_object().
    }
    set  self:inputFile to  inputFile.
    if not inputFile:istype("Boolean") {
      set  self:inputFile to  Util:absPath(Util,inputFile, env).
    }
    set  self:output to  output.
    set  self:outputFile to  outputFile.
    if not outputFile:istype("Boolean") {
      set  self:outputFile to  Util:absPath(Util,outputFile, env).
    }
    set  self:env to  env.
    set  self:fs to  fs.
    set  self:searchPaths to  searchPaths.
    set  self:resultCallback to  resultCallback.
    set  self:callback to  progCallback.
    set  self:locations to  queue().
    set  self:shellInput to  false.

    if args[0]:contains("/") {
      self:locations:push(args[0] + ".ksm").
      self:locations:push(args[0] + ".ks").
      self:locations:push(args[0]).
    } else {
      for path in searchPaths {
        local prefix to  path.
        if not prefix:endsWith("/")
          set  prefix to  prefix + "/".
        self:locations:push(prefix + args[0] + ".ksm").
        self:locations:push(prefix + args[0] + ".ks").
        self:locations:push(prefix + args[0]).
      }
    }

    if not self:inputFile:istype("Boolean") {
local __var58 to self:fs. __var58:open(__var58,self:inputFile, Callback:new(Callback,self, "onOpenInput")).
    } else if not self:outputFile:istype("Boolean") {
local __var59 to self:fs. __var59:create(__var59,self:outputFile, Callback:new(Callback,self, "onOpenOutput")).
    } else {
      self:onOpen(self,false).
    }
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["onOpenInput"] to { parameter self,newIn.
    if newIn:istype("Boolean") {
local __var60 to self:callback. __var60:call(__var60,false).
      return.
    }
    set  self:input to  newIn.
    if not self:outputFile:istype("Boolean") {
local __var61 to self:fs. __var61:create(__var61,self:outputFile, Callback:new(Callback,self, "onOpenOutput")).
    } else {
      self:onOpen(self,false).
    }}.
set  prototype["onOpenOutput"] to { parameter self,newOut.
    if newOut:istype("Boolean") {
local __var62 to self:callback. __var62:call(__var62,false).
      return.
    }
    set  self:output to  newOut.
    self:onOpen(self,false).}.
set  prototype["onOpen"] to { parameter self,prog.
    if not prog:istype("Boolean") and prog:getType(prog) = "file" {
local __var63 to prog:file.
      if prog:haskey("file") and prog:file:haskey("isExecutable") and __var63:isExecutable(__var63) {
local __var64 to prog:file.
        // Standard kOS binary
        local loader to  __var64:exec(__var64).
        if not loader:istype("Boolean") {
          set  self:fs:pid to  self:fs:pid + 1.
          local cleaner to  ProgramCleaner:new(ProgramCleaner,self:fs, self:fs:pid, self:resultCallback).
          local instance to  loader:new(loader,
            self:fs:pid,
            self:input,
            self:output,
            self:args,
            self:env,
            self:fs,
            Callback:new(Callback,cleaner, "onExit")
          ).
          if not cleaner:done {
            self:fs:progs:add(self:fs:pid, instance).
            self:fs:cmds:add(self:fs:pid, self:args).
          }
          if not self:callback:istype("Boolean") {
local __var65 to self:callback. __var65:call(__var65,instance).
          }
          return.
        }
      } else {
        // Shell command
        set  self:shellInput to  prog.
        prog:read(prog,Callback:new(Callback,self, "onReadHashBang")).
        return.
      }
    }

    if self:locations:length = 0 {
      if not self:callback:istype("Boolean") {
local __var66 to self:callback. __var66:call(__var66,false).
      }
      return.
    }
local __var67 to self:fs. __var67:open(__var67,Util:absPath(Util,self:locations:pop(), self:env), Callback:new(Callback,self, "onOpen")).}.
set  prototype["onReadHashBang"] to { parameter self,line.
    if line:istype("Boolean") or not line:startswith("#!") {
      if not self:callback:istype("Boolean") {
local __var68 to self:callback. __var68:call(__var68,false).
      }
      return.
    }

    local newargs to  Util:splitArgs(Util,line:substring(2, line:length - 2)).
    for arg in self:args {
      newargs:add(arg).
    }
    LoaderState:new(LoaderState,newargs, self:shellInput, self:output, false, false, self:env, self:searchPaths, self:fs, self:resultCallback, self:callback).}.
return  result.}):call().
local filesystemManager to ({
local prototype to lex().
local result to lex("prototype", prototype,"classname","filesystemManager").
set  result["new"] to { parameter type,loop.
local self to prototype:copy.
set  self["type"] to  result.
    local mounts to  lex().
    set  self:sec to  {
      return lex("m", mounts).
    }.
    set  self:pid to  1. // Init is 1
    set  self:loop to  loop.
    set  self:progs to  lex().
    set  self:cmds to  lex().
return  self.}.
set  result["__new"] to  result["new"].
set  prototype["mount"] to { parameter self,fs, pathname.
    if not pathname:endsWith("/")
      set  pathname to  pathname + "/".
    local table to  self:sec():m.
    if table:haskey(pathname) {
      table:remove(pathname).
    }
    table:add(pathname, fs).}.
set  prototype["unmount"] to { parameter self,pathname.
    if not pathname:endsWith("/")
      set  pathname to  pathname + "/".
    self:sec():m:remove(pathname).}.
set  prototype["mounts"] to { parameter self.
    return self:sec():m:keys.}.
set  prototype["open"] to { parameter self,pathname, callback.
    self:_open(self,pathname, callback, "r").}.
set  prototype["modify"] to { parameter self,pathname, callback.
    self:_open(self,pathname, callback, "w").}.
set  prototype["create"] to { parameter self,pathname, callback.
    self:_open(self,pathname, callback, "w+").}.
set  prototype["mkdir"] to { parameter self,pathname, callback.
    self:_open(self,pathname, callback, "d").}.
set  prototype["_open"] to { parameter self,pathname, callback, method.
    if not pathname:istype("string") {
      print("Bad pathname type").
      callback:call(callback,false).
      return.
    }
    local parts to  pathname:split("/").
    local prefix to  "/".
    local partsLeft to  parts:copy.
    local mounts to  self:sec():m.
    local root to  mounts["/"].
    until partsLeft:length = 0 {
      if partsleft[0] <> ""
        set  prefix to  prefix + partsLeft[0] + "/".
      partsLeft:remove(0).
      if mounts:haskey(prefix) {
        set  parts to  partsLeft:copy.
        set  root to  mounts[prefix].
      }
    }

    return OpenState:new(OpenState,root, parts, callback, method, self:loop).}.
set  prototype["exec"] to { parameter self,cmd, env, input, output, resultCallback, callback.
    local parsedParts to  Util:splitArgs(Util,cmd).
    local childEnv to  env:copy.

    local inputFile to  false.
    local outputFile to  false.
    local filtered to  list().
    local parsingIn to  false.
    local parsingOut to  false.
    for part in parsedParts {
      local trimmed to  part:trim.
      if parsingIn or parsingOut {
        if parsingIn {
          set  inputFile to  part.
          set  parsingIn to  false.
        } else {
          set  outputFile to  part.
          set  parsingOut to  false.
        }
      } else if trimmed:startsWith("<") {
        local filepart to  trimmed:substring(1, trimmed:length - 1).
        if filepart:trim:length > 0 {
          set  inputFile to  filepart.
        } else {
          set  parsingIn to  true.
        }
      } else if trimmed:startsWith(">") {
        local filepart to  trimmed:substring(1, trimmed:length - 1).
        if filepart:trim:length > 0 {
          set  outputFile to  filepart.
        } else {
          set  parsingOut to  true.
        }
      } else {
        filtered:add(part).
      }
    }
    set  parsedParts to  filtered.

    until parsedParts:length = 0 or not parsedParts[0]:contains("=") {
      local envParts to  parsedParts[0]:split("=").
      local envName to  envParts[0].
      local envVal to  envParts:sublist(1, envParts:length - 1):join("=").
      set  childEnv[envName] to  envVal.

      set  parsedParts to  parsedParts:sublist(1, parsedParts:length - 1).
    }
    
    if parsedParts:length = 0 {
      callback:call(callback,false).
      return false.
    }

    local searchPaths to  env:PATH:split(":").
    
    return LoaderState:new(LoaderState,parsedParts, input, output, inputFile, outputFile, childEnv, searchPaths, self, resultCallback, callback).}.
return  result.}):call().

local loop to  EventLoop:new(EventLoop).
local term to  TerminalFile:new(TerminalFile,loop).
loop:register(loop,term).
local fs to  FilesystemManager:new(FilesystemManager,loop).
fs:cmds:add(term:pid, list("<TerminalFile>")).
fs:progs:add(term:pid, term).
if ship:name = "Archive" {
  fs:mount(fs,VolumeFilesystemDir:new(VolumeFilesystemDir,0, "/", "0:", ship:connection, false, fs), "/").
} else {
  fs:mount(fs,VolumeFilesystemDir:new(VolumeFilesystemDir,1, "/", "1:", ship:connection, false, fs), "/").
  fs:mount(fs,VolumeFilesystemDir:new(VolumeFilesystemDir,0, "/", "0:", homeconnection, false, fs), "/archive/").
}
fs:mount(fs,MemoryFilesystemDir:new(MemoryFilesystemDir), "/tmp/").
local sys to  MemoryFilesystemDir:new(MemoryFilesystemDir).
sys:registerFile(sys,"class", MemoryFilesystemDir:new(MemoryFilesystemDir)).
fs:mount(fs,sys, "/sys/").
local dev to  MemoryFilesystemDir:new(MemoryFilesystemDir).
fs:mount(fs,dev, "/dev/").
dev:registerFile(dev,"tty0", term).
dev:registerFile(dev,"null", NullFile:new(NullFile)).

local env to  lex(
  "PATH", "/bin:/usr/bin",
  "CWD", "/"
).
local init to  Init:new(Init,1, term, term, list(), env, fs, Callback:new(Callback,loop, "stop")).
if command_override:istype("string") {
  init:queue:clear().
  init:queue:push(command_override).
}
if command_override:istype("list") {
  init:queue:clear().
  for line in command_override {
    init:queue:push(line).
  }
}
fs:cmds:add(init:pid, list("<Init>")).
fs:progs:add(init:pid, init).
loop:start(loop).