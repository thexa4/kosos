# Kash
Kash is a bash-like shell. You execute programs by specifying their name along with any parameters. Parameters with spaces can be surrounded with double quotes.

## Redirection
The output of a program can be redirected to a file: `ls /bin > /tmp/ls.txt`

Input can also be supplied through redirection: `cat < /tmp/ls.txt`

## Job control
Programs can be ran in the background using the `&` operator: `cat &`.

To see a list of running jobs, use: `jobs`.

To return a program to the foreground, use `fg <id>` or `fg` if you want to foreground the last job.

If you started a job in the foreground you can move it to the background using the HOME key.

To quit a running job, use the END key.